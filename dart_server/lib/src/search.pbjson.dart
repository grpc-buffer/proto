///
//  Generated code. Do not modify.
//  source: search.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use inSearchItemSuggestionsRequestDescriptor instead')
const InSearchItemSuggestionsRequest$json = const {
  '1': 'InSearchItemSuggestionsRequest',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `InSearchItemSuggestionsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchItemSuggestionsRequestDescriptor = $convert.base64Decode('Ch5JblNlYXJjaEl0ZW1TdWdnZXN0aW9uc1JlcXVlc3QSFAoFdmFsdWUYASABKAlSBXZhbHVl');
@$core.Deprecated('Use inSearchItemSuggestionsResponseDescriptor instead')
const InSearchItemSuggestionsResponse$json = const {
  '1': 'InSearchItemSuggestionsResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'suggestions', '3': 2, '4': 3, '5': 9, '10': 'suggestions'},
  ],
};

/// Descriptor for `InSearchItemSuggestionsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchItemSuggestionsResponseDescriptor = $convert.base64Decode('Ch9JblNlYXJjaEl0ZW1TdWdnZXN0aW9uc1Jlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEiAKC3N1Z2dlc3Rpb25zGAIgAygJUgtzdWdnZXN0aW9ucw==');
@$core.Deprecated('Use inSearchPackageSuggestionsRequestDescriptor instead')
const InSearchPackageSuggestionsRequest$json = const {
  '1': 'InSearchPackageSuggestionsRequest',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `InSearchPackageSuggestionsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchPackageSuggestionsRequestDescriptor = $convert.base64Decode('CiFJblNlYXJjaFBhY2thZ2VTdWdnZXN0aW9uc1JlcXVlc3QSFAoFdmFsdWUYASABKAlSBXZhbHVl');
@$core.Deprecated('Use inSearchPackageSuggestionsResponseDescriptor instead')
const InSearchPackageSuggestionsResponse$json = const {
  '1': 'InSearchPackageSuggestionsResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'suggestions', '3': 2, '4': 3, '5': 9, '10': 'suggestions'},
  ],
};

/// Descriptor for `InSearchPackageSuggestionsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchPackageSuggestionsResponseDescriptor = $convert.base64Decode('CiJJblNlYXJjaFBhY2thZ2VTdWdnZXN0aW9uc1Jlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEiAKC3N1Z2dlc3Rpb25zGAIgAygJUgtzdWdnZXN0aW9ucw==');
@$core.Deprecated('Use inSearchItemRequestDescriptor instead')
const InSearchItemRequest$json = const {
  '1': 'InSearchItemRequest',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `InSearchItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchItemRequestDescriptor = $convert.base64Decode('ChNJblNlYXJjaEl0ZW1SZXF1ZXN0EhQKBXZhbHVlGAEgASgJUgV2YWx1ZQ==');
@$core.Deprecated('Use inSearchItemResponseDescriptor instead')
const InSearchItemResponse$json = const {
  '1': 'InSearchItemResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'item_response', '3': 2, '4': 3, '5': 11, '6': '.search_service.v1.SearchItem', '10': 'itemResponse'},
  ],
};

/// Descriptor for `InSearchItemResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchItemResponseDescriptor = $convert.base64Decode('ChRJblNlYXJjaEl0ZW1SZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxJCCg1pdGVtX3Jlc3BvbnNlGAIgAygLMh0uc2VhcmNoX3NlcnZpY2UudjEuU2VhcmNoSXRlbVIMaXRlbVJlc3BvbnNl');
@$core.Deprecated('Use inSearchPackageRequestDescriptor instead')
const InSearchPackageRequest$json = const {
  '1': 'InSearchPackageRequest',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `InSearchPackageRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchPackageRequestDescriptor = $convert.base64Decode('ChZJblNlYXJjaFBhY2thZ2VSZXF1ZXN0EhQKBXZhbHVlGAEgASgJUgV2YWx1ZQ==');
@$core.Deprecated('Use inSearchPackageResponseDescriptor instead')
const InSearchPackageResponse$json = const {
  '1': 'InSearchPackageResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'package_response', '3': 2, '4': 3, '5': 11, '6': '.search_service.v1.SearchPackage', '10': 'packageResponse'},
  ],
};

/// Descriptor for `InSearchPackageResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchPackageResponseDescriptor = $convert.base64Decode('ChdJblNlYXJjaFBhY2thZ2VSZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxJLChBwYWNrYWdlX3Jlc3BvbnNlGAIgAygLMiAuc2VhcmNoX3NlcnZpY2UudjEuU2VhcmNoUGFja2FnZVIPcGFja2FnZVJlc3BvbnNl');
@$core.Deprecated('Use inSearchSuggestionsRequestDescriptor instead')
const InSearchSuggestionsRequest$json = const {
  '1': 'InSearchSuggestionsRequest',
  '2': const [
    const {'1': 'index', '3': 1, '4': 1, '5': 9, '10': 'index'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `InSearchSuggestionsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchSuggestionsRequestDescriptor = $convert.base64Decode('ChpJblNlYXJjaFN1Z2dlc3Rpb25zUmVxdWVzdBIUCgVpbmRleBgBIAEoCVIFaW5kZXgSFAoFdmFsdWUYAiABKAlSBXZhbHVl');
@$core.Deprecated('Use inSearchSuggestionsResponseDescriptor instead')
const InSearchSuggestionsResponse$json = const {
  '1': 'InSearchSuggestionsResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'suggestions', '3': 2, '4': 3, '5': 9, '10': 'suggestions'},
  ],
};

/// Descriptor for `InSearchSuggestionsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchSuggestionsResponseDescriptor = $convert.base64Decode('ChtJblNlYXJjaFN1Z2dlc3Rpb25zUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSIAoLc3VnZ2VzdGlvbnMYAiADKAlSC3N1Z2dlc3Rpb25z');
@$core.Deprecated('Use inSearchRequestDescriptor instead')
const InSearchRequest$json = const {
  '1': 'InSearchRequest',
  '2': const [
    const {'1': 'index', '3': 1, '4': 1, '5': 9, '10': 'index'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `InSearchRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchRequestDescriptor = $convert.base64Decode('Cg9JblNlYXJjaFJlcXVlc3QSFAoFaW5kZXgYASABKAlSBWluZGV4EhQKBXZhbHVlGAIgASgJUgV2YWx1ZQ==');
@$core.Deprecated('Use inSearchResponseDescriptor instead')
const InSearchResponse$json = const {
  '1': 'InSearchResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'package_response', '3': 2, '4': 3, '5': 11, '6': '.search_service.v1.SearchPackage', '10': 'packageResponse'},
    const {'1': 'item_response', '3': 3, '4': 3, '5': 11, '6': '.search_service.v1.SearchItem', '10': 'itemResponse'},
  ],
};

/// Descriptor for `InSearchResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inSearchResponseDescriptor = $convert.base64Decode('ChBJblNlYXJjaFJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEksKEHBhY2thZ2VfcmVzcG9uc2UYAiADKAsyIC5zZWFyY2hfc2VydmljZS52MS5TZWFyY2hQYWNrYWdlUg9wYWNrYWdlUmVzcG9uc2USQgoNaXRlbV9yZXNwb25zZRgDIAMoCzIdLnNlYXJjaF9zZXJ2aWNlLnYxLlNlYXJjaEl0ZW1SDGl0ZW1SZXNwb25zZQ==');
@$core.Deprecated('Use searchPackageDescriptor instead')
const SearchPackage$json = const {
  '1': 'SearchPackage',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'createdAt', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updatedAt', '3': 4, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'images', '3': 5, '4': 1, '5': 9, '10': 'images'},
    const {'1': 'estimatedPrice', '3': 6, '4': 1, '5': 1, '10': 'estimatedPrice'},
    const {'1': 'maxNumberOfItems', '3': 7, '4': 1, '5': 1, '10': 'maxNumberOfItems'},
    const {'1': 'minNumberOfItems', '3': 8, '4': 1, '5': 1, '10': 'minNumberOfItems'},
    const {'1': 'items', '3': 9, '4': 3, '5': 11, '6': '.search_service.v1.SearchPackageItem', '10': 'items'},
    const {'1': 'packageID', '3': 10, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'categoryID', '3': 11, '4': 1, '5': 9, '10': 'categoryID'},
    const {'1': 'serviceAreaID', '3': 12, '4': 1, '5': 9, '10': 'serviceAreaID'},
  ],
};

/// Descriptor for `SearchPackage`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchPackageDescriptor = $convert.base64Decode('Cg1TZWFyY2hQYWNrYWdlEhIKBG5hbWUYASABKAlSBG5hbWUSIAoLZGVzY3JpcHRpb24YAiABKAlSC2Rlc2NyaXB0aW9uEhwKCWNyZWF0ZWRBdBgDIAEoCVIJY3JlYXRlZEF0EhwKCXVwZGF0ZWRBdBgEIAEoCVIJdXBkYXRlZEF0EhYKBmltYWdlcxgFIAEoCVIGaW1hZ2VzEiYKDmVzdGltYXRlZFByaWNlGAYgASgBUg5lc3RpbWF0ZWRQcmljZRIqChBtYXhOdW1iZXJPZkl0ZW1zGAcgASgBUhBtYXhOdW1iZXJPZkl0ZW1zEioKEG1pbk51bWJlck9mSXRlbXMYCCABKAFSEG1pbk51bWJlck9mSXRlbXMSOgoFaXRlbXMYCSADKAsyJC5zZWFyY2hfc2VydmljZS52MS5TZWFyY2hQYWNrYWdlSXRlbVIFaXRlbXMSHAoJcGFja2FnZUlEGAogASgJUglwYWNrYWdlSUQSHgoKY2F0ZWdvcnlJRBgLIAEoCVIKY2F0ZWdvcnlJRBIkCg1zZXJ2aWNlQXJlYUlEGAwgASgJUg1zZXJ2aWNlQXJlYUlE');
@$core.Deprecated('Use searchImagesDescriptor instead')
const SearchImages$json = const {
  '1': 'SearchImages',
  '2': const [
    const {'1': 'imageUrl', '3': 1, '4': 1, '5': 9, '10': 'imageUrl'},
  ],
};

/// Descriptor for `SearchImages`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchImagesDescriptor = $convert.base64Decode('CgxTZWFyY2hJbWFnZXMSGgoIaW1hZ2VVcmwYASABKAlSCGltYWdlVXJs');
@$core.Deprecated('Use searchPackageItemDescriptor instead')
const SearchPackageItem$json = const {
  '1': 'SearchPackageItem',
  '2': const [
    const {'1': 'packageItemID', '3': 1, '4': 1, '5': 9, '10': 'packageItemID'},
    const {'1': 'substitutes', '3': 2, '4': 3, '5': 11, '6': '.search_service.v1.SearchItemData', '10': 'substitutes'},
    const {'1': 'category', '3': 3, '4': 1, '5': 9, '10': 'category'},
    const {'1': 'packageID', '3': 4, '4': 1, '5': 9, '10': 'packageID'},
  ],
};

/// Descriptor for `SearchPackageItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchPackageItemDescriptor = $convert.base64Decode('ChFTZWFyY2hQYWNrYWdlSXRlbRIkCg1wYWNrYWdlSXRlbUlEGAEgASgJUg1wYWNrYWdlSXRlbUlEEkMKC3N1YnN0aXR1dGVzGAIgAygLMiEuc2VhcmNoX3NlcnZpY2UudjEuU2VhcmNoSXRlbURhdGFSC3N1YnN0aXR1dGVzEhoKCGNhdGVnb3J5GAMgASgJUghjYXRlZ29yeRIcCglwYWNrYWdlSUQYBCABKAlSCXBhY2thZ2VJRA==');
@$core.Deprecated('Use searchItemDataDescriptor instead')
const SearchItemData$json = const {
  '1': 'SearchItemData',
  '2': const [
    const {'1': 'itemID', '3': 1, '4': 1, '5': 9, '10': 'itemID'},
    const {'1': 'quantity', '3': 2, '4': 1, '5': 1, '10': 'quantity'},
    const {'1': 'price', '3': 3, '4': 1, '5': 1, '10': 'price'},
    const {'1': 'isRequired', '3': 4, '4': 1, '5': 8, '10': 'isRequired'},
    const {'1': 'isDefault', '3': 5, '4': 1, '5': 8, '10': 'isDefault'},
    const {'1': 'description', '3': 6, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'units', '3': 7, '4': 1, '5': 9, '10': 'units'},
    const {'1': 'name', '3': 8, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'image', '3': 9, '4': 1, '5': 9, '10': 'image'},
  ],
};

/// Descriptor for `SearchItemData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchItemDataDescriptor = $convert.base64Decode('Cg5TZWFyY2hJdGVtRGF0YRIWCgZpdGVtSUQYASABKAlSBml0ZW1JRBIaCghxdWFudGl0eRgCIAEoAVIIcXVhbnRpdHkSFAoFcHJpY2UYAyABKAFSBXByaWNlEh4KCmlzUmVxdWlyZWQYBCABKAhSCmlzUmVxdWlyZWQSHAoJaXNEZWZhdWx0GAUgASgIUglpc0RlZmF1bHQSIAoLZGVzY3JpcHRpb24YBiABKAlSC2Rlc2NyaXB0aW9uEhQKBXVuaXRzGAcgASgJUgV1bml0cxISCgRuYW1lGAggASgJUgRuYW1lEhQKBWltYWdlGAkgASgJUgVpbWFnZQ==');
@$core.Deprecated('Use searchItemDescriptor instead')
const SearchItem$json = const {
  '1': 'SearchItem',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'itemCategoryID', '3': 4, '4': 1, '5': 9, '10': 'itemCategoryID'},
    const {'1': 'image', '3': 5, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'unit', '3': 6, '4': 1, '5': 9, '10': 'unit'},
    const {'1': 'created_at', '3': 7, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 8, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `SearchItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchItemDescriptor = $convert.base64Decode('CgpTZWFyY2hJdGVtEg4KAmlkGAEgASgJUgJpZBISCgRuYW1lGAIgASgJUgRuYW1lEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbhImCg5pdGVtQ2F0ZWdvcnlJRBgEIAEoCVIOaXRlbUNhdGVnb3J5SUQSFAoFaW1hZ2UYBSABKAlSBWltYWdlEhIKBHVuaXQYBiABKAlSBHVuaXQSHQoKY3JlYXRlZF9hdBgHIAEoCVIJY3JlYXRlZEF0Eh0KCnVwZGF0ZWRfYXQYCCABKAlSCXVwZGF0ZWRBdA==');
