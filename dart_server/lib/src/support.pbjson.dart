///
//  Generated code. Do not modify.
//  source: support.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use createFAQCategoryRequestDescriptor instead')
const CreateFAQCategoryRequest$json = const {
  '1': 'CreateFAQCategoryRequest',
  '2': const [
    const {'1': 'logoUrl', '3': 1, '4': 1, '5': 9, '10': 'logoUrl'},
    const {'1': 'categoryTitle', '3': 2, '4': 1, '5': 9, '10': 'categoryTitle'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
  ],
};

/// Descriptor for `CreateFAQCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createFAQCategoryRequestDescriptor = $convert.base64Decode('ChhDcmVhdGVGQVFDYXRlZ29yeVJlcXVlc3QSGAoHbG9nb1VybBgBIAEoCVIHbG9nb1VybBIkCg1jYXRlZ29yeVRpdGxlGAIgASgJUg1jYXRlZ29yeVRpdGxlEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbg==');
@$core.Deprecated('Use fAQCategoryItemDescriptor instead')
const FAQCategoryItem$json = const {
  '1': 'FAQCategoryItem',
  '2': const [
    const {'1': 'logoUrl', '3': 1, '4': 1, '5': 9, '10': 'logoUrl'},
    const {'1': 'categoryTitle', '3': 2, '4': 1, '5': 9, '10': 'categoryTitle'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'categoryID', '3': 4, '4': 1, '5': 9, '10': 'categoryID'},
    const {'1': 'updated_at', '3': 5, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'created_at', '3': 6, '4': 1, '5': 9, '10': 'createdAt'},
  ],
};

/// Descriptor for `FAQCategoryItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fAQCategoryItemDescriptor = $convert.base64Decode('Cg9GQVFDYXRlZ29yeUl0ZW0SGAoHbG9nb1VybBgBIAEoCVIHbG9nb1VybBIkCg1jYXRlZ29yeVRpdGxlGAIgASgJUg1jYXRlZ29yeVRpdGxlEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbhIeCgpjYXRlZ29yeUlEGAQgASgJUgpjYXRlZ29yeUlEEh0KCnVwZGF0ZWRfYXQYBSABKAlSCXVwZGF0ZWRBdBIdCgpjcmVhdGVkX2F0GAYgASgJUgljcmVhdGVkQXQ=');
@$core.Deprecated('Use fAQCategoryItemRequestDescriptor instead')
const FAQCategoryItemRequest$json = const {
  '1': 'FAQCategoryItemRequest',
  '2': const [
    const {'1': 'categoryID', '3': 1, '4': 1, '5': 9, '10': 'categoryID'},
  ],
};

/// Descriptor for `FAQCategoryItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fAQCategoryItemRequestDescriptor = $convert.base64Decode('ChZGQVFDYXRlZ29yeUl0ZW1SZXF1ZXN0Eh4KCmNhdGVnb3J5SUQYASABKAlSCmNhdGVnb3J5SUQ=');
@$core.Deprecated('Use listFAQCategoriesResponseDescriptor instead')
const ListFAQCategoriesResponse$json = const {
  '1': 'ListFAQCategoriesResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.support.v1.FAQCategoryItem', '10': 'data'},
  ],
};

/// Descriptor for `ListFAQCategoriesResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listFAQCategoriesResponseDescriptor = $convert.base64Decode('ChlMaXN0RkFRQ2F0ZWdvcmllc1Jlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USLwoEZGF0YRgDIAMoCzIbLnN1cHBvcnQudjEuRkFRQ2F0ZWdvcnlJdGVtUgRkYXRh');
@$core.Deprecated('Use ticketRequestDescriptor instead')
const TicketRequest$json = const {
  '1': 'TicketRequest',
  '2': const [
    const {'1': 'ticketID', '3': 1, '4': 1, '5': 9, '10': 'ticketID'},
    const {'1': 'sender', '3': 2, '4': 1, '5': 9, '10': 'sender'},
  ],
};

/// Descriptor for `TicketRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List ticketRequestDescriptor = $convert.base64Decode('Cg1UaWNrZXRSZXF1ZXN0EhoKCHRpY2tldElEGAEgASgJUgh0aWNrZXRJRBIWCgZzZW5kZXIYAiABKAlSBnNlbmRlcg==');
@$core.Deprecated('Use chatDescriptor instead')
const Chat$json = const {
  '1': 'Chat',
  '2': const [
    const {'1': 'sender', '3': 1, '4': 1, '5': 9, '10': 'sender'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'created_at', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'isAgent', '3': 4, '4': 1, '5': 8, '10': 'isAgent'},
    const {'1': 'fileUrl', '3': 5, '4': 1, '5': 9, '10': 'fileUrl'},
  ],
};

/// Descriptor for `Chat`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List chatDescriptor = $convert.base64Decode('CgRDaGF0EhYKBnNlbmRlchgBIAEoCVIGc2VuZGVyEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USHQoKY3JlYXRlZF9hdBgDIAEoCVIJY3JlYXRlZEF0EhgKB2lzQWdlbnQYBCABKAhSB2lzQWdlbnQSGAoHZmlsZVVybBgFIAEoCVIHZmlsZVVybA==');
@$core.Deprecated('Use supportItemDescriptor instead')
const SupportItem$json = const {
  '1': 'SupportItem',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'sender', '3': 2, '4': 1, '5': 9, '10': 'sender'},
    const {'1': 'chats', '3': 3, '4': 3, '5': 11, '6': '.support.v1.Chat', '10': 'chats'},
    const {'1': 'topic', '3': 4, '4': 1, '5': 9, '10': 'topic'},
    const {'1': 'updated_at', '3': 6, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'created_at', '3': 7, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'status', '3': 8, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `SupportItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List supportItemDescriptor = $convert.base64Decode('CgtTdXBwb3J0SXRlbRIOCgJpZBgBIAEoCVICaWQSFgoGc2VuZGVyGAIgASgJUgZzZW5kZXISJgoFY2hhdHMYAyADKAsyEC5zdXBwb3J0LnYxLkNoYXRSBWNoYXRzEhQKBXRvcGljGAQgASgJUgV0b3BpYxIdCgp1cGRhdGVkX2F0GAYgASgJUgl1cGRhdGVkQXQSHQoKY3JlYXRlZF9hdBgHIAEoCVIJY3JlYXRlZEF0EhYKBnN0YXR1cxgIIAEoCVIGc3RhdHVz');
@$core.Deprecated('Use supportRequestDescriptor instead')
const SupportRequest$json = const {
  '1': 'SupportRequest',
  '2': const [
    const {'1': 'sender', '3': 1, '4': 1, '5': 9, '10': 'sender'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'topic', '3': 3, '4': 1, '5': 9, '10': 'topic'},
    const {'1': 'isAgent', '3': 4, '4': 1, '5': 8, '10': 'isAgent'},
    const {'1': 'updated_at', '3': 5, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'created_at', '3': 6, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'fileUrl', '3': 7, '4': 1, '5': 9, '10': 'fileUrl'},
    const {'1': 'id', '3': 8, '4': 1, '5': 9, '10': 'id'},
  ],
};

/// Descriptor for `SupportRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List supportRequestDescriptor = $convert.base64Decode('Cg5TdXBwb3J0UmVxdWVzdBIWCgZzZW5kZXIYASABKAlSBnNlbmRlchIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEhQKBXRvcGljGAMgASgJUgV0b3BpYxIYCgdpc0FnZW50GAQgASgIUgdpc0FnZW50Eh0KCnVwZGF0ZWRfYXQYBSABKAlSCXVwZGF0ZWRBdBIdCgpjcmVhdGVkX2F0GAYgASgJUgljcmVhdGVkQXQSGAoHZmlsZVVybBgHIAEoCVIHZmlsZVVybBIOCgJpZBgIIAEoCVICaWQ=');
@$core.Deprecated('Use emptySupportRequestDescriptor instead')
const EmptySupportRequest$json = const {
  '1': 'EmptySupportRequest',
};

/// Descriptor for `EmptySupportRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptySupportRequestDescriptor = $convert.base64Decode('ChNFbXB0eVN1cHBvcnRSZXF1ZXN0');
@$core.Deprecated('Use supportRequestItemDescriptor instead')
const SupportRequestItem$json = const {
  '1': 'SupportRequestItem',
  '2': const [
    const {'1': 'field', '3': 1, '4': 1, '5': 9, '10': 'field'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `SupportRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List supportRequestItemDescriptor = $convert.base64Decode('ChJTdXBwb3J0UmVxdWVzdEl0ZW0SFAoFZmllbGQYASABKAlSBWZpZWxkEhQKBXZhbHVlGAIgASgJUgV2YWx1ZQ==');
@$core.Deprecated('Use supportResponseDescriptor instead')
const SupportResponse$json = const {
  '1': 'SupportResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 9, '10': 'data'},
  ],
};

/// Descriptor for `SupportResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List supportResponseDescriptor = $convert.base64Decode('Cg9TdXBwb3J0UmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRISCgRkYXRhGAMgASgJUgRkYXRh');
@$core.Deprecated('Use viewSupportResponseDescriptor instead')
const ViewSupportResponse$json = const {
  '1': 'ViewSupportResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 11, '6': '.support.v1.SupportItem', '10': 'data'},
  ],
};

/// Descriptor for `ViewSupportResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewSupportResponseDescriptor = $convert.base64Decode('ChNWaWV3U3VwcG9ydFJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USKwoEZGF0YRgDIAEoCzIXLnN1cHBvcnQudjEuU3VwcG9ydEl0ZW1SBGRhdGE=');
@$core.Deprecated('Use listSupportResponseDescriptor instead')
const ListSupportResponse$json = const {
  '1': 'ListSupportResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.support.v1.SupportItem', '10': 'data'},
  ],
};

/// Descriptor for `ListSupportResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listSupportResponseDescriptor = $convert.base64Decode('ChNMaXN0U3VwcG9ydFJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USKwoEZGF0YRgDIAMoCzIXLnN1cHBvcnQudjEuU3VwcG9ydEl0ZW1SBGRhdGE=');
@$core.Deprecated('Use fAQCreateRequestDescriptor instead')
const FAQCreateRequest$json = const {
  '1': 'FAQCreateRequest',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'categoryID', '3': 3, '4': 1, '5': 9, '10': 'categoryID'},
  ],
};

/// Descriptor for `FAQCreateRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fAQCreateRequestDescriptor = $convert.base64Decode('ChBGQVFDcmVhdGVSZXF1ZXN0EhQKBXRpdGxlGAEgASgJUgV0aXRsZRIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEh4KCmNhdGVnb3J5SUQYAyABKAlSCmNhdGVnb3J5SUQ=');
@$core.Deprecated('Use fAQItemDescriptor instead')
const FAQItem$json = const {
  '1': 'FAQItem',
  '2': const [
    const {'1': 'ID', '3': 1, '4': 1, '5': 9, '10': 'ID'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'created_at', '3': 4, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 5, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'categoryID', '3': 6, '4': 1, '5': 9, '10': 'categoryID'},
  ],
};

/// Descriptor for `FAQItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fAQItemDescriptor = $convert.base64Decode('CgdGQVFJdGVtEg4KAklEGAEgASgJUgJJRBIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSGAoHbWVzc2FnZRgDIAEoCVIHbWVzc2FnZRIdCgpjcmVhdGVkX2F0GAQgASgJUgljcmVhdGVkQXQSHQoKdXBkYXRlZF9hdBgFIAEoCVIJdXBkYXRlZEF0Eh4KCmNhdGVnb3J5SUQYBiABKAlSCmNhdGVnb3J5SUQ=');
@$core.Deprecated('Use fAQRequestItemDescriptor instead')
const FAQRequestItem$json = const {
  '1': 'FAQRequestItem',
  '2': const [
    const {'1': 'faqID', '3': 1, '4': 1, '5': 9, '10': 'faqID'},
  ],
};

/// Descriptor for `FAQRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fAQRequestItemDescriptor = $convert.base64Decode('Cg5GQVFSZXF1ZXN0SXRlbRIUCgVmYXFJRBgBIAEoCVIFZmFxSUQ=');
@$core.Deprecated('Use listFAQRequestDescriptor instead')
const ListFAQRequest$json = const {
  '1': 'ListFAQRequest',
};

/// Descriptor for `ListFAQRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listFAQRequestDescriptor = $convert.base64Decode('Cg5MaXN0RkFRUmVxdWVzdA==');
@$core.Deprecated('Use listFAQByCategoryRequestDescriptor instead')
const ListFAQByCategoryRequest$json = const {
  '1': 'ListFAQByCategoryRequest',
  '2': const [
    const {'1': 'categoryID', '3': 1, '4': 1, '5': 9, '10': 'categoryID'},
  ],
};

/// Descriptor for `ListFAQByCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listFAQByCategoryRequestDescriptor = $convert.base64Decode('ChhMaXN0RkFRQnlDYXRlZ29yeVJlcXVlc3QSHgoKY2F0ZWdvcnlJRBgBIAEoCVIKY2F0ZWdvcnlJRA==');
@$core.Deprecated('Use fAQResponseDescriptor instead')
const FAQResponse$json = const {
  '1': 'FAQResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'data', '3': 3, '4': 1, '5': 9, '10': 'data'},
  ],
};

/// Descriptor for `FAQResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fAQResponseDescriptor = $convert.base64Decode('CgtGQVFSZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEhYKBnN0YXR1cxgCIAEoCVIGc3RhdHVzEhIKBGRhdGEYAyABKAlSBGRhdGE=');
@$core.Deprecated('Use listFAQResponseDescriptor instead')
const ListFAQResponse$json = const {
  '1': 'ListFAQResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.support.v1.FAQItem', '10': 'data'},
  ],
};

/// Descriptor for `ListFAQResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listFAQResponseDescriptor = $convert.base64Decode('Cg9MaXN0RkFRUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZRIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cxInCgRkYXRhGAMgAygLMhMuc3VwcG9ydC52MS5GQVFJdGVtUgRkYXRh');
@$core.Deprecated('Use agentFAQCreateRequestDescriptor instead')
const AgentFAQCreateRequest$json = const {
  '1': 'AgentFAQCreateRequest',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `AgentFAQCreateRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agentFAQCreateRequestDescriptor = $convert.base64Decode('ChVBZ2VudEZBUUNyZWF0ZVJlcXVlc3QSFAoFdGl0bGUYASABKAlSBXRpdGxlEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use agentFAQItemDescriptor instead')
const AgentFAQItem$json = const {
  '1': 'AgentFAQItem',
  '2': const [
    const {'1': 'ID', '3': 1, '4': 1, '5': 9, '10': 'ID'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'created_at', '3': 4, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 5, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `AgentFAQItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agentFAQItemDescriptor = $convert.base64Decode('CgxBZ2VudEZBUUl0ZW0SDgoCSUQYASABKAlSAklEEhQKBXRpdGxlGAIgASgJUgV0aXRsZRIYCgdtZXNzYWdlGAMgASgJUgdtZXNzYWdlEh0KCmNyZWF0ZWRfYXQYBCABKAlSCWNyZWF0ZWRBdBIdCgp1cGRhdGVkX2F0GAUgASgJUgl1cGRhdGVkQXQ=');
@$core.Deprecated('Use agentFAQRequestItemDescriptor instead')
const AgentFAQRequestItem$json = const {
  '1': 'AgentFAQRequestItem',
  '2': const [
    const {'1': 'faqID', '3': 1, '4': 1, '5': 9, '10': 'faqID'},
  ],
};

/// Descriptor for `AgentFAQRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agentFAQRequestItemDescriptor = $convert.base64Decode('ChNBZ2VudEZBUVJlcXVlc3RJdGVtEhQKBWZhcUlEGAEgASgJUgVmYXFJRA==');
@$core.Deprecated('Use listAgentFAQResponseDescriptor instead')
const ListAgentFAQResponse$json = const {
  '1': 'ListAgentFAQResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.support.v1.AgentFAQItem', '10': 'data'},
  ],
};

/// Descriptor for `ListAgentFAQResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listAgentFAQResponseDescriptor = $convert.base64Decode('ChRMaXN0QWdlbnRGQVFSZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEhYKBnN0YXR1cxgCIAEoCVIGc3RhdHVzEiwKBGRhdGEYAyADKAsyGC5zdXBwb3J0LnYxLkFnZW50RkFRSXRlbVIEZGF0YQ==');
@$core.Deprecated('Use videoRequestDescriptor instead')
const VideoRequest$json = const {
  '1': 'VideoRequest',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'videoUrl', '3': 2, '4': 1, '5': 9, '10': 'videoUrl'},
    const {'1': 'createdBy', '3': 3, '4': 1, '5': 9, '10': 'createdBy'},
  ],
};

/// Descriptor for `VideoRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List videoRequestDescriptor = $convert.base64Decode('CgxWaWRlb1JlcXVlc3QSFAoFdGl0bGUYASABKAlSBXRpdGxlEhoKCHZpZGVvVXJsGAIgASgJUgh2aWRlb1VybBIcCgljcmVhdGVkQnkYAyABKAlSCWNyZWF0ZWRCeQ==');
@$core.Deprecated('Use videoGuideRequestDescriptor instead')
const VideoGuideRequest$json = const {
  '1': 'VideoGuideRequest',
  '2': const [
    const {'1': 'videoGuideID', '3': 1, '4': 1, '5': 9, '10': 'videoGuideID'},
  ],
};

/// Descriptor for `VideoGuideRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List videoGuideRequestDescriptor = $convert.base64Decode('ChFWaWRlb0d1aWRlUmVxdWVzdBIiCgx2aWRlb0d1aWRlSUQYASABKAlSDHZpZGVvR3VpZGVJRA==');
@$core.Deprecated('Use listVideoGuideRequestDescriptor instead')
const ListVideoGuideRequest$json = const {
  '1': 'ListVideoGuideRequest',
};

/// Descriptor for `ListVideoGuideRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listVideoGuideRequestDescriptor = $convert.base64Decode('ChVMaXN0VmlkZW9HdWlkZVJlcXVlc3Q=');
@$core.Deprecated('Use videoGuideItemDescriptor instead')
const VideoGuideItem$json = const {
  '1': 'VideoGuideItem',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'videoUrl', '3': 2, '4': 1, '5': 9, '10': 'videoUrl'},
    const {'1': 'createdBy', '3': 3, '4': 1, '5': 9, '10': 'createdBy'},
    const {'1': 'createdAt', '3': 4, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'videoGuideID', '3': 5, '4': 1, '5': 9, '10': 'videoGuideID'},
    const {'1': 'updatedAt', '3': 6, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `VideoGuideItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List videoGuideItemDescriptor = $convert.base64Decode('Cg5WaWRlb0d1aWRlSXRlbRIUCgV0aXRsZRgBIAEoCVIFdGl0bGUSGgoIdmlkZW9VcmwYAiABKAlSCHZpZGVvVXJsEhwKCWNyZWF0ZWRCeRgDIAEoCVIJY3JlYXRlZEJ5EhwKCWNyZWF0ZWRBdBgEIAEoCVIJY3JlYXRlZEF0EiIKDHZpZGVvR3VpZGVJRBgFIAEoCVIMdmlkZW9HdWlkZUlEEhwKCXVwZGF0ZWRBdBgGIAEoCVIJdXBkYXRlZEF0');
@$core.Deprecated('Use videoGuideResponseDescriptor instead')
const VideoGuideResponse$json = const {
  '1': 'VideoGuideResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `VideoGuideResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List videoGuideResponseDescriptor = $convert.base64Decode('ChJWaWRlb0d1aWRlUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZRIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use listVideoGuideResponseDescriptor instead')
const ListVideoGuideResponse$json = const {
  '1': 'ListVideoGuideResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.support.v1.VideoGuideItem', '10': 'data'},
  ],
};

/// Descriptor for `ListVideoGuideResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listVideoGuideResponseDescriptor = $convert.base64Decode('ChZMaXN0VmlkZW9HdWlkZVJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USFgoGc3RhdHVzGAIgASgJUgZzdGF0dXMSLgoEZGF0YRgDIAMoCzIaLnN1cHBvcnQudjEuVmlkZW9HdWlkZUl0ZW1SBGRhdGE=');
