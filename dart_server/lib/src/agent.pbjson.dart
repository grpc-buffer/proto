///
//  Generated code. Do not modify.
//  source: agent.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use availabilityStatusDescriptor instead')
const AvailabilityStatus$json = const {
  '1': 'AvailabilityStatus',
  '2': const [
    const {'1': 'AVAILABLE', '2': 0},
    const {'1': 'ON_ERRAND', '2': 1},
    const {'1': 'UNAVAILABLE', '2': 2},
  ],
};

/// Descriptor for `AvailabilityStatus`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List availabilityStatusDescriptor = $convert.base64Decode('ChJBdmFpbGFiaWxpdHlTdGF0dXMSDQoJQVZBSUxBQkxFEAASDQoJT05fRVJSQU5EEAESDwoLVU5BVkFJTEFCTEUQAg==');
@$core.Deprecated('Use orderRequestStatusDescriptor instead')
const OrderRequestStatus$json = const {
  '1': 'OrderRequestStatus',
  '2': const [
    const {'1': 'UNASSIGNED', '2': 0},
    const {'1': 'ASSIGNED', '2': 1},
    const {'1': 'ARRIVED_AT_STORE', '2': 2},
    const {'1': 'PURCHASED_ITEM', '2': 3},
    const {'1': 'AWAITING_CONFIRMATION', '2': 4},
    const {'1': 'DROPPED_OFF', '2': 5},
    const {'1': 'ON_HOLD', '2': 6},
    const {'1': 'COMPLETED', '2': 7},
  ],
};

/// Descriptor for `OrderRequestStatus`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List orderRequestStatusDescriptor = $convert.base64Decode('ChJPcmRlclJlcXVlc3RTdGF0dXMSDgoKVU5BU1NJR05FRBAAEgwKCEFTU0lHTkVEEAESFAoQQVJSSVZFRF9BVF9TVE9SRRACEhIKDlBVUkNIQVNFRF9JVEVNEAMSGQoVQVdBSVRJTkdfQ09ORklSTUFUSU9OEAQSDwoLRFJPUFBFRF9PRkYQBRILCgdPTl9IT0xEEAYSDQoJQ09NUExFVEVEEAc=');
@$core.Deprecated('Use acceptanceStatusDescriptor instead')
const AcceptanceStatus$json = const {
  '1': 'AcceptanceStatus',
  '2': const [
    const {'1': 'accept', '2': 0},
    const {'1': 'decline', '2': 1},
  ],
};

/// Descriptor for `AcceptanceStatus`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List acceptanceStatusDescriptor = $convert.base64Decode('ChBBY2NlcHRhbmNlU3RhdHVzEgoKBmFjY2VwdBAAEgsKB2RlY2xpbmUQAQ==');
@$core.Deprecated('Use agentDescriptor instead')
const Agent$json = const {
  '1': 'Agent',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'email', '3': 2, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phone_number', '3': 3, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'first_name', '3': 4, '4': 1, '5': 9, '10': 'firstName'},
    const {'1': 'last_name', '3': 5, '4': 1, '5': 9, '10': 'lastName'},
    const {'1': 'address', '3': 6, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'state', '3': 7, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'wallet', '3': 8, '4': 1, '5': 11, '6': '.Wallet', '10': 'wallet'},
    const {'1': 'status', '3': 9, '4': 1, '5': 14, '6': '.AvailabilityStatus', '10': 'status'},
  ],
};

/// Descriptor for `Agent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agentDescriptor = $convert.base64Decode('CgVBZ2VudBIOCgJpZBgBIAEoCVICaWQSFAoFZW1haWwYAiABKAlSBWVtYWlsEiEKDHBob25lX251bWJlchgDIAEoCVILcGhvbmVOdW1iZXISHQoKZmlyc3RfbmFtZRgEIAEoCVIJZmlyc3ROYW1lEhsKCWxhc3RfbmFtZRgFIAEoCVIIbGFzdE5hbWUSGAoHYWRkcmVzcxgGIAEoCVIHYWRkcmVzcxIUCgVzdGF0ZRgHIAEoCVIFc3RhdGUSHwoGd2FsbGV0GAggASgLMgcuV2FsbGV0UgZ3YWxsZXQSKwoGc3RhdHVzGAkgASgOMhMuQXZhaWxhYmlsaXR5U3RhdHVzUgZzdGF0dXM=');
@$core.Deprecated('Use createAgentRequestDescriptor instead')
const CreateAgentRequest$json = const {
  '1': 'CreateAgentRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'email', '3': 2, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phone_number', '3': 3, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'first_name', '3': 4, '4': 1, '5': 9, '10': 'firstName'},
    const {'1': 'last_name', '3': 5, '4': 1, '5': 9, '10': 'lastName'},
    const {'1': 'address', '3': 6, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'state', '3': 7, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'country', '3': 8, '4': 1, '5': 9, '10': 'country'},
  ],
};

/// Descriptor for `CreateAgentRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createAgentRequestDescriptor = $convert.base64Decode('ChJDcmVhdGVBZ2VudFJlcXVlc3QSDgoCaWQYASABKAlSAmlkEhQKBWVtYWlsGAIgASgJUgVlbWFpbBIhCgxwaG9uZV9udW1iZXIYAyABKAlSC3Bob25lTnVtYmVyEh0KCmZpcnN0X25hbWUYBCABKAlSCWZpcnN0TmFtZRIbCglsYXN0X25hbWUYBSABKAlSCGxhc3ROYW1lEhgKB2FkZHJlc3MYBiABKAlSB2FkZHJlc3MSFAoFc3RhdGUYByABKAlSBXN0YXRlEhgKB2NvdW50cnkYCCABKAlSB2NvdW50cnk=');
@$core.Deprecated('Use createAgentResponseDescriptor instead')
const CreateAgentResponse$json = const {
  '1': 'CreateAgentResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `CreateAgentResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createAgentResponseDescriptor = $convert.base64Decode('ChNDcmVhdGVBZ2VudFJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use agentFilterDescriptor instead')
const AgentFilter$json = const {
  '1': 'AgentFilter',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.AvailabilityStatus', '10': 'status'},
    const {'1': 'state', '3': 2, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'address', '3': 3, '4': 1, '5': 9, '10': 'address'},
  ],
};

/// Descriptor for `AgentFilter`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agentFilterDescriptor = $convert.base64Decode('CgtBZ2VudEZpbHRlchIrCgZzdGF0dXMYASABKA4yEy5BdmFpbGFiaWxpdHlTdGF0dXNSBnN0YXR1cxIUCgVzdGF0ZRgCIAEoCVIFc3RhdGUSGAoHYWRkcmVzcxgDIAEoCVIHYWRkcmVzcw==');
@$core.Deprecated('Use getAllAgentsRequestDescriptor instead')
const GetAllAgentsRequest$json = const {
  '1': 'GetAllAgentsRequest',
  '2': const [
    const {'1': 'page', '3': 1, '4': 1, '5': 13, '10': 'page'},
    const {'1': 'size', '3': 2, '4': 1, '5': 13, '10': 'size'},
    const {'1': 'filterBy', '3': 3, '4': 1, '5': 11, '6': '.AgentFilter', '10': 'filterBy'},
  ],
};

/// Descriptor for `GetAllAgentsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllAgentsRequestDescriptor = $convert.base64Decode('ChNHZXRBbGxBZ2VudHNSZXF1ZXN0EhIKBHBhZ2UYASABKA1SBHBhZ2USEgoEc2l6ZRgCIAEoDVIEc2l6ZRIoCghmaWx0ZXJCeRgDIAEoCzIMLkFnZW50RmlsdGVyUghmaWx0ZXJCeQ==');
@$core.Deprecated('Use getAllAgentsResponseDescriptor instead')
const GetAllAgentsResponse$json = const {
  '1': 'GetAllAgentsResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'agents', '3': 2, '4': 3, '5': 11, '6': '.Agent', '10': 'agents'},
    const {'1': 'page', '3': 3, '4': 1, '5': 13, '10': 'page'},
    const {'1': 'size', '3': 4, '4': 1, '5': 13, '10': 'size'},
    const {'1': 'total', '3': 5, '4': 1, '5': 3, '10': 'total'},
  ],
};

/// Descriptor for `GetAllAgentsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllAgentsResponseDescriptor = $convert.base64Decode('ChRHZXRBbGxBZ2VudHNSZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEh4KBmFnZW50cxgCIAMoCzIGLkFnZW50UgZhZ2VudHMSEgoEcGFnZRgDIAEoDVIEcGFnZRISCgRzaXplGAQgASgNUgRzaXplEhQKBXRvdGFsGAUgASgDUgV0b3RhbA==');
@$core.Deprecated('Use getAgentByIdRequestDescriptor instead')
const GetAgentByIdRequest$json = const {
  '1': 'GetAgentByIdRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
  ],
};

/// Descriptor for `GetAgentByIdRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAgentByIdRequestDescriptor = $convert.base64Decode('ChNHZXRBZ2VudEJ5SWRSZXF1ZXN0Eg4KAmlkGAEgASgJUgJpZA==');
@$core.Deprecated('Use getAgentByIdResponseDescriptor instead')
const GetAgentByIdResponse$json = const {
  '1': 'GetAgentByIdResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'agent', '3': 2, '4': 1, '5': 11, '6': '.Agent', '10': 'agent'},
  ],
};

/// Descriptor for `GetAgentByIdResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAgentByIdResponseDescriptor = $convert.base64Decode('ChRHZXRBZ2VudEJ5SWRSZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEhwKBWFnZW50GAIgASgLMgYuQWdlbnRSBWFnZW50');
@$core.Deprecated('Use getAgentInfoRequestDescriptor instead')
const GetAgentInfoRequest$json = const {
  '1': 'GetAgentInfoRequest',
};

/// Descriptor for `GetAgentInfoRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAgentInfoRequestDescriptor = $convert.base64Decode('ChNHZXRBZ2VudEluZm9SZXF1ZXN0');
@$core.Deprecated('Use getAgentInfoResponseDescriptor instead')
const GetAgentInfoResponse$json = const {
  '1': 'GetAgentInfoResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'agent', '3': 2, '4': 1, '5': 11, '6': '.Agent', '10': 'agent'},
  ],
};

/// Descriptor for `GetAgentInfoResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAgentInfoResponseDescriptor = $convert.base64Decode('ChRHZXRBZ2VudEluZm9SZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEhwKBWFnZW50GAIgASgLMgYuQWdlbnRSBWFnZW50');
@$core.Deprecated('Use changeAvailabilityStatusRequestDescriptor instead')
const ChangeAvailabilityStatusRequest$json = const {
  '1': 'ChangeAvailabilityStatusRequest',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.AvailabilityStatus', '10': 'status'},
  ],
};

/// Descriptor for `ChangeAvailabilityStatusRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List changeAvailabilityStatusRequestDescriptor = $convert.base64Decode('Ch9DaGFuZ2VBdmFpbGFiaWxpdHlTdGF0dXNSZXF1ZXN0EisKBnN0YXR1cxgBIAEoDjITLkF2YWlsYWJpbGl0eVN0YXR1c1IGc3RhdHVz');
@$core.Deprecated('Use changeAvailabilityStatusResponseDescriptor instead')
const ChangeAvailabilityStatusResponse$json = const {
  '1': 'ChangeAvailabilityStatusResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `ChangeAvailabilityStatusResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List changeAvailabilityStatusResponseDescriptor = $convert.base64Decode('CiBDaGFuZ2VBdmFpbGFiaWxpdHlTdGF0dXNSZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdl');
@$core.Deprecated('Use walletDescriptor instead')
const Wallet$json = const {
  '1': 'Wallet',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'agent_id', '3': 2, '4': 1, '5': 9, '10': 'agentId'},
    const {'1': 'balance', '3': 3, '4': 1, '5': 2, '10': 'balance'},
    const {'1': 'currency', '3': 4, '4': 1, '5': 9, '10': 'currency'},
  ],
};

/// Descriptor for `Wallet`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List walletDescriptor = $convert.base64Decode('CgZXYWxsZXQSDgoCaWQYASABKAlSAmlkEhkKCGFnZW50X2lkGAIgASgJUgdhZ2VudElkEhgKB2JhbGFuY2UYAyABKAJSB2JhbGFuY2USGgoIY3VycmVuY3kYBCABKAlSCGN1cnJlbmN5');
@$core.Deprecated('Use walletFilterDescriptor instead')
const WalletFilter$json = const {
  '1': 'WalletFilter',
  '2': const [
    const {'1': 'currency', '3': 1, '4': 1, '5': 9, '10': 'currency'},
  ],
};

/// Descriptor for `WalletFilter`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List walletFilterDescriptor = $convert.base64Decode('CgxXYWxsZXRGaWx0ZXISGgoIY3VycmVuY3kYASABKAlSCGN1cnJlbmN5');
@$core.Deprecated('Use getAllAgentWalletsRequestDescriptor instead')
const GetAllAgentWalletsRequest$json = const {
  '1': 'GetAllAgentWalletsRequest',
  '2': const [
    const {'1': 'page', '3': 1, '4': 1, '5': 13, '10': 'page'},
    const {'1': 'size', '3': 2, '4': 1, '5': 13, '10': 'size'},
    const {'1': 'filterBy', '3': 3, '4': 1, '5': 11, '6': '.WalletFilter', '10': 'filterBy'},
  ],
};

/// Descriptor for `GetAllAgentWalletsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllAgentWalletsRequestDescriptor = $convert.base64Decode('ChlHZXRBbGxBZ2VudFdhbGxldHNSZXF1ZXN0EhIKBHBhZ2UYASABKA1SBHBhZ2USEgoEc2l6ZRgCIAEoDVIEc2l6ZRIpCghmaWx0ZXJCeRgDIAEoCzINLldhbGxldEZpbHRlclIIZmlsdGVyQnk=');
@$core.Deprecated('Use getAllAgentWalletsResponseDescriptor instead')
const GetAllAgentWalletsResponse$json = const {
  '1': 'GetAllAgentWalletsResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'wallets', '3': 2, '4': 3, '5': 11, '6': '.Wallet', '10': 'wallets'},
    const {'1': 'page', '3': 3, '4': 1, '5': 13, '10': 'page'},
    const {'1': 'size', '3': 4, '4': 1, '5': 13, '10': 'size'},
    const {'1': 'total', '3': 5, '4': 1, '5': 3, '10': 'total'},
  ],
};

/// Descriptor for `GetAllAgentWalletsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllAgentWalletsResponseDescriptor = $convert.base64Decode('ChpHZXRBbGxBZ2VudFdhbGxldHNSZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEiEKB3dhbGxldHMYAiADKAsyBy5XYWxsZXRSB3dhbGxldHMSEgoEcGFnZRgDIAEoDVIEcGFnZRISCgRzaXplGAQgASgNUgRzaXplEhQKBXRvdGFsGAUgASgDUgV0b3RhbA==');
@$core.Deprecated('Use getWalletByAgentIdRequestDescriptor instead')
const GetWalletByAgentIdRequest$json = const {
  '1': 'GetWalletByAgentIdRequest',
  '2': const [
    const {'1': 'agent_id', '3': 1, '4': 1, '5': 9, '10': 'agentId'},
  ],
};

/// Descriptor for `GetWalletByAgentIdRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getWalletByAgentIdRequestDescriptor = $convert.base64Decode('ChlHZXRXYWxsZXRCeUFnZW50SWRSZXF1ZXN0EhkKCGFnZW50X2lkGAEgASgJUgdhZ2VudElk');
@$core.Deprecated('Use getWalletByAgentIdResponseDescriptor instead')
const GetWalletByAgentIdResponse$json = const {
  '1': 'GetWalletByAgentIdResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'wallet', '3': 2, '4': 1, '5': 11, '6': '.Wallet', '10': 'wallet'},
  ],
};

/// Descriptor for `GetWalletByAgentIdResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getWalletByAgentIdResponseDescriptor = $convert.base64Decode('ChpHZXRXYWxsZXRCeUFnZW50SWRSZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEh8KBndhbGxldBgCIAEoCzIHLldhbGxldFIGd2FsbGV0');
@$core.Deprecated('Use valueDescriptor instead')
const Value$json = const {
  '1': 'Value',
  '2': const [
    const {'1': 'string_value', '3': 1, '4': 1, '5': 9, '9': 0, '10': 'stringValue'},
    const {'1': 'double_value', '3': 2, '4': 1, '5': 1, '9': 0, '10': 'doubleValue'},
    const {'1': 'uint_value', '3': 3, '4': 1, '5': 13, '9': 0, '10': 'uintValue'},
  ],
  '8': const [
    const {'1': 'kind'},
  ],
};

/// Descriptor for `Value`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List valueDescriptor = $convert.base64Decode('CgVWYWx1ZRIjCgxzdHJpbmdfdmFsdWUYASABKAlIAFILc3RyaW5nVmFsdWUSIwoMZG91YmxlX3ZhbHVlGAIgASgBSABSC2RvdWJsZVZhbHVlEh8KCnVpbnRfdmFsdWUYAyABKA1IAFIJdWludFZhbHVlQgYKBGtpbmQ=');
@$core.Deprecated('Use orderItemsDescriptor instead')
const OrderItems$json = const {
  '1': 'OrderItems',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'quantity', '3': 2, '4': 1, '5': 4, '10': 'quantity'},
    const {'1': 'price', '3': 3, '4': 1, '5': 2, '10': 'price'},
  ],
};

/// Descriptor for `OrderItems`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderItemsDescriptor = $convert.base64Decode('CgpPcmRlckl0ZW1zEhIKBG5hbWUYASABKAlSBG5hbWUSGgoIcXVhbnRpdHkYAiABKARSCHF1YW50aXR5EhQKBXByaWNlGAMgASgCUgVwcmljZQ==');
@$core.Deprecated('Use orderRequestDescriptor instead')
const OrderRequest$json = const {
  '1': 'OrderRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'order_id', '3': 2, '4': 1, '5': 9, '10': 'orderId'},
    const {'1': 'order_items', '3': 3, '4': 3, '5': 11, '6': '.OrderItems', '10': 'orderItems'},
    const {'1': 'status', '3': 4, '4': 1, '5': 14, '6': '.OrderRequestStatus', '10': 'status'},
    const {'1': 'duration', '3': 5, '4': 1, '5': 13, '10': 'duration'},
    const {'1': 'earnable_amount', '3': 6, '4': 1, '5': 2, '10': 'earnableAmount'},
    const {'1': 'delivery_location', '3': 7, '4': 1, '5': 9, '10': 'deliveryLocation'},
    const {'1': 'agent_id', '3': 8, '4': 1, '5': 9, '10': 'agentId'},
    const {'1': 'customer_id', '3': 9, '4': 1, '5': 9, '10': 'customerId'},
  ],
};

/// Descriptor for `OrderRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderRequestDescriptor = $convert.base64Decode('CgxPcmRlclJlcXVlc3QSDgoCaWQYASABKAlSAmlkEhkKCG9yZGVyX2lkGAIgASgJUgdvcmRlcklkEiwKC29yZGVyX2l0ZW1zGAMgAygLMgsuT3JkZXJJdGVtc1IKb3JkZXJJdGVtcxIrCgZzdGF0dXMYBCABKA4yEy5PcmRlclJlcXVlc3RTdGF0dXNSBnN0YXR1cxIaCghkdXJhdGlvbhgFIAEoDVIIZHVyYXRpb24SJwoPZWFybmFibGVfYW1vdW50GAYgASgCUg5lYXJuYWJsZUFtb3VudBIrChFkZWxpdmVyeV9sb2NhdGlvbhgHIAEoCVIQZGVsaXZlcnlMb2NhdGlvbhIZCghhZ2VudF9pZBgIIAEoCVIHYWdlbnRJZBIfCgtjdXN0b21lcl9pZBgJIAEoCVIKY3VzdG9tZXJJZA==');
@$core.Deprecated('Use createOrderRequest_RequestDescriptor instead')
const CreateOrderRequest_Request$json = const {
  '1': 'CreateOrderRequest_Request',
  '2': const [
    const {'1': 'order_id', '3': 1, '4': 1, '5': 9, '10': 'orderId'},
    const {'1': 'order_items', '3': 2, '4': 3, '5': 11, '6': '.OrderItems', '10': 'orderItems'},
    const {'1': 'duration', '3': 3, '4': 1, '5': 13, '10': 'duration'},
    const {'1': 'earnable_amount', '3': 4, '4': 1, '5': 2, '10': 'earnableAmount'},
    const {'1': 'delivery_location', '3': 5, '4': 1, '5': 9, '10': 'deliveryLocation'},
    const {'1': 'customer_id', '3': 6, '4': 1, '5': 9, '10': 'customerId'},
  ],
};

/// Descriptor for `CreateOrderRequest_Request`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createOrderRequest_RequestDescriptor = $convert.base64Decode('ChpDcmVhdGVPcmRlclJlcXVlc3RfUmVxdWVzdBIZCghvcmRlcl9pZBgBIAEoCVIHb3JkZXJJZBIsCgtvcmRlcl9pdGVtcxgCIAMoCzILLk9yZGVySXRlbXNSCm9yZGVySXRlbXMSGgoIZHVyYXRpb24YAyABKA1SCGR1cmF0aW9uEicKD2Vhcm5hYmxlX2Ftb3VudBgEIAEoAlIOZWFybmFibGVBbW91bnQSKwoRZGVsaXZlcnlfbG9jYXRpb24YBSABKAlSEGRlbGl2ZXJ5TG9jYXRpb24SHwoLY3VzdG9tZXJfaWQYBiABKAlSCmN1c3RvbWVySWQ=');
@$core.Deprecated('Use createOrderRequest_ResponseDescriptor instead')
const CreateOrderRequest_Response$json = const {
  '1': 'CreateOrderRequest_Response',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `CreateOrderRequest_Response`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createOrderRequest_ResponseDescriptor = $convert.base64Decode('ChtDcmVhdGVPcmRlclJlcXVlc3RfUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use orderRequestFilterDescriptor instead')
const OrderRequestFilter$json = const {
  '1': 'OrderRequestFilter',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.OrderRequestStatus', '10': 'status'},
    const {'1': 'agent_id', '3': 2, '4': 1, '5': 9, '10': 'agentId'},
    const {'1': 'customer_id', '3': 3, '4': 1, '5': 9, '10': 'customerId'},
  ],
};

/// Descriptor for `OrderRequestFilter`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderRequestFilterDescriptor = $convert.base64Decode('ChJPcmRlclJlcXVlc3RGaWx0ZXISKwoGc3RhdHVzGAEgASgOMhMuT3JkZXJSZXF1ZXN0U3RhdHVzUgZzdGF0dXMSGQoIYWdlbnRfaWQYAiABKAlSB2FnZW50SWQSHwoLY3VzdG9tZXJfaWQYAyABKAlSCmN1c3RvbWVySWQ=');
@$core.Deprecated('Use getAllOrderRequests_RequestDescriptor instead')
const GetAllOrderRequests_Request$json = const {
  '1': 'GetAllOrderRequests_Request',
  '2': const [
    const {'1': 'page', '3': 1, '4': 1, '5': 13, '10': 'page'},
    const {'1': 'size', '3': 2, '4': 1, '5': 13, '10': 'size'},
    const {'1': 'filterBy', '3': 3, '4': 1, '5': 11, '6': '.OrderRequestFilter', '10': 'filterBy'},
  ],
};

/// Descriptor for `GetAllOrderRequests_Request`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllOrderRequests_RequestDescriptor = $convert.base64Decode('ChtHZXRBbGxPcmRlclJlcXVlc3RzX1JlcXVlc3QSEgoEcGFnZRgBIAEoDVIEcGFnZRISCgRzaXplGAIgASgNUgRzaXplEi8KCGZpbHRlckJ5GAMgASgLMhMuT3JkZXJSZXF1ZXN0RmlsdGVyUghmaWx0ZXJCeQ==');
@$core.Deprecated('Use getAllOrderRequests_ResponseDescriptor instead')
const GetAllOrderRequests_Response$json = const {
  '1': 'GetAllOrderRequests_Response',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'order_requests', '3': 2, '4': 3, '5': 11, '6': '.OrderRequest', '10': 'orderRequests'},
    const {'1': 'page', '3': 3, '4': 1, '5': 13, '10': 'page'},
    const {'1': 'size', '3': 4, '4': 1, '5': 13, '10': 'size'},
    const {'1': 'total', '3': 5, '4': 1, '5': 3, '10': 'total'},
  ],
};

/// Descriptor for `GetAllOrderRequests_Response`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllOrderRequests_ResponseDescriptor = $convert.base64Decode('ChxHZXRBbGxPcmRlclJlcXVlc3RzX1Jlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USNAoOb3JkZXJfcmVxdWVzdHMYAiADKAsyDS5PcmRlclJlcXVlc3RSDW9yZGVyUmVxdWVzdHMSEgoEcGFnZRgDIAEoDVIEcGFnZRISCgRzaXplGAQgASgNUgRzaXplEhQKBXRvdGFsGAUgASgDUgV0b3RhbA==');
@$core.Deprecated('Use sendOrderRequestToAgent_RequestDescriptor instead')
const SendOrderRequestToAgent_Request$json = const {
  '1': 'SendOrderRequestToAgent_Request',
  '2': const [
    const {'1': 'order_request_id', '3': 1, '4': 1, '5': 9, '10': 'orderRequestId'},
    const {'1': 'agent_id', '3': 2, '4': 1, '5': 9, '10': 'agentId'},
    const {'1': 'acceptance_duration', '3': 3, '4': 1, '5': 4, '10': 'acceptanceDuration'},
  ],
};

/// Descriptor for `SendOrderRequestToAgent_Request`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendOrderRequestToAgent_RequestDescriptor = $convert.base64Decode('Ch9TZW5kT3JkZXJSZXF1ZXN0VG9BZ2VudF9SZXF1ZXN0EigKEG9yZGVyX3JlcXVlc3RfaWQYASABKAlSDm9yZGVyUmVxdWVzdElkEhkKCGFnZW50X2lkGAIgASgJUgdhZ2VudElkEi8KE2FjY2VwdGFuY2VfZHVyYXRpb24YAyABKARSEmFjY2VwdGFuY2VEdXJhdGlvbg==');
@$core.Deprecated('Use sendOrderRequestToAgent_ResponseDescriptor instead')
const SendOrderRequestToAgent_Response$json = const {
  '1': 'SendOrderRequestToAgent_Response',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'request_id', '3': 2, '4': 1, '5': 9, '10': 'requestId'},
  ],
};

/// Descriptor for `SendOrderRequestToAgent_Response`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendOrderRequestToAgent_ResponseDescriptor = $convert.base64Decode('CiBTZW5kT3JkZXJSZXF1ZXN0VG9BZ2VudF9SZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEh0KCnJlcXVlc3RfaWQYAiABKAlSCXJlcXVlc3RJZA==');
@$core.Deprecated('Use getAssignedOrderRequest_RequestDescriptor instead')
const GetAssignedOrderRequest_Request$json = const {
  '1': 'GetAssignedOrderRequest_Request',
};

/// Descriptor for `GetAssignedOrderRequest_Request`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAssignedOrderRequest_RequestDescriptor = $convert.base64Decode('Ch9HZXRBc3NpZ25lZE9yZGVyUmVxdWVzdF9SZXF1ZXN0');
@$core.Deprecated('Use getAssignedOrderRequest_ResponseDescriptor instead')
const GetAssignedOrderRequest_Response$json = const {
  '1': 'GetAssignedOrderRequest_Response',
  '2': const [
    const {'1': 'order_request', '3': 1, '4': 1, '5': 11, '6': '.OrderRequest', '10': 'orderRequest'},
  ],
};

/// Descriptor for `GetAssignedOrderRequest_Response`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAssignedOrderRequest_ResponseDescriptor = $convert.base64Decode('CiBHZXRBc3NpZ25lZE9yZGVyUmVxdWVzdF9SZXNwb25zZRIyCg1vcmRlcl9yZXF1ZXN0GAEgASgLMg0uT3JkZXJSZXF1ZXN0UgxvcmRlclJlcXVlc3Q=');
@$core.Deprecated('Use acceptOrDeclineOrderRequest_RequestDescriptor instead')
const AcceptOrDeclineOrderRequest_Request$json = const {
  '1': 'AcceptOrDeclineOrderRequest_Request',
  '2': const [
    const {'1': 'accepted', '3': 1, '4': 1, '5': 14, '6': '.AcceptanceStatus', '10': 'accepted'},
    const {'1': 'order_request_id', '3': 2, '4': 1, '5': 9, '10': 'orderRequestId'},
    const {'1': 'reject_reason', '3': 3, '4': 1, '5': 9, '10': 'rejectReason'},
    const {'1': 'request_id', '3': 4, '4': 1, '5': 9, '10': 'requestId'},
  ],
};

/// Descriptor for `AcceptOrDeclineOrderRequest_Request`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List acceptOrDeclineOrderRequest_RequestDescriptor = $convert.base64Decode('CiNBY2NlcHRPckRlY2xpbmVPcmRlclJlcXVlc3RfUmVxdWVzdBItCghhY2NlcHRlZBgBIAEoDjIRLkFjY2VwdGFuY2VTdGF0dXNSCGFjY2VwdGVkEigKEG9yZGVyX3JlcXVlc3RfaWQYAiABKAlSDm9yZGVyUmVxdWVzdElkEiMKDXJlamVjdF9yZWFzb24YAyABKAlSDHJlamVjdFJlYXNvbhIdCgpyZXF1ZXN0X2lkGAQgASgJUglyZXF1ZXN0SWQ=');
@$core.Deprecated('Use acceptOrDeclineOrderRequest_ResponseDescriptor instead')
const AcceptOrDeclineOrderRequest_Response$json = const {
  '1': 'AcceptOrDeclineOrderRequest_Response',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'order_request', '3': 2, '4': 1, '5': 11, '6': '.OrderRequest', '10': 'orderRequest'},
  ],
};

/// Descriptor for `AcceptOrDeclineOrderRequest_Response`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List acceptOrDeclineOrderRequest_ResponseDescriptor = $convert.base64Decode('CiRBY2NlcHRPckRlY2xpbmVPcmRlclJlcXVlc3RfUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZRIyCg1vcmRlcl9yZXF1ZXN0GAIgASgLMg0uT3JkZXJSZXF1ZXN0UgxvcmRlclJlcXVlc3Q=');
@$core.Deprecated('Use updateOrderRequestStatus_RequestDescriptor instead')
const UpdateOrderRequestStatus_Request$json = const {
  '1': 'UpdateOrderRequestStatus_Request',
  '2': const [
    const {'1': 'order_request_id', '3': 1, '4': 1, '5': 9, '10': 'orderRequestId'},
    const {'1': 'status', '3': 2, '4': 1, '5': 14, '6': '.OrderRequestStatus', '10': 'status'},
  ],
};

/// Descriptor for `UpdateOrderRequestStatus_Request`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateOrderRequestStatus_RequestDescriptor = $convert.base64Decode('CiBVcGRhdGVPcmRlclJlcXVlc3RTdGF0dXNfUmVxdWVzdBIoChBvcmRlcl9yZXF1ZXN0X2lkGAEgASgJUg5vcmRlclJlcXVlc3RJZBIrCgZzdGF0dXMYAiABKA4yEy5PcmRlclJlcXVlc3RTdGF0dXNSBnN0YXR1cw==');
@$core.Deprecated('Use updateOrderRequestStatus_ResponseDescriptor instead')
const UpdateOrderRequestStatus_Response$json = const {
  '1': 'UpdateOrderRequestStatus_Response',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `UpdateOrderRequestStatus_Response`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateOrderRequestStatus_ResponseDescriptor = $convert.base64Decode('CiFVcGRhdGVPcmRlclJlcXVlc3RTdGF0dXNfUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use getOrderRequestHistory_RequestDescriptor instead')
const GetOrderRequestHistory_Request$json = const {
  '1': 'GetOrderRequestHistory_Request',
  '2': const [
    const {'1': 'agent_id', '3': 1, '4': 1, '5': 9, '10': 'agentId'},
    const {'1': 'page', '3': 2, '4': 1, '5': 13, '10': 'page'},
    const {'1': 'size', '3': 3, '4': 1, '5': 13, '10': 'size'},
  ],
};

/// Descriptor for `GetOrderRequestHistory_Request`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getOrderRequestHistory_RequestDescriptor = $convert.base64Decode('Ch5HZXRPcmRlclJlcXVlc3RIaXN0b3J5X1JlcXVlc3QSGQoIYWdlbnRfaWQYASABKAlSB2FnZW50SWQSEgoEcGFnZRgCIAEoDVIEcGFnZRISCgRzaXplGAMgASgNUgRzaXpl');
@$core.Deprecated('Use getOrderRequestHistory_ResponseDescriptor instead')
const GetOrderRequestHistory_Response$json = const {
  '1': 'GetOrderRequestHistory_Response',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'order_requests', '3': 2, '4': 3, '5': 11, '6': '.OrderRequest', '10': 'orderRequests'},
    const {'1': 'page', '3': 3, '4': 1, '5': 13, '10': 'page'},
    const {'1': 'size', '3': 4, '4': 1, '5': 13, '10': 'size'},
    const {'1': 'total', '3': 5, '4': 1, '5': 3, '10': 'total'},
  ],
};

/// Descriptor for `GetOrderRequestHistory_Response`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getOrderRequestHistory_ResponseDescriptor = $convert.base64Decode('Ch9HZXRPcmRlclJlcXVlc3RIaXN0b3J5X1Jlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USNAoOb3JkZXJfcmVxdWVzdHMYAiADKAsyDS5PcmRlclJlcXVlc3RSDW9yZGVyUmVxdWVzdHMSEgoEcGFnZRgDIAEoDVIEcGFnZRISCgRzaXplGAQgASgNUgRzaXplEhQKBXRvdGFsGAUgASgDUgV0b3RhbA==');
