///
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use otpTypeDescriptor instead')
const OtpType$json = const {
  '1': 'OtpType',
  '2': const [
    const {'1': 'LOGIN', '2': 0},
    const {'1': 'REG', '2': 1},
    const {'1': 'RESET_PASSWORD', '2': 2},
  ],
};

/// Descriptor for `OtpType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List otpTypeDescriptor = $convert.base64Decode('CgdPdHBUeXBlEgkKBUxPR0lOEAASBwoDUkVHEAESEgoOUkVTRVRfUEFTU1dPUkQQAg==');
@$core.Deprecated('Use generalResponseDescriptor instead')
const GeneralResponse$json = const {
  '1': 'GeneralResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `GeneralResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List generalResponseDescriptor = $convert.base64Decode('Cg9HZW5lcmFsUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use loginRequestDescriptor instead')
const LoginRequest$json = const {
  '1': 'LoginRequest',
  '2': const [
    const {'1': 'login', '3': 1, '4': 1, '5': 9, '10': 'login'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'phoneCode', '3': 3, '4': 1, '5': 9, '10': 'phoneCode'},
  ],
};

/// Descriptor for `LoginRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginRequestDescriptor = $convert.base64Decode('CgxMb2dpblJlcXVlc3QSFAoFbG9naW4YASABKAlSBWxvZ2luEhoKCHBhc3N3b3JkGAIgASgJUghwYXNzd29yZBIcCglwaG9uZUNvZGUYAyABKAlSCXBob25lQ29kZQ==');
@$core.Deprecated('Use loginResponseDescriptor instead')
const LoginResponse$json = const {
  '1': 'LoginResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'requestId', '3': 2, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'isEmailVerified', '3': 3, '4': 1, '5': 9, '10': 'isEmailVerified'},
    const {'1': 'isPhoneVerified', '3': 4, '4': 1, '5': 9, '10': 'isPhoneVerified'},
    const {'1': 'user', '3': 5, '4': 1, '5': 11, '6': '.User', '10': 'user'},
  ],
};

/// Descriptor for `LoginResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginResponseDescriptor = $convert.base64Decode('Cg1Mb2dpblJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USHAoJcmVxdWVzdElkGAIgASgJUglyZXF1ZXN0SWQSKAoPaXNFbWFpbFZlcmlmaWVkGAMgASgJUg9pc0VtYWlsVmVyaWZpZWQSKAoPaXNQaG9uZVZlcmlmaWVkGAQgASgJUg9pc1Bob25lVmVyaWZpZWQSGQoEdXNlchgFIAEoCzIFLlVzZXJSBHVzZXI=');
@$core.Deprecated('Use loginWithGoogleRequestDescriptor instead')
const LoginWithGoogleRequest$json = const {
  '1': 'LoginWithGoogleRequest',
  '2': const [
    const {'1': 'google_access_token', '3': 1, '4': 1, '5': 9, '10': 'googleAccessToken'},
    const {'1': 'channel', '3': 2, '4': 1, '5': 14, '6': '.LoginWithGoogleRequest.Channel', '10': 'channel'},
  ],
  '4': const [LoginWithGoogleRequest_Channel$json],
};

@$core.Deprecated('Use loginWithGoogleRequestDescriptor instead')
const LoginWithGoogleRequest_Channel$json = const {
  '1': 'Channel',
  '2': const [
    const {'1': 'WEB', '2': 0},
    const {'1': 'MOBILE', '2': 1},
  ],
};

/// Descriptor for `LoginWithGoogleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginWithGoogleRequestDescriptor = $convert.base64Decode('ChZMb2dpbldpdGhHb29nbGVSZXF1ZXN0Ei4KE2dvb2dsZV9hY2Nlc3NfdG9rZW4YASABKAlSEWdvb2dsZUFjY2Vzc1Rva2VuEjkKB2NoYW5uZWwYAiABKA4yHy5Mb2dpbldpdGhHb29nbGVSZXF1ZXN0LkNoYW5uZWxSB2NoYW5uZWwiHgoHQ2hhbm5lbBIHCgNXRUIQABIKCgZNT0JJTEUQAQ==');
@$core.Deprecated('Use loginWithGoogleResponseDescriptor instead')
const LoginWithGoogleResponse$json = const {
  '1': 'LoginWithGoogleResponse',
  '2': const [
    const {'1': 'isNewUser', '3': 1, '4': 1, '5': 9, '10': 'isNewUser'},
    const {'1': 'isEmailVerified', '3': 2, '4': 1, '5': 9, '10': 'isEmailVerified'},
    const {'1': 'isPhoneVerified', '3': 3, '4': 1, '5': 9, '10': 'isPhoneVerified'},
    const {'1': 'oauthId', '3': 4, '4': 1, '5': 9, '10': 'oauthId'},
    const {'1': 'user', '3': 5, '4': 1, '5': 11, '6': '.User', '10': 'user'},
    const {'1': 'access_token', '3': 6, '4': 1, '5': 9, '10': 'accessToken'},
  ],
};

/// Descriptor for `LoginWithGoogleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginWithGoogleResponseDescriptor = $convert.base64Decode('ChdMb2dpbldpdGhHb29nbGVSZXNwb25zZRIcCglpc05ld1VzZXIYASABKAlSCWlzTmV3VXNlchIoCg9pc0VtYWlsVmVyaWZpZWQYAiABKAlSD2lzRW1haWxWZXJpZmllZBIoCg9pc1Bob25lVmVyaWZpZWQYAyABKAlSD2lzUGhvbmVWZXJpZmllZBIYCgdvYXV0aElkGAQgASgJUgdvYXV0aElkEhkKBHVzZXIYBSABKAsyBS5Vc2VyUgR1c2VyEiEKDGFjY2Vzc190b2tlbhgGIAEoCVILYWNjZXNzVG9rZW4=');
@$core.Deprecated('Use registerRequestDescriptor instead')
const RegisterRequest$json = const {
  '1': 'RegisterRequest',
  '2': const [
    const {'1': 'firstName', '3': 1, '4': 1, '5': 9, '10': 'firstName'},
    const {'1': 'lastName', '3': 2, '4': 1, '5': 9, '10': 'lastName'},
    const {'1': 'email', '3': 3, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phoneNumber', '3': 4, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'phoneCode', '3': 5, '4': 1, '5': 9, '10': 'phoneCode'},
    const {'1': 'password', '3': 6, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'address', '3': 7, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'state', '3': 8, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'country', '3': 9, '4': 1, '5': 9, '10': 'country'},
    const {'1': 'oauthId', '3': 10, '4': 1, '5': 9, '9': 0, '10': 'oauthId', '17': true},
    const {'1': 'referralUsed', '3': 11, '4': 1, '5': 9, '10': 'referralUsed'},
  ],
  '8': const [
    const {'1': '_oauthId'},
  ],
};

/// Descriptor for `RegisterRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List registerRequestDescriptor = $convert.base64Decode('Cg9SZWdpc3RlclJlcXVlc3QSHAoJZmlyc3ROYW1lGAEgASgJUglmaXJzdE5hbWUSGgoIbGFzdE5hbWUYAiABKAlSCGxhc3ROYW1lEhQKBWVtYWlsGAMgASgJUgVlbWFpbBIgCgtwaG9uZU51bWJlchgEIAEoCVILcGhvbmVOdW1iZXISHAoJcGhvbmVDb2RlGAUgASgJUglwaG9uZUNvZGUSGgoIcGFzc3dvcmQYBiABKAlSCHBhc3N3b3JkEhgKB2FkZHJlc3MYByABKAlSB2FkZHJlc3MSFAoFc3RhdGUYCCABKAlSBXN0YXRlEhgKB2NvdW50cnkYCSABKAlSB2NvdW50cnkSHQoHb2F1dGhJZBgKIAEoCUgAUgdvYXV0aElkiAEBEiIKDHJlZmVycmFsVXNlZBgLIAEoCVIMcmVmZXJyYWxVc2VkQgoKCF9vYXV0aElk');
@$core.Deprecated('Use registerResponseDescriptor instead')
const RegisterResponse$json = const {
  '1': 'RegisterResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'isEmailVerified', '3': 2, '4': 1, '5': 9, '10': 'isEmailVerified'},
    const {'1': 'isPhoneVerified', '3': 3, '4': 1, '5': 9, '10': 'isPhoneVerified'},
  ],
};

/// Descriptor for `RegisterResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List registerResponseDescriptor = $convert.base64Decode('ChBSZWdpc3RlclJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USKAoPaXNFbWFpbFZlcmlmaWVkGAIgASgJUg9pc0VtYWlsVmVyaWZpZWQSKAoPaXNQaG9uZVZlcmlmaWVkGAMgASgJUg9pc1Bob25lVmVyaWZpZWQ=');
@$core.Deprecated('Use initPhoneVerificationRequestDescriptor instead')
const InitPhoneVerificationRequest$json = const {
  '1': 'InitPhoneVerificationRequest',
  '2': const [
    const {'1': 'phoneNumber', '3': 1, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'phoneCode', '3': 2, '4': 1, '5': 9, '10': 'phoneCode'},
    const {'1': 'type', '3': 3, '4': 1, '5': 14, '6': '.OtpType', '10': 'type'},
  ],
};

/// Descriptor for `InitPhoneVerificationRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List initPhoneVerificationRequestDescriptor = $convert.base64Decode('ChxJbml0UGhvbmVWZXJpZmljYXRpb25SZXF1ZXN0EiAKC3Bob25lTnVtYmVyGAEgASgJUgtwaG9uZU51bWJlchIcCglwaG9uZUNvZGUYAiABKAlSCXBob25lQ29kZRIcCgR0eXBlGAMgASgOMgguT3RwVHlwZVIEdHlwZQ==');
@$core.Deprecated('Use initEmailVerificationRequestDescriptor instead')
const InitEmailVerificationRequest$json = const {
  '1': 'InitEmailVerificationRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'type', '3': 2, '4': 1, '5': 14, '6': '.OtpType', '10': 'type'},
  ],
};

/// Descriptor for `InitEmailVerificationRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List initEmailVerificationRequestDescriptor = $convert.base64Decode('ChxJbml0RW1haWxWZXJpZmljYXRpb25SZXF1ZXN0EhQKBWVtYWlsGAEgASgJUgVlbWFpbBIcCgR0eXBlGAIgASgOMgguT3RwVHlwZVIEdHlwZQ==');
@$core.Deprecated('Use initEmailVerificationResponseDescriptor instead')
const InitEmailVerificationResponse$json = const {
  '1': 'InitEmailVerificationResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'requestId', '3': 2, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'type', '3': 3, '4': 1, '5': 14, '6': '.OtpType', '10': 'type'},
  ],
};

/// Descriptor for `InitEmailVerificationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List initEmailVerificationResponseDescriptor = $convert.base64Decode('Ch1Jbml0RW1haWxWZXJpZmljYXRpb25SZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEhwKCXJlcXVlc3RJZBgCIAEoCVIJcmVxdWVzdElkEhwKBHR5cGUYAyABKA4yCC5PdHBUeXBlUgR0eXBl');
@$core.Deprecated('Use initPhoneVerificationResponseDescriptor instead')
const InitPhoneVerificationResponse$json = const {
  '1': 'InitPhoneVerificationResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'requestId', '3': 2, '4': 1, '5': 9, '10': 'requestId'},
  ],
};

/// Descriptor for `InitPhoneVerificationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List initPhoneVerificationResponseDescriptor = $convert.base64Decode('Ch1Jbml0UGhvbmVWZXJpZmljYXRpb25SZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEhwKCXJlcXVlc3RJZBgCIAEoCVIJcmVxdWVzdElk');
@$core.Deprecated('Use emailVerificationRequestDescriptor instead')
const EmailVerificationRequest$json = const {
  '1': 'EmailVerificationRequest',
  '2': const [
    const {'1': 'requestID', '3': 1, '4': 1, '5': 9, '10': 'requestID'},
    const {'1': 'otp', '3': 2, '4': 1, '5': 9, '10': 'otp'},
    const {'1': 'email', '3': 3, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'type', '3': 4, '4': 1, '5': 14, '6': '.EmailVerificationRequest.OtpType', '10': 'type'},
  ],
  '4': const [EmailVerificationRequest_OtpType$json],
};

@$core.Deprecated('Use emailVerificationRequestDescriptor instead')
const EmailVerificationRequest_OtpType$json = const {
  '1': 'OtpType',
  '2': const [
    const {'1': 'LOGIN', '2': 0},
    const {'1': 'REG', '2': 1},
  ],
};

/// Descriptor for `EmailVerificationRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emailVerificationRequestDescriptor = $convert.base64Decode('ChhFbWFpbFZlcmlmaWNhdGlvblJlcXVlc3QSHAoJcmVxdWVzdElEGAEgASgJUglyZXF1ZXN0SUQSEAoDb3RwGAIgASgJUgNvdHASFAoFZW1haWwYAyABKAlSBWVtYWlsEjUKBHR5cGUYBCABKA4yIS5FbWFpbFZlcmlmaWNhdGlvblJlcXVlc3QuT3RwVHlwZVIEdHlwZSIdCgdPdHBUeXBlEgkKBUxPR0lOEAASBwoDUkVHEAE=');
@$core.Deprecated('Use phoneVerificationRequestDescriptor instead')
const PhoneVerificationRequest$json = const {
  '1': 'PhoneVerificationRequest',
  '2': const [
    const {'1': 'requestID', '3': 1, '4': 1, '5': 9, '10': 'requestID'},
    const {'1': 'otp', '3': 2, '4': 1, '5': 9, '10': 'otp'},
    const {'1': 'phoneNumber', '3': 3, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'phoneCode', '3': 4, '4': 1, '5': 9, '10': 'phoneCode'},
    const {'1': 'type', '3': 5, '4': 1, '5': 14, '6': '.OtpType', '10': 'type'},
  ],
};

/// Descriptor for `PhoneVerificationRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List phoneVerificationRequestDescriptor = $convert.base64Decode('ChhQaG9uZVZlcmlmaWNhdGlvblJlcXVlc3QSHAoJcmVxdWVzdElEGAEgASgJUglyZXF1ZXN0SUQSEAoDb3RwGAIgASgJUgNvdHASIAoLcGhvbmVOdW1iZXIYAyABKAlSC3Bob25lTnVtYmVyEhwKCXBob25lQ29kZRgEIAEoCVIJcGhvbmVDb2RlEhwKBHR5cGUYBSABKA4yCC5PdHBUeXBlUgR0eXBl');
@$core.Deprecated('Use phoneVerificationResponseDescriptor instead')
const PhoneVerificationResponse$json = const {
  '1': 'PhoneVerificationResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `PhoneVerificationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List phoneVerificationResponseDescriptor = $convert.base64Decode('ChlQaG9uZVZlcmlmaWNhdGlvblJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use verifyLoginRequestDescriptor instead')
const VerifyLoginRequest$json = const {
  '1': 'VerifyLoginRequest',
  '2': const [
    const {'1': 'login', '3': 1, '4': 1, '5': 9, '10': 'login'},
    const {'1': 'requestId', '3': 2, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'otp', '3': 3, '4': 1, '5': 9, '10': 'otp'},
    const {'1': 'type', '3': 4, '4': 1, '5': 14, '6': '.OtpType', '10': 'type'},
    const {'1': 'phoneCode', '3': 5, '4': 1, '5': 9, '9': 0, '10': 'phoneCode', '17': true},
  ],
  '8': const [
    const {'1': '_phoneCode'},
  ],
};

/// Descriptor for `VerifyLoginRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verifyLoginRequestDescriptor = $convert.base64Decode('ChJWZXJpZnlMb2dpblJlcXVlc3QSFAoFbG9naW4YASABKAlSBWxvZ2luEhwKCXJlcXVlc3RJZBgCIAEoCVIJcmVxdWVzdElkEhAKA290cBgDIAEoCVIDb3RwEhwKBHR5cGUYBCABKA4yCC5PdHBUeXBlUgR0eXBlEiEKCXBob25lQ29kZRgFIAEoCUgAUglwaG9uZUNvZGWIAQFCDAoKX3Bob25lQ29kZQ==');
@$core.Deprecated('Use verifyLoginResponseDescriptor instead')
const VerifyLoginResponse$json = const {
  '1': 'VerifyLoginResponse',
  '2': const [
    const {'1': 'access_token', '3': 1, '4': 1, '5': 9, '10': 'accessToken'},
    const {'1': 'userInfo', '3': 2, '4': 1, '5': 11, '6': '.User', '10': 'userInfo'},
  ],
};

/// Descriptor for `VerifyLoginResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verifyLoginResponseDescriptor = $convert.base64Decode('ChNWZXJpZnlMb2dpblJlc3BvbnNlEiEKDGFjY2Vzc190b2tlbhgBIAEoCVILYWNjZXNzVG9rZW4SIQoIdXNlckluZm8YAiABKAsyBS5Vc2VyUgh1c2VySW5mbw==');
@$core.Deprecated('Use emailVerificationResponseDescriptor instead')
const EmailVerificationResponse$json = const {
  '1': 'EmailVerificationResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `EmailVerificationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emailVerificationResponseDescriptor = $convert.base64Decode('ChlFbWFpbFZlcmlmaWNhdGlvblJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use logoutRequestDescriptor instead')
const LogoutRequest$json = const {
  '1': 'LogoutRequest',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 9, '10': 'userId'},
  ],
};

/// Descriptor for `LogoutRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List logoutRequestDescriptor = $convert.base64Decode('Cg1Mb2dvdXRSZXF1ZXN0EhcKB3VzZXJfaWQYASABKAlSBnVzZXJJZA==');
@$core.Deprecated('Use logoutResponseDescriptor instead')
const LogoutResponse$json = const {
  '1': 'LogoutResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `LogoutResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List logoutResponseDescriptor = $convert.base64Decode('Cg5Mb2dvdXRSZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdl');
@$core.Deprecated('Use countryDescriptor instead')
const Country$json = const {
  '1': 'Country',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'iso', '3': 3, '4': 1, '5': 9, '10': 'iso'},
    const {'1': 'phone_code', '3': 4, '4': 1, '5': 9, '10': 'phoneCode'},
    const {'1': 'imageUrl', '3': 5, '4': 1, '5': 9, '10': 'imageUrl'},
  ],
};

/// Descriptor for `Country`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List countryDescriptor = $convert.base64Decode('CgdDb3VudHJ5Eg4KAmlkGAEgASgJUgJpZBISCgRuYW1lGAIgASgJUgRuYW1lEhAKA2lzbxgDIAEoCVIDaXNvEh0KCnBob25lX2NvZGUYBCABKAlSCXBob25lQ29kZRIaCghpbWFnZVVybBgFIAEoCVIIaW1hZ2VVcmw=');
@$core.Deprecated('Use getAllCountryRequestDescriptor instead')
const GetAllCountryRequest$json = const {
  '1': 'GetAllCountryRequest',
};

/// Descriptor for `GetAllCountryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllCountryRequestDescriptor = $convert.base64Decode('ChRHZXRBbGxDb3VudHJ5UmVxdWVzdA==');
@$core.Deprecated('Use countryRequestDescriptor instead')
const CountryRequest$json = const {
  '1': 'CountryRequest',
  '2': const [
    const {'1': 'countryID', '3': 1, '4': 1, '5': 9, '10': 'countryID'},
  ],
};

/// Descriptor for `CountryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List countryRequestDescriptor = $convert.base64Decode('Cg5Db3VudHJ5UmVxdWVzdBIcCgljb3VudHJ5SUQYASABKAlSCWNvdW50cnlJRA==');
@$core.Deprecated('Use getAllCountryResponseDescriptor instead')
const GetAllCountryResponse$json = const {
  '1': 'GetAllCountryResponse',
  '2': const [
    const {'1': 'countries', '3': 1, '4': 3, '5': 11, '6': '.Country', '10': 'countries'},
  ],
};

/// Descriptor for `GetAllCountryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllCountryResponseDescriptor = $convert.base64Decode('ChVHZXRBbGxDb3VudHJ5UmVzcG9uc2USJgoJY291bnRyaWVzGAEgAygLMgguQ291bnRyeVIJY291bnRyaWVz');
@$core.Deprecated('Use userRequestDescriptor instead')
const UserRequest$json = const {
  '1': 'UserRequest',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 9, '10': 'userId'},
    const {'1': 'oldPassword', '3': 2, '4': 1, '5': 9, '10': 'oldPassword'},
    const {'1': 'newPassword', '3': 3, '4': 1, '5': 9, '10': 'newPassword'},
  ],
};

/// Descriptor for `UserRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userRequestDescriptor = $convert.base64Decode('CgtVc2VyUmVxdWVzdBIWCgZ1c2VySWQYASABKAlSBnVzZXJJZBIgCgtvbGRQYXNzd29yZBgCIAEoCVILb2xkUGFzc3dvcmQSIAoLbmV3UGFzc3dvcmQYAyABKAlSC25ld1Bhc3N3b3Jk');
@$core.Deprecated('Use updatePasswordResponseDescriptor instead')
const UpdatePasswordResponse$json = const {
  '1': 'UpdatePasswordResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `UpdatePasswordResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updatePasswordResponseDescriptor = $convert.base64Decode('ChZVcGRhdGVQYXNzd29yZFJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use resetPasswordRequestDescriptor instead')
const ResetPasswordRequest$json = const {
  '1': 'ResetPasswordRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
  ],
};

/// Descriptor for `ResetPasswordRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resetPasswordRequestDescriptor = $convert.base64Decode('ChRSZXNldFBhc3N3b3JkUmVxdWVzdBIUCgVlbWFpbBgBIAEoCVIFZW1haWw=');
@$core.Deprecated('Use resetPasswordResponseDescriptor instead')
const ResetPasswordResponse$json = const {
  '1': 'ResetPasswordResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'requestId', '3': 2, '4': 1, '5': 9, '10': 'requestId'},
  ],
};

/// Descriptor for `ResetPasswordResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resetPasswordResponseDescriptor = $convert.base64Decode('ChVSZXNldFBhc3N3b3JkUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZRIcCglyZXF1ZXN0SWQYAiABKAlSCXJlcXVlc3RJZA==');
@$core.Deprecated('Use verifyPasswordResetRequestDescriptor instead')
const VerifyPasswordResetRequest$json = const {
  '1': 'VerifyPasswordResetRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'requestId', '3': 2, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'otp', '3': 3, '4': 1, '5': 9, '10': 'otp'},
    const {'1': 'type', '3': 4, '4': 1, '5': 14, '6': '.OtpType', '10': 'type'},
  ],
};

/// Descriptor for `VerifyPasswordResetRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verifyPasswordResetRequestDescriptor = $convert.base64Decode('ChpWZXJpZnlQYXNzd29yZFJlc2V0UmVxdWVzdBIUCgVlbWFpbBgBIAEoCVIFZW1haWwSHAoJcmVxdWVzdElkGAIgASgJUglyZXF1ZXN0SWQSEAoDb3RwGAMgASgJUgNvdHASHAoEdHlwZRgEIAEoDjIILk90cFR5cGVSBHR5cGU=');
@$core.Deprecated('Use verifyPasswordResetResponseDescriptor instead')
const VerifyPasswordResetResponse$json = const {
  '1': 'VerifyPasswordResetResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `VerifyPasswordResetResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verifyPasswordResetResponseDescriptor = $convert.base64Decode('ChtWZXJpZnlQYXNzd29yZFJlc2V0UmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use setNewPasswordRequestDescriptor instead')
const SetNewPasswordRequest$json = const {
  '1': 'SetNewPasswordRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'requestId', '3': 2, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'newPassword', '3': 3, '4': 1, '5': 9, '10': 'newPassword'},
  ],
};

/// Descriptor for `SetNewPasswordRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setNewPasswordRequestDescriptor = $convert.base64Decode('ChVTZXROZXdQYXNzd29yZFJlcXVlc3QSFAoFZW1haWwYASABKAlSBWVtYWlsEhwKCXJlcXVlc3RJZBgCIAEoCVIJcmVxdWVzdElkEiAKC25ld1Bhc3N3b3JkGAMgASgJUgtuZXdQYXNzd29yZA==');
@$core.Deprecated('Use setNewPasswordResponseDescriptor instead')
const SetNewPasswordResponse$json = const {
  '1': 'SetNewPasswordResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `SetNewPasswordResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setNewPasswordResponseDescriptor = $convert.base64Decode('ChZTZXROZXdQYXNzd29yZFJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use userDescriptor instead')
const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'first_name', '3': 1, '4': 1, '5': 9, '10': 'firstName'},
    const {'1': 'last_name', '3': 2, '4': 1, '5': 9, '10': 'lastName'},
    const {'1': 'email', '3': 3, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phone', '3': 4, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'phoneCode', '3': 5, '4': 1, '5': 9, '10': 'phoneCode'},
    const {'1': 'password', '3': 6, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'address', '3': 7, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'state', '3': 8, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'country', '3': 9, '4': 1, '5': 9, '10': 'country'},
    const {'1': 'userID', '3': 10, '4': 1, '5': 9, '10': 'userID'},
    const {'1': 'referralUsed', '3': 11, '4': 1, '5': 9, '10': 'referralUsed'},
    const {'1': 'referralCode', '3': 12, '4': 1, '5': 9, '10': 'referralCode'},
    const {'1': 'referralCount', '3': 13, '4': 1, '5': 3, '10': 'referralCount'},
    const {'1': 'profilePicture', '3': 14, '4': 1, '5': 9, '10': 'profilePicture'},
  ],
};

/// Descriptor for `User`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userDescriptor = $convert.base64Decode('CgRVc2VyEh0KCmZpcnN0X25hbWUYASABKAlSCWZpcnN0TmFtZRIbCglsYXN0X25hbWUYAiABKAlSCGxhc3ROYW1lEhQKBWVtYWlsGAMgASgJUgVlbWFpbBIUCgVwaG9uZRgEIAEoCVIFcGhvbmUSHAoJcGhvbmVDb2RlGAUgASgJUglwaG9uZUNvZGUSGgoIcGFzc3dvcmQYBiABKAlSCHBhc3N3b3JkEhgKB2FkZHJlc3MYByABKAlSB2FkZHJlc3MSFAoFc3RhdGUYCCABKAlSBXN0YXRlEhgKB2NvdW50cnkYCSABKAlSB2NvdW50cnkSFgoGdXNlcklEGAogASgJUgZ1c2VySUQSIgoMcmVmZXJyYWxVc2VkGAsgASgJUgxyZWZlcnJhbFVzZWQSIgoMcmVmZXJyYWxDb2RlGAwgASgJUgxyZWZlcnJhbENvZGUSJAoNcmVmZXJyYWxDb3VudBgNIAEoA1INcmVmZXJyYWxDb3VudBImCg5wcm9maWxlUGljdHVyZRgOIAEoCVIOcHJvZmlsZVBpY3R1cmU=');
@$core.Deprecated('Use updateUserInformationRequestDescriptor instead')
const UpdateUserInformationRequest$json = const {
  '1': 'UpdateUserInformationRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'first_name', '3': 2, '4': 1, '5': 9, '10': 'firstName'},
    const {'1': 'last_name', '3': 3, '4': 1, '5': 9, '10': 'lastName'},
    const {'1': 'address', '3': 4, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'state', '3': 5, '4': 1, '5': 9, '10': 'state'},
  ],
};

/// Descriptor for `UpdateUserInformationRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateUserInformationRequestDescriptor = $convert.base64Decode('ChxVcGRhdGVVc2VySW5mb3JtYXRpb25SZXF1ZXN0Eg4KAmlkGAEgASgJUgJpZBIdCgpmaXJzdF9uYW1lGAIgASgJUglmaXJzdE5hbWUSGwoJbGFzdF9uYW1lGAMgASgJUghsYXN0TmFtZRIYCgdhZGRyZXNzGAQgASgJUgdhZGRyZXNzEhQKBXN0YXRlGAUgASgJUgVzdGF0ZQ==');
@$core.Deprecated('Use updateUserInformationResponseDescriptor instead')
const UpdateUserInformationResponse$json = const {
  '1': 'UpdateUserInformationResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'user', '3': 2, '4': 1, '5': 11, '6': '.User', '10': 'user'},
  ],
};

/// Descriptor for `UpdateUserInformationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateUserInformationResponseDescriptor = $convert.base64Decode('Ch1VcGRhdGVVc2VySW5mb3JtYXRpb25SZXNwb25zZRIYCgdtZXNzYWdlGAEgASgJUgdtZXNzYWdlEhkKBHVzZXIYAiABKAsyBS5Vc2VyUgR1c2Vy');
@$core.Deprecated('Use deleteUserRequestDescriptor instead')
const DeleteUserRequest$json = const {
  '1': 'DeleteUserRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
  ],
};

/// Descriptor for `DeleteUserRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteUserRequestDescriptor = $convert.base64Decode('ChFEZWxldGVVc2VyUmVxdWVzdBIOCgJpZBgBIAEoCVICaWQ=');
@$core.Deprecated('Use deleteUserResponseDescriptor instead')
const DeleteUserResponse$json = const {
  '1': 'DeleteUserResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `DeleteUserResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteUserResponseDescriptor = $convert.base64Decode('ChJEZWxldGVVc2VyUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use roleDescriptor instead')
const Role$json = const {
  '1': 'Role',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'createdBy', '3': 3, '4': 1, '5': 9, '10': 'createdBy'},
    const {'1': 'id', '3': 4, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'updatedAt', '3': 5, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'createdAt', '3': 6, '4': 1, '5': 9, '10': 'createdAt'},
  ],
};

/// Descriptor for `Role`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List roleDescriptor = $convert.base64Decode('CgRSb2xlEhIKBG5hbWUYASABKAlSBG5hbWUSIAoLZGVzY3JpcHRpb24YAiABKAlSC2Rlc2NyaXB0aW9uEhwKCWNyZWF0ZWRCeRgDIAEoCVIJY3JlYXRlZEJ5Eg4KAmlkGAQgASgJUgJpZBIcCgl1cGRhdGVkQXQYBSABKAlSCXVwZGF0ZWRBdBIcCgljcmVhdGVkQXQYBiABKAlSCWNyZWF0ZWRBdA==');
@$core.Deprecated('Use createRoleRequestDescriptor instead')
const CreateRoleRequest$json = const {
  '1': 'CreateRoleRequest',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'identifier', '3': 3, '4': 1, '5': 5, '10': 'identifier'},
    const {'1': 'createdBy', '3': 4, '4': 1, '5': 9, '10': 'createdBy'},
  ],
};

/// Descriptor for `CreateRoleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createRoleRequestDescriptor = $convert.base64Decode('ChFDcmVhdGVSb2xlUmVxdWVzdBISCgRuYW1lGAEgASgJUgRuYW1lEiAKC2Rlc2NyaXB0aW9uGAIgASgJUgtkZXNjcmlwdGlvbhIeCgppZGVudGlmaWVyGAMgASgFUgppZGVudGlmaWVyEhwKCWNyZWF0ZWRCeRgEIAEoCVIJY3JlYXRlZEJ5');
@$core.Deprecated('Use createRoleResponseDescriptor instead')
const CreateRoleResponse$json = const {
  '1': 'CreateRoleResponse',
  '2': const [
    const {'1': 'role_id', '3': 1, '4': 1, '5': 9, '10': 'roleId'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `CreateRoleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createRoleResponseDescriptor = $convert.base64Decode('ChJDcmVhdGVSb2xlUmVzcG9uc2USFwoHcm9sZV9pZBgBIAEoCVIGcm9sZUlkEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use getAllRolesRequestDescriptor instead')
const GetAllRolesRequest$json = const {
  '1': 'GetAllRolesRequest',
};

/// Descriptor for `GetAllRolesRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllRolesRequestDescriptor = $convert.base64Decode('ChJHZXRBbGxSb2xlc1JlcXVlc3Q=');
@$core.Deprecated('Use getAllRolesResponseDescriptor instead')
const GetAllRolesResponse$json = const {
  '1': 'GetAllRolesResponse',
  '2': const [
    const {'1': 'roles', '3': 1, '4': 3, '5': 11, '6': '.Role', '10': 'roles'},
  ],
};

/// Descriptor for `GetAllRolesResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllRolesResponseDescriptor = $convert.base64Decode('ChNHZXRBbGxSb2xlc1Jlc3BvbnNlEhsKBXJvbGVzGAEgAygLMgUuUm9sZVIFcm9sZXM=');
@$core.Deprecated('Use deleteRoleRequestDescriptor instead')
const DeleteRoleRequest$json = const {
  '1': 'DeleteRoleRequest',
  '2': const [
    const {'1': 'role_id', '3': 1, '4': 1, '5': 9, '10': 'roleId'},
  ],
};

/// Descriptor for `DeleteRoleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteRoleRequestDescriptor = $convert.base64Decode('ChFEZWxldGVSb2xlUmVxdWVzdBIXCgdyb2xlX2lkGAEgASgJUgZyb2xlSWQ=');
@$core.Deprecated('Use deleteRoleResponseDescriptor instead')
const DeleteRoleResponse$json = const {
  '1': 'DeleteRoleResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `DeleteRoleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteRoleResponseDescriptor = $convert.base64Decode('ChJEZWxldGVSb2xlUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use updateRoleRequestDescriptor instead')
const UpdateRoleRequest$json = const {
  '1': 'UpdateRoleRequest',
  '2': const [
    const {'1': 'role_id', '3': 1, '4': 1, '5': 9, '10': 'roleId'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'identifier', '3': 3, '4': 1, '5': 5, '10': 'identifier'},
    const {'1': 'userID', '3': 4, '4': 1, '5': 9, '10': 'userID'},
    const {'1': 'name', '3': 5, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `UpdateRoleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateRoleRequestDescriptor = $convert.base64Decode('ChFVcGRhdGVSb2xlUmVxdWVzdBIXCgdyb2xlX2lkGAEgASgJUgZyb2xlSWQSIAoLZGVzY3JpcHRpb24YAiABKAlSC2Rlc2NyaXB0aW9uEh4KCmlkZW50aWZpZXIYAyABKAVSCmlkZW50aWZpZXISFgoGdXNlcklEGAQgASgJUgZ1c2VySUQSEgoEbmFtZRgFIAEoCVIEbmFtZQ==');
@$core.Deprecated('Use updateRoleResponseDescriptor instead')
const UpdateRoleResponse$json = const {
  '1': 'UpdateRoleResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `UpdateRoleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateRoleResponseDescriptor = $convert.base64Decode('ChJVcGRhdGVSb2xlUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use assignPermissionsToRoleRequestDescriptor instead')
const AssignPermissionsToRoleRequest$json = const {
  '1': 'AssignPermissionsToRoleRequest',
  '2': const [
    const {'1': 'role_id', '3': 1, '4': 1, '5': 9, '10': 'roleId'},
    const {'1': 'permission_ids', '3': 2, '4': 3, '5': 9, '10': 'permissionIds'},
  ],
};

/// Descriptor for `AssignPermissionsToRoleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List assignPermissionsToRoleRequestDescriptor = $convert.base64Decode('Ch5Bc3NpZ25QZXJtaXNzaW9uc1RvUm9sZVJlcXVlc3QSFwoHcm9sZV9pZBgBIAEoCVIGcm9sZUlkEiUKDnBlcm1pc3Npb25faWRzGAIgAygJUg1wZXJtaXNzaW9uSWRz');
@$core.Deprecated('Use assignPermissionsToRoleResponseDescriptor instead')
const AssignPermissionsToRoleResponse$json = const {
  '1': 'AssignPermissionsToRoleResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `AssignPermissionsToRoleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List assignPermissionsToRoleResponseDescriptor = $convert.base64Decode('Ch9Bc3NpZ25QZXJtaXNzaW9uc1RvUm9sZVJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use removePermissionsFromRoleRequestDescriptor instead')
const RemovePermissionsFromRoleRequest$json = const {
  '1': 'RemovePermissionsFromRoleRequest',
  '2': const [
    const {'1': 'role_id', '3': 1, '4': 1, '5': 9, '10': 'roleId'},
    const {'1': 'permission_ids', '3': 2, '4': 3, '5': 9, '10': 'permissionIds'},
  ],
};

/// Descriptor for `RemovePermissionsFromRoleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List removePermissionsFromRoleRequestDescriptor = $convert.base64Decode('CiBSZW1vdmVQZXJtaXNzaW9uc0Zyb21Sb2xlUmVxdWVzdBIXCgdyb2xlX2lkGAEgASgJUgZyb2xlSWQSJQoOcGVybWlzc2lvbl9pZHMYAiADKAlSDXBlcm1pc3Npb25JZHM=');
@$core.Deprecated('Use removePermissionsFromRoleResponseDescriptor instead')
const RemovePermissionsFromRoleResponse$json = const {
  '1': 'RemovePermissionsFromRoleResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `RemovePermissionsFromRoleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List removePermissionsFromRoleResponseDescriptor = $convert.base64Decode('CiFSZW1vdmVQZXJtaXNzaW9uc0Zyb21Sb2xlUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use permissionDescriptor instead')
const Permission$json = const {
  '1': 'Permission',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
  ],
};

/// Descriptor for `Permission`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List permissionDescriptor = $convert.base64Decode('CgpQZXJtaXNzaW9uEhIKBG5hbWUYASABKAlSBG5hbWUSIAoLZGVzY3JpcHRpb24YAiABKAlSC2Rlc2NyaXB0aW9u');
@$core.Deprecated('Use createPermissionRequestDescriptor instead')
const CreatePermissionRequest$json = const {
  '1': 'CreatePermissionRequest',
  '2': const [
    const {'1': 'permission', '3': 1, '4': 1, '5': 9, '10': 'permission'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
  ],
};

/// Descriptor for `CreatePermissionRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createPermissionRequestDescriptor = $convert.base64Decode('ChdDcmVhdGVQZXJtaXNzaW9uUmVxdWVzdBIeCgpwZXJtaXNzaW9uGAEgASgJUgpwZXJtaXNzaW9uEiAKC2Rlc2NyaXB0aW9uGAIgASgJUgtkZXNjcmlwdGlvbg==');
@$core.Deprecated('Use createPermissionResponseDescriptor instead')
const CreatePermissionResponse$json = const {
  '1': 'CreatePermissionResponse',
  '2': const [
    const {'1': 'permission_id', '3': 1, '4': 1, '5': 9, '10': 'permissionId'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `CreatePermissionResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createPermissionResponseDescriptor = $convert.base64Decode('ChhDcmVhdGVQZXJtaXNzaW9uUmVzcG9uc2USIwoNcGVybWlzc2lvbl9pZBgBIAEoCVIMcGVybWlzc2lvbklkEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use getAllPermissionsRequestDescriptor instead')
const GetAllPermissionsRequest$json = const {
  '1': 'GetAllPermissionsRequest',
};

/// Descriptor for `GetAllPermissionsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllPermissionsRequestDescriptor = $convert.base64Decode('ChhHZXRBbGxQZXJtaXNzaW9uc1JlcXVlc3Q=');
@$core.Deprecated('Use getAllPermissionsResponseDescriptor instead')
const GetAllPermissionsResponse$json = const {
  '1': 'GetAllPermissionsResponse',
  '2': const [
    const {'1': 'permissions', '3': 1, '4': 3, '5': 11, '6': '.Permission', '10': 'permissions'},
  ],
};

/// Descriptor for `GetAllPermissionsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllPermissionsResponseDescriptor = $convert.base64Decode('ChlHZXRBbGxQZXJtaXNzaW9uc1Jlc3BvbnNlEi0KC3Blcm1pc3Npb25zGAEgAygLMgsuUGVybWlzc2lvblILcGVybWlzc2lvbnM=');
@$core.Deprecated('Use getPermissionByTitleRequestDescriptor instead')
const GetPermissionByTitleRequest$json = const {
  '1': 'GetPermissionByTitleRequest',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
  ],
};

/// Descriptor for `GetPermissionByTitleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getPermissionByTitleRequestDescriptor = $convert.base64Decode('ChtHZXRQZXJtaXNzaW9uQnlUaXRsZVJlcXVlc3QSFAoFdGl0bGUYASABKAlSBXRpdGxl');
@$core.Deprecated('Use getPermissionByTitleResponseDescriptor instead')
const GetPermissionByTitleResponse$json = const {
  '1': 'GetPermissionByTitleResponse',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
  ],
};

/// Descriptor for `GetPermissionByTitleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getPermissionByTitleResponseDescriptor = $convert.base64Decode('ChxHZXRQZXJtaXNzaW9uQnlUaXRsZVJlc3BvbnNlEg4KAmlkGAEgASgJUgJpZBIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSIAoLZGVzY3JpcHRpb24YAyABKAlSC2Rlc2NyaXB0aW9u');
@$core.Deprecated('Use getPermissionsByRoleRequestDescriptor instead')
const GetPermissionsByRoleRequest$json = const {
  '1': 'GetPermissionsByRoleRequest',
  '2': const [
    const {'1': 'role_id', '3': 1, '4': 1, '5': 9, '10': 'roleId'},
  ],
};

/// Descriptor for `GetPermissionsByRoleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getPermissionsByRoleRequestDescriptor = $convert.base64Decode('ChtHZXRQZXJtaXNzaW9uc0J5Um9sZVJlcXVlc3QSFwoHcm9sZV9pZBgBIAEoCVIGcm9sZUlk');
@$core.Deprecated('Use getPermissionsByRoleResponseDescriptor instead')
const GetPermissionsByRoleResponse$json = const {
  '1': 'GetPermissionsByRoleResponse',
  '2': const [
    const {'1': 'permissions', '3': 1, '4': 3, '5': 11, '6': '.Permission', '10': 'permissions'},
  ],
};

/// Descriptor for `GetPermissionsByRoleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getPermissionsByRoleResponseDescriptor = $convert.base64Decode('ChxHZXRQZXJtaXNzaW9uc0J5Um9sZVJlc3BvbnNlEi0KC3Blcm1pc3Npb25zGAEgAygLMgsuUGVybWlzc2lvblILcGVybWlzc2lvbnM=');
@$core.Deprecated('Use hasPermissionRequestDescriptor instead')
const HasPermissionRequest$json = const {
  '1': 'HasPermissionRequest',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'permission', '3': 2, '4': 1, '5': 9, '10': 'permission'},
  ],
};

/// Descriptor for `HasPermissionRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List hasPermissionRequestDescriptor = $convert.base64Decode('ChRIYXNQZXJtaXNzaW9uUmVxdWVzdBIUCgV0b2tlbhgBIAEoCVIFdG9rZW4SHgoKcGVybWlzc2lvbhgCIAEoCVIKcGVybWlzc2lvbg==');
@$core.Deprecated('Use hasPermissionResponseDescriptor instead')
const HasPermissionResponse$json = const {
  '1': 'HasPermissionResponse',
  '2': const [
    const {'1': 'granted', '3': 1, '4': 1, '5': 8, '10': 'granted'},
    const {'1': 'user', '3': 2, '4': 1, '5': 11, '6': '.User', '10': 'user'},
  ],
};

/// Descriptor for `HasPermissionResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List hasPermissionResponseDescriptor = $convert.base64Decode('ChVIYXNQZXJtaXNzaW9uUmVzcG9uc2USGAoHZ3JhbnRlZBgBIAEoCFIHZ3JhbnRlZBIZCgR1c2VyGAIgASgLMgUuVXNlclIEdXNlcg==');
@$core.Deprecated('Use agentLoginRequestDescriptor instead')
const AgentLoginRequest$json = const {
  '1': 'AgentLoginRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `AgentLoginRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agentLoginRequestDescriptor = $convert.base64Decode('ChFBZ2VudExvZ2luUmVxdWVzdBIUCgVlbWFpbBgBIAEoCVIFZW1haWwSGgoIcGFzc3dvcmQYAiABKAlSCHBhc3N3b3Jk');
@$core.Deprecated('Use agentLoginResponseDescriptor instead')
const AgentLoginResponse$json = const {
  '1': 'AgentLoginResponse',
  '2': const [
    const {'1': 'access_token', '3': 1, '4': 1, '5': 9, '10': 'accessToken'},
    const {'1': 'userInfo', '3': 2, '4': 1, '5': 11, '6': '.User', '10': 'userInfo'},
  ],
};

/// Descriptor for `AgentLoginResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agentLoginResponseDescriptor = $convert.base64Decode('ChJBZ2VudExvZ2luUmVzcG9uc2USIQoMYWNjZXNzX3Rva2VuGAEgASgJUgthY2Nlc3NUb2tlbhIhCgh1c2VySW5mbxgCIAEoCzIFLlVzZXJSCHVzZXJJbmZv');
@$core.Deprecated('Use userToRoleRequestDescriptor instead')
const UserToRoleRequest$json = const {
  '1': 'UserToRoleRequest',
  '2': const [
    const {'1': 'userID', '3': 1, '4': 1, '5': 9, '10': 'userID'},
    const {'1': 'roleID', '3': 2, '4': 1, '5': 9, '10': 'roleID'},
  ],
};

/// Descriptor for `UserToRoleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userToRoleRequestDescriptor = $convert.base64Decode('ChFVc2VyVG9Sb2xlUmVxdWVzdBIWCgZ1c2VySUQYASABKAlSBnVzZXJJRBIWCgZyb2xlSUQYAiABKAlSBnJvbGVJRA==');
@$core.Deprecated('Use userToRoleResponseDescriptor instead')
const UserToRoleResponse$json = const {
  '1': 'UserToRoleResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `UserToRoleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userToRoleResponseDescriptor = $convert.base64Decode('ChJVc2VyVG9Sb2xlUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZRIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use emptyUserListRequestDescriptor instead')
const EmptyUserListRequest$json = const {
  '1': 'EmptyUserListRequest',
};

/// Descriptor for `EmptyUserListRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyUserListRequestDescriptor = $convert.base64Decode('ChRFbXB0eVVzZXJMaXN0UmVxdWVzdA==');
@$core.Deprecated('Use referralListRequestDescriptor instead')
const ReferralListRequest$json = const {
  '1': 'ReferralListRequest',
  '2': const [
    const {'1': 'referralCode', '3': 1, '4': 1, '5': 9, '10': 'referralCode'},
  ],
};

/// Descriptor for `ReferralListRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List referralListRequestDescriptor = $convert.base64Decode('ChNSZWZlcnJhbExpc3RSZXF1ZXN0EiIKDHJlZmVycmFsQ29kZRgBIAEoCVIMcmVmZXJyYWxDb2Rl');
@$core.Deprecated('Use adminUserDescriptor instead')
const AdminUser$json = const {
  '1': 'AdminUser',
  '2': const [
    const {'1': 'first_name', '3': 1, '4': 1, '5': 9, '10': 'firstName'},
    const {'1': 'last_name', '3': 2, '4': 1, '5': 9, '10': 'lastName'},
    const {'1': 'email', '3': 3, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phone', '3': 4, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'phoneCode', '3': 5, '4': 1, '5': 9, '10': 'phoneCode'},
    const {'1': 'password', '3': 6, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'userID', '3': 7, '4': 1, '5': 9, '10': 'userID'},
  ],
};

/// Descriptor for `AdminUser`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List adminUserDescriptor = $convert.base64Decode('CglBZG1pblVzZXISHQoKZmlyc3RfbmFtZRgBIAEoCVIJZmlyc3ROYW1lEhsKCWxhc3RfbmFtZRgCIAEoCVIIbGFzdE5hbWUSFAoFZW1haWwYAyABKAlSBWVtYWlsEhQKBXBob25lGAQgASgJUgVwaG9uZRIcCglwaG9uZUNvZGUYBSABKAlSCXBob25lQ29kZRIaCghwYXNzd29yZBgGIAEoCVIIcGFzc3dvcmQSFgoGdXNlcklEGAcgASgJUgZ1c2VySUQ=');
@$core.Deprecated('Use userListResponseDescriptor instead')
const UserListResponse$json = const {
  '1': 'UserListResponse',
  '2': const [
    const {'1': 'users', '3': 1, '4': 3, '5': 11, '6': '.User', '10': 'users'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `UserListResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userListResponseDescriptor = $convert.base64Decode('ChBVc2VyTGlzdFJlc3BvbnNlEhsKBXVzZXJzGAEgAygLMgUuVXNlclIFdXNlcnMSFgoGc3RhdHVzGAIgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use profilePicsInfoDescriptor instead')
const ProfilePicsInfo$json = const {
  '1': 'ProfilePicsInfo',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 9, '10': 'userId'},
    const {'1': 'image_type', '3': 2, '4': 1, '5': 9, '10': 'imageType'},
  ],
};

/// Descriptor for `ProfilePicsInfo`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List profilePicsInfoDescriptor = $convert.base64Decode('Cg9Qcm9maWxlUGljc0luZm8SFwoHdXNlcl9pZBgBIAEoCVIGdXNlcklkEh0KCmltYWdlX3R5cGUYAiABKAlSCWltYWdlVHlwZQ==');
@$core.Deprecated('Use uploadDPRequestDescriptor instead')
const UploadDPRequest$json = const {
  '1': 'UploadDPRequest',
  '2': const [
    const {'1': 'info', '3': 1, '4': 1, '5': 11, '6': '.ProfilePicsInfo', '9': 0, '10': 'info'},
    const {'1': 'chunk_data', '3': 2, '4': 1, '5': 12, '9': 0, '10': 'chunkData'},
  ],
  '8': const [
    const {'1': 'data'},
  ],
};

/// Descriptor for `UploadDPRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List uploadDPRequestDescriptor = $convert.base64Decode('Cg9VcGxvYWREUFJlcXVlc3QSJgoEaW5mbxgBIAEoCzIQLlByb2ZpbGVQaWNzSW5mb0gAUgRpbmZvEh8KCmNodW5rX2RhdGEYAiABKAxIAFIJY2h1bmtEYXRhQgYKBGRhdGE=');
@$core.Deprecated('Use uploadImageResponseDescriptor instead')
const UploadImageResponse$json = const {
  '1': 'UploadImageResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'imageUrl', '3': 2, '4': 1, '5': 9, '10': 'imageUrl'},
  ],
};

/// Descriptor for `UploadImageResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List uploadImageResponseDescriptor = $convert.base64Decode('ChNVcGxvYWRJbWFnZVJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USGgoIaW1hZ2VVcmwYAiABKAlSCGltYWdlVXJs');
