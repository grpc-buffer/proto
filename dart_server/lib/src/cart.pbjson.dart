///
//  Generated code. Do not modify.
//  source: cart.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use emptyDescriptor instead')
const Empty$json = const {
  '1': 'Empty',
};

/// Descriptor for `Empty`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyDescriptor = $convert.base64Decode('CgVFbXB0eQ==');
@$core.Deprecated('Use cartRequestItemDescriptor instead')
const CartRequestItem$json = const {
  '1': 'CartRequestItem',
  '2': const [
    const {'1': 'requestId', '3': 1, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'quantity', '3': 2, '4': 1, '5': 5, '10': 'quantity'},
    const {'1': 'item', '3': 3, '4': 1, '5': 9, '10': 'item'},
    const {'1': 'packageID', '3': 4, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'userID', '3': 5, '4': 1, '5': 9, '10': 'userID'},
  ],
};

/// Descriptor for `CartRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cartRequestItemDescriptor = $convert.base64Decode('Cg9DYXJ0UmVxdWVzdEl0ZW0SHAoJcmVxdWVzdElkGAEgASgJUglyZXF1ZXN0SWQSGgoIcXVhbnRpdHkYAiABKAVSCHF1YW50aXR5EhIKBGl0ZW0YAyABKAlSBGl0ZW0SHAoJcGFja2FnZUlEGAQgASgJUglwYWNrYWdlSUQSFgoGdXNlcklEGAUgASgJUgZ1c2VySUQ=');
@$core.Deprecated('Use cartResponseDescriptor instead')
const CartResponse$json = const {
  '1': 'CartResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 11, '6': '.cart_service.v1.CartItems', '10': 'data'},
  ],
};

/// Descriptor for `CartResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cartResponseDescriptor = $convert.base64Decode('CgxDYXJ0UmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIuCgRkYXRhGAMgASgLMhouY2FydF9zZXJ2aWNlLnYxLkNhcnRJdGVtc1IEZGF0YQ==');
@$core.Deprecated('Use cartItemsDescriptor instead')
const CartItems$json = const {
  '1': 'CartItems',
  '2': const [
    const {'1': 'packageID', '3': 1, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'packageName', '3': 2, '4': 1, '5': 9, '10': 'packageName'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'basePrice', '3': 4, '4': 1, '5': 1, '10': 'basePrice'},
    const {'1': 'serviceAreaID', '3': 6, '4': 1, '5': 9, '10': 'serviceAreaID'},
    const {'1': 'image', '3': 7, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'userID', '3': 8, '4': 1, '5': 9, '10': 'userID'},
    const {'1': 'quantity', '3': 9, '4': 1, '5': 5, '10': 'quantity'},
    const {'1': 'items', '3': 10, '4': 3, '5': 11, '6': '.cart_service.v1.CartPackageItem', '10': 'items'},
    const {'1': 'cartID', '3': 11, '4': 1, '5': 9, '10': 'cartID'},
  ],
};

/// Descriptor for `CartItems`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cartItemsDescriptor = $convert.base64Decode('CglDYXJ0SXRlbXMSHAoJcGFja2FnZUlEGAEgASgJUglwYWNrYWdlSUQSIAoLcGFja2FnZU5hbWUYAiABKAlSC3BhY2thZ2VOYW1lEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbhIcCgliYXNlUHJpY2UYBCABKAFSCWJhc2VQcmljZRIkCg1zZXJ2aWNlQXJlYUlEGAYgASgJUg1zZXJ2aWNlQXJlYUlEEhQKBWltYWdlGAcgASgJUgVpbWFnZRIWCgZ1c2VySUQYCCABKAlSBnVzZXJJRBIaCghxdWFudGl0eRgJIAEoBVIIcXVhbnRpdHkSNgoFaXRlbXMYCiADKAsyIC5jYXJ0X3NlcnZpY2UudjEuQ2FydFBhY2thZ2VJdGVtUgVpdGVtcxIWCgZjYXJ0SUQYCyABKAlSBmNhcnRJRA==');
@$core.Deprecated('Use cartPackageItemDescriptor instead')
const CartPackageItem$json = const {
  '1': 'CartPackageItem',
  '2': const [
    const {'1': 'itemID', '3': 1, '4': 1, '5': 9, '10': 'itemID'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'itemCategoryID', '3': 4, '4': 1, '5': 9, '10': 'itemCategoryID'},
    const {'1': 'image', '3': 5, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'unit', '3': 6, '4': 1, '5': 9, '10': 'unit'},
  ],
};

/// Descriptor for `CartPackageItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cartPackageItemDescriptor = $convert.base64Decode('Cg9DYXJ0UGFja2FnZUl0ZW0SFgoGaXRlbUlEGAEgASgJUgZpdGVtSUQSEgoEbmFtZRgCIAEoCVIEbmFtZRIgCgtkZXNjcmlwdGlvbhgDIAEoCVILZGVzY3JpcHRpb24SJgoOaXRlbUNhdGVnb3J5SUQYBCABKAlSDml0ZW1DYXRlZ29yeUlEEhQKBWltYWdlGAUgASgJUgVpbWFnZRISCgR1bml0GAYgASgJUgR1bml0');
@$core.Deprecated('Use cartRequestDescriptor instead')
const CartRequest$json = const {
  '1': 'CartRequest',
  '2': const [
    const {'1': 'packageID', '3': 1, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'packageName', '3': 2, '4': 1, '5': 9, '10': 'packageName'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'basePrice', '3': 4, '4': 1, '5': 1, '10': 'basePrice'},
    const {'1': 'serviceAreaID', '3': 6, '4': 1, '5': 9, '10': 'serviceAreaID'},
    const {'1': 'image', '3': 7, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'userID', '3': 8, '4': 1, '5': 9, '10': 'userID'},
    const {'1': 'quantity', '3': 9, '4': 1, '5': 5, '10': 'quantity'},
    const {'1': 'items', '3': 10, '4': 3, '5': 11, '6': '.cart_service.v1.CartPackageItem', '10': 'items'},
  ],
};

/// Descriptor for `CartRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cartRequestDescriptor = $convert.base64Decode('CgtDYXJ0UmVxdWVzdBIcCglwYWNrYWdlSUQYASABKAlSCXBhY2thZ2VJRBIgCgtwYWNrYWdlTmFtZRgCIAEoCVILcGFja2FnZU5hbWUSIAoLZGVzY3JpcHRpb24YAyABKAlSC2Rlc2NyaXB0aW9uEhwKCWJhc2VQcmljZRgEIAEoAVIJYmFzZVByaWNlEiQKDXNlcnZpY2VBcmVhSUQYBiABKAlSDXNlcnZpY2VBcmVhSUQSFAoFaW1hZ2UYByABKAlSBWltYWdlEhYKBnVzZXJJRBgIIAEoCVIGdXNlcklEEhoKCHF1YW50aXR5GAkgASgFUghxdWFudGl0eRI2CgVpdGVtcxgKIAMoCzIgLmNhcnRfc2VydmljZS52MS5DYXJ0UGFja2FnZUl0ZW1SBWl0ZW1z');
@$core.Deprecated('Use listCartResponseDescriptor instead')
const ListCartResponse$json = const {
  '1': 'ListCartResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.cart_service.v1.CartItems', '10': 'data'},
    const {'1': 'count', '3': 4, '4': 1, '5': 5, '10': 'count'},
  ],
};

/// Descriptor for `ListCartResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listCartResponseDescriptor = $convert.base64Decode('ChBMaXN0Q2FydFJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USLgoEZGF0YRgDIAMoCzIaLmNhcnRfc2VydmljZS52MS5DYXJ0SXRlbXNSBGRhdGESFAoFY291bnQYBCABKAVSBWNvdW50');
