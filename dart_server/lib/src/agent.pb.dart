///
//  Generated code. Do not modify.
//  source: agent.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'agent.pbenum.dart';

export 'agent.pbenum.dart';

class Agent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Agent', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phoneNumber')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'firstName')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastName')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state')
    ..aOM<Wallet>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'wallet', subBuilder: Wallet.create)
    ..e<AvailabilityStatus>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: AvailabilityStatus.AVAILABLE, valueOf: AvailabilityStatus.valueOf, enumValues: AvailabilityStatus.values)
    ..hasRequiredFields = false
  ;

  Agent._() : super();
  factory Agent({
    $core.String? id,
    $core.String? email,
    $core.String? phoneNumber,
    $core.String? firstName,
    $core.String? lastName,
    $core.String? address,
    $core.String? state,
    Wallet? wallet,
    AvailabilityStatus? status,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (email != null) {
      _result.email = email;
    }
    if (phoneNumber != null) {
      _result.phoneNumber = phoneNumber;
    }
    if (firstName != null) {
      _result.firstName = firstName;
    }
    if (lastName != null) {
      _result.lastName = lastName;
    }
    if (address != null) {
      _result.address = address;
    }
    if (state != null) {
      _result.state = state;
    }
    if (wallet != null) {
      _result.wallet = wallet;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory Agent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Agent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Agent clone() => Agent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Agent copyWith(void Function(Agent) updates) => super.copyWith((message) => updates(message as Agent)) as Agent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Agent create() => Agent._();
  Agent createEmptyInstance() => create();
  static $pb.PbList<Agent> createRepeated() => $pb.PbList<Agent>();
  @$core.pragma('dart2js:noInline')
  static Agent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Agent>(create);
  static Agent? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get email => $_getSZ(1);
  @$pb.TagNumber(2)
  set email($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmail() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmail() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get phoneNumber => $_getSZ(2);
  @$pb.TagNumber(3)
  set phoneNumber($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPhoneNumber() => $_has(2);
  @$pb.TagNumber(3)
  void clearPhoneNumber() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get firstName => $_getSZ(3);
  @$pb.TagNumber(4)
  set firstName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasFirstName() => $_has(3);
  @$pb.TagNumber(4)
  void clearFirstName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get lastName => $_getSZ(4);
  @$pb.TagNumber(5)
  set lastName($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLastName() => $_has(4);
  @$pb.TagNumber(5)
  void clearLastName() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get address => $_getSZ(5);
  @$pb.TagNumber(6)
  set address($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasAddress() => $_has(5);
  @$pb.TagNumber(6)
  void clearAddress() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get state => $_getSZ(6);
  @$pb.TagNumber(7)
  set state($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasState() => $_has(6);
  @$pb.TagNumber(7)
  void clearState() => clearField(7);

  @$pb.TagNumber(8)
  Wallet get wallet => $_getN(7);
  @$pb.TagNumber(8)
  set wallet(Wallet v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasWallet() => $_has(7);
  @$pb.TagNumber(8)
  void clearWallet() => clearField(8);
  @$pb.TagNumber(8)
  Wallet ensureWallet() => $_ensure(7);

  @$pb.TagNumber(9)
  AvailabilityStatus get status => $_getN(8);
  @$pb.TagNumber(9)
  set status(AvailabilityStatus v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasStatus() => $_has(8);
  @$pb.TagNumber(9)
  void clearStatus() => clearField(9);
}

class CreateAgentRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CreateAgentRequest', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phoneNumber')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'firstName')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastName')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'country')
    ..hasRequiredFields = false
  ;

  CreateAgentRequest._() : super();
  factory CreateAgentRequest({
    $core.String? id,
    $core.String? email,
    $core.String? phoneNumber,
    $core.String? firstName,
    $core.String? lastName,
    $core.String? address,
    $core.String? state,
    $core.String? country,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (email != null) {
      _result.email = email;
    }
    if (phoneNumber != null) {
      _result.phoneNumber = phoneNumber;
    }
    if (firstName != null) {
      _result.firstName = firstName;
    }
    if (lastName != null) {
      _result.lastName = lastName;
    }
    if (address != null) {
      _result.address = address;
    }
    if (state != null) {
      _result.state = state;
    }
    if (country != null) {
      _result.country = country;
    }
    return _result;
  }
  factory CreateAgentRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateAgentRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CreateAgentRequest clone() => CreateAgentRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CreateAgentRequest copyWith(void Function(CreateAgentRequest) updates) => super.copyWith((message) => updates(message as CreateAgentRequest)) as CreateAgentRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CreateAgentRequest create() => CreateAgentRequest._();
  CreateAgentRequest createEmptyInstance() => create();
  static $pb.PbList<CreateAgentRequest> createRepeated() => $pb.PbList<CreateAgentRequest>();
  @$core.pragma('dart2js:noInline')
  static CreateAgentRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateAgentRequest>(create);
  static CreateAgentRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get email => $_getSZ(1);
  @$pb.TagNumber(2)
  set email($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmail() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmail() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get phoneNumber => $_getSZ(2);
  @$pb.TagNumber(3)
  set phoneNumber($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPhoneNumber() => $_has(2);
  @$pb.TagNumber(3)
  void clearPhoneNumber() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get firstName => $_getSZ(3);
  @$pb.TagNumber(4)
  set firstName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasFirstName() => $_has(3);
  @$pb.TagNumber(4)
  void clearFirstName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get lastName => $_getSZ(4);
  @$pb.TagNumber(5)
  set lastName($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLastName() => $_has(4);
  @$pb.TagNumber(5)
  void clearLastName() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get address => $_getSZ(5);
  @$pb.TagNumber(6)
  set address($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasAddress() => $_has(5);
  @$pb.TagNumber(6)
  void clearAddress() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get state => $_getSZ(6);
  @$pb.TagNumber(7)
  set state($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasState() => $_has(6);
  @$pb.TagNumber(7)
  void clearState() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get country => $_getSZ(7);
  @$pb.TagNumber(8)
  set country($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasCountry() => $_has(7);
  @$pb.TagNumber(8)
  void clearCountry() => clearField(8);
}

class CreateAgentResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CreateAgentResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  CreateAgentResponse._() : super();
  factory CreateAgentResponse({
    $core.String? message,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory CreateAgentResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateAgentResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CreateAgentResponse clone() => CreateAgentResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CreateAgentResponse copyWith(void Function(CreateAgentResponse) updates) => super.copyWith((message) => updates(message as CreateAgentResponse)) as CreateAgentResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CreateAgentResponse create() => CreateAgentResponse._();
  CreateAgentResponse createEmptyInstance() => create();
  static $pb.PbList<CreateAgentResponse> createRepeated() => $pb.PbList<CreateAgentResponse>();
  @$core.pragma('dart2js:noInline')
  static CreateAgentResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateAgentResponse>(create);
  static CreateAgentResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

class AgentFilter extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AgentFilter', createEmptyInstance: create)
    ..e<AvailabilityStatus>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: AvailabilityStatus.AVAILABLE, valueOf: AvailabilityStatus.valueOf, enumValues: AvailabilityStatus.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..hasRequiredFields = false
  ;

  AgentFilter._() : super();
  factory AgentFilter({
    AvailabilityStatus? status,
    $core.String? state,
    $core.String? address,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (state != null) {
      _result.state = state;
    }
    if (address != null) {
      _result.address = address;
    }
    return _result;
  }
  factory AgentFilter.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AgentFilter.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AgentFilter clone() => AgentFilter()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AgentFilter copyWith(void Function(AgentFilter) updates) => super.copyWith((message) => updates(message as AgentFilter)) as AgentFilter; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AgentFilter create() => AgentFilter._();
  AgentFilter createEmptyInstance() => create();
  static $pb.PbList<AgentFilter> createRepeated() => $pb.PbList<AgentFilter>();
  @$core.pragma('dart2js:noInline')
  static AgentFilter getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AgentFilter>(create);
  static AgentFilter? _defaultInstance;

  @$pb.TagNumber(1)
  AvailabilityStatus get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(AvailabilityStatus v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get state => $_getSZ(1);
  @$pb.TagNumber(2)
  set state($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasState() => $_has(1);
  @$pb.TagNumber(2)
  void clearState() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get address => $_getSZ(2);
  @$pb.TagNumber(3)
  set address($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAddress() => $_has(2);
  @$pb.TagNumber(3)
  void clearAddress() => clearField(3);
}

class GetAllAgentsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAllAgentsRequest', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.OU3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OU3)
    ..aOM<AgentFilter>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'filterBy', protoName: 'filterBy', subBuilder: AgentFilter.create)
    ..hasRequiredFields = false
  ;

  GetAllAgentsRequest._() : super();
  factory GetAllAgentsRequest({
    $core.int? page,
    $core.int? size,
    AgentFilter? filterBy,
  }) {
    final _result = create();
    if (page != null) {
      _result.page = page;
    }
    if (size != null) {
      _result.size = size;
    }
    if (filterBy != null) {
      _result.filterBy = filterBy;
    }
    return _result;
  }
  factory GetAllAgentsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllAgentsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAllAgentsRequest clone() => GetAllAgentsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAllAgentsRequest copyWith(void Function(GetAllAgentsRequest) updates) => super.copyWith((message) => updates(message as GetAllAgentsRequest)) as GetAllAgentsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllAgentsRequest create() => GetAllAgentsRequest._();
  GetAllAgentsRequest createEmptyInstance() => create();
  static $pb.PbList<GetAllAgentsRequest> createRepeated() => $pb.PbList<GetAllAgentsRequest>();
  @$core.pragma('dart2js:noInline')
  static GetAllAgentsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllAgentsRequest>(create);
  static GetAllAgentsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get page => $_getIZ(0);
  @$pb.TagNumber(1)
  set page($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPage() => $_has(0);
  @$pb.TagNumber(1)
  void clearPage() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get size => $_getIZ(1);
  @$pb.TagNumber(2)
  set size($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSize() => $_has(1);
  @$pb.TagNumber(2)
  void clearSize() => clearField(2);

  @$pb.TagNumber(3)
  AgentFilter get filterBy => $_getN(2);
  @$pb.TagNumber(3)
  set filterBy(AgentFilter v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasFilterBy() => $_has(2);
  @$pb.TagNumber(3)
  void clearFilterBy() => clearField(3);
  @$pb.TagNumber(3)
  AgentFilter ensureFilterBy() => $_ensure(2);
}

class GetAllAgentsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAllAgentsResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..pc<Agent>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agents', $pb.PbFieldType.PM, subBuilder: Agent.create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.OU3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OU3)
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total')
    ..hasRequiredFields = false
  ;

  GetAllAgentsResponse._() : super();
  factory GetAllAgentsResponse({
    $core.String? message,
    $core.Iterable<Agent>? agents,
    $core.int? page,
    $core.int? size,
    $fixnum.Int64? total,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (agents != null) {
      _result.agents.addAll(agents);
    }
    if (page != null) {
      _result.page = page;
    }
    if (size != null) {
      _result.size = size;
    }
    if (total != null) {
      _result.total = total;
    }
    return _result;
  }
  factory GetAllAgentsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllAgentsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAllAgentsResponse clone() => GetAllAgentsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAllAgentsResponse copyWith(void Function(GetAllAgentsResponse) updates) => super.copyWith((message) => updates(message as GetAllAgentsResponse)) as GetAllAgentsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllAgentsResponse create() => GetAllAgentsResponse._();
  GetAllAgentsResponse createEmptyInstance() => create();
  static $pb.PbList<GetAllAgentsResponse> createRepeated() => $pb.PbList<GetAllAgentsResponse>();
  @$core.pragma('dart2js:noInline')
  static GetAllAgentsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllAgentsResponse>(create);
  static GetAllAgentsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Agent> get agents => $_getList(1);

  @$pb.TagNumber(3)
  $core.int get page => $_getIZ(2);
  @$pb.TagNumber(3)
  set page($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPage() => $_has(2);
  @$pb.TagNumber(3)
  void clearPage() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get size => $_getIZ(3);
  @$pb.TagNumber(4)
  set size($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSize() => $_has(3);
  @$pb.TagNumber(4)
  void clearSize() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get total => $_getI64(4);
  @$pb.TagNumber(5)
  set total($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTotal() => $_has(4);
  @$pb.TagNumber(5)
  void clearTotal() => clearField(5);
}

class GetAgentByIdRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAgentByIdRequest', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..hasRequiredFields = false
  ;

  GetAgentByIdRequest._() : super();
  factory GetAgentByIdRequest({
    $core.String? id,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory GetAgentByIdRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAgentByIdRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAgentByIdRequest clone() => GetAgentByIdRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAgentByIdRequest copyWith(void Function(GetAgentByIdRequest) updates) => super.copyWith((message) => updates(message as GetAgentByIdRequest)) as GetAgentByIdRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAgentByIdRequest create() => GetAgentByIdRequest._();
  GetAgentByIdRequest createEmptyInstance() => create();
  static $pb.PbList<GetAgentByIdRequest> createRepeated() => $pb.PbList<GetAgentByIdRequest>();
  @$core.pragma('dart2js:noInline')
  static GetAgentByIdRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAgentByIdRequest>(create);
  static GetAgentByIdRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
}

class GetAgentByIdResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAgentByIdResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOM<Agent>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agent', subBuilder: Agent.create)
    ..hasRequiredFields = false
  ;

  GetAgentByIdResponse._() : super();
  factory GetAgentByIdResponse({
    $core.String? message,
    Agent? agent,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (agent != null) {
      _result.agent = agent;
    }
    return _result;
  }
  factory GetAgentByIdResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAgentByIdResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAgentByIdResponse clone() => GetAgentByIdResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAgentByIdResponse copyWith(void Function(GetAgentByIdResponse) updates) => super.copyWith((message) => updates(message as GetAgentByIdResponse)) as GetAgentByIdResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAgentByIdResponse create() => GetAgentByIdResponse._();
  GetAgentByIdResponse createEmptyInstance() => create();
  static $pb.PbList<GetAgentByIdResponse> createRepeated() => $pb.PbList<GetAgentByIdResponse>();
  @$core.pragma('dart2js:noInline')
  static GetAgentByIdResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAgentByIdResponse>(create);
  static GetAgentByIdResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  Agent get agent => $_getN(1);
  @$pb.TagNumber(2)
  set agent(Agent v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAgent() => $_has(1);
  @$pb.TagNumber(2)
  void clearAgent() => clearField(2);
  @$pb.TagNumber(2)
  Agent ensureAgent() => $_ensure(1);
}

class GetAgentInfoRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAgentInfoRequest', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  GetAgentInfoRequest._() : super();
  factory GetAgentInfoRequest() => create();
  factory GetAgentInfoRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAgentInfoRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAgentInfoRequest clone() => GetAgentInfoRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAgentInfoRequest copyWith(void Function(GetAgentInfoRequest) updates) => super.copyWith((message) => updates(message as GetAgentInfoRequest)) as GetAgentInfoRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAgentInfoRequest create() => GetAgentInfoRequest._();
  GetAgentInfoRequest createEmptyInstance() => create();
  static $pb.PbList<GetAgentInfoRequest> createRepeated() => $pb.PbList<GetAgentInfoRequest>();
  @$core.pragma('dart2js:noInline')
  static GetAgentInfoRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAgentInfoRequest>(create);
  static GetAgentInfoRequest? _defaultInstance;
}

class GetAgentInfoResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAgentInfoResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOM<Agent>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agent', subBuilder: Agent.create)
    ..hasRequiredFields = false
  ;

  GetAgentInfoResponse._() : super();
  factory GetAgentInfoResponse({
    $core.String? message,
    Agent? agent,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (agent != null) {
      _result.agent = agent;
    }
    return _result;
  }
  factory GetAgentInfoResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAgentInfoResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAgentInfoResponse clone() => GetAgentInfoResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAgentInfoResponse copyWith(void Function(GetAgentInfoResponse) updates) => super.copyWith((message) => updates(message as GetAgentInfoResponse)) as GetAgentInfoResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAgentInfoResponse create() => GetAgentInfoResponse._();
  GetAgentInfoResponse createEmptyInstance() => create();
  static $pb.PbList<GetAgentInfoResponse> createRepeated() => $pb.PbList<GetAgentInfoResponse>();
  @$core.pragma('dart2js:noInline')
  static GetAgentInfoResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAgentInfoResponse>(create);
  static GetAgentInfoResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  Agent get agent => $_getN(1);
  @$pb.TagNumber(2)
  set agent(Agent v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAgent() => $_has(1);
  @$pb.TagNumber(2)
  void clearAgent() => clearField(2);
  @$pb.TagNumber(2)
  Agent ensureAgent() => $_ensure(1);
}

class ChangeAvailabilityStatusRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ChangeAvailabilityStatusRequest', createEmptyInstance: create)
    ..e<AvailabilityStatus>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: AvailabilityStatus.AVAILABLE, valueOf: AvailabilityStatus.valueOf, enumValues: AvailabilityStatus.values)
    ..hasRequiredFields = false
  ;

  ChangeAvailabilityStatusRequest._() : super();
  factory ChangeAvailabilityStatusRequest({
    AvailabilityStatus? status,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ChangeAvailabilityStatusRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ChangeAvailabilityStatusRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ChangeAvailabilityStatusRequest clone() => ChangeAvailabilityStatusRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ChangeAvailabilityStatusRequest copyWith(void Function(ChangeAvailabilityStatusRequest) updates) => super.copyWith((message) => updates(message as ChangeAvailabilityStatusRequest)) as ChangeAvailabilityStatusRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ChangeAvailabilityStatusRequest create() => ChangeAvailabilityStatusRequest._();
  ChangeAvailabilityStatusRequest createEmptyInstance() => create();
  static $pb.PbList<ChangeAvailabilityStatusRequest> createRepeated() => $pb.PbList<ChangeAvailabilityStatusRequest>();
  @$core.pragma('dart2js:noInline')
  static ChangeAvailabilityStatusRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ChangeAvailabilityStatusRequest>(create);
  static ChangeAvailabilityStatusRequest? _defaultInstance;

  @$pb.TagNumber(1)
  AvailabilityStatus get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(AvailabilityStatus v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);
}

class ChangeAvailabilityStatusResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ChangeAvailabilityStatusResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  ChangeAvailabilityStatusResponse._() : super();
  factory ChangeAvailabilityStatusResponse({
    $core.String? message,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory ChangeAvailabilityStatusResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ChangeAvailabilityStatusResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ChangeAvailabilityStatusResponse clone() => ChangeAvailabilityStatusResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ChangeAvailabilityStatusResponse copyWith(void Function(ChangeAvailabilityStatusResponse) updates) => super.copyWith((message) => updates(message as ChangeAvailabilityStatusResponse)) as ChangeAvailabilityStatusResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ChangeAvailabilityStatusResponse create() => ChangeAvailabilityStatusResponse._();
  ChangeAvailabilityStatusResponse createEmptyInstance() => create();
  static $pb.PbList<ChangeAvailabilityStatusResponse> createRepeated() => $pb.PbList<ChangeAvailabilityStatusResponse>();
  @$core.pragma('dart2js:noInline')
  static ChangeAvailabilityStatusResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ChangeAvailabilityStatusResponse>(create);
  static ChangeAvailabilityStatusResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

class Wallet extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Wallet', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agentId')
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'balance', $pb.PbFieldType.OF)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..hasRequiredFields = false
  ;

  Wallet._() : super();
  factory Wallet({
    $core.String? id,
    $core.String? agentId,
    $core.double? balance,
    $core.String? currency,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (agentId != null) {
      _result.agentId = agentId;
    }
    if (balance != null) {
      _result.balance = balance;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    return _result;
  }
  factory Wallet.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Wallet.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Wallet clone() => Wallet()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Wallet copyWith(void Function(Wallet) updates) => super.copyWith((message) => updates(message as Wallet)) as Wallet; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Wallet create() => Wallet._();
  Wallet createEmptyInstance() => create();
  static $pb.PbList<Wallet> createRepeated() => $pb.PbList<Wallet>();
  @$core.pragma('dart2js:noInline')
  static Wallet getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Wallet>(create);
  static Wallet? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get agentId => $_getSZ(1);
  @$pb.TagNumber(2)
  set agentId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAgentId() => $_has(1);
  @$pb.TagNumber(2)
  void clearAgentId() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get balance => $_getN(2);
  @$pb.TagNumber(3)
  set balance($core.double v) { $_setFloat(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasBalance() => $_has(2);
  @$pb.TagNumber(3)
  void clearBalance() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get currency => $_getSZ(3);
  @$pb.TagNumber(4)
  set currency($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCurrency() => $_has(3);
  @$pb.TagNumber(4)
  void clearCurrency() => clearField(4);
}

class WalletFilter extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WalletFilter', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..hasRequiredFields = false
  ;

  WalletFilter._() : super();
  factory WalletFilter({
    $core.String? currency,
  }) {
    final _result = create();
    if (currency != null) {
      _result.currency = currency;
    }
    return _result;
  }
  factory WalletFilter.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WalletFilter.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WalletFilter clone() => WalletFilter()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WalletFilter copyWith(void Function(WalletFilter) updates) => super.copyWith((message) => updates(message as WalletFilter)) as WalletFilter; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WalletFilter create() => WalletFilter._();
  WalletFilter createEmptyInstance() => create();
  static $pb.PbList<WalletFilter> createRepeated() => $pb.PbList<WalletFilter>();
  @$core.pragma('dart2js:noInline')
  static WalletFilter getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WalletFilter>(create);
  static WalletFilter? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get currency => $_getSZ(0);
  @$pb.TagNumber(1)
  set currency($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCurrency() => $_has(0);
  @$pb.TagNumber(1)
  void clearCurrency() => clearField(1);
}

class GetAllAgentWalletsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAllAgentWalletsRequest', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.OU3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OU3)
    ..aOM<WalletFilter>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'filterBy', protoName: 'filterBy', subBuilder: WalletFilter.create)
    ..hasRequiredFields = false
  ;

  GetAllAgentWalletsRequest._() : super();
  factory GetAllAgentWalletsRequest({
    $core.int? page,
    $core.int? size,
    WalletFilter? filterBy,
  }) {
    final _result = create();
    if (page != null) {
      _result.page = page;
    }
    if (size != null) {
      _result.size = size;
    }
    if (filterBy != null) {
      _result.filterBy = filterBy;
    }
    return _result;
  }
  factory GetAllAgentWalletsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllAgentWalletsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAllAgentWalletsRequest clone() => GetAllAgentWalletsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAllAgentWalletsRequest copyWith(void Function(GetAllAgentWalletsRequest) updates) => super.copyWith((message) => updates(message as GetAllAgentWalletsRequest)) as GetAllAgentWalletsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllAgentWalletsRequest create() => GetAllAgentWalletsRequest._();
  GetAllAgentWalletsRequest createEmptyInstance() => create();
  static $pb.PbList<GetAllAgentWalletsRequest> createRepeated() => $pb.PbList<GetAllAgentWalletsRequest>();
  @$core.pragma('dart2js:noInline')
  static GetAllAgentWalletsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllAgentWalletsRequest>(create);
  static GetAllAgentWalletsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get page => $_getIZ(0);
  @$pb.TagNumber(1)
  set page($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPage() => $_has(0);
  @$pb.TagNumber(1)
  void clearPage() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get size => $_getIZ(1);
  @$pb.TagNumber(2)
  set size($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSize() => $_has(1);
  @$pb.TagNumber(2)
  void clearSize() => clearField(2);

  @$pb.TagNumber(3)
  WalletFilter get filterBy => $_getN(2);
  @$pb.TagNumber(3)
  set filterBy(WalletFilter v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasFilterBy() => $_has(2);
  @$pb.TagNumber(3)
  void clearFilterBy() => clearField(3);
  @$pb.TagNumber(3)
  WalletFilter ensureFilterBy() => $_ensure(2);
}

class GetAllAgentWalletsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAllAgentWalletsResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..pc<Wallet>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'wallets', $pb.PbFieldType.PM, subBuilder: Wallet.create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.OU3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OU3)
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total')
    ..hasRequiredFields = false
  ;

  GetAllAgentWalletsResponse._() : super();
  factory GetAllAgentWalletsResponse({
    $core.String? message,
    $core.Iterable<Wallet>? wallets,
    $core.int? page,
    $core.int? size,
    $fixnum.Int64? total,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (wallets != null) {
      _result.wallets.addAll(wallets);
    }
    if (page != null) {
      _result.page = page;
    }
    if (size != null) {
      _result.size = size;
    }
    if (total != null) {
      _result.total = total;
    }
    return _result;
  }
  factory GetAllAgentWalletsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllAgentWalletsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAllAgentWalletsResponse clone() => GetAllAgentWalletsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAllAgentWalletsResponse copyWith(void Function(GetAllAgentWalletsResponse) updates) => super.copyWith((message) => updates(message as GetAllAgentWalletsResponse)) as GetAllAgentWalletsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllAgentWalletsResponse create() => GetAllAgentWalletsResponse._();
  GetAllAgentWalletsResponse createEmptyInstance() => create();
  static $pb.PbList<GetAllAgentWalletsResponse> createRepeated() => $pb.PbList<GetAllAgentWalletsResponse>();
  @$core.pragma('dart2js:noInline')
  static GetAllAgentWalletsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllAgentWalletsResponse>(create);
  static GetAllAgentWalletsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Wallet> get wallets => $_getList(1);

  @$pb.TagNumber(3)
  $core.int get page => $_getIZ(2);
  @$pb.TagNumber(3)
  set page($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPage() => $_has(2);
  @$pb.TagNumber(3)
  void clearPage() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get size => $_getIZ(3);
  @$pb.TagNumber(4)
  set size($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSize() => $_has(3);
  @$pb.TagNumber(4)
  void clearSize() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get total => $_getI64(4);
  @$pb.TagNumber(5)
  set total($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTotal() => $_has(4);
  @$pb.TagNumber(5)
  void clearTotal() => clearField(5);
}

class GetWalletByAgentIdRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetWalletByAgentIdRequest', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agentId')
    ..hasRequiredFields = false
  ;

  GetWalletByAgentIdRequest._() : super();
  factory GetWalletByAgentIdRequest({
    $core.String? agentId,
  }) {
    final _result = create();
    if (agentId != null) {
      _result.agentId = agentId;
    }
    return _result;
  }
  factory GetWalletByAgentIdRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetWalletByAgentIdRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetWalletByAgentIdRequest clone() => GetWalletByAgentIdRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetWalletByAgentIdRequest copyWith(void Function(GetWalletByAgentIdRequest) updates) => super.copyWith((message) => updates(message as GetWalletByAgentIdRequest)) as GetWalletByAgentIdRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetWalletByAgentIdRequest create() => GetWalletByAgentIdRequest._();
  GetWalletByAgentIdRequest createEmptyInstance() => create();
  static $pb.PbList<GetWalletByAgentIdRequest> createRepeated() => $pb.PbList<GetWalletByAgentIdRequest>();
  @$core.pragma('dart2js:noInline')
  static GetWalletByAgentIdRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetWalletByAgentIdRequest>(create);
  static GetWalletByAgentIdRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get agentId => $_getSZ(0);
  @$pb.TagNumber(1)
  set agentId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAgentId() => $_has(0);
  @$pb.TagNumber(1)
  void clearAgentId() => clearField(1);
}

class GetWalletByAgentIdResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetWalletByAgentIdResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOM<Wallet>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'wallet', subBuilder: Wallet.create)
    ..hasRequiredFields = false
  ;

  GetWalletByAgentIdResponse._() : super();
  factory GetWalletByAgentIdResponse({
    $core.String? message,
    Wallet? wallet,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (wallet != null) {
      _result.wallet = wallet;
    }
    return _result;
  }
  factory GetWalletByAgentIdResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetWalletByAgentIdResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetWalletByAgentIdResponse clone() => GetWalletByAgentIdResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetWalletByAgentIdResponse copyWith(void Function(GetWalletByAgentIdResponse) updates) => super.copyWith((message) => updates(message as GetWalletByAgentIdResponse)) as GetWalletByAgentIdResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetWalletByAgentIdResponse create() => GetWalletByAgentIdResponse._();
  GetWalletByAgentIdResponse createEmptyInstance() => create();
  static $pb.PbList<GetWalletByAgentIdResponse> createRepeated() => $pb.PbList<GetWalletByAgentIdResponse>();
  @$core.pragma('dart2js:noInline')
  static GetWalletByAgentIdResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetWalletByAgentIdResponse>(create);
  static GetWalletByAgentIdResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  Wallet get wallet => $_getN(1);
  @$pb.TagNumber(2)
  set wallet(Wallet v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasWallet() => $_has(1);
  @$pb.TagNumber(2)
  void clearWallet() => clearField(2);
  @$pb.TagNumber(2)
  Wallet ensureWallet() => $_ensure(1);
}

enum Value_Kind {
  stringValue, 
  doubleValue, 
  uintValue, 
  notSet
}

class Value extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Value_Kind> _Value_KindByTag = {
    1 : Value_Kind.stringValue,
    2 : Value_Kind.doubleValue,
    3 : Value_Kind.uintValue,
    0 : Value_Kind.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Value', createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stringValue')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'doubleValue', $pb.PbFieldType.OD)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'uintValue', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  Value._() : super();
  factory Value({
    $core.String? stringValue,
    $core.double? doubleValue,
    $core.int? uintValue,
  }) {
    final _result = create();
    if (stringValue != null) {
      _result.stringValue = stringValue;
    }
    if (doubleValue != null) {
      _result.doubleValue = doubleValue;
    }
    if (uintValue != null) {
      _result.uintValue = uintValue;
    }
    return _result;
  }
  factory Value.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Value.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Value clone() => Value()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Value copyWith(void Function(Value) updates) => super.copyWith((message) => updates(message as Value)) as Value; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Value create() => Value._();
  Value createEmptyInstance() => create();
  static $pb.PbList<Value> createRepeated() => $pb.PbList<Value>();
  @$core.pragma('dart2js:noInline')
  static Value getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Value>(create);
  static Value? _defaultInstance;

  Value_Kind whichKind() => _Value_KindByTag[$_whichOneof(0)]!;
  void clearKind() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get stringValue => $_getSZ(0);
  @$pb.TagNumber(1)
  set stringValue($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStringValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearStringValue() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get doubleValue => $_getN(1);
  @$pb.TagNumber(2)
  set doubleValue($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDoubleValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearDoubleValue() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get uintValue => $_getIZ(2);
  @$pb.TagNumber(3)
  set uintValue($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasUintValue() => $_has(2);
  @$pb.TagNumber(3)
  void clearUintValue() => clearField(3);
}

class OrderItems extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderItems', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..a<$fixnum.Int64>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'price', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  OrderItems._() : super();
  factory OrderItems({
    $core.String? name,
    $fixnum.Int64? quantity,
    $core.double? price,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (price != null) {
      _result.price = price;
    }
    return _result;
  }
  factory OrderItems.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderItems.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderItems clone() => OrderItems()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderItems copyWith(void Function(OrderItems) updates) => super.copyWith((message) => updates(message as OrderItems)) as OrderItems; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderItems create() => OrderItems._();
  OrderItems createEmptyInstance() => create();
  static $pb.PbList<OrderItems> createRepeated() => $pb.PbList<OrderItems>();
  @$core.pragma('dart2js:noInline')
  static OrderItems getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderItems>(create);
  static OrderItems? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get quantity => $_getI64(1);
  @$pb.TagNumber(2)
  set quantity($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasQuantity() => $_has(1);
  @$pb.TagNumber(2)
  void clearQuantity() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get price => $_getN(2);
  @$pb.TagNumber(3)
  set price($core.double v) { $_setFloat(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPrice() => $_has(2);
  @$pb.TagNumber(3)
  void clearPrice() => clearField(3);
}

class OrderRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderRequest', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderId')
    ..pc<OrderItems>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderItems', $pb.PbFieldType.PM, subBuilder: OrderItems.create)
    ..e<OrderRequestStatus>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: OrderRequestStatus.UNASSIGNED, valueOf: OrderRequestStatus.valueOf, enumValues: OrderRequestStatus.values)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'duration', $pb.PbFieldType.OU3)
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'earnableAmount', $pb.PbFieldType.OF)
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryLocation')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agentId')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'customerId')
    ..hasRequiredFields = false
  ;

  OrderRequest._() : super();
  factory OrderRequest({
    $core.String? id,
    $core.String? orderId,
    $core.Iterable<OrderItems>? orderItems,
    OrderRequestStatus? status,
    $core.int? duration,
    $core.double? earnableAmount,
    $core.String? deliveryLocation,
    $core.String? agentId,
    $core.String? customerId,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (orderId != null) {
      _result.orderId = orderId;
    }
    if (orderItems != null) {
      _result.orderItems.addAll(orderItems);
    }
    if (status != null) {
      _result.status = status;
    }
    if (duration != null) {
      _result.duration = duration;
    }
    if (earnableAmount != null) {
      _result.earnableAmount = earnableAmount;
    }
    if (deliveryLocation != null) {
      _result.deliveryLocation = deliveryLocation;
    }
    if (agentId != null) {
      _result.agentId = agentId;
    }
    if (customerId != null) {
      _result.customerId = customerId;
    }
    return _result;
  }
  factory OrderRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderRequest clone() => OrderRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderRequest copyWith(void Function(OrderRequest) updates) => super.copyWith((message) => updates(message as OrderRequest)) as OrderRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderRequest create() => OrderRequest._();
  OrderRequest createEmptyInstance() => create();
  static $pb.PbList<OrderRequest> createRepeated() => $pb.PbList<OrderRequest>();
  @$core.pragma('dart2js:noInline')
  static OrderRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderRequest>(create);
  static OrderRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get orderId => $_getSZ(1);
  @$pb.TagNumber(2)
  set orderId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOrderId() => $_has(1);
  @$pb.TagNumber(2)
  void clearOrderId() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<OrderItems> get orderItems => $_getList(2);

  @$pb.TagNumber(4)
  OrderRequestStatus get status => $_getN(3);
  @$pb.TagNumber(4)
  set status(OrderRequestStatus v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStatus() => $_has(3);
  @$pb.TagNumber(4)
  void clearStatus() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get duration => $_getIZ(4);
  @$pb.TagNumber(5)
  set duration($core.int v) { $_setUnsignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDuration() => $_has(4);
  @$pb.TagNumber(5)
  void clearDuration() => clearField(5);

  @$pb.TagNumber(6)
  $core.double get earnableAmount => $_getN(5);
  @$pb.TagNumber(6)
  set earnableAmount($core.double v) { $_setFloat(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasEarnableAmount() => $_has(5);
  @$pb.TagNumber(6)
  void clearEarnableAmount() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get deliveryLocation => $_getSZ(6);
  @$pb.TagNumber(7)
  set deliveryLocation($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasDeliveryLocation() => $_has(6);
  @$pb.TagNumber(7)
  void clearDeliveryLocation() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get agentId => $_getSZ(7);
  @$pb.TagNumber(8)
  set agentId($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasAgentId() => $_has(7);
  @$pb.TagNumber(8)
  void clearAgentId() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get customerId => $_getSZ(8);
  @$pb.TagNumber(9)
  set customerId($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasCustomerId() => $_has(8);
  @$pb.TagNumber(9)
  void clearCustomerId() => clearField(9);
}

class CreateOrderRequest_Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CreateOrderRequest_Request', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderId')
    ..pc<OrderItems>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderItems', $pb.PbFieldType.PM, subBuilder: OrderItems.create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'duration', $pb.PbFieldType.OU3)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'earnableAmount', $pb.PbFieldType.OF)
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryLocation')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'customerId')
    ..hasRequiredFields = false
  ;

  CreateOrderRequest_Request._() : super();
  factory CreateOrderRequest_Request({
    $core.String? orderId,
    $core.Iterable<OrderItems>? orderItems,
    $core.int? duration,
    $core.double? earnableAmount,
    $core.String? deliveryLocation,
    $core.String? customerId,
  }) {
    final _result = create();
    if (orderId != null) {
      _result.orderId = orderId;
    }
    if (orderItems != null) {
      _result.orderItems.addAll(orderItems);
    }
    if (duration != null) {
      _result.duration = duration;
    }
    if (earnableAmount != null) {
      _result.earnableAmount = earnableAmount;
    }
    if (deliveryLocation != null) {
      _result.deliveryLocation = deliveryLocation;
    }
    if (customerId != null) {
      _result.customerId = customerId;
    }
    return _result;
  }
  factory CreateOrderRequest_Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateOrderRequest_Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CreateOrderRequest_Request clone() => CreateOrderRequest_Request()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CreateOrderRequest_Request copyWith(void Function(CreateOrderRequest_Request) updates) => super.copyWith((message) => updates(message as CreateOrderRequest_Request)) as CreateOrderRequest_Request; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CreateOrderRequest_Request create() => CreateOrderRequest_Request._();
  CreateOrderRequest_Request createEmptyInstance() => create();
  static $pb.PbList<CreateOrderRequest_Request> createRepeated() => $pb.PbList<CreateOrderRequest_Request>();
  @$core.pragma('dart2js:noInline')
  static CreateOrderRequest_Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateOrderRequest_Request>(create);
  static CreateOrderRequest_Request? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get orderId => $_getSZ(0);
  @$pb.TagNumber(1)
  set orderId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderId() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<OrderItems> get orderItems => $_getList(1);

  @$pb.TagNumber(3)
  $core.int get duration => $_getIZ(2);
  @$pb.TagNumber(3)
  set duration($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDuration() => $_has(2);
  @$pb.TagNumber(3)
  void clearDuration() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get earnableAmount => $_getN(3);
  @$pb.TagNumber(4)
  set earnableAmount($core.double v) { $_setFloat(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasEarnableAmount() => $_has(3);
  @$pb.TagNumber(4)
  void clearEarnableAmount() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get deliveryLocation => $_getSZ(4);
  @$pb.TagNumber(5)
  set deliveryLocation($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDeliveryLocation() => $_has(4);
  @$pb.TagNumber(5)
  void clearDeliveryLocation() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get customerId => $_getSZ(5);
  @$pb.TagNumber(6)
  set customerId($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasCustomerId() => $_has(5);
  @$pb.TagNumber(6)
  void clearCustomerId() => clearField(6);
}

class CreateOrderRequest_Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CreateOrderRequest_Response', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  CreateOrderRequest_Response._() : super();
  factory CreateOrderRequest_Response({
    $core.String? message,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory CreateOrderRequest_Response.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateOrderRequest_Response.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CreateOrderRequest_Response clone() => CreateOrderRequest_Response()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CreateOrderRequest_Response copyWith(void Function(CreateOrderRequest_Response) updates) => super.copyWith((message) => updates(message as CreateOrderRequest_Response)) as CreateOrderRequest_Response; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CreateOrderRequest_Response create() => CreateOrderRequest_Response._();
  CreateOrderRequest_Response createEmptyInstance() => create();
  static $pb.PbList<CreateOrderRequest_Response> createRepeated() => $pb.PbList<CreateOrderRequest_Response>();
  @$core.pragma('dart2js:noInline')
  static CreateOrderRequest_Response getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateOrderRequest_Response>(create);
  static CreateOrderRequest_Response? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

class OrderRequestFilter extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderRequestFilter', createEmptyInstance: create)
    ..e<OrderRequestStatus>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: OrderRequestStatus.UNASSIGNED, valueOf: OrderRequestStatus.valueOf, enumValues: OrderRequestStatus.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agentId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'customerId')
    ..hasRequiredFields = false
  ;

  OrderRequestFilter._() : super();
  factory OrderRequestFilter({
    OrderRequestStatus? status,
    $core.String? agentId,
    $core.String? customerId,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (agentId != null) {
      _result.agentId = agentId;
    }
    if (customerId != null) {
      _result.customerId = customerId;
    }
    return _result;
  }
  factory OrderRequestFilter.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderRequestFilter.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderRequestFilter clone() => OrderRequestFilter()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderRequestFilter copyWith(void Function(OrderRequestFilter) updates) => super.copyWith((message) => updates(message as OrderRequestFilter)) as OrderRequestFilter; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderRequestFilter create() => OrderRequestFilter._();
  OrderRequestFilter createEmptyInstance() => create();
  static $pb.PbList<OrderRequestFilter> createRepeated() => $pb.PbList<OrderRequestFilter>();
  @$core.pragma('dart2js:noInline')
  static OrderRequestFilter getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderRequestFilter>(create);
  static OrderRequestFilter? _defaultInstance;

  @$pb.TagNumber(1)
  OrderRequestStatus get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(OrderRequestStatus v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get agentId => $_getSZ(1);
  @$pb.TagNumber(2)
  set agentId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAgentId() => $_has(1);
  @$pb.TagNumber(2)
  void clearAgentId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get customerId => $_getSZ(2);
  @$pb.TagNumber(3)
  set customerId($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCustomerId() => $_has(2);
  @$pb.TagNumber(3)
  void clearCustomerId() => clearField(3);
}

class GetAllOrderRequests_Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAllOrderRequests_Request', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.OU3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OU3)
    ..aOM<OrderRequestFilter>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'filterBy', protoName: 'filterBy', subBuilder: OrderRequestFilter.create)
    ..hasRequiredFields = false
  ;

  GetAllOrderRequests_Request._() : super();
  factory GetAllOrderRequests_Request({
    $core.int? page,
    $core.int? size,
    OrderRequestFilter? filterBy,
  }) {
    final _result = create();
    if (page != null) {
      _result.page = page;
    }
    if (size != null) {
      _result.size = size;
    }
    if (filterBy != null) {
      _result.filterBy = filterBy;
    }
    return _result;
  }
  factory GetAllOrderRequests_Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllOrderRequests_Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAllOrderRequests_Request clone() => GetAllOrderRequests_Request()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAllOrderRequests_Request copyWith(void Function(GetAllOrderRequests_Request) updates) => super.copyWith((message) => updates(message as GetAllOrderRequests_Request)) as GetAllOrderRequests_Request; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllOrderRequests_Request create() => GetAllOrderRequests_Request._();
  GetAllOrderRequests_Request createEmptyInstance() => create();
  static $pb.PbList<GetAllOrderRequests_Request> createRepeated() => $pb.PbList<GetAllOrderRequests_Request>();
  @$core.pragma('dart2js:noInline')
  static GetAllOrderRequests_Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllOrderRequests_Request>(create);
  static GetAllOrderRequests_Request? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get page => $_getIZ(0);
  @$pb.TagNumber(1)
  set page($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPage() => $_has(0);
  @$pb.TagNumber(1)
  void clearPage() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get size => $_getIZ(1);
  @$pb.TagNumber(2)
  set size($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSize() => $_has(1);
  @$pb.TagNumber(2)
  void clearSize() => clearField(2);

  @$pb.TagNumber(3)
  OrderRequestFilter get filterBy => $_getN(2);
  @$pb.TagNumber(3)
  set filterBy(OrderRequestFilter v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasFilterBy() => $_has(2);
  @$pb.TagNumber(3)
  void clearFilterBy() => clearField(3);
  @$pb.TagNumber(3)
  OrderRequestFilter ensureFilterBy() => $_ensure(2);
}

class GetAllOrderRequests_Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAllOrderRequests_Response', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..pc<OrderRequest>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderRequests', $pb.PbFieldType.PM, subBuilder: OrderRequest.create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.OU3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OU3)
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total')
    ..hasRequiredFields = false
  ;

  GetAllOrderRequests_Response._() : super();
  factory GetAllOrderRequests_Response({
    $core.String? message,
    $core.Iterable<OrderRequest>? orderRequests,
    $core.int? page,
    $core.int? size,
    $fixnum.Int64? total,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (orderRequests != null) {
      _result.orderRequests.addAll(orderRequests);
    }
    if (page != null) {
      _result.page = page;
    }
    if (size != null) {
      _result.size = size;
    }
    if (total != null) {
      _result.total = total;
    }
    return _result;
  }
  factory GetAllOrderRequests_Response.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllOrderRequests_Response.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAllOrderRequests_Response clone() => GetAllOrderRequests_Response()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAllOrderRequests_Response copyWith(void Function(GetAllOrderRequests_Response) updates) => super.copyWith((message) => updates(message as GetAllOrderRequests_Response)) as GetAllOrderRequests_Response; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllOrderRequests_Response create() => GetAllOrderRequests_Response._();
  GetAllOrderRequests_Response createEmptyInstance() => create();
  static $pb.PbList<GetAllOrderRequests_Response> createRepeated() => $pb.PbList<GetAllOrderRequests_Response>();
  @$core.pragma('dart2js:noInline')
  static GetAllOrderRequests_Response getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllOrderRequests_Response>(create);
  static GetAllOrderRequests_Response? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<OrderRequest> get orderRequests => $_getList(1);

  @$pb.TagNumber(3)
  $core.int get page => $_getIZ(2);
  @$pb.TagNumber(3)
  set page($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPage() => $_has(2);
  @$pb.TagNumber(3)
  void clearPage() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get size => $_getIZ(3);
  @$pb.TagNumber(4)
  set size($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSize() => $_has(3);
  @$pb.TagNumber(4)
  void clearSize() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get total => $_getI64(4);
  @$pb.TagNumber(5)
  set total($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTotal() => $_has(4);
  @$pb.TagNumber(5)
  void clearTotal() => clearField(5);
}

class SendOrderRequestToAgent_Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SendOrderRequestToAgent_Request', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderRequestId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agentId')
    ..a<$fixnum.Int64>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'acceptanceDuration', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false
  ;

  SendOrderRequestToAgent_Request._() : super();
  factory SendOrderRequestToAgent_Request({
    $core.String? orderRequestId,
    $core.String? agentId,
    $fixnum.Int64? acceptanceDuration,
  }) {
    final _result = create();
    if (orderRequestId != null) {
      _result.orderRequestId = orderRequestId;
    }
    if (agentId != null) {
      _result.agentId = agentId;
    }
    if (acceptanceDuration != null) {
      _result.acceptanceDuration = acceptanceDuration;
    }
    return _result;
  }
  factory SendOrderRequestToAgent_Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SendOrderRequestToAgent_Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SendOrderRequestToAgent_Request clone() => SendOrderRequestToAgent_Request()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SendOrderRequestToAgent_Request copyWith(void Function(SendOrderRequestToAgent_Request) updates) => super.copyWith((message) => updates(message as SendOrderRequestToAgent_Request)) as SendOrderRequestToAgent_Request; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SendOrderRequestToAgent_Request create() => SendOrderRequestToAgent_Request._();
  SendOrderRequestToAgent_Request createEmptyInstance() => create();
  static $pb.PbList<SendOrderRequestToAgent_Request> createRepeated() => $pb.PbList<SendOrderRequestToAgent_Request>();
  @$core.pragma('dart2js:noInline')
  static SendOrderRequestToAgent_Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SendOrderRequestToAgent_Request>(create);
  static SendOrderRequestToAgent_Request? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get orderRequestId => $_getSZ(0);
  @$pb.TagNumber(1)
  set orderRequestId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderRequestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderRequestId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get agentId => $_getSZ(1);
  @$pb.TagNumber(2)
  set agentId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAgentId() => $_has(1);
  @$pb.TagNumber(2)
  void clearAgentId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get acceptanceDuration => $_getI64(2);
  @$pb.TagNumber(3)
  set acceptanceDuration($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAcceptanceDuration() => $_has(2);
  @$pb.TagNumber(3)
  void clearAcceptanceDuration() => clearField(3);
}

class SendOrderRequestToAgent_Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SendOrderRequestToAgent_Response', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'requestId')
    ..hasRequiredFields = false
  ;

  SendOrderRequestToAgent_Response._() : super();
  factory SendOrderRequestToAgent_Response({
    $core.String? message,
    $core.String? requestId,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (requestId != null) {
      _result.requestId = requestId;
    }
    return _result;
  }
  factory SendOrderRequestToAgent_Response.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SendOrderRequestToAgent_Response.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SendOrderRequestToAgent_Response clone() => SendOrderRequestToAgent_Response()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SendOrderRequestToAgent_Response copyWith(void Function(SendOrderRequestToAgent_Response) updates) => super.copyWith((message) => updates(message as SendOrderRequestToAgent_Response)) as SendOrderRequestToAgent_Response; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SendOrderRequestToAgent_Response create() => SendOrderRequestToAgent_Response._();
  SendOrderRequestToAgent_Response createEmptyInstance() => create();
  static $pb.PbList<SendOrderRequestToAgent_Response> createRepeated() => $pb.PbList<SendOrderRequestToAgent_Response>();
  @$core.pragma('dart2js:noInline')
  static SendOrderRequestToAgent_Response getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SendOrderRequestToAgent_Response>(create);
  static SendOrderRequestToAgent_Response? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get requestId => $_getSZ(1);
  @$pb.TagNumber(2)
  set requestId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRequestId() => $_has(1);
  @$pb.TagNumber(2)
  void clearRequestId() => clearField(2);
}

class GetAssignedOrderRequest_Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAssignedOrderRequest_Request', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  GetAssignedOrderRequest_Request._() : super();
  factory GetAssignedOrderRequest_Request() => create();
  factory GetAssignedOrderRequest_Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAssignedOrderRequest_Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAssignedOrderRequest_Request clone() => GetAssignedOrderRequest_Request()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAssignedOrderRequest_Request copyWith(void Function(GetAssignedOrderRequest_Request) updates) => super.copyWith((message) => updates(message as GetAssignedOrderRequest_Request)) as GetAssignedOrderRequest_Request; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAssignedOrderRequest_Request create() => GetAssignedOrderRequest_Request._();
  GetAssignedOrderRequest_Request createEmptyInstance() => create();
  static $pb.PbList<GetAssignedOrderRequest_Request> createRepeated() => $pb.PbList<GetAssignedOrderRequest_Request>();
  @$core.pragma('dart2js:noInline')
  static GetAssignedOrderRequest_Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAssignedOrderRequest_Request>(create);
  static GetAssignedOrderRequest_Request? _defaultInstance;
}

class GetAssignedOrderRequest_Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAssignedOrderRequest_Response', createEmptyInstance: create)
    ..aOM<OrderRequest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderRequest', subBuilder: OrderRequest.create)
    ..hasRequiredFields = false
  ;

  GetAssignedOrderRequest_Response._() : super();
  factory GetAssignedOrderRequest_Response({
    OrderRequest? orderRequest,
  }) {
    final _result = create();
    if (orderRequest != null) {
      _result.orderRequest = orderRequest;
    }
    return _result;
  }
  factory GetAssignedOrderRequest_Response.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAssignedOrderRequest_Response.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAssignedOrderRequest_Response clone() => GetAssignedOrderRequest_Response()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAssignedOrderRequest_Response copyWith(void Function(GetAssignedOrderRequest_Response) updates) => super.copyWith((message) => updates(message as GetAssignedOrderRequest_Response)) as GetAssignedOrderRequest_Response; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAssignedOrderRequest_Response create() => GetAssignedOrderRequest_Response._();
  GetAssignedOrderRequest_Response createEmptyInstance() => create();
  static $pb.PbList<GetAssignedOrderRequest_Response> createRepeated() => $pb.PbList<GetAssignedOrderRequest_Response>();
  @$core.pragma('dart2js:noInline')
  static GetAssignedOrderRequest_Response getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAssignedOrderRequest_Response>(create);
  static GetAssignedOrderRequest_Response? _defaultInstance;

  @$pb.TagNumber(1)
  OrderRequest get orderRequest => $_getN(0);
  @$pb.TagNumber(1)
  set orderRequest(OrderRequest v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderRequest() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderRequest() => clearField(1);
  @$pb.TagNumber(1)
  OrderRequest ensureOrderRequest() => $_ensure(0);
}

class AcceptOrDeclineOrderRequest_Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AcceptOrDeclineOrderRequest_Request', createEmptyInstance: create)
    ..e<AcceptanceStatus>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'accepted', $pb.PbFieldType.OE, defaultOrMaker: AcceptanceStatus.accept, valueOf: AcceptanceStatus.valueOf, enumValues: AcceptanceStatus.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderRequestId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rejectReason')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'requestId')
    ..hasRequiredFields = false
  ;

  AcceptOrDeclineOrderRequest_Request._() : super();
  factory AcceptOrDeclineOrderRequest_Request({
    AcceptanceStatus? accepted,
    $core.String? orderRequestId,
    $core.String? rejectReason,
    $core.String? requestId,
  }) {
    final _result = create();
    if (accepted != null) {
      _result.accepted = accepted;
    }
    if (orderRequestId != null) {
      _result.orderRequestId = orderRequestId;
    }
    if (rejectReason != null) {
      _result.rejectReason = rejectReason;
    }
    if (requestId != null) {
      _result.requestId = requestId;
    }
    return _result;
  }
  factory AcceptOrDeclineOrderRequest_Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AcceptOrDeclineOrderRequest_Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AcceptOrDeclineOrderRequest_Request clone() => AcceptOrDeclineOrderRequest_Request()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AcceptOrDeclineOrderRequest_Request copyWith(void Function(AcceptOrDeclineOrderRequest_Request) updates) => super.copyWith((message) => updates(message as AcceptOrDeclineOrderRequest_Request)) as AcceptOrDeclineOrderRequest_Request; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AcceptOrDeclineOrderRequest_Request create() => AcceptOrDeclineOrderRequest_Request._();
  AcceptOrDeclineOrderRequest_Request createEmptyInstance() => create();
  static $pb.PbList<AcceptOrDeclineOrderRequest_Request> createRepeated() => $pb.PbList<AcceptOrDeclineOrderRequest_Request>();
  @$core.pragma('dart2js:noInline')
  static AcceptOrDeclineOrderRequest_Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AcceptOrDeclineOrderRequest_Request>(create);
  static AcceptOrDeclineOrderRequest_Request? _defaultInstance;

  @$pb.TagNumber(1)
  AcceptanceStatus get accepted => $_getN(0);
  @$pb.TagNumber(1)
  set accepted(AcceptanceStatus v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasAccepted() => $_has(0);
  @$pb.TagNumber(1)
  void clearAccepted() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get orderRequestId => $_getSZ(1);
  @$pb.TagNumber(2)
  set orderRequestId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOrderRequestId() => $_has(1);
  @$pb.TagNumber(2)
  void clearOrderRequestId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get rejectReason => $_getSZ(2);
  @$pb.TagNumber(3)
  set rejectReason($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRejectReason() => $_has(2);
  @$pb.TagNumber(3)
  void clearRejectReason() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get requestId => $_getSZ(3);
  @$pb.TagNumber(4)
  set requestId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasRequestId() => $_has(3);
  @$pb.TagNumber(4)
  void clearRequestId() => clearField(4);
}

class AcceptOrDeclineOrderRequest_Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AcceptOrDeclineOrderRequest_Response', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOM<OrderRequest>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderRequest', subBuilder: OrderRequest.create)
    ..hasRequiredFields = false
  ;

  AcceptOrDeclineOrderRequest_Response._() : super();
  factory AcceptOrDeclineOrderRequest_Response({
    $core.String? message,
    OrderRequest? orderRequest,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (orderRequest != null) {
      _result.orderRequest = orderRequest;
    }
    return _result;
  }
  factory AcceptOrDeclineOrderRequest_Response.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AcceptOrDeclineOrderRequest_Response.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AcceptOrDeclineOrderRequest_Response clone() => AcceptOrDeclineOrderRequest_Response()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AcceptOrDeclineOrderRequest_Response copyWith(void Function(AcceptOrDeclineOrderRequest_Response) updates) => super.copyWith((message) => updates(message as AcceptOrDeclineOrderRequest_Response)) as AcceptOrDeclineOrderRequest_Response; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AcceptOrDeclineOrderRequest_Response create() => AcceptOrDeclineOrderRequest_Response._();
  AcceptOrDeclineOrderRequest_Response createEmptyInstance() => create();
  static $pb.PbList<AcceptOrDeclineOrderRequest_Response> createRepeated() => $pb.PbList<AcceptOrDeclineOrderRequest_Response>();
  @$core.pragma('dart2js:noInline')
  static AcceptOrDeclineOrderRequest_Response getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AcceptOrDeclineOrderRequest_Response>(create);
  static AcceptOrDeclineOrderRequest_Response? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  OrderRequest get orderRequest => $_getN(1);
  @$pb.TagNumber(2)
  set orderRequest(OrderRequest v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasOrderRequest() => $_has(1);
  @$pb.TagNumber(2)
  void clearOrderRequest() => clearField(2);
  @$pb.TagNumber(2)
  OrderRequest ensureOrderRequest() => $_ensure(1);
}

class UpdateOrderRequestStatus_Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateOrderRequestStatus_Request', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderRequestId')
    ..e<OrderRequestStatus>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: OrderRequestStatus.UNASSIGNED, valueOf: OrderRequestStatus.valueOf, enumValues: OrderRequestStatus.values)
    ..hasRequiredFields = false
  ;

  UpdateOrderRequestStatus_Request._() : super();
  factory UpdateOrderRequestStatus_Request({
    $core.String? orderRequestId,
    OrderRequestStatus? status,
  }) {
    final _result = create();
    if (orderRequestId != null) {
      _result.orderRequestId = orderRequestId;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory UpdateOrderRequestStatus_Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateOrderRequestStatus_Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateOrderRequestStatus_Request clone() => UpdateOrderRequestStatus_Request()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateOrderRequestStatus_Request copyWith(void Function(UpdateOrderRequestStatus_Request) updates) => super.copyWith((message) => updates(message as UpdateOrderRequestStatus_Request)) as UpdateOrderRequestStatus_Request; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateOrderRequestStatus_Request create() => UpdateOrderRequestStatus_Request._();
  UpdateOrderRequestStatus_Request createEmptyInstance() => create();
  static $pb.PbList<UpdateOrderRequestStatus_Request> createRepeated() => $pb.PbList<UpdateOrderRequestStatus_Request>();
  @$core.pragma('dart2js:noInline')
  static UpdateOrderRequestStatus_Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateOrderRequestStatus_Request>(create);
  static UpdateOrderRequestStatus_Request? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get orderRequestId => $_getSZ(0);
  @$pb.TagNumber(1)
  set orderRequestId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderRequestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderRequestId() => clearField(1);

  @$pb.TagNumber(2)
  OrderRequestStatus get status => $_getN(1);
  @$pb.TagNumber(2)
  set status(OrderRequestStatus v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class UpdateOrderRequestStatus_Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateOrderRequestStatus_Response', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  UpdateOrderRequestStatus_Response._() : super();
  factory UpdateOrderRequestStatus_Response({
    $core.String? message,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory UpdateOrderRequestStatus_Response.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateOrderRequestStatus_Response.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateOrderRequestStatus_Response clone() => UpdateOrderRequestStatus_Response()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateOrderRequestStatus_Response copyWith(void Function(UpdateOrderRequestStatus_Response) updates) => super.copyWith((message) => updates(message as UpdateOrderRequestStatus_Response)) as UpdateOrderRequestStatus_Response; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateOrderRequestStatus_Response create() => UpdateOrderRequestStatus_Response._();
  UpdateOrderRequestStatus_Response createEmptyInstance() => create();
  static $pb.PbList<UpdateOrderRequestStatus_Response> createRepeated() => $pb.PbList<UpdateOrderRequestStatus_Response>();
  @$core.pragma('dart2js:noInline')
  static UpdateOrderRequestStatus_Response getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateOrderRequestStatus_Response>(create);
  static UpdateOrderRequestStatus_Response? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

class GetOrderRequestHistory_Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetOrderRequestHistory_Request', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agentId')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.OU3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  GetOrderRequestHistory_Request._() : super();
  factory GetOrderRequestHistory_Request({
    $core.String? agentId,
    $core.int? page,
    $core.int? size,
  }) {
    final _result = create();
    if (agentId != null) {
      _result.agentId = agentId;
    }
    if (page != null) {
      _result.page = page;
    }
    if (size != null) {
      _result.size = size;
    }
    return _result;
  }
  factory GetOrderRequestHistory_Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetOrderRequestHistory_Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetOrderRequestHistory_Request clone() => GetOrderRequestHistory_Request()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetOrderRequestHistory_Request copyWith(void Function(GetOrderRequestHistory_Request) updates) => super.copyWith((message) => updates(message as GetOrderRequestHistory_Request)) as GetOrderRequestHistory_Request; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetOrderRequestHistory_Request create() => GetOrderRequestHistory_Request._();
  GetOrderRequestHistory_Request createEmptyInstance() => create();
  static $pb.PbList<GetOrderRequestHistory_Request> createRepeated() => $pb.PbList<GetOrderRequestHistory_Request>();
  @$core.pragma('dart2js:noInline')
  static GetOrderRequestHistory_Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetOrderRequestHistory_Request>(create);
  static GetOrderRequestHistory_Request? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get agentId => $_getSZ(0);
  @$pb.TagNumber(1)
  set agentId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAgentId() => $_has(0);
  @$pb.TagNumber(1)
  void clearAgentId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get page => $_getIZ(1);
  @$pb.TagNumber(2)
  set page($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPage() => $_has(1);
  @$pb.TagNumber(2)
  void clearPage() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get size => $_getIZ(2);
  @$pb.TagNumber(3)
  set size($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSize() => $_has(2);
  @$pb.TagNumber(3)
  void clearSize() => clearField(3);
}

class GetOrderRequestHistory_Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetOrderRequestHistory_Response', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..pc<OrderRequest>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderRequests', $pb.PbFieldType.PM, subBuilder: OrderRequest.create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page', $pb.PbFieldType.OU3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OU3)
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total')
    ..hasRequiredFields = false
  ;

  GetOrderRequestHistory_Response._() : super();
  factory GetOrderRequestHistory_Response({
    $core.String? message,
    $core.Iterable<OrderRequest>? orderRequests,
    $core.int? page,
    $core.int? size,
    $fixnum.Int64? total,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (orderRequests != null) {
      _result.orderRequests.addAll(orderRequests);
    }
    if (page != null) {
      _result.page = page;
    }
    if (size != null) {
      _result.size = size;
    }
    if (total != null) {
      _result.total = total;
    }
    return _result;
  }
  factory GetOrderRequestHistory_Response.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetOrderRequestHistory_Response.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetOrderRequestHistory_Response clone() => GetOrderRequestHistory_Response()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetOrderRequestHistory_Response copyWith(void Function(GetOrderRequestHistory_Response) updates) => super.copyWith((message) => updates(message as GetOrderRequestHistory_Response)) as GetOrderRequestHistory_Response; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetOrderRequestHistory_Response create() => GetOrderRequestHistory_Response._();
  GetOrderRequestHistory_Response createEmptyInstance() => create();
  static $pb.PbList<GetOrderRequestHistory_Response> createRepeated() => $pb.PbList<GetOrderRequestHistory_Response>();
  @$core.pragma('dart2js:noInline')
  static GetOrderRequestHistory_Response getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetOrderRequestHistory_Response>(create);
  static GetOrderRequestHistory_Response? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<OrderRequest> get orderRequests => $_getList(1);

  @$pb.TagNumber(3)
  $core.int get page => $_getIZ(2);
  @$pb.TagNumber(3)
  set page($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPage() => $_has(2);
  @$pb.TagNumber(3)
  void clearPage() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get size => $_getIZ(3);
  @$pb.TagNumber(4)
  set size($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSize() => $_has(3);
  @$pb.TagNumber(4)
  void clearSize() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get total => $_getI64(4);
  @$pb.TagNumber(5)
  set total($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTotal() => $_has(4);
  @$pb.TagNumber(5)
  void clearTotal() => clearField(5);
}

