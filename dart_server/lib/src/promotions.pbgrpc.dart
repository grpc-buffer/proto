///
//  Generated code. Do not modify.
//  source: promotions.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'promotions.pb.dart' as $0;
export 'promotions.pb.dart';

class promotionServiceClient extends $grpc.Client {
  static final _$addBanner =
      $grpc.ClientMethod<$0.BannerInfo, $0.PromotionsResponse>(
          '/promotion_service.v1.promotionService/AddBanner',
          ($0.BannerInfo value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.PromotionsResponse.fromBuffer(value));
  static final _$listBanner =
      $grpc.ClientMethod<$0.BannerRequest, $0.BannerList>(
          '/promotion_service.v1.promotionService/ListBanner',
          ($0.BannerRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.BannerList.fromBuffer(value));
  static final _$deleteBanner =
      $grpc.ClientMethod<$0.BannerRequest, $0.PromotionsResponse>(
          '/promotion_service.v1.promotionService/DeleteBanner',
          ($0.BannerRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.PromotionsResponse.fromBuffer(value));

  promotionServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.PromotionsResponse> addBanner($0.BannerInfo request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addBanner, request, options: options);
  }

  $grpc.ResponseFuture<$0.BannerList> listBanner($0.BannerRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listBanner, request, options: options);
  }

  $grpc.ResponseFuture<$0.PromotionsResponse> deleteBanner(
      $0.BannerRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteBanner, request, options: options);
  }
}

abstract class promotionServiceBase extends $grpc.Service {
  $core.String get $name => 'promotion_service.v1.promotionService';

  promotionServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.BannerInfo, $0.PromotionsResponse>(
        'AddBanner',
        addBanner_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.BannerInfo.fromBuffer(value),
        ($0.PromotionsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.BannerRequest, $0.BannerList>(
        'ListBanner',
        listBanner_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.BannerRequest.fromBuffer(value),
        ($0.BannerList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.BannerRequest, $0.PromotionsResponse>(
        'DeleteBanner',
        deleteBanner_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.BannerRequest.fromBuffer(value),
        ($0.PromotionsResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.PromotionsResponse> addBanner_Pre(
      $grpc.ServiceCall call, $async.Future<$0.BannerInfo> request) async {
    return addBanner(call, await request);
  }

  $async.Future<$0.BannerList> listBanner_Pre(
      $grpc.ServiceCall call, $async.Future<$0.BannerRequest> request) async {
    return listBanner(call, await request);
  }

  $async.Future<$0.PromotionsResponse> deleteBanner_Pre(
      $grpc.ServiceCall call, $async.Future<$0.BannerRequest> request) async {
    return deleteBanner(call, await request);
  }

  $async.Future<$0.PromotionsResponse> addBanner(
      $grpc.ServiceCall call, $0.BannerInfo request);
  $async.Future<$0.BannerList> listBanner(
      $grpc.ServiceCall call, $0.BannerRequest request);
  $async.Future<$0.PromotionsResponse> deleteBanner(
      $grpc.ServiceCall call, $0.BannerRequest request);
}
