///
//  Generated code. Do not modify.
//  source: payment.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use initializePaymentRequestDescriptor instead')
const InitializePaymentRequest$json = const {
  '1': 'InitializePaymentRequest',
  '2': const [
    const {'1': 'amount', '3': 3, '4': 1, '5': 2, '10': 'amount'},
    const {'1': 'currency', '3': 4, '4': 1, '5': 9, '10': 'currency'},
  ],
};

/// Descriptor for `InitializePaymentRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List initializePaymentRequestDescriptor = $convert.base64Decode('ChhJbml0aWFsaXplUGF5bWVudFJlcXVlc3QSFgoGYW1vdW50GAMgASgCUgZhbW91bnQSGgoIY3VycmVuY3kYBCABKAlSCGN1cnJlbmN5');
@$core.Deprecated('Use initializePaymentResponseDescriptor instead')
const InitializePaymentResponse$json = const {
  '1': 'InitializePaymentResponse',
  '2': const [
    const {'1': 'reference', '3': 1, '4': 1, '5': 9, '10': 'reference'},
    const {'1': 'checkoutUrl', '3': 2, '4': 1, '5': 9, '10': 'checkoutUrl'},
  ],
};

/// Descriptor for `InitializePaymentResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List initializePaymentResponseDescriptor = $convert.base64Decode('ChlJbml0aWFsaXplUGF5bWVudFJlc3BvbnNlEhwKCXJlZmVyZW5jZRgBIAEoCVIJcmVmZXJlbmNlEiAKC2NoZWNrb3V0VXJsGAIgASgJUgtjaGVja291dFVybA==');
@$core.Deprecated('Use verifyPaymentRequestDescriptor instead')
const VerifyPaymentRequest$json = const {
  '1': 'VerifyPaymentRequest',
  '2': const [
    const {'1': 'reference', '3': 1, '4': 1, '5': 9, '10': 'reference'},
  ],
};

/// Descriptor for `VerifyPaymentRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verifyPaymentRequestDescriptor = $convert.base64Decode('ChRWZXJpZnlQYXltZW50UmVxdWVzdBIcCglyZWZlcmVuY2UYASABKAlSCXJlZmVyZW5jZQ==');
@$core.Deprecated('Use verifyPaymentResponseDescriptor instead')
const VerifyPaymentResponse$json = const {
  '1': 'VerifyPaymentResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'reference', '3': 2, '4': 1, '5': 9, '10': 'reference'},
    const {'1': 'channel', '3': 3, '4': 1, '5': 9, '10': 'channel'},
  ],
};

/// Descriptor for `VerifyPaymentResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verifyPaymentResponseDescriptor = $convert.base64Decode('ChVWZXJpZnlQYXltZW50UmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSHAoJcmVmZXJlbmNlGAIgASgJUglyZWZlcmVuY2USGAoHY2hhbm5lbBgDIAEoCVIHY2hhbm5lbA==');
