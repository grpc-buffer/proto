///
//  Generated code. Do not modify.
//  source: restaurant.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'restaurant.pb.dart' as $0;
export 'restaurant.pb.dart';

class RestaurantServiceClient extends $grpc.Client {
  static final _$createRestaurant =
      $grpc.ClientMethod<$0.RestaurantItems, $0.RestaurantResponse>(
          '/restaurant_service.v1.RestaurantService/CreateRestaurant',
          ($0.RestaurantItems value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RestaurantResponse.fromBuffer(value));
  static final _$addRestaurantCategory = $grpc.ClientMethod<
          $0.RestaurantCategoryRequest, $0.RestaurantCategoryResponse>(
      '/restaurant_service.v1.RestaurantService/AddRestaurantCategory',
      ($0.RestaurantCategoryRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.RestaurantCategoryResponse.fromBuffer(value));
  static final _$updateRestaurant =
      $grpc.ClientMethod<$0.RestaurantItems, $0.RestaurantResponse>(
          '/restaurant_service.v1.RestaurantService/UpdateRestaurant',
          ($0.RestaurantItems value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RestaurantResponse.fromBuffer(value));
  static final _$viewRestaurant =
      $grpc.ClientMethod<$0.RestaurantRequestItem, $0.ViewRestaurantResponse>(
          '/restaurant_service.v1.RestaurantService/ViewRestaurant',
          ($0.RestaurantRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ViewRestaurantResponse.fromBuffer(value));
  static final _$viewCategory =
      $grpc.ClientMethod<$0.ViewCategoryRequest, $0.RestaurantCategoryResponse>(
          '/restaurant_service.v1.RestaurantService/ViewCategory',
          ($0.ViewCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RestaurantCategoryResponse.fromBuffer(value));
  static final _$deleteRestaurant =
      $grpc.ClientMethod<$0.RestaurantRequestItem, $0.RestaurantResponse>(
          '/restaurant_service.v1.RestaurantService/DeleteRestaurant',
          ($0.RestaurantRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RestaurantResponse.fromBuffer(value));
  static final _$listRestaurants =
      $grpc.ClientMethod<$0.ListRequest, $0.ListRestaurantResponse>(
          '/restaurant_service.v1.RestaurantService/ListRestaurants',
          ($0.ListRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListRestaurantResponse.fromBuffer(value));
  static final _$listRestaurantsCategories =
      $grpc.ClientMethod<$0.RestaurantRequestItem, $0.ListCategoriesResponse>(
          '/restaurant_service.v1.RestaurantService/ListRestaurantsCategories',
          ($0.RestaurantRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListCategoriesResponse.fromBuffer(value));
  static final _$updateSchedule =
      $grpc.ClientMethod<$0.ScheduleItem, $0.ScheduleResponse>(
          '/restaurant_service.v1.RestaurantService/UpdateSchedule',
          ($0.ScheduleItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ScheduleResponse.fromBuffer(value));
  static final _$addSchedule =
      $grpc.ClientMethod<$0.ScheduleRequest, $0.ScheduleResponse>(
          '/restaurant_service.v1.RestaurantService/AddSchedule',
          ($0.ScheduleRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ScheduleResponse.fromBuffer(value));
  static final _$deleteSchedule =
      $grpc.ClientMethod<$0.ScheduleRequestItem, $0.ScheduleResponse>(
          '/restaurant_service.v1.RestaurantService/DeleteSchedule',
          ($0.ScheduleRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ScheduleResponse.fromBuffer(value));
  static final _$viewSchedule =
      $grpc.ClientMethod<$0.ScheduleRequestItem, $0.ViewScheduleResponse>(
          '/restaurant_service.v1.RestaurantService/ViewSchedule',
          ($0.ScheduleRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ViewScheduleResponse.fromBuffer(value));
  static final _$listSchedule =
      $grpc.ClientMethod<$0.EmptySchedule, $0.ListScheduleResponse>(
          '/restaurant_service.v1.RestaurantService/ListSchedule',
          ($0.EmptySchedule value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListScheduleResponse.fromBuffer(value));
  static final _$listScheduleByRestaurant =
      $grpc.ClientMethod<$0.ScheduleRequestItem, $0.ListScheduleResponse>(
          '/restaurant_service.v1.RestaurantService/ListScheduleByRestaurant',
          ($0.ScheduleRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListScheduleResponse.fromBuffer(value));
  static final _$updateTag = $grpc.ClientMethod<$0.TagItem, $0.TagResponse>(
      '/restaurant_service.v1.RestaurantService/UpdateTag',
      ($0.TagItem value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.TagResponse.fromBuffer(value));
  static final _$addTag = $grpc.ClientMethod<$0.TagRequest, $0.TagResponse>(
      '/restaurant_service.v1.RestaurantService/AddTag',
      ($0.TagRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.TagResponse.fromBuffer(value));
  static final _$viewTag = $grpc.ClientMethod<$0.TagRequestItem, $0.TagItem>(
      '/restaurant_service.v1.RestaurantService/ViewTag',
      ($0.TagRequestItem value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.TagItem.fromBuffer(value));
  static final _$deleteTag =
      $grpc.ClientMethod<$0.TagRequestItem, $0.TagResponse>(
          '/restaurant_service.v1.RestaurantService/DeleteTag',
          ($0.TagRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.TagResponse.fromBuffer(value));
  static final _$listTag = $grpc.ClientMethod<$0.EmptyTag, $0.ListTagResponse>(
      '/restaurant_service.v1.RestaurantService/ListTag',
      ($0.EmptyTag value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ListTagResponse.fromBuffer(value));
  static final _$listTagByRestaurant =
      $grpc.ClientMethod<$0.TagRequestItem, $0.ListTagResponse>(
          '/restaurant_service.v1.RestaurantService/ListTagByRestaurant',
          ($0.TagRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListTagResponse.fromBuffer(value));

  RestaurantServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.RestaurantResponse> createRestaurant(
      $0.RestaurantItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createRestaurant, request, options: options);
  }

  $grpc.ResponseFuture<$0.RestaurantCategoryResponse> addRestaurantCategory(
      $0.RestaurantCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addRestaurantCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.RestaurantResponse> updateRestaurant(
      $0.RestaurantItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateRestaurant, request, options: options);
  }

  $grpc.ResponseFuture<$0.ViewRestaurantResponse> viewRestaurant(
      $0.RestaurantRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewRestaurant, request, options: options);
  }

  $grpc.ResponseFuture<$0.RestaurantCategoryResponse> viewCategory(
      $0.ViewCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.RestaurantResponse> deleteRestaurant(
      $0.RestaurantRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteRestaurant, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListRestaurantResponse> listRestaurants(
      $0.ListRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listRestaurants, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListCategoriesResponse> listRestaurantsCategories(
      $0.RestaurantRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listRestaurantsCategories, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.ScheduleResponse> updateSchedule(
      $0.ScheduleItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateSchedule, request, options: options);
  }

  $grpc.ResponseFuture<$0.ScheduleResponse> addSchedule(
      $0.ScheduleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addSchedule, request, options: options);
  }

  $grpc.ResponseFuture<$0.ScheduleResponse> deleteSchedule(
      $0.ScheduleRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteSchedule, request, options: options);
  }

  $grpc.ResponseFuture<$0.ViewScheduleResponse> viewSchedule(
      $0.ScheduleRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewSchedule, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListScheduleResponse> listSchedule(
      $0.EmptySchedule request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listSchedule, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListScheduleResponse> listScheduleByRestaurant(
      $0.ScheduleRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listScheduleByRestaurant, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.TagResponse> updateTag($0.TagItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.TagResponse> addTag($0.TagRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.TagItem> viewTag($0.TagRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.TagResponse> deleteTag($0.TagRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListTagResponse> listTag($0.EmptyTag request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListTagResponse> listTagByRestaurant(
      $0.TagRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listTagByRestaurant, request, options: options);
  }
}

abstract class RestaurantServiceBase extends $grpc.Service {
  $core.String get $name => 'restaurant_service.v1.RestaurantService';

  RestaurantServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.RestaurantItems, $0.RestaurantResponse>(
        'CreateRestaurant',
        createRestaurant_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RestaurantItems.fromBuffer(value),
        ($0.RestaurantResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RestaurantCategoryRequest,
            $0.RestaurantCategoryResponse>(
        'AddRestaurantCategory',
        addRestaurantCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.RestaurantCategoryRequest.fromBuffer(value),
        ($0.RestaurantCategoryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RestaurantItems, $0.RestaurantResponse>(
        'UpdateRestaurant',
        updateRestaurant_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RestaurantItems.fromBuffer(value),
        ($0.RestaurantResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RestaurantRequestItem,
            $0.ViewRestaurantResponse>(
        'ViewRestaurant',
        viewRestaurant_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.RestaurantRequestItem.fromBuffer(value),
        ($0.ViewRestaurantResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ViewCategoryRequest,
            $0.RestaurantCategoryResponse>(
        'ViewCategory',
        viewCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ViewCategoryRequest.fromBuffer(value),
        ($0.RestaurantCategoryResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.RestaurantRequestItem, $0.RestaurantResponse>(
            'DeleteRestaurant',
            deleteRestaurant_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.RestaurantRequestItem.fromBuffer(value),
            ($0.RestaurantResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ListRequest, $0.ListRestaurantResponse>(
        'ListRestaurants',
        listRestaurants_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ListRequest.fromBuffer(value),
        ($0.ListRestaurantResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RestaurantRequestItem,
            $0.ListCategoriesResponse>(
        'ListRestaurantsCategories',
        listRestaurantsCategories_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.RestaurantRequestItem.fromBuffer(value),
        ($0.ListCategoriesResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ScheduleItem, $0.ScheduleResponse>(
        'UpdateSchedule',
        updateSchedule_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ScheduleItem.fromBuffer(value),
        ($0.ScheduleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ScheduleRequest, $0.ScheduleResponse>(
        'AddSchedule',
        addSchedule_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ScheduleRequest.fromBuffer(value),
        ($0.ScheduleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ScheduleRequestItem, $0.ScheduleResponse>(
        'DeleteSchedule',
        deleteSchedule_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ScheduleRequestItem.fromBuffer(value),
        ($0.ScheduleResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.ScheduleRequestItem, $0.ViewScheduleResponse>(
            'ViewSchedule',
            viewSchedule_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.ScheduleRequestItem.fromBuffer(value),
            ($0.ViewScheduleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptySchedule, $0.ListScheduleResponse>(
        'ListSchedule',
        listSchedule_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EmptySchedule.fromBuffer(value),
        ($0.ListScheduleResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.ScheduleRequestItem, $0.ListScheduleResponse>(
            'ListScheduleByRestaurant',
            listScheduleByRestaurant_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.ScheduleRequestItem.fromBuffer(value),
            ($0.ListScheduleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TagItem, $0.TagResponse>(
        'UpdateTag',
        updateTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TagItem.fromBuffer(value),
        ($0.TagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TagRequest, $0.TagResponse>(
        'AddTag',
        addTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TagRequest.fromBuffer(value),
        ($0.TagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TagRequestItem, $0.TagItem>(
        'ViewTag',
        viewTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TagRequestItem.fromBuffer(value),
        ($0.TagItem value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TagRequestItem, $0.TagResponse>(
        'DeleteTag',
        deleteTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TagRequestItem.fromBuffer(value),
        ($0.TagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyTag, $0.ListTagResponse>(
        'ListTag',
        listTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EmptyTag.fromBuffer(value),
        ($0.ListTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TagRequestItem, $0.ListTagResponse>(
        'ListTagByRestaurant',
        listTagByRestaurant_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TagRequestItem.fromBuffer(value),
        ($0.ListTagResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.RestaurantResponse> createRestaurant_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RestaurantItems> request) async {
    return createRestaurant(call, await request);
  }

  $async.Future<$0.RestaurantCategoryResponse> addRestaurantCategory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.RestaurantCategoryRequest> request) async {
    return addRestaurantCategory(call, await request);
  }

  $async.Future<$0.RestaurantResponse> updateRestaurant_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RestaurantItems> request) async {
    return updateRestaurant(call, await request);
  }

  $async.Future<$0.ViewRestaurantResponse> viewRestaurant_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.RestaurantRequestItem> request) async {
    return viewRestaurant(call, await request);
  }

  $async.Future<$0.RestaurantCategoryResponse> viewCategory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ViewCategoryRequest> request) async {
    return viewCategory(call, await request);
  }

  $async.Future<$0.RestaurantResponse> deleteRestaurant_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.RestaurantRequestItem> request) async {
    return deleteRestaurant(call, await request);
  }

  $async.Future<$0.ListRestaurantResponse> listRestaurants_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ListRequest> request) async {
    return listRestaurants(call, await request);
  }

  $async.Future<$0.ListCategoriesResponse> listRestaurantsCategories_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.RestaurantRequestItem> request) async {
    return listRestaurantsCategories(call, await request);
  }

  $async.Future<$0.ScheduleResponse> updateSchedule_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ScheduleItem> request) async {
    return updateSchedule(call, await request);
  }

  $async.Future<$0.ScheduleResponse> addSchedule_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ScheduleRequest> request) async {
    return addSchedule(call, await request);
  }

  $async.Future<$0.ScheduleResponse> deleteSchedule_Pre($grpc.ServiceCall call,
      $async.Future<$0.ScheduleRequestItem> request) async {
    return deleteSchedule(call, await request);
  }

  $async.Future<$0.ViewScheduleResponse> viewSchedule_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ScheduleRequestItem> request) async {
    return viewSchedule(call, await request);
  }

  $async.Future<$0.ListScheduleResponse> listSchedule_Pre(
      $grpc.ServiceCall call, $async.Future<$0.EmptySchedule> request) async {
    return listSchedule(call, await request);
  }

  $async.Future<$0.ListScheduleResponse> listScheduleByRestaurant_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ScheduleRequestItem> request) async {
    return listScheduleByRestaurant(call, await request);
  }

  $async.Future<$0.TagResponse> updateTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TagItem> request) async {
    return updateTag(call, await request);
  }

  $async.Future<$0.TagResponse> addTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TagRequest> request) async {
    return addTag(call, await request);
  }

  $async.Future<$0.TagItem> viewTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TagRequestItem> request) async {
    return viewTag(call, await request);
  }

  $async.Future<$0.TagResponse> deleteTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TagRequestItem> request) async {
    return deleteTag(call, await request);
  }

  $async.Future<$0.ListTagResponse> listTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.EmptyTag> request) async {
    return listTag(call, await request);
  }

  $async.Future<$0.ListTagResponse> listTagByRestaurant_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TagRequestItem> request) async {
    return listTagByRestaurant(call, await request);
  }

  $async.Future<$0.RestaurantResponse> createRestaurant(
      $grpc.ServiceCall call, $0.RestaurantItems request);
  $async.Future<$0.RestaurantCategoryResponse> addRestaurantCategory(
      $grpc.ServiceCall call, $0.RestaurantCategoryRequest request);
  $async.Future<$0.RestaurantResponse> updateRestaurant(
      $grpc.ServiceCall call, $0.RestaurantItems request);
  $async.Future<$0.ViewRestaurantResponse> viewRestaurant(
      $grpc.ServiceCall call, $0.RestaurantRequestItem request);
  $async.Future<$0.RestaurantCategoryResponse> viewCategory(
      $grpc.ServiceCall call, $0.ViewCategoryRequest request);
  $async.Future<$0.RestaurantResponse> deleteRestaurant(
      $grpc.ServiceCall call, $0.RestaurantRequestItem request);
  $async.Future<$0.ListRestaurantResponse> listRestaurants(
      $grpc.ServiceCall call, $0.ListRequest request);
  $async.Future<$0.ListCategoriesResponse> listRestaurantsCategories(
      $grpc.ServiceCall call, $0.RestaurantRequestItem request);
  $async.Future<$0.ScheduleResponse> updateSchedule(
      $grpc.ServiceCall call, $0.ScheduleItem request);
  $async.Future<$0.ScheduleResponse> addSchedule(
      $grpc.ServiceCall call, $0.ScheduleRequest request);
  $async.Future<$0.ScheduleResponse> deleteSchedule(
      $grpc.ServiceCall call, $0.ScheduleRequestItem request);
  $async.Future<$0.ViewScheduleResponse> viewSchedule(
      $grpc.ServiceCall call, $0.ScheduleRequestItem request);
  $async.Future<$0.ListScheduleResponse> listSchedule(
      $grpc.ServiceCall call, $0.EmptySchedule request);
  $async.Future<$0.ListScheduleResponse> listScheduleByRestaurant(
      $grpc.ServiceCall call, $0.ScheduleRequestItem request);
  $async.Future<$0.TagResponse> updateTag(
      $grpc.ServiceCall call, $0.TagItem request);
  $async.Future<$0.TagResponse> addTag(
      $grpc.ServiceCall call, $0.TagRequest request);
  $async.Future<$0.TagItem> viewTag(
      $grpc.ServiceCall call, $0.TagRequestItem request);
  $async.Future<$0.TagResponse> deleteTag(
      $grpc.ServiceCall call, $0.TagRequestItem request);
  $async.Future<$0.ListTagResponse> listTag(
      $grpc.ServiceCall call, $0.EmptyTag request);
  $async.Future<$0.ListTagResponse> listTagByRestaurant(
      $grpc.ServiceCall call, $0.TagRequestItem request);
}
