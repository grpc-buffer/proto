///
//  Generated code. Do not modify.
//  source: search.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class InSearchItemSuggestionsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchItemSuggestionsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  InSearchItemSuggestionsRequest._() : super();
  factory InSearchItemSuggestionsRequest({
    $core.String? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory InSearchItemSuggestionsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchItemSuggestionsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchItemSuggestionsRequest clone() => InSearchItemSuggestionsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchItemSuggestionsRequest copyWith(void Function(InSearchItemSuggestionsRequest) updates) => super.copyWith((message) => updates(message as InSearchItemSuggestionsRequest)) as InSearchItemSuggestionsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchItemSuggestionsRequest create() => InSearchItemSuggestionsRequest._();
  InSearchItemSuggestionsRequest createEmptyInstance() => create();
  static $pb.PbList<InSearchItemSuggestionsRequest> createRepeated() => $pb.PbList<InSearchItemSuggestionsRequest>();
  @$core.pragma('dart2js:noInline')
  static InSearchItemSuggestionsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchItemSuggestionsRequest>(create);
  static InSearchItemSuggestionsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class InSearchItemSuggestionsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchItemSuggestionsResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'suggestions')
    ..hasRequiredFields = false
  ;

  InSearchItemSuggestionsResponse._() : super();
  factory InSearchItemSuggestionsResponse({
    $core.String? status,
    $core.Iterable<$core.String>? suggestions,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (suggestions != null) {
      _result.suggestions.addAll(suggestions);
    }
    return _result;
  }
  factory InSearchItemSuggestionsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchItemSuggestionsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchItemSuggestionsResponse clone() => InSearchItemSuggestionsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchItemSuggestionsResponse copyWith(void Function(InSearchItemSuggestionsResponse) updates) => super.copyWith((message) => updates(message as InSearchItemSuggestionsResponse)) as InSearchItemSuggestionsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchItemSuggestionsResponse create() => InSearchItemSuggestionsResponse._();
  InSearchItemSuggestionsResponse createEmptyInstance() => create();
  static $pb.PbList<InSearchItemSuggestionsResponse> createRepeated() => $pb.PbList<InSearchItemSuggestionsResponse>();
  @$core.pragma('dart2js:noInline')
  static InSearchItemSuggestionsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchItemSuggestionsResponse>(create);
  static InSearchItemSuggestionsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get suggestions => $_getList(1);
}

class InSearchPackageSuggestionsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchPackageSuggestionsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  InSearchPackageSuggestionsRequest._() : super();
  factory InSearchPackageSuggestionsRequest({
    $core.String? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory InSearchPackageSuggestionsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchPackageSuggestionsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchPackageSuggestionsRequest clone() => InSearchPackageSuggestionsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchPackageSuggestionsRequest copyWith(void Function(InSearchPackageSuggestionsRequest) updates) => super.copyWith((message) => updates(message as InSearchPackageSuggestionsRequest)) as InSearchPackageSuggestionsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchPackageSuggestionsRequest create() => InSearchPackageSuggestionsRequest._();
  InSearchPackageSuggestionsRequest createEmptyInstance() => create();
  static $pb.PbList<InSearchPackageSuggestionsRequest> createRepeated() => $pb.PbList<InSearchPackageSuggestionsRequest>();
  @$core.pragma('dart2js:noInline')
  static InSearchPackageSuggestionsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchPackageSuggestionsRequest>(create);
  static InSearchPackageSuggestionsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class InSearchPackageSuggestionsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchPackageSuggestionsResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'suggestions')
    ..hasRequiredFields = false
  ;

  InSearchPackageSuggestionsResponse._() : super();
  factory InSearchPackageSuggestionsResponse({
    $core.String? status,
    $core.Iterable<$core.String>? suggestions,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (suggestions != null) {
      _result.suggestions.addAll(suggestions);
    }
    return _result;
  }
  factory InSearchPackageSuggestionsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchPackageSuggestionsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchPackageSuggestionsResponse clone() => InSearchPackageSuggestionsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchPackageSuggestionsResponse copyWith(void Function(InSearchPackageSuggestionsResponse) updates) => super.copyWith((message) => updates(message as InSearchPackageSuggestionsResponse)) as InSearchPackageSuggestionsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchPackageSuggestionsResponse create() => InSearchPackageSuggestionsResponse._();
  InSearchPackageSuggestionsResponse createEmptyInstance() => create();
  static $pb.PbList<InSearchPackageSuggestionsResponse> createRepeated() => $pb.PbList<InSearchPackageSuggestionsResponse>();
  @$core.pragma('dart2js:noInline')
  static InSearchPackageSuggestionsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchPackageSuggestionsResponse>(create);
  static InSearchPackageSuggestionsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get suggestions => $_getList(1);
}

class InSearchItemRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchItemRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  InSearchItemRequest._() : super();
  factory InSearchItemRequest({
    $core.String? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory InSearchItemRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchItemRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchItemRequest clone() => InSearchItemRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchItemRequest copyWith(void Function(InSearchItemRequest) updates) => super.copyWith((message) => updates(message as InSearchItemRequest)) as InSearchItemRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchItemRequest create() => InSearchItemRequest._();
  InSearchItemRequest createEmptyInstance() => create();
  static $pb.PbList<InSearchItemRequest> createRepeated() => $pb.PbList<InSearchItemRequest>();
  @$core.pragma('dart2js:noInline')
  static InSearchItemRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchItemRequest>(create);
  static InSearchItemRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class InSearchItemResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchItemResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pc<SearchItem>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemResponse', $pb.PbFieldType.PM, subBuilder: SearchItem.create)
    ..hasRequiredFields = false
  ;

  InSearchItemResponse._() : super();
  factory InSearchItemResponse({
    $core.String? status,
    $core.Iterable<SearchItem>? itemResponse,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (itemResponse != null) {
      _result.itemResponse.addAll(itemResponse);
    }
    return _result;
  }
  factory InSearchItemResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchItemResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchItemResponse clone() => InSearchItemResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchItemResponse copyWith(void Function(InSearchItemResponse) updates) => super.copyWith((message) => updates(message as InSearchItemResponse)) as InSearchItemResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchItemResponse create() => InSearchItemResponse._();
  InSearchItemResponse createEmptyInstance() => create();
  static $pb.PbList<InSearchItemResponse> createRepeated() => $pb.PbList<InSearchItemResponse>();
  @$core.pragma('dart2js:noInline')
  static InSearchItemResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchItemResponse>(create);
  static InSearchItemResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<SearchItem> get itemResponse => $_getList(1);
}

class InSearchPackageRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchPackageRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  InSearchPackageRequest._() : super();
  factory InSearchPackageRequest({
    $core.String? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory InSearchPackageRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchPackageRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchPackageRequest clone() => InSearchPackageRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchPackageRequest copyWith(void Function(InSearchPackageRequest) updates) => super.copyWith((message) => updates(message as InSearchPackageRequest)) as InSearchPackageRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchPackageRequest create() => InSearchPackageRequest._();
  InSearchPackageRequest createEmptyInstance() => create();
  static $pb.PbList<InSearchPackageRequest> createRepeated() => $pb.PbList<InSearchPackageRequest>();
  @$core.pragma('dart2js:noInline')
  static InSearchPackageRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchPackageRequest>(create);
  static InSearchPackageRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class InSearchPackageResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchPackageResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pc<SearchPackage>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageResponse', $pb.PbFieldType.PM, subBuilder: SearchPackage.create)
    ..hasRequiredFields = false
  ;

  InSearchPackageResponse._() : super();
  factory InSearchPackageResponse({
    $core.String? status,
    $core.Iterable<SearchPackage>? packageResponse,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (packageResponse != null) {
      _result.packageResponse.addAll(packageResponse);
    }
    return _result;
  }
  factory InSearchPackageResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchPackageResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchPackageResponse clone() => InSearchPackageResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchPackageResponse copyWith(void Function(InSearchPackageResponse) updates) => super.copyWith((message) => updates(message as InSearchPackageResponse)) as InSearchPackageResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchPackageResponse create() => InSearchPackageResponse._();
  InSearchPackageResponse createEmptyInstance() => create();
  static $pb.PbList<InSearchPackageResponse> createRepeated() => $pb.PbList<InSearchPackageResponse>();
  @$core.pragma('dart2js:noInline')
  static InSearchPackageResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchPackageResponse>(create);
  static InSearchPackageResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<SearchPackage> get packageResponse => $_getList(1);
}

class InSearchSuggestionsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchSuggestionsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'index')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  InSearchSuggestionsRequest._() : super();
  factory InSearchSuggestionsRequest({
    $core.String? index,
    $core.String? value,
  }) {
    final _result = create();
    if (index != null) {
      _result.index = index;
    }
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory InSearchSuggestionsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchSuggestionsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchSuggestionsRequest clone() => InSearchSuggestionsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchSuggestionsRequest copyWith(void Function(InSearchSuggestionsRequest) updates) => super.copyWith((message) => updates(message as InSearchSuggestionsRequest)) as InSearchSuggestionsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchSuggestionsRequest create() => InSearchSuggestionsRequest._();
  InSearchSuggestionsRequest createEmptyInstance() => create();
  static $pb.PbList<InSearchSuggestionsRequest> createRepeated() => $pb.PbList<InSearchSuggestionsRequest>();
  @$core.pragma('dart2js:noInline')
  static InSearchSuggestionsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchSuggestionsRequest>(create);
  static InSearchSuggestionsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get index => $_getSZ(0);
  @$pb.TagNumber(1)
  set index($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIndex() => $_has(0);
  @$pb.TagNumber(1)
  void clearIndex() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get value => $_getSZ(1);
  @$pb.TagNumber(2)
  set value($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
}

class InSearchSuggestionsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchSuggestionsResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'suggestions')
    ..hasRequiredFields = false
  ;

  InSearchSuggestionsResponse._() : super();
  factory InSearchSuggestionsResponse({
    $core.String? status,
    $core.Iterable<$core.String>? suggestions,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (suggestions != null) {
      _result.suggestions.addAll(suggestions);
    }
    return _result;
  }
  factory InSearchSuggestionsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchSuggestionsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchSuggestionsResponse clone() => InSearchSuggestionsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchSuggestionsResponse copyWith(void Function(InSearchSuggestionsResponse) updates) => super.copyWith((message) => updates(message as InSearchSuggestionsResponse)) as InSearchSuggestionsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchSuggestionsResponse create() => InSearchSuggestionsResponse._();
  InSearchSuggestionsResponse createEmptyInstance() => create();
  static $pb.PbList<InSearchSuggestionsResponse> createRepeated() => $pb.PbList<InSearchSuggestionsResponse>();
  @$core.pragma('dart2js:noInline')
  static InSearchSuggestionsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchSuggestionsResponse>(create);
  static InSearchSuggestionsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get suggestions => $_getList(1);
}

class InSearchRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'index')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  InSearchRequest._() : super();
  factory InSearchRequest({
    $core.String? index,
    $core.String? value,
  }) {
    final _result = create();
    if (index != null) {
      _result.index = index;
    }
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory InSearchRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchRequest clone() => InSearchRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchRequest copyWith(void Function(InSearchRequest) updates) => super.copyWith((message) => updates(message as InSearchRequest)) as InSearchRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchRequest create() => InSearchRequest._();
  InSearchRequest createEmptyInstance() => create();
  static $pb.PbList<InSearchRequest> createRepeated() => $pb.PbList<InSearchRequest>();
  @$core.pragma('dart2js:noInline')
  static InSearchRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchRequest>(create);
  static InSearchRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get index => $_getSZ(0);
  @$pb.TagNumber(1)
  set index($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIndex() => $_has(0);
  @$pb.TagNumber(1)
  void clearIndex() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get value => $_getSZ(1);
  @$pb.TagNumber(2)
  set value($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
}

class InSearchResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InSearchResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pc<SearchPackage>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageResponse', $pb.PbFieldType.PM, subBuilder: SearchPackage.create)
    ..pc<SearchItem>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemResponse', $pb.PbFieldType.PM, subBuilder: SearchItem.create)
    ..hasRequiredFields = false
  ;

  InSearchResponse._() : super();
  factory InSearchResponse({
    $core.String? status,
    $core.Iterable<SearchPackage>? packageResponse,
    $core.Iterable<SearchItem>? itemResponse,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (packageResponse != null) {
      _result.packageResponse.addAll(packageResponse);
    }
    if (itemResponse != null) {
      _result.itemResponse.addAll(itemResponse);
    }
    return _result;
  }
  factory InSearchResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InSearchResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InSearchResponse clone() => InSearchResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InSearchResponse copyWith(void Function(InSearchResponse) updates) => super.copyWith((message) => updates(message as InSearchResponse)) as InSearchResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InSearchResponse create() => InSearchResponse._();
  InSearchResponse createEmptyInstance() => create();
  static $pb.PbList<InSearchResponse> createRepeated() => $pb.PbList<InSearchResponse>();
  @$core.pragma('dart2js:noInline')
  static InSearchResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InSearchResponse>(create);
  static InSearchResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<SearchPackage> get packageResponse => $_getList(1);

  @$pb.TagNumber(3)
  $core.List<SearchItem> get itemResponse => $_getList(2);
}

class SearchPackage extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SearchPackage', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt', protoName: 'createdAt')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt', protoName: 'updatedAt')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'images')
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'estimatedPrice', $pb.PbFieldType.OD, protoName: 'estimatedPrice')
    ..a<$core.double>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxNumberOfItems', $pb.PbFieldType.OD, protoName: 'maxNumberOfItems')
    ..a<$core.double>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minNumberOfItems', $pb.PbFieldType.OD, protoName: 'minNumberOfItems')
    ..pc<SearchPackageItem>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: SearchPackageItem.create)
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categoryID', protoName: 'categoryID')
    ..aOS(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..hasRequiredFields = false
  ;

  SearchPackage._() : super();
  factory SearchPackage({
    $core.String? name,
    $core.String? description,
    $core.String? createdAt,
    $core.String? updatedAt,
    $core.String? images,
    $core.double? estimatedPrice,
    $core.double? maxNumberOfItems,
    $core.double? minNumberOfItems,
    $core.Iterable<SearchPackageItem>? items,
    $core.String? packageID,
    $core.String? categoryID,
    $core.String? serviceAreaID,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (description != null) {
      _result.description = description;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    if (images != null) {
      _result.images = images;
    }
    if (estimatedPrice != null) {
      _result.estimatedPrice = estimatedPrice;
    }
    if (maxNumberOfItems != null) {
      _result.maxNumberOfItems = maxNumberOfItems;
    }
    if (minNumberOfItems != null) {
      _result.minNumberOfItems = minNumberOfItems;
    }
    if (items != null) {
      _result.items.addAll(items);
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (categoryID != null) {
      _result.categoryID = categoryID;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    return _result;
  }
  factory SearchPackage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchPackage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SearchPackage clone() => SearchPackage()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SearchPackage copyWith(void Function(SearchPackage) updates) => super.copyWith((message) => updates(message as SearchPackage)) as SearchPackage; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchPackage create() => SearchPackage._();
  SearchPackage createEmptyInstance() => create();
  static $pb.PbList<SearchPackage> createRepeated() => $pb.PbList<SearchPackage>();
  @$core.pragma('dart2js:noInline')
  static SearchPackage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchPackage>(create);
  static SearchPackage? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get description => $_getSZ(1);
  @$pb.TagNumber(2)
  set description($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDescription() => $_has(1);
  @$pb.TagNumber(2)
  void clearDescription() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get createdAt => $_getSZ(2);
  @$pb.TagNumber(3)
  set createdAt($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCreatedAt() => $_has(2);
  @$pb.TagNumber(3)
  void clearCreatedAt() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get updatedAt => $_getSZ(3);
  @$pb.TagNumber(4)
  set updatedAt($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasUpdatedAt() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpdatedAt() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get images => $_getSZ(4);
  @$pb.TagNumber(5)
  set images($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasImages() => $_has(4);
  @$pb.TagNumber(5)
  void clearImages() => clearField(5);

  @$pb.TagNumber(6)
  $core.double get estimatedPrice => $_getN(5);
  @$pb.TagNumber(6)
  set estimatedPrice($core.double v) { $_setDouble(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasEstimatedPrice() => $_has(5);
  @$pb.TagNumber(6)
  void clearEstimatedPrice() => clearField(6);

  @$pb.TagNumber(7)
  $core.double get maxNumberOfItems => $_getN(6);
  @$pb.TagNumber(7)
  set maxNumberOfItems($core.double v) { $_setDouble(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasMaxNumberOfItems() => $_has(6);
  @$pb.TagNumber(7)
  void clearMaxNumberOfItems() => clearField(7);

  @$pb.TagNumber(8)
  $core.double get minNumberOfItems => $_getN(7);
  @$pb.TagNumber(8)
  set minNumberOfItems($core.double v) { $_setDouble(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasMinNumberOfItems() => $_has(7);
  @$pb.TagNumber(8)
  void clearMinNumberOfItems() => clearField(8);

  @$pb.TagNumber(9)
  $core.List<SearchPackageItem> get items => $_getList(8);

  @$pb.TagNumber(10)
  $core.String get packageID => $_getSZ(9);
  @$pb.TagNumber(10)
  set packageID($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasPackageID() => $_has(9);
  @$pb.TagNumber(10)
  void clearPackageID() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get categoryID => $_getSZ(10);
  @$pb.TagNumber(11)
  set categoryID($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasCategoryID() => $_has(10);
  @$pb.TagNumber(11)
  void clearCategoryID() => clearField(11);

  @$pb.TagNumber(12)
  $core.String get serviceAreaID => $_getSZ(11);
  @$pb.TagNumber(12)
  set serviceAreaID($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasServiceAreaID() => $_has(11);
  @$pb.TagNumber(12)
  void clearServiceAreaID() => clearField(12);
}

class SearchImages extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SearchImages', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'imageUrl', protoName: 'imageUrl')
    ..hasRequiredFields = false
  ;

  SearchImages._() : super();
  factory SearchImages({
    $core.String? imageUrl,
  }) {
    final _result = create();
    if (imageUrl != null) {
      _result.imageUrl = imageUrl;
    }
    return _result;
  }
  factory SearchImages.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchImages.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SearchImages clone() => SearchImages()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SearchImages copyWith(void Function(SearchImages) updates) => super.copyWith((message) => updates(message as SearchImages)) as SearchImages; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchImages create() => SearchImages._();
  SearchImages createEmptyInstance() => create();
  static $pb.PbList<SearchImages> createRepeated() => $pb.PbList<SearchImages>();
  @$core.pragma('dart2js:noInline')
  static SearchImages getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchImages>(create);
  static SearchImages? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get imageUrl => $_getSZ(0);
  @$pb.TagNumber(1)
  set imageUrl($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasImageUrl() => $_has(0);
  @$pb.TagNumber(1)
  void clearImageUrl() => clearField(1);
}

class SearchPackageItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SearchPackageItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageItemID', protoName: 'packageItemID')
    ..pc<SearchItemData>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'substitutes', $pb.PbFieldType.PM, subBuilder: SearchItemData.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'category')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..hasRequiredFields = false
  ;

  SearchPackageItem._() : super();
  factory SearchPackageItem({
    $core.String? packageItemID,
    $core.Iterable<SearchItemData>? substitutes,
    $core.String? category,
    $core.String? packageID,
  }) {
    final _result = create();
    if (packageItemID != null) {
      _result.packageItemID = packageItemID;
    }
    if (substitutes != null) {
      _result.substitutes.addAll(substitutes);
    }
    if (category != null) {
      _result.category = category;
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    return _result;
  }
  factory SearchPackageItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchPackageItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SearchPackageItem clone() => SearchPackageItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SearchPackageItem copyWith(void Function(SearchPackageItem) updates) => super.copyWith((message) => updates(message as SearchPackageItem)) as SearchPackageItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchPackageItem create() => SearchPackageItem._();
  SearchPackageItem createEmptyInstance() => create();
  static $pb.PbList<SearchPackageItem> createRepeated() => $pb.PbList<SearchPackageItem>();
  @$core.pragma('dart2js:noInline')
  static SearchPackageItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchPackageItem>(create);
  static SearchPackageItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageItemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageItemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageItemID() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<SearchItemData> get substitutes => $_getList(1);

  @$pb.TagNumber(3)
  $core.String get category => $_getSZ(2);
  @$pb.TagNumber(3)
  set category($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCategory() => $_has(2);
  @$pb.TagNumber(3)
  void clearCategory() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get packageID => $_getSZ(3);
  @$pb.TagNumber(4)
  set packageID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPackageID() => $_has(3);
  @$pb.TagNumber(4)
  void clearPackageID() => clearField(4);
}

class SearchItemData extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SearchItemData', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.OD)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'price', $pb.PbFieldType.OD)
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isRequired', protoName: 'isRequired')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isDefault', protoName: 'isDefault')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'units')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..hasRequiredFields = false
  ;

  SearchItemData._() : super();
  factory SearchItemData({
    $core.String? itemID,
    $core.double? quantity,
    $core.double? price,
    $core.bool? isRequired,
    $core.bool? isDefault,
    $core.String? description,
    $core.String? units,
    $core.String? name,
    $core.String? image,
  }) {
    final _result = create();
    if (itemID != null) {
      _result.itemID = itemID;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (price != null) {
      _result.price = price;
    }
    if (isRequired != null) {
      _result.isRequired = isRequired;
    }
    if (isDefault != null) {
      _result.isDefault = isDefault;
    }
    if (description != null) {
      _result.description = description;
    }
    if (units != null) {
      _result.units = units;
    }
    if (name != null) {
      _result.name = name;
    }
    if (image != null) {
      _result.image = image;
    }
    return _result;
  }
  factory SearchItemData.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchItemData.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SearchItemData clone() => SearchItemData()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SearchItemData copyWith(void Function(SearchItemData) updates) => super.copyWith((message) => updates(message as SearchItemData)) as SearchItemData; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchItemData create() => SearchItemData._();
  SearchItemData createEmptyInstance() => create();
  static $pb.PbList<SearchItemData> createRepeated() => $pb.PbList<SearchItemData>();
  @$core.pragma('dart2js:noInline')
  static SearchItemData getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchItemData>(create);
  static SearchItemData? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get itemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set itemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemID() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get quantity => $_getN(1);
  @$pb.TagNumber(2)
  set quantity($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasQuantity() => $_has(1);
  @$pb.TagNumber(2)
  void clearQuantity() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get price => $_getN(2);
  @$pb.TagNumber(3)
  set price($core.double v) { $_setDouble(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPrice() => $_has(2);
  @$pb.TagNumber(3)
  void clearPrice() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get isRequired => $_getBF(3);
  @$pb.TagNumber(4)
  set isRequired($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasIsRequired() => $_has(3);
  @$pb.TagNumber(4)
  void clearIsRequired() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get isDefault => $_getBF(4);
  @$pb.TagNumber(5)
  set isDefault($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasIsDefault() => $_has(4);
  @$pb.TagNumber(5)
  void clearIsDefault() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get description => $_getSZ(5);
  @$pb.TagNumber(6)
  set description($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDescription() => $_has(5);
  @$pb.TagNumber(6)
  void clearDescription() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get units => $_getSZ(6);
  @$pb.TagNumber(7)
  set units($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasUnits() => $_has(6);
  @$pb.TagNumber(7)
  void clearUnits() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get name => $_getSZ(7);
  @$pb.TagNumber(8)
  set name($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasName() => $_has(7);
  @$pb.TagNumber(8)
  void clearName() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get image => $_getSZ(8);
  @$pb.TagNumber(9)
  set image($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasImage() => $_has(8);
  @$pb.TagNumber(9)
  void clearImage() => clearField(9);
}

class SearchItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SearchItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'search_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemCategoryID', protoName: 'itemCategoryID')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'unit')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt')
    ..hasRequiredFields = false
  ;

  SearchItem._() : super();
  factory SearchItem({
    $core.String? id,
    $core.String? name,
    $core.String? description,
    $core.String? itemCategoryID,
    $core.String? image,
    $core.String? unit,
    $core.String? createdAt,
    $core.String? updatedAt,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    if (description != null) {
      _result.description = description;
    }
    if (itemCategoryID != null) {
      _result.itemCategoryID = itemCategoryID;
    }
    if (image != null) {
      _result.image = image;
    }
    if (unit != null) {
      _result.unit = unit;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    return _result;
  }
  factory SearchItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SearchItem clone() => SearchItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SearchItem copyWith(void Function(SearchItem) updates) => super.copyWith((message) => updates(message as SearchItem)) as SearchItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchItem create() => SearchItem._();
  SearchItem createEmptyInstance() => create();
  static $pb.PbList<SearchItem> createRepeated() => $pb.PbList<SearchItem>();
  @$core.pragma('dart2js:noInline')
  static SearchItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchItem>(create);
  static SearchItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get itemCategoryID => $_getSZ(3);
  @$pb.TagNumber(4)
  set itemCategoryID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasItemCategoryID() => $_has(3);
  @$pb.TagNumber(4)
  void clearItemCategoryID() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get image => $_getSZ(4);
  @$pb.TagNumber(5)
  set image($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasImage() => $_has(4);
  @$pb.TagNumber(5)
  void clearImage() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get unit => $_getSZ(5);
  @$pb.TagNumber(6)
  set unit($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasUnit() => $_has(5);
  @$pb.TagNumber(6)
  void clearUnit() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get createdAt => $_getSZ(6);
  @$pb.TagNumber(7)
  set createdAt($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasCreatedAt() => $_has(6);
  @$pb.TagNumber(7)
  void clearCreatedAt() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get updatedAt => $_getSZ(7);
  @$pb.TagNumber(8)
  set updatedAt($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasUpdatedAt() => $_has(7);
  @$pb.TagNumber(8)
  void clearUpdatedAt() => clearField(8);
}

