///
//  Generated code. Do not modify.
//  source: location.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class EmptyLocationRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'EmptyLocationRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  EmptyLocationRequest._() : super();
  factory EmptyLocationRequest() => create();
  factory EmptyLocationRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory EmptyLocationRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  EmptyLocationRequest clone() => EmptyLocationRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  EmptyLocationRequest copyWith(void Function(EmptyLocationRequest) updates) => super.copyWith((message) => updates(message as EmptyLocationRequest)) as EmptyLocationRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static EmptyLocationRequest create() => EmptyLocationRequest._();
  EmptyLocationRequest createEmptyInstance() => create();
  static $pb.PbList<EmptyLocationRequest> createRepeated() => $pb.PbList<EmptyLocationRequest>();
  @$core.pragma('dart2js:noInline')
  static EmptyLocationRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<EmptyLocationRequest>(create);
  static EmptyLocationRequest? _defaultInstance;
}

class LocationRequestItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LocationRequestItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'locationID', protoName: 'locationID')
    ..hasRequiredFields = false
  ;

  LocationRequestItem._() : super();
  factory LocationRequestItem({
    $core.String? locationID,
  }) {
    final _result = create();
    if (locationID != null) {
      _result.locationID = locationID;
    }
    return _result;
  }
  factory LocationRequestItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LocationRequestItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LocationRequestItem clone() => LocationRequestItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LocationRequestItem copyWith(void Function(LocationRequestItem) updates) => super.copyWith((message) => updates(message as LocationRequestItem)) as LocationRequestItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LocationRequestItem create() => LocationRequestItem._();
  LocationRequestItem createEmptyInstance() => create();
  static $pb.PbList<LocationRequestItem> createRepeated() => $pb.PbList<LocationRequestItem>();
  @$core.pragma('dart2js:noInline')
  static LocationRequestItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LocationRequestItem>(create);
  static LocationRequestItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get locationID => $_getSZ(0);
  @$pb.TagNumber(1)
  set locationID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLocationID() => $_has(0);
  @$pb.TagNumber(1)
  void clearLocationID() => clearField(1);
}

class RestaurantLocationItems extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RestaurantLocationItems', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'longitude')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'latitude')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'country')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lga')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'town')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'street')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'number')
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'restaurantId', protoName: 'restaurantId')
    ..aOS(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aOS(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt')
    ..hasRequiredFields = false
  ;

  RestaurantLocationItems._() : super();
  factory RestaurantLocationItems({
    $core.String? id,
    $core.String? longitude,
    $core.String? latitude,
    $core.String? country,
    $core.String? state,
    $core.String? lga,
    $core.String? town,
    $core.String? street,
    $core.String? number,
    $core.String? description,
    $core.String? restaurantId,
    $core.String? createdAt,
    $core.String? updatedAt,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (longitude != null) {
      _result.longitude = longitude;
    }
    if (latitude != null) {
      _result.latitude = latitude;
    }
    if (country != null) {
      _result.country = country;
    }
    if (state != null) {
      _result.state = state;
    }
    if (lga != null) {
      _result.lga = lga;
    }
    if (town != null) {
      _result.town = town;
    }
    if (street != null) {
      _result.street = street;
    }
    if (number != null) {
      _result.number = number;
    }
    if (description != null) {
      _result.description = description;
    }
    if (restaurantId != null) {
      _result.restaurantId = restaurantId;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    return _result;
  }
  factory RestaurantLocationItems.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RestaurantLocationItems.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RestaurantLocationItems clone() => RestaurantLocationItems()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RestaurantLocationItems copyWith(void Function(RestaurantLocationItems) updates) => super.copyWith((message) => updates(message as RestaurantLocationItems)) as RestaurantLocationItems; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RestaurantLocationItems create() => RestaurantLocationItems._();
  RestaurantLocationItems createEmptyInstance() => create();
  static $pb.PbList<RestaurantLocationItems> createRepeated() => $pb.PbList<RestaurantLocationItems>();
  @$core.pragma('dart2js:noInline')
  static RestaurantLocationItems getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RestaurantLocationItems>(create);
  static RestaurantLocationItems? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get longitude => $_getSZ(1);
  @$pb.TagNumber(2)
  set longitude($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLongitude() => $_has(1);
  @$pb.TagNumber(2)
  void clearLongitude() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get latitude => $_getSZ(2);
  @$pb.TagNumber(3)
  set latitude($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasLatitude() => $_has(2);
  @$pb.TagNumber(3)
  void clearLatitude() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get country => $_getSZ(3);
  @$pb.TagNumber(4)
  set country($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCountry() => $_has(3);
  @$pb.TagNumber(4)
  void clearCountry() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get state => $_getSZ(4);
  @$pb.TagNumber(5)
  set state($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasState() => $_has(4);
  @$pb.TagNumber(5)
  void clearState() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get lga => $_getSZ(5);
  @$pb.TagNumber(6)
  set lga($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasLga() => $_has(5);
  @$pb.TagNumber(6)
  void clearLga() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get town => $_getSZ(6);
  @$pb.TagNumber(7)
  set town($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasTown() => $_has(6);
  @$pb.TagNumber(7)
  void clearTown() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get street => $_getSZ(7);
  @$pb.TagNumber(8)
  set street($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasStreet() => $_has(7);
  @$pb.TagNumber(8)
  void clearStreet() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get number => $_getSZ(8);
  @$pb.TagNumber(9)
  set number($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasNumber() => $_has(8);
  @$pb.TagNumber(9)
  void clearNumber() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get description => $_getSZ(9);
  @$pb.TagNumber(10)
  set description($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasDescription() => $_has(9);
  @$pb.TagNumber(10)
  void clearDescription() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get restaurantId => $_getSZ(10);
  @$pb.TagNumber(11)
  set restaurantId($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasRestaurantId() => $_has(10);
  @$pb.TagNumber(11)
  void clearRestaurantId() => clearField(11);

  @$pb.TagNumber(12)
  $core.String get createdAt => $_getSZ(11);
  @$pb.TagNumber(12)
  set createdAt($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasCreatedAt() => $_has(11);
  @$pb.TagNumber(12)
  void clearCreatedAt() => clearField(12);

  @$pb.TagNumber(13)
  $core.String get updatedAt => $_getSZ(12);
  @$pb.TagNumber(13)
  set updatedAt($core.String v) { $_setString(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasUpdatedAt() => $_has(12);
  @$pb.TagNumber(13)
  void clearUpdatedAt() => clearField(13);
}

class LocationItems extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LocationItems', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'city')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'country')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..aOM<Coordinate>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'coordinate', subBuilder: Coordinate.create)
    ..hasRequiredFields = false
  ;

  LocationItems._() : super();
  factory LocationItems({
    $core.String? id,
    $core.String? name,
    $core.String? city,
    $core.String? country,
    $core.String? state,
    $core.String? createdAt,
    $core.String? updatedAt,
    $core.String? serviceAreaID,
    Coordinate? coordinate,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    if (city != null) {
      _result.city = city;
    }
    if (country != null) {
      _result.country = country;
    }
    if (state != null) {
      _result.state = state;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    if (coordinate != null) {
      _result.coordinate = coordinate;
    }
    return _result;
  }
  factory LocationItems.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LocationItems.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LocationItems clone() => LocationItems()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LocationItems copyWith(void Function(LocationItems) updates) => super.copyWith((message) => updates(message as LocationItems)) as LocationItems; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LocationItems create() => LocationItems._();
  LocationItems createEmptyInstance() => create();
  static $pb.PbList<LocationItems> createRepeated() => $pb.PbList<LocationItems>();
  @$core.pragma('dart2js:noInline')
  static LocationItems getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LocationItems>(create);
  static LocationItems? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get city => $_getSZ(2);
  @$pb.TagNumber(3)
  set city($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCity() => $_has(2);
  @$pb.TagNumber(3)
  void clearCity() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get country => $_getSZ(3);
  @$pb.TagNumber(4)
  set country($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCountry() => $_has(3);
  @$pb.TagNumber(4)
  void clearCountry() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get state => $_getSZ(4);
  @$pb.TagNumber(5)
  set state($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasState() => $_has(4);
  @$pb.TagNumber(5)
  void clearState() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get createdAt => $_getSZ(5);
  @$pb.TagNumber(6)
  set createdAt($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasCreatedAt() => $_has(5);
  @$pb.TagNumber(6)
  void clearCreatedAt() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get updatedAt => $_getSZ(6);
  @$pb.TagNumber(7)
  set updatedAt($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasUpdatedAt() => $_has(6);
  @$pb.TagNumber(7)
  void clearUpdatedAt() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get serviceAreaID => $_getSZ(7);
  @$pb.TagNumber(8)
  set serviceAreaID($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasServiceAreaID() => $_has(7);
  @$pb.TagNumber(8)
  void clearServiceAreaID() => clearField(8);

  @$pb.TagNumber(9)
  Coordinate get coordinate => $_getN(8);
  @$pb.TagNumber(9)
  set coordinate(Coordinate v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasCoordinate() => $_has(8);
  @$pb.TagNumber(9)
  void clearCoordinate() => clearField(9);
  @$pb.TagNumber(9)
  Coordinate ensureCoordinate() => $_ensure(8);
}

class UserLocationItems extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UserLocationItems', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currentLongitude', protoName: 'currentLongitude')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currentLatitude', protoName: 'currentLatitude')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'country')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lga')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'town')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'street')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'number')
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'permanentLongitude', protoName: 'permanentLongitude')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'permanentLatitude', protoName: 'permanentLatitude')
    ..aOS(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId')
    ..aOS(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aOS(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt')
    ..hasRequiredFields = false
  ;

  UserLocationItems._() : super();
  factory UserLocationItems({
    $core.String? id,
    $core.String? currentLongitude,
    $core.String? currentLatitude,
    $core.String? country,
    $core.String? state,
    $core.String? lga,
    $core.String? town,
    $core.String? street,
    $core.String? number,
    $core.String? permanentLongitude,
    $core.String? permanentLatitude,
    $core.String? userId,
    $core.String? createdAt,
    $core.String? updatedAt,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (currentLongitude != null) {
      _result.currentLongitude = currentLongitude;
    }
    if (currentLatitude != null) {
      _result.currentLatitude = currentLatitude;
    }
    if (country != null) {
      _result.country = country;
    }
    if (state != null) {
      _result.state = state;
    }
    if (lga != null) {
      _result.lga = lga;
    }
    if (town != null) {
      _result.town = town;
    }
    if (street != null) {
      _result.street = street;
    }
    if (number != null) {
      _result.number = number;
    }
    if (permanentLongitude != null) {
      _result.permanentLongitude = permanentLongitude;
    }
    if (permanentLatitude != null) {
      _result.permanentLatitude = permanentLatitude;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    return _result;
  }
  factory UserLocationItems.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UserLocationItems.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UserLocationItems clone() => UserLocationItems()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UserLocationItems copyWith(void Function(UserLocationItems) updates) => super.copyWith((message) => updates(message as UserLocationItems)) as UserLocationItems; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UserLocationItems create() => UserLocationItems._();
  UserLocationItems createEmptyInstance() => create();
  static $pb.PbList<UserLocationItems> createRepeated() => $pb.PbList<UserLocationItems>();
  @$core.pragma('dart2js:noInline')
  static UserLocationItems getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UserLocationItems>(create);
  static UserLocationItems? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get currentLongitude => $_getSZ(1);
  @$pb.TagNumber(2)
  set currentLongitude($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCurrentLongitude() => $_has(1);
  @$pb.TagNumber(2)
  void clearCurrentLongitude() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get currentLatitude => $_getSZ(2);
  @$pb.TagNumber(3)
  set currentLatitude($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCurrentLatitude() => $_has(2);
  @$pb.TagNumber(3)
  void clearCurrentLatitude() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get country => $_getSZ(3);
  @$pb.TagNumber(4)
  set country($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCountry() => $_has(3);
  @$pb.TagNumber(4)
  void clearCountry() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get state => $_getSZ(4);
  @$pb.TagNumber(5)
  set state($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasState() => $_has(4);
  @$pb.TagNumber(5)
  void clearState() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get lga => $_getSZ(5);
  @$pb.TagNumber(6)
  set lga($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasLga() => $_has(5);
  @$pb.TagNumber(6)
  void clearLga() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get town => $_getSZ(6);
  @$pb.TagNumber(7)
  set town($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasTown() => $_has(6);
  @$pb.TagNumber(7)
  void clearTown() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get street => $_getSZ(7);
  @$pb.TagNumber(8)
  set street($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasStreet() => $_has(7);
  @$pb.TagNumber(8)
  void clearStreet() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get number => $_getSZ(8);
  @$pb.TagNumber(9)
  set number($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasNumber() => $_has(8);
  @$pb.TagNumber(9)
  void clearNumber() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get permanentLongitude => $_getSZ(9);
  @$pb.TagNumber(10)
  set permanentLongitude($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasPermanentLongitude() => $_has(9);
  @$pb.TagNumber(10)
  void clearPermanentLongitude() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get permanentLatitude => $_getSZ(10);
  @$pb.TagNumber(11)
  set permanentLatitude($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasPermanentLatitude() => $_has(10);
  @$pb.TagNumber(11)
  void clearPermanentLatitude() => clearField(11);

  @$pb.TagNumber(12)
  $core.String get userId => $_getSZ(11);
  @$pb.TagNumber(12)
  set userId($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasUserId() => $_has(11);
  @$pb.TagNumber(12)
  void clearUserId() => clearField(12);

  @$pb.TagNumber(13)
  $core.String get createdAt => $_getSZ(12);
  @$pb.TagNumber(13)
  set createdAt($core.String v) { $_setString(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasCreatedAt() => $_has(12);
  @$pb.TagNumber(13)
  void clearCreatedAt() => clearField(13);

  @$pb.TagNumber(14)
  $core.String get updatedAt => $_getSZ(13);
  @$pb.TagNumber(14)
  set updatedAt($core.String v) { $_setString(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasUpdatedAt() => $_has(13);
  @$pb.TagNumber(14)
  void clearUpdatedAt() => clearField(14);
}

class LocationResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LocationResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data')
    ..hasRequiredFields = false
  ;

  LocationResponse._() : super();
  factory LocationResponse({
    $core.String? status,
    $core.String? message,
    $core.String? data,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (data != null) {
      _result.data = data;
    }
    return _result;
  }
  factory LocationResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LocationResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LocationResponse clone() => LocationResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LocationResponse copyWith(void Function(LocationResponse) updates) => super.copyWith((message) => updates(message as LocationResponse)) as LocationResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LocationResponse create() => LocationResponse._();
  LocationResponse createEmptyInstance() => create();
  static $pb.PbList<LocationResponse> createRepeated() => $pb.PbList<LocationResponse>();
  @$core.pragma('dart2js:noInline')
  static LocationResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LocationResponse>(create);
  static LocationResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get data => $_getSZ(2);
  @$pb.TagNumber(3)
  set data($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasData() => $_has(2);
  @$pb.TagNumber(3)
  void clearData() => clearField(3);
}

class ListUsersLocationResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListUsersLocationResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..pc<UserLocationItems>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: UserLocationItems.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ListUsersLocationResponse._() : super();
  factory ListUsersLocationResponse({
    $core.Iterable<UserLocationItems>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ListUsersLocationResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListUsersLocationResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListUsersLocationResponse clone() => ListUsersLocationResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListUsersLocationResponse copyWith(void Function(ListUsersLocationResponse) updates) => super.copyWith((message) => updates(message as ListUsersLocationResponse)) as ListUsersLocationResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListUsersLocationResponse create() => ListUsersLocationResponse._();
  ListUsersLocationResponse createEmptyInstance() => create();
  static $pb.PbList<ListUsersLocationResponse> createRepeated() => $pb.PbList<ListUsersLocationResponse>();
  @$core.pragma('dart2js:noInline')
  static ListUsersLocationResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListUsersLocationResponse>(create);
  static ListUsersLocationResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<UserLocationItems> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class ListLocationResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListLocationResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..pc<LocationItems>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: LocationItems.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ListLocationResponse._() : super();
  factory ListLocationResponse({
    $core.Iterable<LocationItems>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ListLocationResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListLocationResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListLocationResponse clone() => ListLocationResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListLocationResponse copyWith(void Function(ListLocationResponse) updates) => super.copyWith((message) => updates(message as ListLocationResponse)) as ListLocationResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListLocationResponse create() => ListLocationResponse._();
  ListLocationResponse createEmptyInstance() => create();
  static $pb.PbList<ListLocationResponse> createRepeated() => $pb.PbList<ListLocationResponse>();
  @$core.pragma('dart2js:noInline')
  static ListLocationResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListLocationResponse>(create);
  static ListLocationResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<LocationItems> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class ListRestaurantLocationResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListRestaurantLocationResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..pc<RestaurantLocationItems>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: RestaurantLocationItems.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ListRestaurantLocationResponse._() : super();
  factory ListRestaurantLocationResponse({
    $core.Iterable<RestaurantLocationItems>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ListRestaurantLocationResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListRestaurantLocationResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListRestaurantLocationResponse clone() => ListRestaurantLocationResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListRestaurantLocationResponse copyWith(void Function(ListRestaurantLocationResponse) updates) => super.copyWith((message) => updates(message as ListRestaurantLocationResponse)) as ListRestaurantLocationResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListRestaurantLocationResponse create() => ListRestaurantLocationResponse._();
  ListRestaurantLocationResponse createEmptyInstance() => create();
  static $pb.PbList<ListRestaurantLocationResponse> createRepeated() => $pb.PbList<ListRestaurantLocationResponse>();
  @$core.pragma('dart2js:noInline')
  static ListRestaurantLocationResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListRestaurantLocationResponse>(create);
  static ListRestaurantLocationResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<RestaurantLocationItems> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class ViewUserLocationResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ViewUserLocationResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOM<UserLocationItems>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: UserLocationItems.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  ViewUserLocationResponse._() : super();
  factory ViewUserLocationResponse({
    $core.String? status,
    UserLocationItems? data,
    $core.String? message,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (data != null) {
      _result.data = data;
    }
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory ViewUserLocationResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ViewUserLocationResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ViewUserLocationResponse clone() => ViewUserLocationResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ViewUserLocationResponse copyWith(void Function(ViewUserLocationResponse) updates) => super.copyWith((message) => updates(message as ViewUserLocationResponse)) as ViewUserLocationResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ViewUserLocationResponse create() => ViewUserLocationResponse._();
  ViewUserLocationResponse createEmptyInstance() => create();
  static $pb.PbList<ViewUserLocationResponse> createRepeated() => $pb.PbList<ViewUserLocationResponse>();
  @$core.pragma('dart2js:noInline')
  static ViewUserLocationResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ViewUserLocationResponse>(create);
  static ViewUserLocationResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  UserLocationItems get data => $_getN(1);
  @$pb.TagNumber(2)
  set data(UserLocationItems v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasData() => $_has(1);
  @$pb.TagNumber(2)
  void clearData() => clearField(2);
  @$pb.TagNumber(2)
  UserLocationItems ensureData() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get message => $_getSZ(2);
  @$pb.TagNumber(3)
  set message($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMessage() => $_has(2);
  @$pb.TagNumber(3)
  void clearMessage() => clearField(3);
}

class ViewRestaurantLocationResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ViewRestaurantLocationResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOM<RestaurantLocationItems>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: RestaurantLocationItems.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  ViewRestaurantLocationResponse._() : super();
  factory ViewRestaurantLocationResponse({
    $core.String? status,
    RestaurantLocationItems? data,
    $core.String? message,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (data != null) {
      _result.data = data;
    }
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory ViewRestaurantLocationResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ViewRestaurantLocationResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ViewRestaurantLocationResponse clone() => ViewRestaurantLocationResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ViewRestaurantLocationResponse copyWith(void Function(ViewRestaurantLocationResponse) updates) => super.copyWith((message) => updates(message as ViewRestaurantLocationResponse)) as ViewRestaurantLocationResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ViewRestaurantLocationResponse create() => ViewRestaurantLocationResponse._();
  ViewRestaurantLocationResponse createEmptyInstance() => create();
  static $pb.PbList<ViewRestaurantLocationResponse> createRepeated() => $pb.PbList<ViewRestaurantLocationResponse>();
  @$core.pragma('dart2js:noInline')
  static ViewRestaurantLocationResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ViewRestaurantLocationResponse>(create);
  static ViewRestaurantLocationResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  RestaurantLocationItems get data => $_getN(1);
  @$pb.TagNumber(2)
  set data(RestaurantLocationItems v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasData() => $_has(1);
  @$pb.TagNumber(2)
  void clearData() => clearField(2);
  @$pb.TagNumber(2)
  RestaurantLocationItems ensureData() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get message => $_getSZ(2);
  @$pb.TagNumber(3)
  set message($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMessage() => $_has(2);
  @$pb.TagNumber(3)
  void clearMessage() => clearField(3);
}

class Coordinate extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Coordinate', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'latitude', $pb.PbFieldType.OD)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'longitude', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Coordinate._() : super();
  factory Coordinate({
    $core.double? latitude,
    $core.double? longitude,
  }) {
    final _result = create();
    if (latitude != null) {
      _result.latitude = latitude;
    }
    if (longitude != null) {
      _result.longitude = longitude;
    }
    return _result;
  }
  factory Coordinate.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Coordinate.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Coordinate clone() => Coordinate()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Coordinate copyWith(void Function(Coordinate) updates) => super.copyWith((message) => updates(message as Coordinate)) as Coordinate; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Coordinate create() => Coordinate._();
  Coordinate createEmptyInstance() => create();
  static $pb.PbList<Coordinate> createRepeated() => $pb.PbList<Coordinate>();
  @$core.pragma('dart2js:noInline')
  static Coordinate getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Coordinate>(create);
  static Coordinate? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get latitude => $_getN(0);
  @$pb.TagNumber(1)
  set latitude($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLatitude() => $_has(0);
  @$pb.TagNumber(1)
  void clearLatitude() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get longitude => $_getN(1);
  @$pb.TagNumber(2)
  set longitude($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLongitude() => $_has(1);
  @$pb.TagNumber(2)
  void clearLongitude() => clearField(2);
}

class ServiceArea extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ServiceArea', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaName', protoName: 'serviceAreaName')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'coverageRadius', $pb.PbFieldType.OD, protoName: 'coverageRadius')
    ..aOM<Coordinate>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'coordinate', subBuilder: Coordinate.create)
    ..hasRequiredFields = false
  ;

  ServiceArea._() : super();
  factory ServiceArea({
    $core.String? id,
    $core.String? serviceAreaName,
    $core.String? description,
    $core.double? coverageRadius,
    Coordinate? coordinate,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (serviceAreaName != null) {
      _result.serviceAreaName = serviceAreaName;
    }
    if (description != null) {
      _result.description = description;
    }
    if (coverageRadius != null) {
      _result.coverageRadius = coverageRadius;
    }
    if (coordinate != null) {
      _result.coordinate = coordinate;
    }
    return _result;
  }
  factory ServiceArea.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ServiceArea.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ServiceArea clone() => ServiceArea()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ServiceArea copyWith(void Function(ServiceArea) updates) => super.copyWith((message) => updates(message as ServiceArea)) as ServiceArea; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ServiceArea create() => ServiceArea._();
  ServiceArea createEmptyInstance() => create();
  static $pb.PbList<ServiceArea> createRepeated() => $pb.PbList<ServiceArea>();
  @$core.pragma('dart2js:noInline')
  static ServiceArea getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ServiceArea>(create);
  static ServiceArea? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get serviceAreaName => $_getSZ(1);
  @$pb.TagNumber(2)
  set serviceAreaName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasServiceAreaName() => $_has(1);
  @$pb.TagNumber(2)
  void clearServiceAreaName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get coverageRadius => $_getN(3);
  @$pb.TagNumber(4)
  set coverageRadius($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCoverageRadius() => $_has(3);
  @$pb.TagNumber(4)
  void clearCoverageRadius() => clearField(4);

  @$pb.TagNumber(5)
  Coordinate get coordinate => $_getN(4);
  @$pb.TagNumber(5)
  set coordinate(Coordinate v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasCoordinate() => $_has(4);
  @$pb.TagNumber(5)
  void clearCoordinate() => clearField(5);
  @$pb.TagNumber(5)
  Coordinate ensureCoordinate() => $_ensure(4);
}

class ServiceAreaRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ServiceAreaRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..hasRequiredFields = false
  ;

  ServiceAreaRequest._() : super();
  factory ServiceAreaRequest({
    $core.String? serviceAreaID,
  }) {
    final _result = create();
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    return _result;
  }
  factory ServiceAreaRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ServiceAreaRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ServiceAreaRequest clone() => ServiceAreaRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ServiceAreaRequest copyWith(void Function(ServiceAreaRequest) updates) => super.copyWith((message) => updates(message as ServiceAreaRequest)) as ServiceAreaRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ServiceAreaRequest create() => ServiceAreaRequest._();
  ServiceAreaRequest createEmptyInstance() => create();
  static $pb.PbList<ServiceAreaRequest> createRepeated() => $pb.PbList<ServiceAreaRequest>();
  @$core.pragma('dart2js:noInline')
  static ServiceAreaRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ServiceAreaRequest>(create);
  static ServiceAreaRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get serviceAreaID => $_getSZ(0);
  @$pb.TagNumber(1)
  set serviceAreaID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasServiceAreaID() => $_has(0);
  @$pb.TagNumber(1)
  void clearServiceAreaID() => clearField(1);
}

class ServiceAreaLocationList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ServiceAreaLocationList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..hasRequiredFields = false
  ;

  ServiceAreaLocationList._() : super();
  factory ServiceAreaLocationList({
    $core.String? serviceAreaID,
  }) {
    final _result = create();
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    return _result;
  }
  factory ServiceAreaLocationList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ServiceAreaLocationList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ServiceAreaLocationList clone() => ServiceAreaLocationList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ServiceAreaLocationList copyWith(void Function(ServiceAreaLocationList) updates) => super.copyWith((message) => updates(message as ServiceAreaLocationList)) as ServiceAreaLocationList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ServiceAreaLocationList create() => ServiceAreaLocationList._();
  ServiceAreaLocationList createEmptyInstance() => create();
  static $pb.PbList<ServiceAreaLocationList> createRepeated() => $pb.PbList<ServiceAreaLocationList>();
  @$core.pragma('dart2js:noInline')
  static ServiceAreaLocationList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ServiceAreaLocationList>(create);
  static ServiceAreaLocationList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get serviceAreaID => $_getSZ(0);
  @$pb.TagNumber(1)
  set serviceAreaID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasServiceAreaID() => $_has(0);
  @$pb.TagNumber(1)
  void clearServiceAreaID() => clearField(1);
}

class ListServiceAreaResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListServiceAreaResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'location_service.v1'), createEmptyInstance: create)
    ..pc<ServiceArea>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: ServiceArea.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ListServiceAreaResponse._() : super();
  factory ListServiceAreaResponse({
    $core.Iterable<ServiceArea>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ListServiceAreaResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListServiceAreaResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListServiceAreaResponse clone() => ListServiceAreaResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListServiceAreaResponse copyWith(void Function(ListServiceAreaResponse) updates) => super.copyWith((message) => updates(message as ListServiceAreaResponse)) as ListServiceAreaResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListServiceAreaResponse create() => ListServiceAreaResponse._();
  ListServiceAreaResponse createEmptyInstance() => create();
  static $pb.PbList<ListServiceAreaResponse> createRepeated() => $pb.PbList<ListServiceAreaResponse>();
  @$core.pragma('dart2js:noInline')
  static ListServiceAreaResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListServiceAreaResponse>(create);
  static ListServiceAreaResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<ServiceArea> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

