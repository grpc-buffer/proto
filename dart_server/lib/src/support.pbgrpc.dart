///
//  Generated code. Do not modify.
//  source: support.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'support.pb.dart' as $0;
export 'support.pb.dart';

class SupportClient extends $grpc.Client {
  static final _$createTicket =
      $grpc.ClientMethod<$0.SupportRequest, $0.SupportResponse>(
          '/support.v1.Support/CreateTicket',
          ($0.SupportRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SupportResponse.fromBuffer(value));
  static final _$replyTicket =
      $grpc.ClientMethod<$0.SupportRequest, $0.SupportResponse>(
          '/support.v1.Support/ReplyTicket',
          ($0.SupportRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SupportResponse.fromBuffer(value));
  static final _$closeTicket =
      $grpc.ClientMethod<$0.TicketRequest, $0.SupportResponse>(
          '/support.v1.Support/CloseTicket',
          ($0.TicketRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SupportResponse.fromBuffer(value));
  static final _$viewTicket =
      $grpc.ClientMethod<$0.TicketRequest, $0.ViewSupportResponse>(
          '/support.v1.Support/ViewTicket',
          ($0.TicketRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ViewSupportResponse.fromBuffer(value));
  static final _$getActiveTicket =
      $grpc.ClientMethod<$0.TicketRequest, $0.ListSupportResponse>(
          '/support.v1.Support/GetActiveTicket',
          ($0.TicketRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListSupportResponse.fromBuffer(value));
  static final _$adminViewTicket =
      $grpc.ClientMethod<$0.TicketRequest, $0.ViewSupportResponse>(
          '/support.v1.Support/AdminViewTicket',
          ($0.TicketRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ViewSupportResponse.fromBuffer(value));
  static final _$deleteTicket =
      $grpc.ClientMethod<$0.TicketRequest, $0.SupportResponse>(
          '/support.v1.Support/DeleteTicket',
          ($0.TicketRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SupportResponse.fromBuffer(value));
  static final _$listAllTickets =
      $grpc.ClientMethod<$0.EmptySupportRequest, $0.ListSupportResponse>(
          '/support.v1.Support/ListAllTickets',
          ($0.EmptySupportRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListSupportResponse.fromBuffer(value));
  static final _$listByField =
      $grpc.ClientMethod<$0.SupportRequestItem, $0.ListSupportResponse>(
          '/support.v1.Support/ListByField',
          ($0.SupportRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListSupportResponse.fromBuffer(value));
  static final _$createFAQ =
      $grpc.ClientMethod<$0.FAQCreateRequest, $0.FAQResponse>(
          '/support.v1.Support/CreateFAQ',
          ($0.FAQCreateRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$updateFAQ = $grpc.ClientMethod<$0.FAQItem, $0.FAQResponse>(
      '/support.v1.Support/UpdateFAQ',
      ($0.FAQItem value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$viewFAQ = $grpc.ClientMethod<$0.FAQRequestItem, $0.FAQItem>(
      '/support.v1.Support/ViewFAQ',
      ($0.FAQRequestItem value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.FAQItem.fromBuffer(value));
  static final _$deleteFAQ =
      $grpc.ClientMethod<$0.FAQRequestItem, $0.FAQResponse>(
          '/support.v1.Support/DeleteFAQ',
          ($0.FAQRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$listFAQ =
      $grpc.ClientMethod<$0.ListFAQRequest, $0.ListFAQResponse>(
          '/support.v1.Support/ListFAQ',
          ($0.ListFAQRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListFAQResponse.fromBuffer(value));
  static final _$listFAQByCategory =
      $grpc.ClientMethod<$0.ListFAQByCategoryRequest, $0.ListFAQResponse>(
          '/support.v1.Support/ListFAQByCategory',
          ($0.ListFAQByCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListFAQResponse.fromBuffer(value));
  static final _$createAgentFAQ =
      $grpc.ClientMethod<$0.AgentFAQCreateRequest, $0.FAQResponse>(
          '/support.v1.Support/CreateAgentFAQ',
          ($0.AgentFAQCreateRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$updateAgentFAQ =
      $grpc.ClientMethod<$0.AgentFAQItem, $0.FAQResponse>(
          '/support.v1.Support/UpdateAgentFAQ',
          ($0.AgentFAQItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$viewAgentFAQ =
      $grpc.ClientMethod<$0.AgentFAQRequestItem, $0.AgentFAQItem>(
          '/support.v1.Support/ViewAgentFAQ',
          ($0.AgentFAQRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.AgentFAQItem.fromBuffer(value));
  static final _$deleteAgentFAQ =
      $grpc.ClientMethod<$0.AgentFAQRequestItem, $0.FAQResponse>(
          '/support.v1.Support/DeleteAgentFAQ',
          ($0.AgentFAQRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$listAgentFAQ =
      $grpc.ClientMethod<$0.ListFAQRequest, $0.ListAgentFAQResponse>(
          '/support.v1.Support/ListAgentFAQ',
          ($0.ListFAQRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListAgentFAQResponse.fromBuffer(value));
  static final _$addVideoGuide =
      $grpc.ClientMethod<$0.VideoRequest, $0.VideoGuideResponse>(
          '/support.v1.Support/AddVideoGuide',
          ($0.VideoRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.VideoGuideResponse.fromBuffer(value));
  static final _$listVideoGuide =
      $grpc.ClientMethod<$0.ListVideoGuideRequest, $0.ListVideoGuideResponse>(
          '/support.v1.Support/ListVideoGuide',
          ($0.ListVideoGuideRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListVideoGuideResponse.fromBuffer(value));
  static final _$deleteVideoGuide =
      $grpc.ClientMethod<$0.VideoGuideRequest, $0.VideoGuideResponse>(
          '/support.v1.Support/DeleteVideoGuide',
          ($0.VideoGuideRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.VideoGuideResponse.fromBuffer(value));
  static final _$updateVideoGuide =
      $grpc.ClientMethod<$0.VideoGuideItem, $0.VideoGuideResponse>(
          '/support.v1.Support/UpdateVideoGuide',
          ($0.VideoGuideItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.VideoGuideResponse.fromBuffer(value));
  static final _$createCategory =
      $grpc.ClientMethod<$0.CreateFAQCategoryRequest, $0.FAQResponse>(
          '/support.v1.Support/CreateCategory',
          ($0.CreateFAQCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$updateCategory =
      $grpc.ClientMethod<$0.FAQCategoryItem, $0.FAQResponse>(
          '/support.v1.Support/UpdateCategory',
          ($0.FAQCategoryItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$deleteFAQCategory =
      $grpc.ClientMethod<$0.FAQCategoryItemRequest, $0.FAQResponse>(
          '/support.v1.Support/DeleteFAQCategory',
          ($0.FAQCategoryItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.FAQResponse.fromBuffer(value));
  static final _$listFAQCategories =
      $grpc.ClientMethod<$0.ListFAQRequest, $0.ListFAQCategoriesResponse>(
          '/support.v1.Support/ListFAQCategories',
          ($0.ListFAQRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListFAQCategoriesResponse.fromBuffer(value));

  SupportClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.SupportResponse> createTicket(
      $0.SupportRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createTicket, request, options: options);
  }

  $grpc.ResponseFuture<$0.SupportResponse> replyTicket(
      $0.SupportRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$replyTicket, request, options: options);
  }

  $grpc.ResponseFuture<$0.SupportResponse> closeTicket($0.TicketRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$closeTicket, request, options: options);
  }

  $grpc.ResponseStream<$0.ViewSupportResponse> viewTicket(
      $0.TicketRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$viewTicket, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$0.ListSupportResponse> getActiveTicket(
      $0.TicketRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getActiveTicket, request, options: options);
  }

  $grpc.ResponseFuture<$0.ViewSupportResponse> adminViewTicket(
      $0.TicketRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$adminViewTicket, request, options: options);
  }

  $grpc.ResponseFuture<$0.SupportResponse> deleteTicket(
      $0.TicketRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteTicket, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListSupportResponse> listAllTickets(
      $0.EmptySupportRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listAllTickets, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListSupportResponse> listByField(
      $0.SupportRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listByField, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> createFAQ($0.FAQCreateRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> updateFAQ($0.FAQItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQItem> viewFAQ($0.FAQRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> deleteFAQ($0.FAQRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListFAQResponse> listFAQ($0.ListFAQRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListFAQResponse> listFAQByCategory(
      $0.ListFAQByCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listFAQByCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> createAgentFAQ(
      $0.AgentFAQCreateRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createAgentFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> updateAgentFAQ($0.AgentFAQItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateAgentFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.AgentFAQItem> viewAgentFAQ(
      $0.AgentFAQRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewAgentFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> deleteAgentFAQ(
      $0.AgentFAQRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteAgentFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListAgentFAQResponse> listAgentFAQ(
      $0.ListFAQRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listAgentFAQ, request, options: options);
  }

  $grpc.ResponseFuture<$0.VideoGuideResponse> addVideoGuide(
      $0.VideoRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addVideoGuide, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListVideoGuideResponse> listVideoGuide(
      $0.ListVideoGuideRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listVideoGuide, request, options: options);
  }

  $grpc.ResponseFuture<$0.VideoGuideResponse> deleteVideoGuide(
      $0.VideoGuideRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteVideoGuide, request, options: options);
  }

  $grpc.ResponseFuture<$0.VideoGuideResponse> updateVideoGuide(
      $0.VideoGuideItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateVideoGuide, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> createCategory(
      $0.CreateFAQCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> updateCategory(
      $0.FAQCategoryItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.FAQResponse> deleteFAQCategory(
      $0.FAQCategoryItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteFAQCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListFAQCategoriesResponse> listFAQCategories(
      $0.ListFAQRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listFAQCategories, request, options: options);
  }
}

abstract class SupportServiceBase extends $grpc.Service {
  $core.String get $name => 'support.v1.Support';

  SupportServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.SupportRequest, $0.SupportResponse>(
        'CreateTicket',
        createTicket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SupportRequest.fromBuffer(value),
        ($0.SupportResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SupportRequest, $0.SupportResponse>(
        'ReplyTicket',
        replyTicket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SupportRequest.fromBuffer(value),
        ($0.SupportResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TicketRequest, $0.SupportResponse>(
        'CloseTicket',
        closeTicket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TicketRequest.fromBuffer(value),
        ($0.SupportResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TicketRequest, $0.ViewSupportResponse>(
        'ViewTicket',
        viewTicket_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.TicketRequest.fromBuffer(value),
        ($0.ViewSupportResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TicketRequest, $0.ListSupportResponse>(
        'GetActiveTicket',
        getActiveTicket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TicketRequest.fromBuffer(value),
        ($0.ListSupportResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TicketRequest, $0.ViewSupportResponse>(
        'AdminViewTicket',
        adminViewTicket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TicketRequest.fromBuffer(value),
        ($0.ViewSupportResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TicketRequest, $0.SupportResponse>(
        'DeleteTicket',
        deleteTicket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TicketRequest.fromBuffer(value),
        ($0.SupportResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.EmptySupportRequest, $0.ListSupportResponse>(
            'ListAllTickets',
            listAllTickets_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.EmptySupportRequest.fromBuffer(value),
            ($0.ListSupportResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.SupportRequestItem, $0.ListSupportResponse>(
            'ListByField',
            listByField_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.SupportRequestItem.fromBuffer(value),
            ($0.ListSupportResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.FAQCreateRequest, $0.FAQResponse>(
        'CreateFAQ',
        createFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.FAQCreateRequest.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.FAQItem, $0.FAQResponse>(
        'UpdateFAQ',
        updateFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.FAQItem.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.FAQRequestItem, $0.FAQItem>(
        'ViewFAQ',
        viewFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.FAQRequestItem.fromBuffer(value),
        ($0.FAQItem value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.FAQRequestItem, $0.FAQResponse>(
        'DeleteFAQ',
        deleteFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.FAQRequestItem.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ListFAQRequest, $0.ListFAQResponse>(
        'ListFAQ',
        listFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ListFAQRequest.fromBuffer(value),
        ($0.ListFAQResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.ListFAQByCategoryRequest, $0.ListFAQResponse>(
            'ListFAQByCategory',
            listFAQByCategory_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.ListFAQByCategoryRequest.fromBuffer(value),
            ($0.ListFAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AgentFAQCreateRequest, $0.FAQResponse>(
        'CreateAgentFAQ',
        createAgentFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.AgentFAQCreateRequest.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AgentFAQItem, $0.FAQResponse>(
        'UpdateAgentFAQ',
        updateAgentFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AgentFAQItem.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AgentFAQRequestItem, $0.AgentFAQItem>(
        'ViewAgentFAQ',
        viewAgentFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.AgentFAQRequestItem.fromBuffer(value),
        ($0.AgentFAQItem value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AgentFAQRequestItem, $0.FAQResponse>(
        'DeleteAgentFAQ',
        deleteAgentFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.AgentFAQRequestItem.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ListFAQRequest, $0.ListAgentFAQResponse>(
        'ListAgentFAQ',
        listAgentFAQ_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ListFAQRequest.fromBuffer(value),
        ($0.ListAgentFAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.VideoRequest, $0.VideoGuideResponse>(
        'AddVideoGuide',
        addVideoGuide_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.VideoRequest.fromBuffer(value),
        ($0.VideoGuideResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ListVideoGuideRequest,
            $0.ListVideoGuideResponse>(
        'ListVideoGuide',
        listVideoGuide_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ListVideoGuideRequest.fromBuffer(value),
        ($0.ListVideoGuideResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.VideoGuideRequest, $0.VideoGuideResponse>(
        'DeleteVideoGuide',
        deleteVideoGuide_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.VideoGuideRequest.fromBuffer(value),
        ($0.VideoGuideResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.VideoGuideItem, $0.VideoGuideResponse>(
        'UpdateVideoGuide',
        updateVideoGuide_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.VideoGuideItem.fromBuffer(value),
        ($0.VideoGuideResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CreateFAQCategoryRequest, $0.FAQResponse>(
        'CreateCategory',
        createCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.CreateFAQCategoryRequest.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.FAQCategoryItem, $0.FAQResponse>(
        'UpdateCategory',
        updateCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.FAQCategoryItem.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.FAQCategoryItemRequest, $0.FAQResponse>(
        'DeleteFAQCategory',
        deleteFAQCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.FAQCategoryItemRequest.fromBuffer(value),
        ($0.FAQResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.ListFAQRequest, $0.ListFAQCategoriesResponse>(
            'ListFAQCategories',
            listFAQCategories_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.ListFAQRequest.fromBuffer(value),
            ($0.ListFAQCategoriesResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.SupportResponse> createTicket_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SupportRequest> request) async {
    return createTicket(call, await request);
  }

  $async.Future<$0.SupportResponse> replyTicket_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SupportRequest> request) async {
    return replyTicket(call, await request);
  }

  $async.Future<$0.SupportResponse> closeTicket_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TicketRequest> request) async {
    return closeTicket(call, await request);
  }

  $async.Stream<$0.ViewSupportResponse> viewTicket_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TicketRequest> request) async* {
    yield* viewTicket(call, await request);
  }

  $async.Future<$0.ListSupportResponse> getActiveTicket_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TicketRequest> request) async {
    return getActiveTicket(call, await request);
  }

  $async.Future<$0.ViewSupportResponse> adminViewTicket_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TicketRequest> request) async {
    return adminViewTicket(call, await request);
  }

  $async.Future<$0.SupportResponse> deleteTicket_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TicketRequest> request) async {
    return deleteTicket(call, await request);
  }

  $async.Future<$0.ListSupportResponse> listAllTickets_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptySupportRequest> request) async {
    return listAllTickets(call, await request);
  }

  $async.Future<$0.ListSupportResponse> listByField_Pre($grpc.ServiceCall call,
      $async.Future<$0.SupportRequestItem> request) async {
    return listByField(call, await request);
  }

  $async.Future<$0.FAQResponse> createFAQ_Pre($grpc.ServiceCall call,
      $async.Future<$0.FAQCreateRequest> request) async {
    return createFAQ(call, await request);
  }

  $async.Future<$0.FAQResponse> updateFAQ_Pre(
      $grpc.ServiceCall call, $async.Future<$0.FAQItem> request) async {
    return updateFAQ(call, await request);
  }

  $async.Future<$0.FAQItem> viewFAQ_Pre(
      $grpc.ServiceCall call, $async.Future<$0.FAQRequestItem> request) async {
    return viewFAQ(call, await request);
  }

  $async.Future<$0.FAQResponse> deleteFAQ_Pre(
      $grpc.ServiceCall call, $async.Future<$0.FAQRequestItem> request) async {
    return deleteFAQ(call, await request);
  }

  $async.Future<$0.ListFAQResponse> listFAQ_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ListFAQRequest> request) async {
    return listFAQ(call, await request);
  }

  $async.Future<$0.ListFAQResponse> listFAQByCategory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ListFAQByCategoryRequest> request) async {
    return listFAQByCategory(call, await request);
  }

  $async.Future<$0.FAQResponse> createAgentFAQ_Pre($grpc.ServiceCall call,
      $async.Future<$0.AgentFAQCreateRequest> request) async {
    return createAgentFAQ(call, await request);
  }

  $async.Future<$0.FAQResponse> updateAgentFAQ_Pre(
      $grpc.ServiceCall call, $async.Future<$0.AgentFAQItem> request) async {
    return updateAgentFAQ(call, await request);
  }

  $async.Future<$0.AgentFAQItem> viewAgentFAQ_Pre($grpc.ServiceCall call,
      $async.Future<$0.AgentFAQRequestItem> request) async {
    return viewAgentFAQ(call, await request);
  }

  $async.Future<$0.FAQResponse> deleteAgentFAQ_Pre($grpc.ServiceCall call,
      $async.Future<$0.AgentFAQRequestItem> request) async {
    return deleteAgentFAQ(call, await request);
  }

  $async.Future<$0.ListAgentFAQResponse> listAgentFAQ_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ListFAQRequest> request) async {
    return listAgentFAQ(call, await request);
  }

  $async.Future<$0.VideoGuideResponse> addVideoGuide_Pre(
      $grpc.ServiceCall call, $async.Future<$0.VideoRequest> request) async {
    return addVideoGuide(call, await request);
  }

  $async.Future<$0.ListVideoGuideResponse> listVideoGuide_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ListVideoGuideRequest> request) async {
    return listVideoGuide(call, await request);
  }

  $async.Future<$0.VideoGuideResponse> deleteVideoGuide_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.VideoGuideRequest> request) async {
    return deleteVideoGuide(call, await request);
  }

  $async.Future<$0.VideoGuideResponse> updateVideoGuide_Pre(
      $grpc.ServiceCall call, $async.Future<$0.VideoGuideItem> request) async {
    return updateVideoGuide(call, await request);
  }

  $async.Future<$0.FAQResponse> createCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.CreateFAQCategoryRequest> request) async {
    return createCategory(call, await request);
  }

  $async.Future<$0.FAQResponse> updateCategory_Pre(
      $grpc.ServiceCall call, $async.Future<$0.FAQCategoryItem> request) async {
    return updateCategory(call, await request);
  }

  $async.Future<$0.FAQResponse> deleteFAQCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.FAQCategoryItemRequest> request) async {
    return deleteFAQCategory(call, await request);
  }

  $async.Future<$0.ListFAQCategoriesResponse> listFAQCategories_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ListFAQRequest> request) async {
    return listFAQCategories(call, await request);
  }

  $async.Future<$0.SupportResponse> createTicket(
      $grpc.ServiceCall call, $0.SupportRequest request);
  $async.Future<$0.SupportResponse> replyTicket(
      $grpc.ServiceCall call, $0.SupportRequest request);
  $async.Future<$0.SupportResponse> closeTicket(
      $grpc.ServiceCall call, $0.TicketRequest request);
  $async.Stream<$0.ViewSupportResponse> viewTicket(
      $grpc.ServiceCall call, $0.TicketRequest request);
  $async.Future<$0.ListSupportResponse> getActiveTicket(
      $grpc.ServiceCall call, $0.TicketRequest request);
  $async.Future<$0.ViewSupportResponse> adminViewTicket(
      $grpc.ServiceCall call, $0.TicketRequest request);
  $async.Future<$0.SupportResponse> deleteTicket(
      $grpc.ServiceCall call, $0.TicketRequest request);
  $async.Future<$0.ListSupportResponse> listAllTickets(
      $grpc.ServiceCall call, $0.EmptySupportRequest request);
  $async.Future<$0.ListSupportResponse> listByField(
      $grpc.ServiceCall call, $0.SupportRequestItem request);
  $async.Future<$0.FAQResponse> createFAQ(
      $grpc.ServiceCall call, $0.FAQCreateRequest request);
  $async.Future<$0.FAQResponse> updateFAQ(
      $grpc.ServiceCall call, $0.FAQItem request);
  $async.Future<$0.FAQItem> viewFAQ(
      $grpc.ServiceCall call, $0.FAQRequestItem request);
  $async.Future<$0.FAQResponse> deleteFAQ(
      $grpc.ServiceCall call, $0.FAQRequestItem request);
  $async.Future<$0.ListFAQResponse> listFAQ(
      $grpc.ServiceCall call, $0.ListFAQRequest request);
  $async.Future<$0.ListFAQResponse> listFAQByCategory(
      $grpc.ServiceCall call, $0.ListFAQByCategoryRequest request);
  $async.Future<$0.FAQResponse> createAgentFAQ(
      $grpc.ServiceCall call, $0.AgentFAQCreateRequest request);
  $async.Future<$0.FAQResponse> updateAgentFAQ(
      $grpc.ServiceCall call, $0.AgentFAQItem request);
  $async.Future<$0.AgentFAQItem> viewAgentFAQ(
      $grpc.ServiceCall call, $0.AgentFAQRequestItem request);
  $async.Future<$0.FAQResponse> deleteAgentFAQ(
      $grpc.ServiceCall call, $0.AgentFAQRequestItem request);
  $async.Future<$0.ListAgentFAQResponse> listAgentFAQ(
      $grpc.ServiceCall call, $0.ListFAQRequest request);
  $async.Future<$0.VideoGuideResponse> addVideoGuide(
      $grpc.ServiceCall call, $0.VideoRequest request);
  $async.Future<$0.ListVideoGuideResponse> listVideoGuide(
      $grpc.ServiceCall call, $0.ListVideoGuideRequest request);
  $async.Future<$0.VideoGuideResponse> deleteVideoGuide(
      $grpc.ServiceCall call, $0.VideoGuideRequest request);
  $async.Future<$0.VideoGuideResponse> updateVideoGuide(
      $grpc.ServiceCall call, $0.VideoGuideItem request);
  $async.Future<$0.FAQResponse> createCategory(
      $grpc.ServiceCall call, $0.CreateFAQCategoryRequest request);
  $async.Future<$0.FAQResponse> updateCategory(
      $grpc.ServiceCall call, $0.FAQCategoryItem request);
  $async.Future<$0.FAQResponse> deleteFAQCategory(
      $grpc.ServiceCall call, $0.FAQCategoryItemRequest request);
  $async.Future<$0.ListFAQCategoriesResponse> listFAQCategories(
      $grpc.ServiceCall call, $0.ListFAQRequest request);
}
