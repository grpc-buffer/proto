///
//  Generated code. Do not modify.
//  source: promotions.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use bannerInfoDescriptor instead')
const BannerInfo$json = const {
  '1': 'BannerInfo',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'serviceAreaID', '3': 3, '4': 1, '5': 9, '10': 'serviceAreaID'},
    const {'1': 'bannerID', '3': 4, '4': 1, '5': 9, '10': 'bannerID'},
    const {'1': 'bannerImage', '3': 5, '4': 1, '5': 9, '10': 'bannerImage'},
    const {'1': 'bucketPath', '3': 6, '4': 1, '5': 9, '10': 'bucketPath'},
    const {'1': 'imageType', '3': 7, '4': 1, '5': 9, '10': 'imageType'},
    const {'1': 'link', '3': 8, '4': 1, '5': 9, '10': 'link'},
  ],
};

/// Descriptor for `BannerInfo`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bannerInfoDescriptor = $convert.base64Decode('CgpCYW5uZXJJbmZvEhQKBXRpdGxlGAEgASgJUgV0aXRsZRIgCgtkZXNjcmlwdGlvbhgCIAEoCVILZGVzY3JpcHRpb24SJAoNc2VydmljZUFyZWFJRBgDIAEoCVINc2VydmljZUFyZWFJRBIaCghiYW5uZXJJRBgEIAEoCVIIYmFubmVySUQSIAoLYmFubmVySW1hZ2UYBSABKAlSC2Jhbm5lckltYWdlEh4KCmJ1Y2tldFBhdGgYBiABKAlSCmJ1Y2tldFBhdGgSHAoJaW1hZ2VUeXBlGAcgASgJUglpbWFnZVR5cGUSEgoEbGluaxgIIAEoCVIEbGluaw==');
@$core.Deprecated('Use bannerListDescriptor instead')
const BannerList$json = const {
  '1': 'BannerList',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.promotion_service.v1.BannerInfo', '10': 'data'},
  ],
};

/// Descriptor for `BannerList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bannerListDescriptor = $convert.base64Decode('CgpCYW5uZXJMaXN0EjQKBGRhdGEYASADKAsyIC5wcm9tb3Rpb25fc2VydmljZS52MS5CYW5uZXJJbmZvUgRkYXRh');
@$core.Deprecated('Use bannerRequestDescriptor instead')
const BannerRequest$json = const {
  '1': 'BannerRequest',
  '2': const [
    const {'1': 'bannerID', '3': 1, '4': 1, '5': 9, '10': 'bannerID'},
    const {'1': 'serviceAreaID', '3': 2, '4': 1, '5': 9, '10': 'serviceAreaID'},
  ],
};

/// Descriptor for `BannerRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bannerRequestDescriptor = $convert.base64Decode('Cg1CYW5uZXJSZXF1ZXN0EhoKCGJhbm5lcklEGAEgASgJUghiYW5uZXJJRBIkCg1zZXJ2aWNlQXJlYUlEGAIgASgJUg1zZXJ2aWNlQXJlYUlE');
@$core.Deprecated('Use promotionsResponseDescriptor instead')
const PromotionsResponse$json = const {
  '1': 'PromotionsResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `PromotionsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List promotionsResponseDescriptor = $convert.base64Decode('ChJQcm9tb3Rpb25zUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZQ==');
