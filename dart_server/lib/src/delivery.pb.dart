///
//  Generated code. Do not modify.
//  source: delivery.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class InDeliveryRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InDeliveryRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schedule')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'customerAddress')
    ..aOM<InDeliveryCoordinatesRequest>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'customerCoordinates', subBuilder: InDeliveryCoordinatesRequest.create)
    ..aOM<Delivery_Urgency>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'urgencyDetails', subBuilder: Delivery_Urgency.create)
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..hasRequiredFields = false
  ;

  InDeliveryRequest._() : super();
  factory InDeliveryRequest({
    $core.String? schedule,
    $core.String? customerAddress,
    InDeliveryCoordinatesRequest? customerCoordinates,
    Delivery_Urgency? urgencyDetails,
    $core.String? currency,
  }) {
    final _result = create();
    if (schedule != null) {
      _result.schedule = schedule;
    }
    if (customerAddress != null) {
      _result.customerAddress = customerAddress;
    }
    if (customerCoordinates != null) {
      _result.customerCoordinates = customerCoordinates;
    }
    if (urgencyDetails != null) {
      _result.urgencyDetails = urgencyDetails;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    return _result;
  }
  factory InDeliveryRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InDeliveryRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InDeliveryRequest clone() => InDeliveryRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InDeliveryRequest copyWith(void Function(InDeliveryRequest) updates) => super.copyWith((message) => updates(message as InDeliveryRequest)) as InDeliveryRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InDeliveryRequest create() => InDeliveryRequest._();
  InDeliveryRequest createEmptyInstance() => create();
  static $pb.PbList<InDeliveryRequest> createRepeated() => $pb.PbList<InDeliveryRequest>();
  @$core.pragma('dart2js:noInline')
  static InDeliveryRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InDeliveryRequest>(create);
  static InDeliveryRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get schedule => $_getSZ(0);
  @$pb.TagNumber(1)
  set schedule($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSchedule() => $_has(0);
  @$pb.TagNumber(1)
  void clearSchedule() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get customerAddress => $_getSZ(1);
  @$pb.TagNumber(2)
  set customerAddress($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCustomerAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearCustomerAddress() => clearField(2);

  @$pb.TagNumber(3)
  InDeliveryCoordinatesRequest get customerCoordinates => $_getN(2);
  @$pb.TagNumber(3)
  set customerCoordinates(InDeliveryCoordinatesRequest v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasCustomerCoordinates() => $_has(2);
  @$pb.TagNumber(3)
  void clearCustomerCoordinates() => clearField(3);
  @$pb.TagNumber(3)
  InDeliveryCoordinatesRequest ensureCustomerCoordinates() => $_ensure(2);

  @$pb.TagNumber(4)
  Delivery_Urgency get urgencyDetails => $_getN(3);
  @$pb.TagNumber(4)
  set urgencyDetails(Delivery_Urgency v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasUrgencyDetails() => $_has(3);
  @$pb.TagNumber(4)
  void clearUrgencyDetails() => clearField(4);
  @$pb.TagNumber(4)
  Delivery_Urgency ensureUrgencyDetails() => $_ensure(3);

  @$pb.TagNumber(5)
  $core.String get currency => $_getSZ(4);
  @$pb.TagNumber(5)
  set currency($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasCurrency() => $_has(4);
  @$pb.TagNumber(5)
  void clearCurrency() => clearField(5);
}

class InDeliveryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InDeliveryResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryFee', $pb.PbFieldType.OD)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..hasRequiredFields = false
  ;

  InDeliveryResponse._() : super();
  factory InDeliveryResponse({
    $core.double? deliveryFee,
    $core.String? currency,
  }) {
    final _result = create();
    if (deliveryFee != null) {
      _result.deliveryFee = deliveryFee;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    return _result;
  }
  factory InDeliveryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InDeliveryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InDeliveryResponse clone() => InDeliveryResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InDeliveryResponse copyWith(void Function(InDeliveryResponse) updates) => super.copyWith((message) => updates(message as InDeliveryResponse)) as InDeliveryResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InDeliveryResponse create() => InDeliveryResponse._();
  InDeliveryResponse createEmptyInstance() => create();
  static $pb.PbList<InDeliveryResponse> createRepeated() => $pb.PbList<InDeliveryResponse>();
  @$core.pragma('dart2js:noInline')
  static InDeliveryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InDeliveryResponse>(create);
  static InDeliveryResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get deliveryFee => $_getN(0);
  @$pb.TagNumber(1)
  set deliveryFee($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDeliveryFee() => $_has(0);
  @$pb.TagNumber(1)
  void clearDeliveryFee() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get currency => $_getSZ(1);
  @$pb.TagNumber(2)
  set currency($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCurrency() => $_has(1);
  @$pb.TagNumber(2)
  void clearCurrency() => clearField(2);
}

class InDeliveryCoordinatesRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InDeliveryCoordinatesRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'latitude', $pb.PbFieldType.OD)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'longitude', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  InDeliveryCoordinatesRequest._() : super();
  factory InDeliveryCoordinatesRequest({
    $core.double? latitude,
    $core.double? longitude,
  }) {
    final _result = create();
    if (latitude != null) {
      _result.latitude = latitude;
    }
    if (longitude != null) {
      _result.longitude = longitude;
    }
    return _result;
  }
  factory InDeliveryCoordinatesRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InDeliveryCoordinatesRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InDeliveryCoordinatesRequest clone() => InDeliveryCoordinatesRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InDeliveryCoordinatesRequest copyWith(void Function(InDeliveryCoordinatesRequest) updates) => super.copyWith((message) => updates(message as InDeliveryCoordinatesRequest)) as InDeliveryCoordinatesRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InDeliveryCoordinatesRequest create() => InDeliveryCoordinatesRequest._();
  InDeliveryCoordinatesRequest createEmptyInstance() => create();
  static $pb.PbList<InDeliveryCoordinatesRequest> createRepeated() => $pb.PbList<InDeliveryCoordinatesRequest>();
  @$core.pragma('dart2js:noInline')
  static InDeliveryCoordinatesRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InDeliveryCoordinatesRequest>(create);
  static InDeliveryCoordinatesRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get latitude => $_getN(0);
  @$pb.TagNumber(1)
  set latitude($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLatitude() => $_has(0);
  @$pb.TagNumber(1)
  void clearLatitude() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get longitude => $_getN(1);
  @$pb.TagNumber(2)
  set longitude($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLongitude() => $_has(1);
  @$pb.TagNumber(2)
  void clearLongitude() => clearField(2);
}

class TipRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TipRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..aOM<Tip>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tip', subBuilder: Tip.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..hasRequiredFields = false
  ;

  TipRequest._() : super();
  factory TipRequest({
    Tip? tip,
    $core.String? currency,
  }) {
    final _result = create();
    if (tip != null) {
      _result.tip = tip;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    return _result;
  }
  factory TipRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TipRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TipRequest clone() => TipRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TipRequest copyWith(void Function(TipRequest) updates) => super.copyWith((message) => updates(message as TipRequest)) as TipRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TipRequest create() => TipRequest._();
  TipRequest createEmptyInstance() => create();
  static $pb.PbList<TipRequest> createRepeated() => $pb.PbList<TipRequest>();
  @$core.pragma('dart2js:noInline')
  static TipRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TipRequest>(create);
  static TipRequest? _defaultInstance;

  @$pb.TagNumber(1)
  Tip get tip => $_getN(0);
  @$pb.TagNumber(1)
  set tip(Tip v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasTip() => $_has(0);
  @$pb.TagNumber(1)
  void clearTip() => clearField(1);
  @$pb.TagNumber(1)
  Tip ensureTip() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get currency => $_getSZ(1);
  @$pb.TagNumber(2)
  set currency($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCurrency() => $_has(1);
  @$pb.TagNumber(2)
  void clearCurrency() => clearField(2);
}

class TipResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TipResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tip', $pb.PbFieldType.OD)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  TipResponse._() : super();
  factory TipResponse({
    $core.double? tip,
    $core.String? currency,
    $core.String? status,
  }) {
    final _result = create();
    if (tip != null) {
      _result.tip = tip;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory TipResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TipResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TipResponse clone() => TipResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TipResponse copyWith(void Function(TipResponse) updates) => super.copyWith((message) => updates(message as TipResponse)) as TipResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TipResponse create() => TipResponse._();
  TipResponse createEmptyInstance() => create();
  static $pb.PbList<TipResponse> createRepeated() => $pb.PbList<TipResponse>();
  @$core.pragma('dart2js:noInline')
  static TipResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TipResponse>(create);
  static TipResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get tip => $_getN(0);
  @$pb.TagNumber(1)
  set tip($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTip() => $_has(0);
  @$pb.TagNumber(1)
  void clearTip() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get currency => $_getSZ(1);
  @$pb.TagNumber(2)
  set currency($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCurrency() => $_has(1);
  @$pb.TagNumber(2)
  void clearCurrency() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class GetPayLoadResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetPayLoadResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..aOM<DeliveryPayload>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Data', protoName: 'Data', subBuilder: DeliveryPayload.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  GetPayLoadResponse._() : super();
  factory GetPayLoadResponse({
    DeliveryPayload? data,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data = data;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory GetPayLoadResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetPayLoadResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetPayLoadResponse clone() => GetPayLoadResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetPayLoadResponse copyWith(void Function(GetPayLoadResponse) updates) => super.copyWith((message) => updates(message as GetPayLoadResponse)) as GetPayLoadResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetPayLoadResponse create() => GetPayLoadResponse._();
  GetPayLoadResponse createEmptyInstance() => create();
  static $pb.PbList<GetPayLoadResponse> createRepeated() => $pb.PbList<GetPayLoadResponse>();
  @$core.pragma('dart2js:noInline')
  static GetPayLoadResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetPayLoadResponse>(create);
  static GetPayLoadResponse? _defaultInstance;

  @$pb.TagNumber(1)
  DeliveryPayload get data => $_getN(0);
  @$pb.TagNumber(1)
  set data(DeliveryPayload v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasData() => $_has(0);
  @$pb.TagNumber(1)
  void clearData() => clearField(1);
  @$pb.TagNumber(1)
  DeliveryPayload ensureData() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class DeliveryPayload extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeliveryPayload', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceArea')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'country')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..pc<Tip>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tips', $pb.PbFieldType.PM, subBuilder: Tip.create)
    ..pc<Delivery_Urgency>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryUrgencies', $pb.PbFieldType.PM, subBuilder: Delivery_Urgency.create)
    ..hasRequiredFields = false
  ;

  DeliveryPayload._() : super();
  factory DeliveryPayload({
    $core.String? serviceArea,
    $core.String? description,
    $core.String? type,
    $core.String? country,
    $core.String? currency,
    $core.Iterable<Tip>? tips,
    $core.Iterable<Delivery_Urgency>? deliveryUrgencies,
  }) {
    final _result = create();
    if (serviceArea != null) {
      _result.serviceArea = serviceArea;
    }
    if (description != null) {
      _result.description = description;
    }
    if (type != null) {
      _result.type = type;
    }
    if (country != null) {
      _result.country = country;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    if (tips != null) {
      _result.tips.addAll(tips);
    }
    if (deliveryUrgencies != null) {
      _result.deliveryUrgencies.addAll(deliveryUrgencies);
    }
    return _result;
  }
  factory DeliveryPayload.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeliveryPayload.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeliveryPayload clone() => DeliveryPayload()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeliveryPayload copyWith(void Function(DeliveryPayload) updates) => super.copyWith((message) => updates(message as DeliveryPayload)) as DeliveryPayload; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeliveryPayload create() => DeliveryPayload._();
  DeliveryPayload createEmptyInstance() => create();
  static $pb.PbList<DeliveryPayload> createRepeated() => $pb.PbList<DeliveryPayload>();
  @$core.pragma('dart2js:noInline')
  static DeliveryPayload getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeliveryPayload>(create);
  static DeliveryPayload? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get serviceArea => $_getSZ(0);
  @$pb.TagNumber(1)
  set serviceArea($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasServiceArea() => $_has(0);
  @$pb.TagNumber(1)
  void clearServiceArea() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get description => $_getSZ(1);
  @$pb.TagNumber(2)
  set description($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDescription() => $_has(1);
  @$pb.TagNumber(2)
  void clearDescription() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get type => $_getSZ(2);
  @$pb.TagNumber(3)
  set type($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasType() => $_has(2);
  @$pb.TagNumber(3)
  void clearType() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get country => $_getSZ(3);
  @$pb.TagNumber(4)
  set country($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCountry() => $_has(3);
  @$pb.TagNumber(4)
  void clearCountry() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get currency => $_getSZ(4);
  @$pb.TagNumber(5)
  set currency($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasCurrency() => $_has(4);
  @$pb.TagNumber(5)
  void clearCurrency() => clearField(5);

  @$pb.TagNumber(6)
  $core.List<Tip> get tips => $_getList(5);

  @$pb.TagNumber(7)
  $core.List<Delivery_Urgency> get deliveryUrgencies => $_getList(6);
}

class DeliveryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeliveryResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  DeliveryResponse._() : super();
  factory DeliveryResponse({
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory DeliveryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeliveryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeliveryResponse clone() => DeliveryResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeliveryResponse copyWith(void Function(DeliveryResponse) updates) => super.copyWith((message) => updates(message as DeliveryResponse)) as DeliveryResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeliveryResponse create() => DeliveryResponse._();
  DeliveryResponse createEmptyInstance() => create();
  static $pb.PbList<DeliveryResponse> createRepeated() => $pb.PbList<DeliveryResponse>();
  @$core.pragma('dart2js:noInline')
  static DeliveryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeliveryResponse>(create);
  static DeliveryResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class DeliveryRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeliveryRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceArea')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'limit', $pb.PbFieldType.O3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offset', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  DeliveryRequest._() : super();
  factory DeliveryRequest({
    $core.String? serviceArea,
    $core.int? limit,
    $core.int? offset,
  }) {
    final _result = create();
    if (serviceArea != null) {
      _result.serviceArea = serviceArea;
    }
    if (limit != null) {
      _result.limit = limit;
    }
    if (offset != null) {
      _result.offset = offset;
    }
    return _result;
  }
  factory DeliveryRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeliveryRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeliveryRequest clone() => DeliveryRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeliveryRequest copyWith(void Function(DeliveryRequest) updates) => super.copyWith((message) => updates(message as DeliveryRequest)) as DeliveryRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeliveryRequest create() => DeliveryRequest._();
  DeliveryRequest createEmptyInstance() => create();
  static $pb.PbList<DeliveryRequest> createRepeated() => $pb.PbList<DeliveryRequest>();
  @$core.pragma('dart2js:noInline')
  static DeliveryRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeliveryRequest>(create);
  static DeliveryRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get serviceArea => $_getSZ(0);
  @$pb.TagNumber(1)
  set serviceArea($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasServiceArea() => $_has(0);
  @$pb.TagNumber(1)
  void clearServiceArea() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get limit => $_getIZ(1);
  @$pb.TagNumber(2)
  set limit($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLimit() => $_has(1);
  @$pb.TagNumber(2)
  void clearLimit() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get offset => $_getIZ(2);
  @$pb.TagNumber(3)
  set offset($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasOffset() => $_has(2);
  @$pb.TagNumber(3)
  void clearOffset() => clearField(3);
}

class DeliveryDeleteRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeliveryDeleteRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceArea')
    ..hasRequiredFields = false
  ;

  DeliveryDeleteRequest._() : super();
  factory DeliveryDeleteRequest({
    $core.String? serviceArea,
  }) {
    final _result = create();
    if (serviceArea != null) {
      _result.serviceArea = serviceArea;
    }
    return _result;
  }
  factory DeliveryDeleteRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeliveryDeleteRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeliveryDeleteRequest clone() => DeliveryDeleteRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeliveryDeleteRequest copyWith(void Function(DeliveryDeleteRequest) updates) => super.copyWith((message) => updates(message as DeliveryDeleteRequest)) as DeliveryDeleteRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeliveryDeleteRequest create() => DeliveryDeleteRequest._();
  DeliveryDeleteRequest createEmptyInstance() => create();
  static $pb.PbList<DeliveryDeleteRequest> createRepeated() => $pb.PbList<DeliveryDeleteRequest>();
  @$core.pragma('dart2js:noInline')
  static DeliveryDeleteRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeliveryDeleteRequest>(create);
  static DeliveryDeleteRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get serviceArea => $_getSZ(0);
  @$pb.TagNumber(1)
  set serviceArea($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasServiceArea() => $_has(0);
  @$pb.TagNumber(1)
  void clearServiceArea() => clearField(1);
}

class GetAllDeliveriesRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAllDeliveriesRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'limit', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offset', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  GetAllDeliveriesRequest._() : super();
  factory GetAllDeliveriesRequest({
    $core.int? limit,
    $core.int? offset,
  }) {
    final _result = create();
    if (limit != null) {
      _result.limit = limit;
    }
    if (offset != null) {
      _result.offset = offset;
    }
    return _result;
  }
  factory GetAllDeliveriesRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllDeliveriesRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAllDeliveriesRequest clone() => GetAllDeliveriesRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAllDeliveriesRequest copyWith(void Function(GetAllDeliveriesRequest) updates) => super.copyWith((message) => updates(message as GetAllDeliveriesRequest)) as GetAllDeliveriesRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllDeliveriesRequest create() => GetAllDeliveriesRequest._();
  GetAllDeliveriesRequest createEmptyInstance() => create();
  static $pb.PbList<GetAllDeliveriesRequest> createRepeated() => $pb.PbList<GetAllDeliveriesRequest>();
  @$core.pragma('dart2js:noInline')
  static GetAllDeliveriesRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllDeliveriesRequest>(create);
  static GetAllDeliveriesRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get limit => $_getIZ(0);
  @$pb.TagNumber(1)
  set limit($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLimit() => $_has(0);
  @$pb.TagNumber(1)
  void clearLimit() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get offset => $_getIZ(1);
  @$pb.TagNumber(2)
  set offset($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOffset() => $_has(1);
  @$pb.TagNumber(2)
  void clearOffset() => clearField(2);
}

class GetAllDeliveryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetAllDeliveryResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..pc<DeliveryPayload>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Data', $pb.PbFieldType.PM, protoName: 'Data', subBuilder: DeliveryPayload.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  GetAllDeliveryResponse._() : super();
  factory GetAllDeliveryResponse({
    $core.Iterable<DeliveryPayload>? data,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory GetAllDeliveryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllDeliveryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetAllDeliveryResponse clone() => GetAllDeliveryResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetAllDeliveryResponse copyWith(void Function(GetAllDeliveryResponse) updates) => super.copyWith((message) => updates(message as GetAllDeliveryResponse)) as GetAllDeliveryResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllDeliveryResponse create() => GetAllDeliveryResponse._();
  GetAllDeliveryResponse createEmptyInstance() => create();
  static $pb.PbList<GetAllDeliveryResponse> createRepeated() => $pb.PbList<GetAllDeliveryResponse>();
  @$core.pragma('dart2js:noInline')
  static GetAllDeliveryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllDeliveryResponse>(create);
  static GetAllDeliveryResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<DeliveryPayload> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class Tip extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Tip', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'amount', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Tip._() : super();
  factory Tip({
    $core.double? amount,
  }) {
    final _result = create();
    if (amount != null) {
      _result.amount = amount;
    }
    return _result;
  }
  factory Tip.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Tip.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Tip clone() => Tip()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Tip copyWith(void Function(Tip) updates) => super.copyWith((message) => updates(message as Tip)) as Tip; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Tip create() => Tip._();
  Tip createEmptyInstance() => create();
  static $pb.PbList<Tip> createRepeated() => $pb.PbList<Tip>();
  @$core.pragma('dart2js:noInline')
  static Tip getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Tip>(create);
  static Tip? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get amount => $_getN(0);
  @$pb.TagNumber(1)
  set amount($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAmount() => $_has(0);
  @$pb.TagNumber(1)
  void clearAmount() => clearField(1);
}

class Delivery_Urgency extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Delivery_Urgency', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'delivery_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'amount', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Delivery_Urgency._() : super();
  factory Delivery_Urgency({
    $core.String? name,
    $core.double? amount,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (amount != null) {
      _result.amount = amount;
    }
    return _result;
  }
  factory Delivery_Urgency.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Delivery_Urgency.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Delivery_Urgency clone() => Delivery_Urgency()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Delivery_Urgency copyWith(void Function(Delivery_Urgency) updates) => super.copyWith((message) => updates(message as Delivery_Urgency)) as Delivery_Urgency; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Delivery_Urgency create() => Delivery_Urgency._();
  Delivery_Urgency createEmptyInstance() => create();
  static $pb.PbList<Delivery_Urgency> createRepeated() => $pb.PbList<Delivery_Urgency>();
  @$core.pragma('dart2js:noInline')
  static Delivery_Urgency getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Delivery_Urgency>(create);
  static Delivery_Urgency? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get amount => $_getN(1);
  @$pb.TagNumber(2)
  set amount($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAmount() => $_has(1);
  @$pb.TagNumber(2)
  void clearAmount() => clearField(2);
}

