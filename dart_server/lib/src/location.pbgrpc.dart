///
//  Generated code. Do not modify.
//  source: location.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'location.pb.dart' as $0;
export 'location.pb.dart';

class LocationServiceClient extends $grpc.Client {
  static final _$addLocation =
      $grpc.ClientMethod<$0.LocationItems, $0.LocationResponse>(
          '/location_service.v1.LocationService/AddLocation',
          ($0.LocationItems value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$updateLocation =
      $grpc.ClientMethod<$0.LocationItems, $0.LocationResponse>(
          '/location_service.v1.LocationService/UpdateLocation',
          ($0.LocationItems value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$viewLocation =
      $grpc.ClientMethod<$0.LocationRequestItem, $0.LocationItems>(
          '/location_service.v1.LocationService/ViewLocation',
          ($0.LocationRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.LocationItems.fromBuffer(value));
  static final _$deleteLocation =
      $grpc.ClientMethod<$0.LocationRequestItem, $0.LocationResponse>(
          '/location_service.v1.LocationService/DeleteLocation',
          ($0.LocationRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$listLocations =
      $grpc.ClientMethod<$0.EmptyLocationRequest, $0.ListLocationResponse>(
          '/location_service.v1.LocationService/ListLocations',
          ($0.EmptyLocationRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListLocationResponse.fromBuffer(value));
  static final _$listByServiceArea =
      $grpc.ClientMethod<$0.ServiceAreaLocationList, $0.ListLocationResponse>(
          '/location_service.v1.LocationService/ListByServiceArea',
          ($0.ServiceAreaLocationList value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListLocationResponse.fromBuffer(value));
  static final _$addRestaurantLocation =
      $grpc.ClientMethod<$0.RestaurantLocationItems, $0.LocationResponse>(
          '/location_service.v1.LocationService/AddRestaurantLocation',
          ($0.RestaurantLocationItems value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$addUserLocation =
      $grpc.ClientMethod<$0.UserLocationItems, $0.LocationResponse>(
          '/location_service.v1.LocationService/AddUserLocation',
          ($0.UserLocationItems value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$updateRestaurantLocation =
      $grpc.ClientMethod<$0.RestaurantLocationItems, $0.LocationResponse>(
          '/location_service.v1.LocationService/UpdateRestaurantLocation',
          ($0.RestaurantLocationItems value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$updateUserLocation =
      $grpc.ClientMethod<$0.UserLocationItems, $0.UserLocationItems>(
          '/location_service.v1.LocationService/UpdateUserLocation',
          ($0.UserLocationItems value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UserLocationItems.fromBuffer(value));
  static final _$viewUserLocation =
      $grpc.ClientMethod<$0.LocationRequestItem, $0.ViewUserLocationResponse>(
          '/location_service.v1.LocationService/ViewUserLocation',
          ($0.LocationRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ViewUserLocationResponse.fromBuffer(value));
  static final _$viewRestaurantLocation = $grpc.ClientMethod<
          $0.LocationRequestItem, $0.ViewRestaurantLocationResponse>(
      '/location_service.v1.LocationService/ViewRestaurantLocation',
      ($0.LocationRequestItem value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ViewRestaurantLocationResponse.fromBuffer(value));
  static final _$listUserLocations =
      $grpc.ClientMethod<$0.EmptyLocationRequest, $0.ListUsersLocationResponse>(
          '/location_service.v1.LocationService/ListUserLocations',
          ($0.EmptyLocationRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListUsersLocationResponse.fromBuffer(value));
  static final _$listRestaurantLocations = $grpc.ClientMethod<
          $0.EmptyLocationRequest, $0.ListRestaurantLocationResponse>(
      '/location_service.v1.LocationService/ListRestaurantLocations',
      ($0.EmptyLocationRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ListRestaurantLocationResponse.fromBuffer(value));
  static final _$addServiceArea =
      $grpc.ClientMethod<$0.ServiceArea, $0.LocationResponse>(
          '/location_service.v1.LocationService/AddServiceArea',
          ($0.ServiceArea value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$viewServiceArea =
      $grpc.ClientMethod<$0.ServiceAreaRequest, $0.ServiceArea>(
          '/location_service.v1.LocationService/ViewServiceArea',
          ($0.ServiceAreaRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ServiceArea.fromBuffer(value));
  static final _$listServiceArea =
      $grpc.ClientMethod<$0.EmptyLocationRequest, $0.ListServiceAreaResponse>(
          '/location_service.v1.LocationService/ListServiceArea',
          ($0.EmptyLocationRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListServiceAreaResponse.fromBuffer(value));
  static final _$editServiceArea =
      $grpc.ClientMethod<$0.ServiceArea, $0.LocationResponse>(
          '/location_service.v1.LocationService/EditServiceArea',
          ($0.ServiceArea value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$removeServiceArea =
      $grpc.ClientMethod<$0.ServiceAreaRequest, $0.LocationResponse>(
          '/location_service.v1.LocationService/RemoveServiceArea',
          ($0.ServiceAreaRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LocationResponse.fromBuffer(value));
  static final _$getMinServiceArea =
      $grpc.ClientMethod<$0.Coordinate, $0.ServiceArea>(
          '/location_service.v1.LocationService/GetMinServiceArea',
          ($0.Coordinate value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ServiceArea.fromBuffer(value));

  LocationServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.LocationResponse> addLocation(
      $0.LocationItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addLocation, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationResponse> updateLocation(
      $0.LocationItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateLocation, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationItems> viewLocation(
      $0.LocationRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewLocation, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationResponse> deleteLocation(
      $0.LocationRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteLocation, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListLocationResponse> listLocations(
      $0.EmptyLocationRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listLocations, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListLocationResponse> listByServiceArea(
      $0.ServiceAreaLocationList request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listByServiceArea, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationResponse> addRestaurantLocation(
      $0.RestaurantLocationItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addRestaurantLocation, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationResponse> addUserLocation(
      $0.UserLocationItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addUserLocation, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationResponse> updateRestaurantLocation(
      $0.RestaurantLocationItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateRestaurantLocation, request,
        options: options);
  }

  $grpc.ResponseStream<$0.UserLocationItems> updateUserLocation(
      $async.Stream<$0.UserLocationItems> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$updateUserLocation, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.ViewUserLocationResponse> viewUserLocation(
      $0.LocationRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewUserLocation, request, options: options);
  }

  $grpc.ResponseFuture<$0.ViewRestaurantLocationResponse>
      viewRestaurantLocation($0.LocationRequestItem request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewRestaurantLocation, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.ListUsersLocationResponse> listUserLocations(
      $0.EmptyLocationRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listUserLocations, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListRestaurantLocationResponse>
      listRestaurantLocations($0.EmptyLocationRequest request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listRestaurantLocations, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.LocationResponse> addServiceArea(
      $0.ServiceArea request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addServiceArea, request, options: options);
  }

  $grpc.ResponseFuture<$0.ServiceArea> viewServiceArea(
      $0.ServiceAreaRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewServiceArea, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListServiceAreaResponse> listServiceArea(
      $0.EmptyLocationRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listServiceArea, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationResponse> editServiceArea(
      $0.ServiceArea request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$editServiceArea, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationResponse> removeServiceArea(
      $0.ServiceAreaRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removeServiceArea, request, options: options);
  }

  $grpc.ResponseFuture<$0.ServiceArea> getMinServiceArea($0.Coordinate request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getMinServiceArea, request, options: options);
  }
}

abstract class LocationServiceBase extends $grpc.Service {
  $core.String get $name => 'location_service.v1.LocationService';

  LocationServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.LocationItems, $0.LocationResponse>(
        'AddLocation',
        addLocation_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LocationItems.fromBuffer(value),
        ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LocationItems, $0.LocationResponse>(
        'UpdateLocation',
        updateLocation_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LocationItems.fromBuffer(value),
        ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LocationRequestItem, $0.LocationItems>(
        'ViewLocation',
        viewLocation_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.LocationRequestItem.fromBuffer(value),
        ($0.LocationItems value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LocationRequestItem, $0.LocationResponse>(
        'DeleteLocation',
        deleteLocation_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.LocationRequestItem.fromBuffer(value),
        ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.EmptyLocationRequest, $0.ListLocationResponse>(
            'ListLocations',
            listLocations_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.EmptyLocationRequest.fromBuffer(value),
            ($0.ListLocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ServiceAreaLocationList,
            $0.ListLocationResponse>(
        'ListByServiceArea',
        listByServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ServiceAreaLocationList.fromBuffer(value),
        ($0.ListLocationResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.RestaurantLocationItems, $0.LocationResponse>(
            'AddRestaurantLocation',
            addRestaurantLocation_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.RestaurantLocationItems.fromBuffer(value),
            ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserLocationItems, $0.LocationResponse>(
        'AddUserLocation',
        addUserLocation_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserLocationItems.fromBuffer(value),
        ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.RestaurantLocationItems, $0.LocationResponse>(
            'UpdateRestaurantLocation',
            updateRestaurantLocation_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.RestaurantLocationItems.fromBuffer(value),
            ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserLocationItems, $0.UserLocationItems>(
        'UpdateUserLocation',
        updateUserLocation,
        true,
        true,
        ($core.List<$core.int> value) => $0.UserLocationItems.fromBuffer(value),
        ($0.UserLocationItems value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LocationRequestItem,
            $0.ViewUserLocationResponse>(
        'ViewUserLocation',
        viewUserLocation_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.LocationRequestItem.fromBuffer(value),
        ($0.ViewUserLocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LocationRequestItem,
            $0.ViewRestaurantLocationResponse>(
        'ViewRestaurantLocation',
        viewRestaurantLocation_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.LocationRequestItem.fromBuffer(value),
        ($0.ViewRestaurantLocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyLocationRequest,
            $0.ListUsersLocationResponse>(
        'ListUserLocations',
        listUserLocations_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.EmptyLocationRequest.fromBuffer(value),
        ($0.ListUsersLocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyLocationRequest,
            $0.ListRestaurantLocationResponse>(
        'ListRestaurantLocations',
        listRestaurantLocations_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.EmptyLocationRequest.fromBuffer(value),
        ($0.ListRestaurantLocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ServiceArea, $0.LocationResponse>(
        'AddServiceArea',
        addServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ServiceArea.fromBuffer(value),
        ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ServiceAreaRequest, $0.ServiceArea>(
        'ViewServiceArea',
        viewServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ServiceAreaRequest.fromBuffer(value),
        ($0.ServiceArea value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyLocationRequest,
            $0.ListServiceAreaResponse>(
        'ListServiceArea',
        listServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.EmptyLocationRequest.fromBuffer(value),
        ($0.ListServiceAreaResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ServiceArea, $0.LocationResponse>(
        'EditServiceArea',
        editServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ServiceArea.fromBuffer(value),
        ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ServiceAreaRequest, $0.LocationResponse>(
        'RemoveServiceArea',
        removeServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ServiceAreaRequest.fromBuffer(value),
        ($0.LocationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Coordinate, $0.ServiceArea>(
        'GetMinServiceArea',
        getMinServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Coordinate.fromBuffer(value),
        ($0.ServiceArea value) => value.writeToBuffer()));
  }

  $async.Future<$0.LocationResponse> addLocation_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LocationItems> request) async {
    return addLocation(call, await request);
  }

  $async.Future<$0.LocationResponse> updateLocation_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LocationItems> request) async {
    return updateLocation(call, await request);
  }

  $async.Future<$0.LocationItems> viewLocation_Pre($grpc.ServiceCall call,
      $async.Future<$0.LocationRequestItem> request) async {
    return viewLocation(call, await request);
  }

  $async.Future<$0.LocationResponse> deleteLocation_Pre($grpc.ServiceCall call,
      $async.Future<$0.LocationRequestItem> request) async {
    return deleteLocation(call, await request);
  }

  $async.Future<$0.ListLocationResponse> listLocations_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptyLocationRequest> request) async {
    return listLocations(call, await request);
  }

  $async.Future<$0.ListLocationResponse> listByServiceArea_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ServiceAreaLocationList> request) async {
    return listByServiceArea(call, await request);
  }

  $async.Future<$0.LocationResponse> addRestaurantLocation_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.RestaurantLocationItems> request) async {
    return addRestaurantLocation(call, await request);
  }

  $async.Future<$0.LocationResponse> addUserLocation_Pre($grpc.ServiceCall call,
      $async.Future<$0.UserLocationItems> request) async {
    return addUserLocation(call, await request);
  }

  $async.Future<$0.LocationResponse> updateRestaurantLocation_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.RestaurantLocationItems> request) async {
    return updateRestaurantLocation(call, await request);
  }

  $async.Future<$0.ViewUserLocationResponse> viewUserLocation_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.LocationRequestItem> request) async {
    return viewUserLocation(call, await request);
  }

  $async.Future<$0.ViewRestaurantLocationResponse> viewRestaurantLocation_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.LocationRequestItem> request) async {
    return viewRestaurantLocation(call, await request);
  }

  $async.Future<$0.ListUsersLocationResponse> listUserLocations_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptyLocationRequest> request) async {
    return listUserLocations(call, await request);
  }

  $async.Future<$0.ListRestaurantLocationResponse> listRestaurantLocations_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptyLocationRequest> request) async {
    return listRestaurantLocations(call, await request);
  }

  $async.Future<$0.LocationResponse> addServiceArea_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ServiceArea> request) async {
    return addServiceArea(call, await request);
  }

  $async.Future<$0.ServiceArea> viewServiceArea_Pre($grpc.ServiceCall call,
      $async.Future<$0.ServiceAreaRequest> request) async {
    return viewServiceArea(call, await request);
  }

  $async.Future<$0.ListServiceAreaResponse> listServiceArea_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptyLocationRequest> request) async {
    return listServiceArea(call, await request);
  }

  $async.Future<$0.LocationResponse> editServiceArea_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ServiceArea> request) async {
    return editServiceArea(call, await request);
  }

  $async.Future<$0.LocationResponse> removeServiceArea_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ServiceAreaRequest> request) async {
    return removeServiceArea(call, await request);
  }

  $async.Future<$0.ServiceArea> getMinServiceArea_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Coordinate> request) async {
    return getMinServiceArea(call, await request);
  }

  $async.Future<$0.LocationResponse> addLocation(
      $grpc.ServiceCall call, $0.LocationItems request);
  $async.Future<$0.LocationResponse> updateLocation(
      $grpc.ServiceCall call, $0.LocationItems request);
  $async.Future<$0.LocationItems> viewLocation(
      $grpc.ServiceCall call, $0.LocationRequestItem request);
  $async.Future<$0.LocationResponse> deleteLocation(
      $grpc.ServiceCall call, $0.LocationRequestItem request);
  $async.Future<$0.ListLocationResponse> listLocations(
      $grpc.ServiceCall call, $0.EmptyLocationRequest request);
  $async.Future<$0.ListLocationResponse> listByServiceArea(
      $grpc.ServiceCall call, $0.ServiceAreaLocationList request);
  $async.Future<$0.LocationResponse> addRestaurantLocation(
      $grpc.ServiceCall call, $0.RestaurantLocationItems request);
  $async.Future<$0.LocationResponse> addUserLocation(
      $grpc.ServiceCall call, $0.UserLocationItems request);
  $async.Future<$0.LocationResponse> updateRestaurantLocation(
      $grpc.ServiceCall call, $0.RestaurantLocationItems request);
  $async.Stream<$0.UserLocationItems> updateUserLocation(
      $grpc.ServiceCall call, $async.Stream<$0.UserLocationItems> request);
  $async.Future<$0.ViewUserLocationResponse> viewUserLocation(
      $grpc.ServiceCall call, $0.LocationRequestItem request);
  $async.Future<$0.ViewRestaurantLocationResponse> viewRestaurantLocation(
      $grpc.ServiceCall call, $0.LocationRequestItem request);
  $async.Future<$0.ListUsersLocationResponse> listUserLocations(
      $grpc.ServiceCall call, $0.EmptyLocationRequest request);
  $async.Future<$0.ListRestaurantLocationResponse> listRestaurantLocations(
      $grpc.ServiceCall call, $0.EmptyLocationRequest request);
  $async.Future<$0.LocationResponse> addServiceArea(
      $grpc.ServiceCall call, $0.ServiceArea request);
  $async.Future<$0.ServiceArea> viewServiceArea(
      $grpc.ServiceCall call, $0.ServiceAreaRequest request);
  $async.Future<$0.ListServiceAreaResponse> listServiceArea(
      $grpc.ServiceCall call, $0.EmptyLocationRequest request);
  $async.Future<$0.LocationResponse> editServiceArea(
      $grpc.ServiceCall call, $0.ServiceArea request);
  $async.Future<$0.LocationResponse> removeServiceArea(
      $grpc.ServiceCall call, $0.ServiceAreaRequest request);
  $async.Future<$0.ServiceArea> getMinServiceArea(
      $grpc.ServiceCall call, $0.Coordinate request);
}
