///
//  Generated code. Do not modify.
//  source: agent.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'agent.pb.dart' as $0;
export 'agent.pb.dart';

class AgentServiceClient extends $grpc.Client {
  static final _$createOrderRequest = $grpc.ClientMethod<
          $0.CreateOrderRequest_Request, $0.CreateOrderRequest_Response>(
      '/AgentService/CreateOrderRequest',
      ($0.CreateOrderRequest_Request value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.CreateOrderRequest_Response.fromBuffer(value));
  static final _$getAllOrderRequests = $grpc.ClientMethod<
          $0.GetAllOrderRequests_Request, $0.GetAllOrderRequests_Response>(
      '/AgentService/GetAllOrderRequests',
      ($0.GetAllOrderRequests_Request value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetAllOrderRequests_Response.fromBuffer(value));
  static final _$sendOrderRequestToAgent = $grpc.ClientMethod<
          $0.SendOrderRequestToAgent_Request,
          $0.SendOrderRequestToAgent_Response>(
      '/AgentService/SendOrderRequestToAgent',
      ($0.SendOrderRequestToAgent_Request value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.SendOrderRequestToAgent_Response.fromBuffer(value));
  static final _$acceptOrDeclineOrderRequest = $grpc.ClientMethod<
          $0.AcceptOrDeclineOrderRequest_Request,
          $0.AcceptOrDeclineOrderRequest_Response>(
      '/AgentService/AcceptOrDeclineOrderRequest',
      ($0.AcceptOrDeclineOrderRequest_Request value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.AcceptOrDeclineOrderRequest_Response.fromBuffer(value));
  static final _$updateOrderRequestStatus = $grpc.ClientMethod<
          $0.UpdateOrderRequestStatus_Request,
          $0.UpdateOrderRequestStatus_Response>(
      '/AgentService/UpdateOrderRequestStatus',
      ($0.UpdateOrderRequestStatus_Request value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.UpdateOrderRequestStatus_Response.fromBuffer(value));
  static final _$getOrderRequestHistory = $grpc.ClientMethod<
          $0.GetOrderRequestHistory_Request,
          $0.GetOrderRequestHistory_Response>(
      '/AgentService/GetOrderRequestHistory',
      ($0.GetOrderRequestHistory_Request value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetOrderRequestHistory_Response.fromBuffer(value));
  static final _$getOrderRequest = $grpc.ClientMethod<
          $0.GetAssignedOrderRequest_Request,
          $0.GetAssignedOrderRequest_Response>(
      '/AgentService/GetOrderRequest',
      ($0.GetAssignedOrderRequest_Request value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetAssignedOrderRequest_Response.fromBuffer(value));
  static final _$createAgent =
      $grpc.ClientMethod<$0.CreateAgentRequest, $0.CreateAgentResponse>(
          '/AgentService/CreateAgent',
          ($0.CreateAgentRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.CreateAgentResponse.fromBuffer(value));
  static final _$getAllAgents =
      $grpc.ClientMethod<$0.GetAllAgentsRequest, $0.GetAllAgentsResponse>(
          '/AgentService/GetAllAgents',
          ($0.GetAllAgentsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetAllAgentsResponse.fromBuffer(value));
  static final _$getAgentById =
      $grpc.ClientMethod<$0.GetAgentByIdRequest, $0.GetAgentByIdResponse>(
          '/AgentService/GetAgentById',
          ($0.GetAgentByIdRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetAgentByIdResponse.fromBuffer(value));
  static final _$getAgentInfo =
      $grpc.ClientMethod<$0.GetAgentInfoRequest, $0.GetAgentInfoResponse>(
          '/AgentService/GetAgentInfo',
          ($0.GetAgentInfoRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetAgentInfoResponse.fromBuffer(value));
  static final _$changeAvailabilityStatus = $grpc.ClientMethod<
          $0.ChangeAvailabilityStatusRequest,
          $0.ChangeAvailabilityStatusResponse>(
      '/AgentService/ChangeAvailabilityStatus',
      ($0.ChangeAvailabilityStatusRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ChangeAvailabilityStatusResponse.fromBuffer(value));
  static final _$getAllAgentWallets =
      $grpc.ClientMethod<$0.GetAllAgentsRequest, $0.GetAllAgentWalletsResponse>(
          '/AgentService/GetAllAgentWallets',
          ($0.GetAllAgentsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetAllAgentWalletsResponse.fromBuffer(value));
  static final _$getWalletByAgentId = $grpc.ClientMethod<
          $0.GetWalletByAgentIdRequest, $0.GetWalletByAgentIdResponse>(
      '/AgentService/GetWalletByAgentId',
      ($0.GetWalletByAgentIdRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetWalletByAgentIdResponse.fromBuffer(value));

  AgentServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.CreateOrderRequest_Response> createOrderRequest(
      $0.CreateOrderRequest_Request request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createOrderRequest, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetAllOrderRequests_Response> getAllOrderRequests(
      $0.GetAllOrderRequests_Request request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllOrderRequests, request, options: options);
  }

  $grpc.ResponseFuture<$0.SendOrderRequestToAgent_Response>
      sendOrderRequestToAgent($0.SendOrderRequestToAgent_Request request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sendOrderRequestToAgent, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.AcceptOrDeclineOrderRequest_Response>
      acceptOrDeclineOrderRequest(
          $0.AcceptOrDeclineOrderRequest_Request request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$acceptOrDeclineOrderRequest, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.UpdateOrderRequestStatus_Response>
      updateOrderRequestStatus($0.UpdateOrderRequestStatus_Request request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateOrderRequestStatus, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.GetOrderRequestHistory_Response>
      getOrderRequestHistory($0.GetOrderRequestHistory_Request request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getOrderRequestHistory, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.GetAssignedOrderRequest_Response> getOrderRequest(
      $0.GetAssignedOrderRequest_Request request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getOrderRequest, request, options: options);
  }

  $grpc.ResponseFuture<$0.CreateAgentResponse> createAgent(
      $0.CreateAgentRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createAgent, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetAllAgentsResponse> getAllAgents(
      $0.GetAllAgentsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllAgents, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetAgentByIdResponse> getAgentById(
      $0.GetAgentByIdRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAgentById, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetAgentInfoResponse> getAgentInfo(
      $0.GetAgentInfoRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAgentInfo, request, options: options);
  }

  $grpc.ResponseFuture<$0.ChangeAvailabilityStatusResponse>
      changeAvailabilityStatus($0.ChangeAvailabilityStatusRequest request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$changeAvailabilityStatus, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.GetAllAgentWalletsResponse> getAllAgentWallets(
      $0.GetAllAgentsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllAgentWallets, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetWalletByAgentIdResponse> getWalletByAgentId(
      $0.GetWalletByAgentIdRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getWalletByAgentId, request, options: options);
  }
}

abstract class AgentServiceBase extends $grpc.Service {
  $core.String get $name => 'AgentService';

  AgentServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CreateOrderRequest_Request,
            $0.CreateOrderRequest_Response>(
        'CreateOrderRequest',
        createOrderRequest_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.CreateOrderRequest_Request.fromBuffer(value),
        ($0.CreateOrderRequest_Response value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetAllOrderRequests_Request,
            $0.GetAllOrderRequests_Response>(
        'GetAllOrderRequests',
        getAllOrderRequests_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetAllOrderRequests_Request.fromBuffer(value),
        ($0.GetAllOrderRequests_Response value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SendOrderRequestToAgent_Request,
            $0.SendOrderRequestToAgent_Response>(
        'SendOrderRequestToAgent',
        sendOrderRequestToAgent_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.SendOrderRequestToAgent_Request.fromBuffer(value),
        ($0.SendOrderRequestToAgent_Response value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AcceptOrDeclineOrderRequest_Request,
            $0.AcceptOrDeclineOrderRequest_Response>(
        'AcceptOrDeclineOrderRequest',
        acceptOrDeclineOrderRequest_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.AcceptOrDeclineOrderRequest_Request.fromBuffer(value),
        ($0.AcceptOrDeclineOrderRequest_Response value) =>
            value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UpdateOrderRequestStatus_Request,
            $0.UpdateOrderRequestStatus_Response>(
        'UpdateOrderRequestStatus',
        updateOrderRequestStatus_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.UpdateOrderRequestStatus_Request.fromBuffer(value),
        ($0.UpdateOrderRequestStatus_Response value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetOrderRequestHistory_Request,
            $0.GetOrderRequestHistory_Response>(
        'GetOrderRequestHistory',
        getOrderRequestHistory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetOrderRequestHistory_Request.fromBuffer(value),
        ($0.GetOrderRequestHistory_Response value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetAssignedOrderRequest_Request,
            $0.GetAssignedOrderRequest_Response>(
        'GetOrderRequest',
        getOrderRequest_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetAssignedOrderRequest_Request.fromBuffer(value),
        ($0.GetAssignedOrderRequest_Response value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.CreateAgentRequest, $0.CreateAgentResponse>(
            'CreateAgent',
            createAgent_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.CreateAgentRequest.fromBuffer(value),
            ($0.CreateAgentResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.GetAllAgentsRequest, $0.GetAllAgentsResponse>(
            'GetAllAgents',
            getAllAgents_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.GetAllAgentsRequest.fromBuffer(value),
            ($0.GetAllAgentsResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.GetAgentByIdRequest, $0.GetAgentByIdResponse>(
            'GetAgentById',
            getAgentById_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.GetAgentByIdRequest.fromBuffer(value),
            ($0.GetAgentByIdResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.GetAgentInfoRequest, $0.GetAgentInfoResponse>(
            'GetAgentInfo',
            getAgentInfo_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.GetAgentInfoRequest.fromBuffer(value),
            ($0.GetAgentInfoResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ChangeAvailabilityStatusRequest,
            $0.ChangeAvailabilityStatusResponse>(
        'ChangeAvailabilityStatus',
        changeAvailabilityStatus_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ChangeAvailabilityStatusRequest.fromBuffer(value),
        ($0.ChangeAvailabilityStatusResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetAllAgentsRequest,
            $0.GetAllAgentWalletsResponse>(
        'GetAllAgentWallets',
        getAllAgentWallets_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetAllAgentsRequest.fromBuffer(value),
        ($0.GetAllAgentWalletsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetWalletByAgentIdRequest,
            $0.GetWalletByAgentIdResponse>(
        'GetWalletByAgentId',
        getWalletByAgentId_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetWalletByAgentIdRequest.fromBuffer(value),
        ($0.GetWalletByAgentIdResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.CreateOrderRequest_Response> createOrderRequest_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.CreateOrderRequest_Request> request) async {
    return createOrderRequest(call, await request);
  }

  $async.Future<$0.GetAllOrderRequests_Response> getAllOrderRequests_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAllOrderRequests_Request> request) async {
    return getAllOrderRequests(call, await request);
  }

  $async.Future<$0.SendOrderRequestToAgent_Response>
      sendOrderRequestToAgent_Pre($grpc.ServiceCall call,
          $async.Future<$0.SendOrderRequestToAgent_Request> request) async {
    return sendOrderRequestToAgent(call, await request);
  }

  $async.Future<$0.AcceptOrDeclineOrderRequest_Response>
      acceptOrDeclineOrderRequest_Pre($grpc.ServiceCall call,
          $async.Future<$0.AcceptOrDeclineOrderRequest_Request> request) async {
    return acceptOrDeclineOrderRequest(call, await request);
  }

  $async.Future<$0.UpdateOrderRequestStatus_Response>
      updateOrderRequestStatus_Pre($grpc.ServiceCall call,
          $async.Future<$0.UpdateOrderRequestStatus_Request> request) async {
    return updateOrderRequestStatus(call, await request);
  }

  $async.Future<$0.GetOrderRequestHistory_Response> getOrderRequestHistory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetOrderRequestHistory_Request> request) async {
    return getOrderRequestHistory(call, await request);
  }

  $async.Future<$0.GetAssignedOrderRequest_Response> getOrderRequest_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAssignedOrderRequest_Request> request) async {
    return getOrderRequest(call, await request);
  }

  $async.Future<$0.CreateAgentResponse> createAgent_Pre($grpc.ServiceCall call,
      $async.Future<$0.CreateAgentRequest> request) async {
    return createAgent(call, await request);
  }

  $async.Future<$0.GetAllAgentsResponse> getAllAgents_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAllAgentsRequest> request) async {
    return getAllAgents(call, await request);
  }

  $async.Future<$0.GetAgentByIdResponse> getAgentById_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAgentByIdRequest> request) async {
    return getAgentById(call, await request);
  }

  $async.Future<$0.GetAgentInfoResponse> getAgentInfo_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAgentInfoRequest> request) async {
    return getAgentInfo(call, await request);
  }

  $async.Future<$0.ChangeAvailabilityStatusResponse>
      changeAvailabilityStatus_Pre($grpc.ServiceCall call,
          $async.Future<$0.ChangeAvailabilityStatusRequest> request) async {
    return changeAvailabilityStatus(call, await request);
  }

  $async.Future<$0.GetAllAgentWalletsResponse> getAllAgentWallets_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAllAgentsRequest> request) async {
    return getAllAgentWallets(call, await request);
  }

  $async.Future<$0.GetWalletByAgentIdResponse> getWalletByAgentId_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetWalletByAgentIdRequest> request) async {
    return getWalletByAgentId(call, await request);
  }

  $async.Future<$0.CreateOrderRequest_Response> createOrderRequest(
      $grpc.ServiceCall call, $0.CreateOrderRequest_Request request);
  $async.Future<$0.GetAllOrderRequests_Response> getAllOrderRequests(
      $grpc.ServiceCall call, $0.GetAllOrderRequests_Request request);
  $async.Future<$0.SendOrderRequestToAgent_Response> sendOrderRequestToAgent(
      $grpc.ServiceCall call, $0.SendOrderRequestToAgent_Request request);
  $async.Future<$0.AcceptOrDeclineOrderRequest_Response>
      acceptOrDeclineOrderRequest($grpc.ServiceCall call,
          $0.AcceptOrDeclineOrderRequest_Request request);
  $async.Future<$0.UpdateOrderRequestStatus_Response> updateOrderRequestStatus(
      $grpc.ServiceCall call, $0.UpdateOrderRequestStatus_Request request);
  $async.Future<$0.GetOrderRequestHistory_Response> getOrderRequestHistory(
      $grpc.ServiceCall call, $0.GetOrderRequestHistory_Request request);
  $async.Future<$0.GetAssignedOrderRequest_Response> getOrderRequest(
      $grpc.ServiceCall call, $0.GetAssignedOrderRequest_Request request);
  $async.Future<$0.CreateAgentResponse> createAgent(
      $grpc.ServiceCall call, $0.CreateAgentRequest request);
  $async.Future<$0.GetAllAgentsResponse> getAllAgents(
      $grpc.ServiceCall call, $0.GetAllAgentsRequest request);
  $async.Future<$0.GetAgentByIdResponse> getAgentById(
      $grpc.ServiceCall call, $0.GetAgentByIdRequest request);
  $async.Future<$0.GetAgentInfoResponse> getAgentInfo(
      $grpc.ServiceCall call, $0.GetAgentInfoRequest request);
  $async.Future<$0.ChangeAvailabilityStatusResponse> changeAvailabilityStatus(
      $grpc.ServiceCall call, $0.ChangeAvailabilityStatusRequest request);
  $async.Future<$0.GetAllAgentWalletsResponse> getAllAgentWallets(
      $grpc.ServiceCall call, $0.GetAllAgentsRequest request);
  $async.Future<$0.GetWalletByAgentIdResponse> getWalletByAgentId(
      $grpc.ServiceCall call, $0.GetWalletByAgentIdRequest request);
}
