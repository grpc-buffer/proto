///
//  Generated code. Do not modify.
//  source: price.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use priceItemDescriptor instead')
const PriceItem$json = const {
  '1': 'PriceItem',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'item', '3': 2, '4': 1, '5': 9, '10': 'item'},
    const {'1': 'location', '3': 3, '4': 1, '5': 9, '10': 'location'},
    const {'1': 'date', '3': 4, '4': 1, '5': 9, '10': 'date'},
    const {'1': 'old_rate', '3': 5, '4': 1, '5': 1, '10': 'oldRate'},
    const {'1': 'new_rate', '3': 6, '4': 1, '5': 1, '10': 'newRate'},
    const {'1': 'discount', '3': 7, '4': 1, '5': 1, '10': 'discount'},
    const {'1': 'created_at', '3': 8, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 9, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `PriceItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List priceItemDescriptor = $convert.base64Decode('CglQcmljZUl0ZW0SDgoCaWQYASABKAlSAmlkEhIKBGl0ZW0YAiABKAlSBGl0ZW0SGgoIbG9jYXRpb24YAyABKAlSCGxvY2F0aW9uEhIKBGRhdGUYBCABKAlSBGRhdGUSGQoIb2xkX3JhdGUYBSABKAFSB29sZFJhdGUSGQoIbmV3X3JhdGUYBiABKAFSB25ld1JhdGUSGgoIZGlzY291bnQYByABKAFSCGRpc2NvdW50Eh0KCmNyZWF0ZWRfYXQYCCABKAlSCWNyZWF0ZWRBdBIdCgp1cGRhdGVkX2F0GAkgASgJUgl1cGRhdGVkQXQ=');
@$core.Deprecated('Use priceRequestDescriptor instead')
const PriceRequest$json = const {
  '1': 'PriceRequest',
  '2': const [
    const {'1': 'item', '3': 1, '4': 1, '5': 9, '10': 'item'},
    const {'1': 'location', '3': 2, '4': 1, '5': 9, '10': 'location'},
    const {'1': 'date', '3': 3, '4': 1, '5': 9, '10': 'date'},
    const {'1': 'old_rate', '3': 4, '4': 1, '5': 1, '10': 'oldRate'},
    const {'1': 'new_rate', '3': 5, '4': 1, '5': 1, '10': 'newRate'},
    const {'1': 'discount', '3': 6, '4': 1, '5': 1, '10': 'discount'},
    const {'1': 'created_at', '3': 7, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 8, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `PriceRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List priceRequestDescriptor = $convert.base64Decode('CgxQcmljZVJlcXVlc3QSEgoEaXRlbRgBIAEoCVIEaXRlbRIaCghsb2NhdGlvbhgCIAEoCVIIbG9jYXRpb24SEgoEZGF0ZRgDIAEoCVIEZGF0ZRIZCghvbGRfcmF0ZRgEIAEoAVIHb2xkUmF0ZRIZCghuZXdfcmF0ZRgFIAEoAVIHbmV3UmF0ZRIaCghkaXNjb3VudBgGIAEoAVIIZGlzY291bnQSHQoKY3JlYXRlZF9hdBgHIAEoCVIJY3JlYXRlZEF0Eh0KCnVwZGF0ZWRfYXQYCCABKAlSCXVwZGF0ZWRBdA==');
@$core.Deprecated('Use emptyPriceRequestDescriptor instead')
const EmptyPriceRequest$json = const {
  '1': 'EmptyPriceRequest',
};

/// Descriptor for `EmptyPriceRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyPriceRequestDescriptor = $convert.base64Decode('ChFFbXB0eVByaWNlUmVxdWVzdA==');
@$core.Deprecated('Use priceRequestItemDescriptor instead')
const PriceRequestItem$json = const {
  '1': 'PriceRequestItem',
  '2': const [
    const {'1': 'requestItem', '3': 1, '4': 1, '5': 9, '10': 'requestItem'},
  ],
};

/// Descriptor for `PriceRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List priceRequestItemDescriptor = $convert.base64Decode('ChBQcmljZVJlcXVlc3RJdGVtEiAKC3JlcXVlc3RJdGVtGAEgASgJUgtyZXF1ZXN0SXRlbQ==');
@$core.Deprecated('Use priceResponseDescriptor instead')
const PriceResponse$json = const {
  '1': 'PriceResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 9, '10': 'data'},
  ],
};

/// Descriptor for `PriceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List priceResponseDescriptor = $convert.base64Decode('Cg1QcmljZVJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USEgoEZGF0YRgDIAEoCVIEZGF0YQ==');
@$core.Deprecated('Use viewPriceResponseDescriptor instead')
const ViewPriceResponse$json = const {
  '1': 'ViewPriceResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 11, '6': '.pricing_service.v1.PriceItem', '10': 'data'},
  ],
};

/// Descriptor for `ViewPriceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewPriceResponseDescriptor = $convert.base64Decode('ChFWaWV3UHJpY2VSZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEjEKBGRhdGEYAyABKAsyHS5wcmljaW5nX3NlcnZpY2UudjEuUHJpY2VJdGVtUgRkYXRh');
@$core.Deprecated('Use listPriceResponseDescriptor instead')
const ListPriceResponse$json = const {
  '1': 'ListPriceResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.pricing_service.v1.PriceItem', '10': 'data'},
  ],
};

/// Descriptor for `ListPriceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listPriceResponseDescriptor = $convert.base64Decode('ChFMaXN0UHJpY2VSZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEjEKBGRhdGEYAyADKAsyHS5wcmljaW5nX3NlcnZpY2UudjEuUHJpY2VJdGVtUgRkYXRh');
