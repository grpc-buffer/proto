///
//  Generated code. Do not modify.
//  source: delivery.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'delivery.pb.dart' as $0;
export 'delivery.pb.dart';

class deliveryServiceClient extends $grpc.Client {
  static final _$createDeliveryService =
      $grpc.ClientMethod<$0.DeliveryPayload, $0.DeliveryResponse>(
          '/delivery_service.v1.deliveryService/CreateDeliveryService',
          ($0.DeliveryPayload value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.DeliveryResponse.fromBuffer(value));
  static final _$fetchDeliveryByServiceArea =
      $grpc.ClientMethod<$0.DeliveryRequest, $0.GetPayLoadResponse>(
          '/delivery_service.v1.deliveryService/FetchDeliveryByServiceArea',
          ($0.DeliveryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetPayLoadResponse.fromBuffer(value));
  static final _$fetchAllDelivery =
      $grpc.ClientMethod<$0.GetAllDeliveriesRequest, $0.GetAllDeliveryResponse>(
          '/delivery_service.v1.deliveryService/FetchAllDelivery',
          ($0.GetAllDeliveriesRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetAllDeliveryResponse.fromBuffer(value));
  static final _$deleteDeliveryByServiceArea =
      $grpc.ClientMethod<$0.DeliveryDeleteRequest, $0.DeliveryResponse>(
          '/delivery_service.v1.deliveryService/DeleteDeliveryByServiceArea',
          ($0.DeliveryDeleteRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.DeliveryResponse.fromBuffer(value));
  static final _$deliveryService =
      $grpc.ClientMethod<$0.InDeliveryRequest, $0.InDeliveryResponse>(
          '/delivery_service.v1.deliveryService/DeliveryService',
          ($0.InDeliveryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.InDeliveryResponse.fromBuffer(value));
  static final _$getTips = $grpc.ClientMethod<$0.TipRequest, $0.TipResponse>(
      '/delivery_service.v1.deliveryService/GetTips',
      ($0.TipRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.TipResponse.fromBuffer(value));

  deliveryServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.DeliveryResponse> createDeliveryService(
      $0.DeliveryPayload request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createDeliveryService, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetPayLoadResponse> fetchDeliveryByServiceArea(
      $0.DeliveryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$fetchDeliveryByServiceArea, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.GetAllDeliveryResponse> fetchAllDelivery(
      $0.GetAllDeliveriesRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$fetchAllDelivery, request, options: options);
  }

  $grpc.ResponseFuture<$0.DeliveryResponse> deleteDeliveryByServiceArea(
      $0.DeliveryDeleteRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteDeliveryByServiceArea, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.InDeliveryResponse> deliveryService(
      $0.InDeliveryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deliveryService, request, options: options);
  }

  $grpc.ResponseFuture<$0.TipResponse> getTips($0.TipRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getTips, request, options: options);
  }
}

abstract class deliveryServiceBase extends $grpc.Service {
  $core.String get $name => 'delivery_service.v1.deliveryService';

  deliveryServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.DeliveryPayload, $0.DeliveryResponse>(
        'CreateDeliveryService',
        createDeliveryService_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DeliveryPayload.fromBuffer(value),
        ($0.DeliveryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeliveryRequest, $0.GetPayLoadResponse>(
        'FetchDeliveryByServiceArea',
        fetchDeliveryByServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DeliveryRequest.fromBuffer(value),
        ($0.GetPayLoadResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetAllDeliveriesRequest,
            $0.GetAllDeliveryResponse>(
        'FetchAllDelivery',
        fetchAllDelivery_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetAllDeliveriesRequest.fromBuffer(value),
        ($0.GetAllDeliveryResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.DeliveryDeleteRequest, $0.DeliveryResponse>(
            'DeleteDeliveryByServiceArea',
            deleteDeliveryByServiceArea_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.DeliveryDeleteRequest.fromBuffer(value),
            ($0.DeliveryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.InDeliveryRequest, $0.InDeliveryResponse>(
        'DeliveryService',
        deliveryService_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.InDeliveryRequest.fromBuffer(value),
        ($0.InDeliveryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TipRequest, $0.TipResponse>(
        'GetTips',
        getTips_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TipRequest.fromBuffer(value),
        ($0.TipResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.DeliveryResponse> createDeliveryService_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DeliveryPayload> request) async {
    return createDeliveryService(call, await request);
  }

  $async.Future<$0.GetPayLoadResponse> fetchDeliveryByServiceArea_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DeliveryRequest> request) async {
    return fetchDeliveryByServiceArea(call, await request);
  }

  $async.Future<$0.GetAllDeliveryResponse> fetchAllDelivery_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAllDeliveriesRequest> request) async {
    return fetchAllDelivery(call, await request);
  }

  $async.Future<$0.DeliveryResponse> deleteDeliveryByServiceArea_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.DeliveryDeleteRequest> request) async {
    return deleteDeliveryByServiceArea(call, await request);
  }

  $async.Future<$0.InDeliveryResponse> deliveryService_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.InDeliveryRequest> request) async {
    return deliveryService(call, await request);
  }

  $async.Future<$0.TipResponse> getTips_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TipRequest> request) async {
    return getTips(call, await request);
  }

  $async.Future<$0.DeliveryResponse> createDeliveryService(
      $grpc.ServiceCall call, $0.DeliveryPayload request);
  $async.Future<$0.GetPayLoadResponse> fetchDeliveryByServiceArea(
      $grpc.ServiceCall call, $0.DeliveryRequest request);
  $async.Future<$0.GetAllDeliveryResponse> fetchAllDelivery(
      $grpc.ServiceCall call, $0.GetAllDeliveriesRequest request);
  $async.Future<$0.DeliveryResponse> deleteDeliveryByServiceArea(
      $grpc.ServiceCall call, $0.DeliveryDeleteRequest request);
  $async.Future<$0.InDeliveryResponse> deliveryService(
      $grpc.ServiceCall call, $0.InDeliveryRequest request);
  $async.Future<$0.TipResponse> getTips(
      $grpc.ServiceCall call, $0.TipRequest request);
}
