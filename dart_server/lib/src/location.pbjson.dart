///
//  Generated code. Do not modify.
//  source: location.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use emptyLocationRequestDescriptor instead')
const EmptyLocationRequest$json = const {
  '1': 'EmptyLocationRequest',
};

/// Descriptor for `EmptyLocationRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyLocationRequestDescriptor = $convert.base64Decode('ChRFbXB0eUxvY2F0aW9uUmVxdWVzdA==');
@$core.Deprecated('Use locationRequestItemDescriptor instead')
const LocationRequestItem$json = const {
  '1': 'LocationRequestItem',
  '2': const [
    const {'1': 'locationID', '3': 1, '4': 1, '5': 9, '10': 'locationID'},
  ],
};

/// Descriptor for `LocationRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationRequestItemDescriptor = $convert.base64Decode('ChNMb2NhdGlvblJlcXVlc3RJdGVtEh4KCmxvY2F0aW9uSUQYASABKAlSCmxvY2F0aW9uSUQ=');
@$core.Deprecated('Use restaurantLocationItemsDescriptor instead')
const RestaurantLocationItems$json = const {
  '1': 'RestaurantLocationItems',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'longitude', '3': 2, '4': 1, '5': 9, '10': 'longitude'},
    const {'1': 'latitude', '3': 3, '4': 1, '5': 9, '10': 'latitude'},
    const {'1': 'country', '3': 4, '4': 1, '5': 9, '10': 'country'},
    const {'1': 'state', '3': 5, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'lga', '3': 6, '4': 1, '5': 9, '10': 'lga'},
    const {'1': 'town', '3': 7, '4': 1, '5': 9, '10': 'town'},
    const {'1': 'street', '3': 8, '4': 1, '5': 9, '10': 'street'},
    const {'1': 'number', '3': 9, '4': 1, '5': 9, '10': 'number'},
    const {'1': 'description', '3': 10, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'restaurantId', '3': 11, '4': 1, '5': 9, '10': 'restaurantId'},
    const {'1': 'created_at', '3': 12, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 13, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `RestaurantLocationItems`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List restaurantLocationItemsDescriptor = $convert.base64Decode('ChdSZXN0YXVyYW50TG9jYXRpb25JdGVtcxIOCgJpZBgBIAEoCVICaWQSHAoJbG9uZ2l0dWRlGAIgASgJUglsb25naXR1ZGUSGgoIbGF0aXR1ZGUYAyABKAlSCGxhdGl0dWRlEhgKB2NvdW50cnkYBCABKAlSB2NvdW50cnkSFAoFc3RhdGUYBSABKAlSBXN0YXRlEhAKA2xnYRgGIAEoCVIDbGdhEhIKBHRvd24YByABKAlSBHRvd24SFgoGc3RyZWV0GAggASgJUgZzdHJlZXQSFgoGbnVtYmVyGAkgASgJUgZudW1iZXISIAoLZGVzY3JpcHRpb24YCiABKAlSC2Rlc2NyaXB0aW9uEiIKDHJlc3RhdXJhbnRJZBgLIAEoCVIMcmVzdGF1cmFudElkEh0KCmNyZWF0ZWRfYXQYDCABKAlSCWNyZWF0ZWRBdBIdCgp1cGRhdGVkX2F0GA0gASgJUgl1cGRhdGVkQXQ=');
@$core.Deprecated('Use locationItemsDescriptor instead')
const LocationItems$json = const {
  '1': 'LocationItems',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'city', '3': 3, '4': 1, '5': 9, '10': 'city'},
    const {'1': 'country', '3': 4, '4': 1, '5': 9, '10': 'country'},
    const {'1': 'state', '3': 5, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'created_at', '3': 6, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 7, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'serviceAreaID', '3': 8, '4': 1, '5': 9, '10': 'serviceAreaID'},
    const {'1': 'coordinate', '3': 9, '4': 1, '5': 11, '6': '.location_service.v1.Coordinate', '10': 'coordinate'},
  ],
};

/// Descriptor for `LocationItems`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationItemsDescriptor = $convert.base64Decode('Cg1Mb2NhdGlvbkl0ZW1zEg4KAmlkGAEgASgJUgJpZBISCgRuYW1lGAIgASgJUgRuYW1lEhIKBGNpdHkYAyABKAlSBGNpdHkSGAoHY291bnRyeRgEIAEoCVIHY291bnRyeRIUCgVzdGF0ZRgFIAEoCVIFc3RhdGUSHQoKY3JlYXRlZF9hdBgGIAEoCVIJY3JlYXRlZEF0Eh0KCnVwZGF0ZWRfYXQYByABKAlSCXVwZGF0ZWRBdBIkCg1zZXJ2aWNlQXJlYUlEGAggASgJUg1zZXJ2aWNlQXJlYUlEEj8KCmNvb3JkaW5hdGUYCSABKAsyHy5sb2NhdGlvbl9zZXJ2aWNlLnYxLkNvb3JkaW5hdGVSCmNvb3JkaW5hdGU=');
@$core.Deprecated('Use userLocationItemsDescriptor instead')
const UserLocationItems$json = const {
  '1': 'UserLocationItems',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'currentLongitude', '3': 2, '4': 1, '5': 9, '10': 'currentLongitude'},
    const {'1': 'currentLatitude', '3': 3, '4': 1, '5': 9, '10': 'currentLatitude'},
    const {'1': 'country', '3': 4, '4': 1, '5': 9, '10': 'country'},
    const {'1': 'state', '3': 5, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'lga', '3': 6, '4': 1, '5': 9, '10': 'lga'},
    const {'1': 'town', '3': 7, '4': 1, '5': 9, '10': 'town'},
    const {'1': 'street', '3': 8, '4': 1, '5': 9, '10': 'street'},
    const {'1': 'number', '3': 9, '4': 1, '5': 9, '10': 'number'},
    const {'1': 'permanentLongitude', '3': 10, '4': 1, '5': 9, '10': 'permanentLongitude'},
    const {'1': 'permanentLatitude', '3': 11, '4': 1, '5': 9, '10': 'permanentLatitude'},
    const {'1': 'userId', '3': 12, '4': 1, '5': 9, '10': 'userId'},
    const {'1': 'created_at', '3': 13, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 14, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `UserLocationItems`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userLocationItemsDescriptor = $convert.base64Decode('ChFVc2VyTG9jYXRpb25JdGVtcxIOCgJpZBgBIAEoCVICaWQSKgoQY3VycmVudExvbmdpdHVkZRgCIAEoCVIQY3VycmVudExvbmdpdHVkZRIoCg9jdXJyZW50TGF0aXR1ZGUYAyABKAlSD2N1cnJlbnRMYXRpdHVkZRIYCgdjb3VudHJ5GAQgASgJUgdjb3VudHJ5EhQKBXN0YXRlGAUgASgJUgVzdGF0ZRIQCgNsZ2EYBiABKAlSA2xnYRISCgR0b3duGAcgASgJUgR0b3duEhYKBnN0cmVldBgIIAEoCVIGc3RyZWV0EhYKBm51bWJlchgJIAEoCVIGbnVtYmVyEi4KEnBlcm1hbmVudExvbmdpdHVkZRgKIAEoCVIScGVybWFuZW50TG9uZ2l0dWRlEiwKEXBlcm1hbmVudExhdGl0dWRlGAsgASgJUhFwZXJtYW5lbnRMYXRpdHVkZRIWCgZ1c2VySWQYDCABKAlSBnVzZXJJZBIdCgpjcmVhdGVkX2F0GA0gASgJUgljcmVhdGVkQXQSHQoKdXBkYXRlZF9hdBgOIAEoCVIJdXBkYXRlZEF0');
@$core.Deprecated('Use locationResponseDescriptor instead')
const LocationResponse$json = const {
  '1': 'LocationResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 9, '10': 'data'},
  ],
};

/// Descriptor for `LocationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationResponseDescriptor = $convert.base64Decode('ChBMb2NhdGlvblJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USEgoEZGF0YRgDIAEoCVIEZGF0YQ==');
@$core.Deprecated('Use listUsersLocationResponseDescriptor instead')
const ListUsersLocationResponse$json = const {
  '1': 'ListUsersLocationResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.location_service.v1.UserLocationItems', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ListUsersLocationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listUsersLocationResponseDescriptor = $convert.base64Decode('ChlMaXN0VXNlcnNMb2NhdGlvblJlc3BvbnNlEjoKBGRhdGEYASADKAsyJi5sb2NhdGlvbl9zZXJ2aWNlLnYxLlVzZXJMb2NhdGlvbkl0ZW1zUgRkYXRhEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USFgoGc3RhdHVzGAMgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use listLocationResponseDescriptor instead')
const ListLocationResponse$json = const {
  '1': 'ListLocationResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.location_service.v1.LocationItems', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ListLocationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listLocationResponseDescriptor = $convert.base64Decode('ChRMaXN0TG9jYXRpb25SZXNwb25zZRI2CgRkYXRhGAEgAygLMiIubG9jYXRpb25fc2VydmljZS52MS5Mb2NhdGlvbkl0ZW1zUgRkYXRhEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USFgoGc3RhdHVzGAMgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use listRestaurantLocationResponseDescriptor instead')
const ListRestaurantLocationResponse$json = const {
  '1': 'ListRestaurantLocationResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.location_service.v1.RestaurantLocationItems', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ListRestaurantLocationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listRestaurantLocationResponseDescriptor = $convert.base64Decode('Ch5MaXN0UmVzdGF1cmFudExvY2F0aW9uUmVzcG9uc2USQAoEZGF0YRgBIAMoCzIsLmxvY2F0aW9uX3NlcnZpY2UudjEuUmVzdGF1cmFudExvY2F0aW9uSXRlbXNSBGRhdGESGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIWCgZzdGF0dXMYAyABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use viewUserLocationResponseDescriptor instead')
const ViewUserLocationResponse$json = const {
  '1': 'ViewUserLocationResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'data', '3': 2, '4': 1, '5': 11, '6': '.location_service.v1.UserLocationItems', '10': 'data'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `ViewUserLocationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewUserLocationResponseDescriptor = $convert.base64Decode('ChhWaWV3VXNlckxvY2F0aW9uUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSOgoEZGF0YRgCIAEoCzImLmxvY2F0aW9uX3NlcnZpY2UudjEuVXNlckxvY2F0aW9uSXRlbXNSBGRhdGESGAoHbWVzc2FnZRgDIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use viewRestaurantLocationResponseDescriptor instead')
const ViewRestaurantLocationResponse$json = const {
  '1': 'ViewRestaurantLocationResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'data', '3': 2, '4': 1, '5': 11, '6': '.location_service.v1.RestaurantLocationItems', '10': 'data'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `ViewRestaurantLocationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewRestaurantLocationResponseDescriptor = $convert.base64Decode('Ch5WaWV3UmVzdGF1cmFudExvY2F0aW9uUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSQAoEZGF0YRgCIAEoCzIsLmxvY2F0aW9uX3NlcnZpY2UudjEuUmVzdGF1cmFudExvY2F0aW9uSXRlbXNSBGRhdGESGAoHbWVzc2FnZRgDIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use coordinateDescriptor instead')
const Coordinate$json = const {
  '1': 'Coordinate',
  '2': const [
    const {'1': 'latitude', '3': 1, '4': 1, '5': 1, '10': 'latitude'},
    const {'1': 'longitude', '3': 2, '4': 1, '5': 1, '10': 'longitude'},
  ],
};

/// Descriptor for `Coordinate`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List coordinateDescriptor = $convert.base64Decode('CgpDb29yZGluYXRlEhoKCGxhdGl0dWRlGAEgASgBUghsYXRpdHVkZRIcCglsb25naXR1ZGUYAiABKAFSCWxvbmdpdHVkZQ==');
@$core.Deprecated('Use serviceAreaDescriptor instead')
const ServiceArea$json = const {
  '1': 'ServiceArea',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'serviceAreaName', '3': 2, '4': 1, '5': 9, '10': 'serviceAreaName'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'coverageRadius', '3': 4, '4': 1, '5': 1, '10': 'coverageRadius'},
    const {'1': 'coordinate', '3': 5, '4': 1, '5': 11, '6': '.location_service.v1.Coordinate', '10': 'coordinate'},
  ],
};

/// Descriptor for `ServiceArea`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List serviceAreaDescriptor = $convert.base64Decode('CgtTZXJ2aWNlQXJlYRIOCgJpZBgBIAEoCVICaWQSKAoPc2VydmljZUFyZWFOYW1lGAIgASgJUg9zZXJ2aWNlQXJlYU5hbWUSIAoLZGVzY3JpcHRpb24YAyABKAlSC2Rlc2NyaXB0aW9uEiYKDmNvdmVyYWdlUmFkaXVzGAQgASgBUg5jb3ZlcmFnZVJhZGl1cxI/Cgpjb29yZGluYXRlGAUgASgLMh8ubG9jYXRpb25fc2VydmljZS52MS5Db29yZGluYXRlUgpjb29yZGluYXRl');
@$core.Deprecated('Use serviceAreaRequestDescriptor instead')
const ServiceAreaRequest$json = const {
  '1': 'ServiceAreaRequest',
  '2': const [
    const {'1': 'serviceAreaID', '3': 1, '4': 1, '5': 9, '10': 'serviceAreaID'},
  ],
};

/// Descriptor for `ServiceAreaRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List serviceAreaRequestDescriptor = $convert.base64Decode('ChJTZXJ2aWNlQXJlYVJlcXVlc3QSJAoNc2VydmljZUFyZWFJRBgBIAEoCVINc2VydmljZUFyZWFJRA==');
@$core.Deprecated('Use serviceAreaLocationListDescriptor instead')
const ServiceAreaLocationList$json = const {
  '1': 'ServiceAreaLocationList',
  '2': const [
    const {'1': 'serviceAreaID', '3': 1, '4': 1, '5': 9, '10': 'serviceAreaID'},
  ],
};

/// Descriptor for `ServiceAreaLocationList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List serviceAreaLocationListDescriptor = $convert.base64Decode('ChdTZXJ2aWNlQXJlYUxvY2F0aW9uTGlzdBIkCg1zZXJ2aWNlQXJlYUlEGAEgASgJUg1zZXJ2aWNlQXJlYUlE');
@$core.Deprecated('Use listServiceAreaResponseDescriptor instead')
const ListServiceAreaResponse$json = const {
  '1': 'ListServiceAreaResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.location_service.v1.ServiceArea', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ListServiceAreaResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listServiceAreaResponseDescriptor = $convert.base64Decode('ChdMaXN0U2VydmljZUFyZWFSZXNwb25zZRI0CgRkYXRhGAEgAygLMiAubG9jYXRpb25fc2VydmljZS52MS5TZXJ2aWNlQXJlYVIEZGF0YRIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEhYKBnN0YXR1cxgDIAEoCVIGc3RhdHVz');
