///
//  Generated code. Do not modify.
//  source: menu.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

class CustomPackage extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CustomPackage', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'themeUrl', protoName: 'themeUrl')
    ..pc<ItemData>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: ItemData.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userID', protoName: 'userID')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt', protoName: 'updatedAt')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt', protoName: 'createdAt')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ID', protoName: 'ID')
    ..a<$core.double>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'estimatedPrice', $pb.PbFieldType.OD, protoName: 'estimatedPrice')
    ..hasRequiredFields = false
  ;

  CustomPackage._() : super();
  factory CustomPackage({
    $core.String? name,
    $core.String? themeUrl,
    $core.Iterable<ItemData>? items,
    $core.String? userID,
    $core.String? updatedAt,
    $core.String? createdAt,
    $core.String? iD,
    $core.double? estimatedPrice,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (themeUrl != null) {
      _result.themeUrl = themeUrl;
    }
    if (items != null) {
      _result.items.addAll(items);
    }
    if (userID != null) {
      _result.userID = userID;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (iD != null) {
      _result.iD = iD;
    }
    if (estimatedPrice != null) {
      _result.estimatedPrice = estimatedPrice;
    }
    return _result;
  }
  factory CustomPackage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CustomPackage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CustomPackage clone() => CustomPackage()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CustomPackage copyWith(void Function(CustomPackage) updates) => super.copyWith((message) => updates(message as CustomPackage)) as CustomPackage; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CustomPackage create() => CustomPackage._();
  CustomPackage createEmptyInstance() => create();
  static $pb.PbList<CustomPackage> createRepeated() => $pb.PbList<CustomPackage>();
  @$core.pragma('dart2js:noInline')
  static CustomPackage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CustomPackage>(create);
  static CustomPackage? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get themeUrl => $_getSZ(1);
  @$pb.TagNumber(2)
  set themeUrl($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasThemeUrl() => $_has(1);
  @$pb.TagNumber(2)
  void clearThemeUrl() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<ItemData> get items => $_getList(2);

  @$pb.TagNumber(4)
  $core.String get userID => $_getSZ(3);
  @$pb.TagNumber(4)
  set userID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasUserID() => $_has(3);
  @$pb.TagNumber(4)
  void clearUserID() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get updatedAt => $_getSZ(4);
  @$pb.TagNumber(5)
  set updatedAt($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasUpdatedAt() => $_has(4);
  @$pb.TagNumber(5)
  void clearUpdatedAt() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get createdAt => $_getSZ(5);
  @$pb.TagNumber(6)
  set createdAt($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasCreatedAt() => $_has(5);
  @$pb.TagNumber(6)
  void clearCreatedAt() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get iD => $_getSZ(6);
  @$pb.TagNumber(7)
  set iD($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasID() => $_has(6);
  @$pb.TagNumber(7)
  void clearID() => clearField(7);

  @$pb.TagNumber(8)
  $core.double get estimatedPrice => $_getN(7);
  @$pb.TagNumber(8)
  set estimatedPrice($core.double v) { $_setDouble(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasEstimatedPrice() => $_has(7);
  @$pb.TagNumber(8)
  void clearEstimatedPrice() => clearField(8);
}

class ListCustomPackageRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListCustomPackageRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userID', protoName: 'userID')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'limit')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page')
    ..hasRequiredFields = false
  ;

  ListCustomPackageRequest._() : super();
  factory ListCustomPackageRequest({
    $core.String? userID,
    $fixnum.Int64? limit,
    $fixnum.Int64? page,
  }) {
    final _result = create();
    if (userID != null) {
      _result.userID = userID;
    }
    if (limit != null) {
      _result.limit = limit;
    }
    if (page != null) {
      _result.page = page;
    }
    return _result;
  }
  factory ListCustomPackageRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListCustomPackageRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListCustomPackageRequest clone() => ListCustomPackageRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListCustomPackageRequest copyWith(void Function(ListCustomPackageRequest) updates) => super.copyWith((message) => updates(message as ListCustomPackageRequest)) as ListCustomPackageRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListCustomPackageRequest create() => ListCustomPackageRequest._();
  ListCustomPackageRequest createEmptyInstance() => create();
  static $pb.PbList<ListCustomPackageRequest> createRepeated() => $pb.PbList<ListCustomPackageRequest>();
  @$core.pragma('dart2js:noInline')
  static ListCustomPackageRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListCustomPackageRequest>(create);
  static ListCustomPackageRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get userID => $_getSZ(0);
  @$pb.TagNumber(1)
  set userID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserID() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserID() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get limit => $_getI64(1);
  @$pb.TagNumber(2)
  set limit($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLimit() => $_has(1);
  @$pb.TagNumber(2)
  void clearLimit() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get page => $_getI64(2);
  @$pb.TagNumber(3)
  set page($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPage() => $_has(2);
  @$pb.TagNumber(3)
  void clearPage() => clearField(3);
}

class AddCustomPackageRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddCustomPackageRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'customPackageID', protoName: 'customPackageID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemName', protoName: 'itemName')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemImage', protoName: 'itemImage')
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemPrice', $pb.PbFieldType.OD, protoName: 'itemPrice')
    ..hasRequiredFields = false
  ;

  AddCustomPackageRequest._() : super();
  factory AddCustomPackageRequest({
    $core.String? customPackageID,
    $core.String? itemName,
    $core.String? itemImage,
    $core.double? itemPrice,
  }) {
    final _result = create();
    if (customPackageID != null) {
      _result.customPackageID = customPackageID;
    }
    if (itemName != null) {
      _result.itemName = itemName;
    }
    if (itemImage != null) {
      _result.itemImage = itemImage;
    }
    if (itemPrice != null) {
      _result.itemPrice = itemPrice;
    }
    return _result;
  }
  factory AddCustomPackageRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddCustomPackageRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddCustomPackageRequest clone() => AddCustomPackageRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddCustomPackageRequest copyWith(void Function(AddCustomPackageRequest) updates) => super.copyWith((message) => updates(message as AddCustomPackageRequest)) as AddCustomPackageRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddCustomPackageRequest create() => AddCustomPackageRequest._();
  AddCustomPackageRequest createEmptyInstance() => create();
  static $pb.PbList<AddCustomPackageRequest> createRepeated() => $pb.PbList<AddCustomPackageRequest>();
  @$core.pragma('dart2js:noInline')
  static AddCustomPackageRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddCustomPackageRequest>(create);
  static AddCustomPackageRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get customPackageID => $_getSZ(0);
  @$pb.TagNumber(1)
  set customPackageID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCustomPackageID() => $_has(0);
  @$pb.TagNumber(1)
  void clearCustomPackageID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get itemName => $_getSZ(1);
  @$pb.TagNumber(2)
  set itemName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemName() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get itemImage => $_getSZ(2);
  @$pb.TagNumber(3)
  set itemImage($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasItemImage() => $_has(2);
  @$pb.TagNumber(3)
  void clearItemImage() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get itemPrice => $_getN(3);
  @$pb.TagNumber(4)
  set itemPrice($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasItemPrice() => $_has(3);
  @$pb.TagNumber(4)
  void clearItemPrice() => clearField(4);
}

class ListCustomPackageResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListCustomPackageResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<CustomPackage>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: CustomPackage.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total')
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pageCount', protoName: 'pageCount')
    ..hasRequiredFields = false
  ;

  ListCustomPackageResponse._() : super();
  factory ListCustomPackageResponse({
    $core.Iterable<CustomPackage>? data,
    $core.String? status,
    $core.String? message,
    $fixnum.Int64? total,
    $fixnum.Int64? pageCount,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (total != null) {
      _result.total = total;
    }
    if (pageCount != null) {
      _result.pageCount = pageCount;
    }
    return _result;
  }
  factory ListCustomPackageResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListCustomPackageResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListCustomPackageResponse clone() => ListCustomPackageResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListCustomPackageResponse copyWith(void Function(ListCustomPackageResponse) updates) => super.copyWith((message) => updates(message as ListCustomPackageResponse)) as ListCustomPackageResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListCustomPackageResponse create() => ListCustomPackageResponse._();
  ListCustomPackageResponse createEmptyInstance() => create();
  static $pb.PbList<ListCustomPackageResponse> createRepeated() => $pb.PbList<ListCustomPackageResponse>();
  @$core.pragma('dart2js:noInline')
  static ListCustomPackageResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListCustomPackageResponse>(create);
  static ListCustomPackageResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<CustomPackage> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get message => $_getSZ(2);
  @$pb.TagNumber(3)
  set message($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMessage() => $_has(2);
  @$pb.TagNumber(3)
  void clearMessage() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get total => $_getI64(3);
  @$pb.TagNumber(4)
  set total($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTotal() => $_has(3);
  @$pb.TagNumber(4)
  void clearTotal() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get pageCount => $_getI64(4);
  @$pb.TagNumber(5)
  set pageCount($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPageCount() => $_has(4);
  @$pb.TagNumber(5)
  void clearPageCount() => clearField(5);
}

class ImageInfo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ImageInfo', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'section')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'imageType', protoName: 'imageType')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sectionID', protoName: 'sectionID')
    ..hasRequiredFields = false
  ;

  ImageInfo._() : super();
  factory ImageInfo({
    $core.String? section,
    $core.String? name,
    $core.String? imageType,
    $core.String? serviceAreaID,
    $core.String? title,
    $core.String? sectionID,
  }) {
    final _result = create();
    if (section != null) {
      _result.section = section;
    }
    if (name != null) {
      _result.name = name;
    }
    if (imageType != null) {
      _result.imageType = imageType;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    if (title != null) {
      _result.title = title;
    }
    if (sectionID != null) {
      _result.sectionID = sectionID;
    }
    return _result;
  }
  factory ImageInfo.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ImageInfo.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ImageInfo clone() => ImageInfo()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ImageInfo copyWith(void Function(ImageInfo) updates) => super.copyWith((message) => updates(message as ImageInfo)) as ImageInfo; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ImageInfo create() => ImageInfo._();
  ImageInfo createEmptyInstance() => create();
  static $pb.PbList<ImageInfo> createRepeated() => $pb.PbList<ImageInfo>();
  @$core.pragma('dart2js:noInline')
  static ImageInfo getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ImageInfo>(create);
  static ImageInfo? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get section => $_getSZ(0);
  @$pb.TagNumber(1)
  set section($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSection() => $_has(0);
  @$pb.TagNumber(1)
  void clearSection() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get imageType => $_getSZ(2);
  @$pb.TagNumber(3)
  set imageType($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasImageType() => $_has(2);
  @$pb.TagNumber(3)
  void clearImageType() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get serviceAreaID => $_getSZ(3);
  @$pb.TagNumber(4)
  set serviceAreaID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasServiceAreaID() => $_has(3);
  @$pb.TagNumber(4)
  void clearServiceAreaID() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get title => $_getSZ(4);
  @$pb.TagNumber(5)
  set title($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTitle() => $_has(4);
  @$pb.TagNumber(5)
  void clearTitle() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get sectionID => $_getSZ(5);
  @$pb.TagNumber(6)
  set sectionID($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasSectionID() => $_has(5);
  @$pb.TagNumber(6)
  void clearSectionID() => clearField(6);
}

enum UploadImageRequest_Data {
  info, 
  chunkData, 
  notSet
}

class UploadImageRequest extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, UploadImageRequest_Data> _UploadImageRequest_DataByTag = {
    1 : UploadImageRequest_Data.info,
    2 : UploadImageRequest_Data.chunkData,
    0 : UploadImageRequest_Data.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UploadImageRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<ImageInfo>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'info', subBuilder: ImageInfo.create)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'chunkData', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  UploadImageRequest._() : super();
  factory UploadImageRequest({
    ImageInfo? info,
    $core.List<$core.int>? chunkData,
  }) {
    final _result = create();
    if (info != null) {
      _result.info = info;
    }
    if (chunkData != null) {
      _result.chunkData = chunkData;
    }
    return _result;
  }
  factory UploadImageRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UploadImageRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UploadImageRequest clone() => UploadImageRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UploadImageRequest copyWith(void Function(UploadImageRequest) updates) => super.copyWith((message) => updates(message as UploadImageRequest)) as UploadImageRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UploadImageRequest create() => UploadImageRequest._();
  UploadImageRequest createEmptyInstance() => create();
  static $pb.PbList<UploadImageRequest> createRepeated() => $pb.PbList<UploadImageRequest>();
  @$core.pragma('dart2js:noInline')
  static UploadImageRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UploadImageRequest>(create);
  static UploadImageRequest? _defaultInstance;

  UploadImageRequest_Data whichData() => _UploadImageRequest_DataByTag[$_whichOneof(0)]!;
  void clearData() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  ImageInfo get info => $_getN(0);
  @$pb.TagNumber(1)
  set info(ImageInfo v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasInfo() => $_has(0);
  @$pb.TagNumber(1)
  void clearInfo() => clearField(1);
  @$pb.TagNumber(1)
  ImageInfo ensureInfo() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.int> get chunkData => $_getN(1);
  @$pb.TagNumber(2)
  set chunkData($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasChunkData() => $_has(1);
  @$pb.TagNumber(2)
  void clearChunkData() => clearField(2);
}

class ItemTagCreation extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemTagCreation', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tagName', protoName: 'tagName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..hasRequiredFields = false
  ;

  ItemTagCreation._() : super();
  factory ItemTagCreation({
    $core.String? tagName,
    $core.String? itemID,
  }) {
    final _result = create();
    if (tagName != null) {
      _result.tagName = tagName;
    }
    if (itemID != null) {
      _result.itemID = itemID;
    }
    return _result;
  }
  factory ItemTagCreation.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemTagCreation.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemTagCreation clone() => ItemTagCreation()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemTagCreation copyWith(void Function(ItemTagCreation) updates) => super.copyWith((message) => updates(message as ItemTagCreation)) as ItemTagCreation; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemTagCreation create() => ItemTagCreation._();
  ItemTagCreation createEmptyInstance() => create();
  static $pb.PbList<ItemTagCreation> createRepeated() => $pb.PbList<ItemTagCreation>();
  @$core.pragma('dart2js:noInline')
  static ItemTagCreation getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemTagCreation>(create);
  static ItemTagCreation? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tagName => $_getSZ(0);
  @$pb.TagNumber(1)
  set tagName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTagName() => $_has(0);
  @$pb.TagNumber(1)
  void clearTagName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get itemID => $_getSZ(1);
  @$pb.TagNumber(2)
  set itemID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemID() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemID() => clearField(2);
}

class ItemTagRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemTagRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tagID', protoName: 'tagID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..hasRequiredFields = false
  ;

  ItemTagRequest._() : super();
  factory ItemTagRequest({
    $core.String? tagID,
    $core.String? itemID,
  }) {
    final _result = create();
    if (tagID != null) {
      _result.tagID = tagID;
    }
    if (itemID != null) {
      _result.itemID = itemID;
    }
    return _result;
  }
  factory ItemTagRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemTagRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemTagRequest clone() => ItemTagRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemTagRequest copyWith(void Function(ItemTagRequest) updates) => super.copyWith((message) => updates(message as ItemTagRequest)) as ItemTagRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemTagRequest create() => ItemTagRequest._();
  ItemTagRequest createEmptyInstance() => create();
  static $pb.PbList<ItemTagRequest> createRepeated() => $pb.PbList<ItemTagRequest>();
  @$core.pragma('dart2js:noInline')
  static ItemTagRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemTagRequest>(create);
  static ItemTagRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tagID => $_getSZ(0);
  @$pb.TagNumber(1)
  set tagID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTagID() => $_has(0);
  @$pb.TagNumber(1)
  void clearTagID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get itemID => $_getSZ(1);
  @$pb.TagNumber(2)
  set itemID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemID() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemID() => clearField(2);
}

class ItemTag extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemTag', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tagName', protoName: 'tagName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tagID', protoName: 'tagID')
    ..hasRequiredFields = false
  ;

  ItemTag._() : super();
  factory ItemTag({
    $core.String? tagName,
    $core.String? itemID,
    $core.String? createdAt,
    $core.String? updatedAt,
    $core.String? tagID,
  }) {
    final _result = create();
    if (tagName != null) {
      _result.tagName = tagName;
    }
    if (itemID != null) {
      _result.itemID = itemID;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    if (tagID != null) {
      _result.tagID = tagID;
    }
    return _result;
  }
  factory ItemTag.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemTag.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemTag clone() => ItemTag()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemTag copyWith(void Function(ItemTag) updates) => super.copyWith((message) => updates(message as ItemTag)) as ItemTag; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemTag create() => ItemTag._();
  ItemTag createEmptyInstance() => create();
  static $pb.PbList<ItemTag> createRepeated() => $pb.PbList<ItemTag>();
  @$core.pragma('dart2js:noInline')
  static ItemTag getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemTag>(create);
  static ItemTag? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tagName => $_getSZ(0);
  @$pb.TagNumber(1)
  set tagName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTagName() => $_has(0);
  @$pb.TagNumber(1)
  void clearTagName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get itemID => $_getSZ(1);
  @$pb.TagNumber(2)
  set itemID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemID() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemID() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get createdAt => $_getSZ(2);
  @$pb.TagNumber(3)
  set createdAt($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCreatedAt() => $_has(2);
  @$pb.TagNumber(3)
  void clearCreatedAt() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get updatedAt => $_getSZ(3);
  @$pb.TagNumber(4)
  set updatedAt($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasUpdatedAt() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpdatedAt() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get tagID => $_getSZ(4);
  @$pb.TagNumber(5)
  set tagID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTagID() => $_has(4);
  @$pb.TagNumber(5)
  void clearTagID() => clearField(5);
}

class ItemTagResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemTagResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  ItemTagResponse._() : super();
  factory ItemTagResponse({
    $core.String? status,
    $core.String? message,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory ItemTagResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemTagResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemTagResponse clone() => ItemTagResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemTagResponse copyWith(void Function(ItemTagResponse) updates) => super.copyWith((message) => updates(message as ItemTagResponse)) as ItemTagResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemTagResponse create() => ItemTagResponse._();
  ItemTagResponse createEmptyInstance() => create();
  static $pb.PbList<ItemTagResponse> createRepeated() => $pb.PbList<ItemTagResponse>();
  @$core.pragma('dart2js:noInline')
  static ItemTagResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemTagResponse>(create);
  static ItemTagResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);
}

class ViewItemTagResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ViewItemTagResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOM<ItemTag>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: ItemTag.create)
    ..hasRequiredFields = false
  ;

  ViewItemTagResponse._() : super();
  factory ViewItemTagResponse({
    $core.String? status,
    $core.String? message,
    ItemTag? data,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (data != null) {
      _result.data = data;
    }
    return _result;
  }
  factory ViewItemTagResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ViewItemTagResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ViewItemTagResponse clone() => ViewItemTagResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ViewItemTagResponse copyWith(void Function(ViewItemTagResponse) updates) => super.copyWith((message) => updates(message as ViewItemTagResponse)) as ViewItemTagResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ViewItemTagResponse create() => ViewItemTagResponse._();
  ViewItemTagResponse createEmptyInstance() => create();
  static $pb.PbList<ViewItemTagResponse> createRepeated() => $pb.PbList<ViewItemTagResponse>();
  @$core.pragma('dart2js:noInline')
  static ViewItemTagResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ViewItemTagResponse>(create);
  static ViewItemTagResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  ItemTag get data => $_getN(2);
  @$pb.TagNumber(3)
  set data(ItemTag v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasData() => $_has(2);
  @$pb.TagNumber(3)
  void clearData() => clearField(3);
  @$pb.TagNumber(3)
  ItemTag ensureData() => $_ensure(2);
}

class ListItemTagResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListItemTagResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..pc<ItemTag>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: ItemTag.create)
    ..hasRequiredFields = false
  ;

  ListItemTagResponse._() : super();
  factory ListItemTagResponse({
    $core.String? status,
    $core.String? message,
    $core.Iterable<ItemTag>? data,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (data != null) {
      _result.data.addAll(data);
    }
    return _result;
  }
  factory ListItemTagResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListItemTagResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListItemTagResponse clone() => ListItemTagResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListItemTagResponse copyWith(void Function(ListItemTagResponse) updates) => super.copyWith((message) => updates(message as ListItemTagResponse)) as ListItemTagResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListItemTagResponse create() => ListItemTagResponse._();
  ListItemTagResponse createEmptyInstance() => create();
  static $pb.PbList<ListItemTagResponse> createRepeated() => $pb.PbList<ListItemTagResponse>();
  @$core.pragma('dart2js:noInline')
  static ListItemTagResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListItemTagResponse>(create);
  static ListItemTagResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<ItemTag> get data => $_getList(2);
}

class PackageTagRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageTagRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tagName', protoName: 'tagName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..hasRequiredFields = false
  ;

  PackageTagRequest._() : super();
  factory PackageTagRequest({
    $core.String? tagName,
    $core.String? packageID,
  }) {
    final _result = create();
    if (tagName != null) {
      _result.tagName = tagName;
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    return _result;
  }
  factory PackageTagRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageTagRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageTagRequest clone() => PackageTagRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageTagRequest copyWith(void Function(PackageTagRequest) updates) => super.copyWith((message) => updates(message as PackageTagRequest)) as PackageTagRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageTagRequest create() => PackageTagRequest._();
  PackageTagRequest createEmptyInstance() => create();
  static $pb.PbList<PackageTagRequest> createRepeated() => $pb.PbList<PackageTagRequest>();
  @$core.pragma('dart2js:noInline')
  static PackageTagRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageTagRequest>(create);
  static PackageTagRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tagName => $_getSZ(0);
  @$pb.TagNumber(1)
  set tagName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTagName() => $_has(0);
  @$pb.TagNumber(1)
  void clearTagName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get packageID => $_getSZ(1);
  @$pb.TagNumber(2)
  set packageID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackageID() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackageID() => clearField(2);
}

class PackageTagRequestItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageTagRequestItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tagID', protoName: 'tagID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..hasRequiredFields = false
  ;

  PackageTagRequestItem._() : super();
  factory PackageTagRequestItem({
    $core.String? tagID,
    $core.String? packageID,
  }) {
    final _result = create();
    if (tagID != null) {
      _result.tagID = tagID;
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    return _result;
  }
  factory PackageTagRequestItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageTagRequestItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageTagRequestItem clone() => PackageTagRequestItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageTagRequestItem copyWith(void Function(PackageTagRequestItem) updates) => super.copyWith((message) => updates(message as PackageTagRequestItem)) as PackageTagRequestItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageTagRequestItem create() => PackageTagRequestItem._();
  PackageTagRequestItem createEmptyInstance() => create();
  static $pb.PbList<PackageTagRequestItem> createRepeated() => $pb.PbList<PackageTagRequestItem>();
  @$core.pragma('dart2js:noInline')
  static PackageTagRequestItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageTagRequestItem>(create);
  static PackageTagRequestItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tagID => $_getSZ(0);
  @$pb.TagNumber(1)
  set tagID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTagID() => $_has(0);
  @$pb.TagNumber(1)
  void clearTagID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get packageID => $_getSZ(1);
  @$pb.TagNumber(2)
  set packageID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackageID() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackageID() => clearField(2);
}

class PackageTagItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageTagItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tagName', protoName: 'tagName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tagID', protoName: 'tagID')
    ..hasRequiredFields = false
  ;

  PackageTagItem._() : super();
  factory PackageTagItem({
    $core.String? tagName,
    $core.String? packageID,
    $core.String? createdAt,
    $core.String? updatedAt,
    $core.String? tagID,
  }) {
    final _result = create();
    if (tagName != null) {
      _result.tagName = tagName;
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    if (tagID != null) {
      _result.tagID = tagID;
    }
    return _result;
  }
  factory PackageTagItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageTagItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageTagItem clone() => PackageTagItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageTagItem copyWith(void Function(PackageTagItem) updates) => super.copyWith((message) => updates(message as PackageTagItem)) as PackageTagItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageTagItem create() => PackageTagItem._();
  PackageTagItem createEmptyInstance() => create();
  static $pb.PbList<PackageTagItem> createRepeated() => $pb.PbList<PackageTagItem>();
  @$core.pragma('dart2js:noInline')
  static PackageTagItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageTagItem>(create);
  static PackageTagItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tagName => $_getSZ(0);
  @$pb.TagNumber(1)
  set tagName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTagName() => $_has(0);
  @$pb.TagNumber(1)
  void clearTagName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get packageID => $_getSZ(1);
  @$pb.TagNumber(2)
  set packageID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackageID() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackageID() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get createdAt => $_getSZ(2);
  @$pb.TagNumber(3)
  set createdAt($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCreatedAt() => $_has(2);
  @$pb.TagNumber(3)
  void clearCreatedAt() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get updatedAt => $_getSZ(3);
  @$pb.TagNumber(4)
  set updatedAt($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasUpdatedAt() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpdatedAt() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get tagID => $_getSZ(4);
  @$pb.TagNumber(5)
  set tagID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTagID() => $_has(4);
  @$pb.TagNumber(5)
  void clearTagID() => clearField(5);
}

class PackageTagResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageTagResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  PackageTagResponse._() : super();
  factory PackageTagResponse({
    $core.String? status,
    $core.String? message,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory PackageTagResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageTagResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageTagResponse clone() => PackageTagResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageTagResponse copyWith(void Function(PackageTagResponse) updates) => super.copyWith((message) => updates(message as PackageTagResponse)) as PackageTagResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageTagResponse create() => PackageTagResponse._();
  PackageTagResponse createEmptyInstance() => create();
  static $pb.PbList<PackageTagResponse> createRepeated() => $pb.PbList<PackageTagResponse>();
  @$core.pragma('dart2js:noInline')
  static PackageTagResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageTagResponse>(create);
  static PackageTagResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);
}

class ViewPackageTagResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ViewPackageTagResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOM<PackageTagItem>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: PackageTagItem.create)
    ..hasRequiredFields = false
  ;

  ViewPackageTagResponse._() : super();
  factory ViewPackageTagResponse({
    $core.String? status,
    $core.String? message,
    PackageTagItem? data,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (data != null) {
      _result.data = data;
    }
    return _result;
  }
  factory ViewPackageTagResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ViewPackageTagResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ViewPackageTagResponse clone() => ViewPackageTagResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ViewPackageTagResponse copyWith(void Function(ViewPackageTagResponse) updates) => super.copyWith((message) => updates(message as ViewPackageTagResponse)) as ViewPackageTagResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ViewPackageTagResponse create() => ViewPackageTagResponse._();
  ViewPackageTagResponse createEmptyInstance() => create();
  static $pb.PbList<ViewPackageTagResponse> createRepeated() => $pb.PbList<ViewPackageTagResponse>();
  @$core.pragma('dart2js:noInline')
  static ViewPackageTagResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ViewPackageTagResponse>(create);
  static ViewPackageTagResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  PackageTagItem get data => $_getN(2);
  @$pb.TagNumber(3)
  set data(PackageTagItem v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasData() => $_has(2);
  @$pb.TagNumber(3)
  void clearData() => clearField(3);
  @$pb.TagNumber(3)
  PackageTagItem ensureData() => $_ensure(2);
}

class ListPackageTagResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListPackageTagResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..pc<PackageTagItem>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: PackageTagItem.create)
    ..hasRequiredFields = false
  ;

  ListPackageTagResponse._() : super();
  factory ListPackageTagResponse({
    $core.String? status,
    $core.String? message,
    $core.Iterable<PackageTagItem>? data,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (data != null) {
      _result.data.addAll(data);
    }
    return _result;
  }
  factory ListPackageTagResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListPackageTagResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListPackageTagResponse clone() => ListPackageTagResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListPackageTagResponse copyWith(void Function(ListPackageTagResponse) updates) => super.copyWith((message) => updates(message as ListPackageTagResponse)) as ListPackageTagResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListPackageTagResponse create() => ListPackageTagResponse._();
  ListPackageTagResponse createEmptyInstance() => create();
  static $pb.PbList<ListPackageTagResponse> createRepeated() => $pb.PbList<ListPackageTagResponse>();
  @$core.pragma('dart2js:noInline')
  static ListPackageTagResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListPackageTagResponse>(create);
  static ListPackageTagResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<PackageTagItem> get data => $_getList(2);
}

class AddPackageCategoryRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddPackageCategoryRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categoryName', protoName: 'categoryName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'icon')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..hasRequiredFields = false
  ;

  AddPackageCategoryRequest._() : super();
  factory AddPackageCategoryRequest({
    $core.String? categoryName,
    $core.String? icon,
    $core.String? description,
    $core.String? image,
    $core.String? serviceAreaID,
  }) {
    final _result = create();
    if (categoryName != null) {
      _result.categoryName = categoryName;
    }
    if (icon != null) {
      _result.icon = icon;
    }
    if (description != null) {
      _result.description = description;
    }
    if (image != null) {
      _result.image = image;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    return _result;
  }
  factory AddPackageCategoryRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddPackageCategoryRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddPackageCategoryRequest clone() => AddPackageCategoryRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddPackageCategoryRequest copyWith(void Function(AddPackageCategoryRequest) updates) => super.copyWith((message) => updates(message as AddPackageCategoryRequest)) as AddPackageCategoryRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddPackageCategoryRequest create() => AddPackageCategoryRequest._();
  AddPackageCategoryRequest createEmptyInstance() => create();
  static $pb.PbList<AddPackageCategoryRequest> createRepeated() => $pb.PbList<AddPackageCategoryRequest>();
  @$core.pragma('dart2js:noInline')
  static AddPackageCategoryRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddPackageCategoryRequest>(create);
  static AddPackageCategoryRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get categoryName => $_getSZ(0);
  @$pb.TagNumber(1)
  set categoryName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCategoryName() => $_has(0);
  @$pb.TagNumber(1)
  void clearCategoryName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get icon => $_getSZ(1);
  @$pb.TagNumber(2)
  set icon($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasIcon() => $_has(1);
  @$pb.TagNumber(2)
  void clearIcon() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get image => $_getSZ(3);
  @$pb.TagNumber(4)
  set image($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasImage() => $_has(3);
  @$pb.TagNumber(4)
  void clearImage() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get serviceAreaID => $_getSZ(4);
  @$pb.TagNumber(5)
  set serviceAreaID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasServiceAreaID() => $_has(4);
  @$pb.TagNumber(5)
  void clearServiceAreaID() => clearField(5);
}

class PackageCategoryItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageCategoryItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categoryName', protoName: 'categoryName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'icon')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categoryID', protoName: 'categoryID')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..hasRequiredFields = false
  ;

  PackageCategoryItem._() : super();
  factory PackageCategoryItem({
    $core.String? categoryName,
    $core.String? icon,
    $core.String? description,
    $core.String? image,
    $core.String? categoryID,
    $core.String? serviceAreaID,
  }) {
    final _result = create();
    if (categoryName != null) {
      _result.categoryName = categoryName;
    }
    if (icon != null) {
      _result.icon = icon;
    }
    if (description != null) {
      _result.description = description;
    }
    if (image != null) {
      _result.image = image;
    }
    if (categoryID != null) {
      _result.categoryID = categoryID;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    return _result;
  }
  factory PackageCategoryItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageCategoryItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageCategoryItem clone() => PackageCategoryItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageCategoryItem copyWith(void Function(PackageCategoryItem) updates) => super.copyWith((message) => updates(message as PackageCategoryItem)) as PackageCategoryItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageCategoryItem create() => PackageCategoryItem._();
  PackageCategoryItem createEmptyInstance() => create();
  static $pb.PbList<PackageCategoryItem> createRepeated() => $pb.PbList<PackageCategoryItem>();
  @$core.pragma('dart2js:noInline')
  static PackageCategoryItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageCategoryItem>(create);
  static PackageCategoryItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get categoryName => $_getSZ(0);
  @$pb.TagNumber(1)
  set categoryName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCategoryName() => $_has(0);
  @$pb.TagNumber(1)
  void clearCategoryName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get icon => $_getSZ(1);
  @$pb.TagNumber(2)
  set icon($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasIcon() => $_has(1);
  @$pb.TagNumber(2)
  void clearIcon() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get image => $_getSZ(3);
  @$pb.TagNumber(4)
  set image($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasImage() => $_has(3);
  @$pb.TagNumber(4)
  void clearImage() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get categoryID => $_getSZ(4);
  @$pb.TagNumber(5)
  set categoryID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasCategoryID() => $_has(4);
  @$pb.TagNumber(5)
  void clearCategoryID() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get serviceAreaID => $_getSZ(5);
  @$pb.TagNumber(6)
  set serviceAreaID($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasServiceAreaID() => $_has(5);
  @$pb.TagNumber(6)
  void clearServiceAreaID() => clearField(6);
}

class PackageCategoryRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageCategoryRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categoryID', protoName: 'categoryID')
    ..hasRequiredFields = false
  ;

  PackageCategoryRequest._() : super();
  factory PackageCategoryRequest({
    $core.String? categoryID,
  }) {
    final _result = create();
    if (categoryID != null) {
      _result.categoryID = categoryID;
    }
    return _result;
  }
  factory PackageCategoryRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageCategoryRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageCategoryRequest clone() => PackageCategoryRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageCategoryRequest copyWith(void Function(PackageCategoryRequest) updates) => super.copyWith((message) => updates(message as PackageCategoryRequest)) as PackageCategoryRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageCategoryRequest create() => PackageCategoryRequest._();
  PackageCategoryRequest createEmptyInstance() => create();
  static $pb.PbList<PackageCategoryRequest> createRepeated() => $pb.PbList<PackageCategoryRequest>();
  @$core.pragma('dart2js:noInline')
  static PackageCategoryRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageCategoryRequest>(create);
  static PackageCategoryRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get categoryID => $_getSZ(0);
  @$pb.TagNumber(1)
  set categoryID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCategoryID() => $_has(0);
  @$pb.TagNumber(1)
  void clearCategoryID() => clearField(1);
}

class ListPackageCategoryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListPackageCategoryResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<PackageCategoryItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: PackageCategoryItem.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ListPackageCategoryResponse._() : super();
  factory ListPackageCategoryResponse({
    $core.Iterable<PackageCategoryItem>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ListPackageCategoryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListPackageCategoryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListPackageCategoryResponse clone() => ListPackageCategoryResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListPackageCategoryResponse copyWith(void Function(ListPackageCategoryResponse) updates) => super.copyWith((message) => updates(message as ListPackageCategoryResponse)) as ListPackageCategoryResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListPackageCategoryResponse create() => ListPackageCategoryResponse._();
  ListPackageCategoryResponse createEmptyInstance() => create();
  static $pb.PbList<ListPackageCategoryResponse> createRepeated() => $pb.PbList<ListPackageCategoryResponse>();
  @$core.pragma('dart2js:noInline')
  static ListPackageCategoryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListPackageCategoryResponse>(create);
  static ListPackageCategoryResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<PackageCategoryItem> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class ViewPackageCategoryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ViewPackageCategoryResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOM<PackageCategoryItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: PackageCategoryItem.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ViewPackageCategoryResponse._() : super();
  factory ViewPackageCategoryResponse({
    PackageCategoryItem? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data = data;
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ViewPackageCategoryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ViewPackageCategoryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ViewPackageCategoryResponse clone() => ViewPackageCategoryResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ViewPackageCategoryResponse copyWith(void Function(ViewPackageCategoryResponse) updates) => super.copyWith((message) => updates(message as ViewPackageCategoryResponse)) as ViewPackageCategoryResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ViewPackageCategoryResponse create() => ViewPackageCategoryResponse._();
  ViewPackageCategoryResponse createEmptyInstance() => create();
  static $pb.PbList<ViewPackageCategoryResponse> createRepeated() => $pb.PbList<ViewPackageCategoryResponse>();
  @$core.pragma('dart2js:noInline')
  static ViewPackageCategoryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ViewPackageCategoryResponse>(create);
  static ViewPackageCategoryResponse? _defaultInstance;

  @$pb.TagNumber(1)
  PackageCategoryItem get data => $_getN(0);
  @$pb.TagNumber(1)
  set data(PackageCategoryItem v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasData() => $_has(0);
  @$pb.TagNumber(1)
  void clearData() => clearField(1);
  @$pb.TagNumber(1)
  PackageCategoryItem ensureData() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class EmptyMenuRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'EmptyMenuRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  EmptyMenuRequest._() : super();
  factory EmptyMenuRequest() => create();
  factory EmptyMenuRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory EmptyMenuRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  EmptyMenuRequest clone() => EmptyMenuRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  EmptyMenuRequest copyWith(void Function(EmptyMenuRequest) updates) => super.copyWith((message) => updates(message as EmptyMenuRequest)) as EmptyMenuRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static EmptyMenuRequest create() => EmptyMenuRequest._();
  EmptyMenuRequest createEmptyInstance() => create();
  static $pb.PbList<EmptyMenuRequest> createRepeated() => $pb.PbList<EmptyMenuRequest>();
  @$core.pragma('dart2js:noInline')
  static EmptyMenuRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<EmptyMenuRequest>(create);
  static EmptyMenuRequest? _defaultInstance;
}

class DecreaseMenuItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DecreaseMenuItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'menuID', protoName: 'menuID')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  DecreaseMenuItem._() : super();
  factory DecreaseMenuItem({
    $core.String? menuID,
    $core.int? quantity,
  }) {
    final _result = create();
    if (menuID != null) {
      _result.menuID = menuID;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    return _result;
  }
  factory DecreaseMenuItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DecreaseMenuItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DecreaseMenuItem clone() => DecreaseMenuItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DecreaseMenuItem copyWith(void Function(DecreaseMenuItem) updates) => super.copyWith((message) => updates(message as DecreaseMenuItem)) as DecreaseMenuItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DecreaseMenuItem create() => DecreaseMenuItem._();
  DecreaseMenuItem createEmptyInstance() => create();
  static $pb.PbList<DecreaseMenuItem> createRepeated() => $pb.PbList<DecreaseMenuItem>();
  @$core.pragma('dart2js:noInline')
  static DecreaseMenuItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DecreaseMenuItem>(create);
  static DecreaseMenuItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get menuID => $_getSZ(0);
  @$pb.TagNumber(1)
  set menuID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMenuID() => $_has(0);
  @$pb.TagNumber(1)
  void clearMenuID() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get quantity => $_getIZ(1);
  @$pb.TagNumber(2)
  set quantity($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasQuantity() => $_has(1);
  @$pb.TagNumber(2)
  void clearQuantity() => clearField(2);
}

class DecreaseMenuRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DecreaseMenuRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<DecreaseMenuItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: DecreaseMenuItem.create)
    ..hasRequiredFields = false
  ;

  DecreaseMenuRequest._() : super();
  factory DecreaseMenuRequest({
    $core.Iterable<DecreaseMenuItem>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory DecreaseMenuRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DecreaseMenuRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DecreaseMenuRequest clone() => DecreaseMenuRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DecreaseMenuRequest copyWith(void Function(DecreaseMenuRequest) updates) => super.copyWith((message) => updates(message as DecreaseMenuRequest)) as DecreaseMenuRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DecreaseMenuRequest create() => DecreaseMenuRequest._();
  DecreaseMenuRequest createEmptyInstance() => create();
  static $pb.PbList<DecreaseMenuRequest> createRepeated() => $pb.PbList<DecreaseMenuRequest>();
  @$core.pragma('dart2js:noInline')
  static DecreaseMenuRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DecreaseMenuRequest>(create);
  static DecreaseMenuRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<DecreaseMenuItem> get items => $_getList(0);
}

class MenuRequestItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuRequestItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'requestId', protoName: 'requestId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type')
    ..hasRequiredFields = false
  ;

  MenuRequestItem._() : super();
  factory MenuRequestItem({
    $core.String? requestId,
    $core.String? type,
  }) {
    final _result = create();
    if (requestId != null) {
      _result.requestId = requestId;
    }
    if (type != null) {
      _result.type = type;
    }
    return _result;
  }
  factory MenuRequestItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuRequestItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuRequestItem clone() => MenuRequestItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuRequestItem copyWith(void Function(MenuRequestItem) updates) => super.copyWith((message) => updates(message as MenuRequestItem)) as MenuRequestItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuRequestItem create() => MenuRequestItem._();
  MenuRequestItem createEmptyInstance() => create();
  static $pb.PbList<MenuRequestItem> createRepeated() => $pb.PbList<MenuRequestItem>();
  @$core.pragma('dart2js:noInline')
  static MenuRequestItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuRequestItem>(create);
  static MenuRequestItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get requestId => $_getSZ(0);
  @$pb.TagNumber(1)
  set requestId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRequestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRequestId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get type => $_getSZ(1);
  @$pb.TagNumber(2)
  set type($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasType() => $_has(1);
  @$pb.TagNumber(2)
  void clearType() => clearField(2);
}

class MenuItems extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuItems', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'category')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'location')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'restaurant')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'details')
    ..a<$core.double>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'price', $pb.PbFieldType.OD)
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..a<$core.int>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.O3)
    ..a<$core.double>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rate', $pb.PbFieldType.OD)
    ..a<$core.int>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inStockQuantity', $pb.PbFieldType.O3)
    ..a<$core.double>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'discount', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  MenuItems._() : super();
  factory MenuItems({
    $core.String? id,
    $core.String? category,
    $core.String? name,
    $core.String? location,
    $core.String? restaurant,
    $core.String? details,
    $core.double? price,
    $core.String? image,
    $core.int? quantity,
    $core.double? rate,
    $core.int? inStockQuantity,
    $core.double? discount,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (category != null) {
      _result.category = category;
    }
    if (name != null) {
      _result.name = name;
    }
    if (location != null) {
      _result.location = location;
    }
    if (restaurant != null) {
      _result.restaurant = restaurant;
    }
    if (details != null) {
      _result.details = details;
    }
    if (price != null) {
      _result.price = price;
    }
    if (image != null) {
      _result.image = image;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (rate != null) {
      _result.rate = rate;
    }
    if (inStockQuantity != null) {
      _result.inStockQuantity = inStockQuantity;
    }
    if (discount != null) {
      _result.discount = discount;
    }
    return _result;
  }
  factory MenuItems.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuItems.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuItems clone() => MenuItems()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuItems copyWith(void Function(MenuItems) updates) => super.copyWith((message) => updates(message as MenuItems)) as MenuItems; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuItems create() => MenuItems._();
  MenuItems createEmptyInstance() => create();
  static $pb.PbList<MenuItems> createRepeated() => $pb.PbList<MenuItems>();
  @$core.pragma('dart2js:noInline')
  static MenuItems getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuItems>(create);
  static MenuItems? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get category => $_getSZ(1);
  @$pb.TagNumber(2)
  set category($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCategory() => $_has(1);
  @$pb.TagNumber(2)
  void clearCategory() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get name => $_getSZ(2);
  @$pb.TagNumber(3)
  set name($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get location => $_getSZ(3);
  @$pb.TagNumber(4)
  set location($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasLocation() => $_has(3);
  @$pb.TagNumber(4)
  void clearLocation() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get restaurant => $_getSZ(4);
  @$pb.TagNumber(5)
  set restaurant($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasRestaurant() => $_has(4);
  @$pb.TagNumber(5)
  void clearRestaurant() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get details => $_getSZ(5);
  @$pb.TagNumber(6)
  set details($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDetails() => $_has(5);
  @$pb.TagNumber(6)
  void clearDetails() => clearField(6);

  @$pb.TagNumber(7)
  $core.double get price => $_getN(6);
  @$pb.TagNumber(7)
  set price($core.double v) { $_setDouble(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasPrice() => $_has(6);
  @$pb.TagNumber(7)
  void clearPrice() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get image => $_getSZ(7);
  @$pb.TagNumber(8)
  set image($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasImage() => $_has(7);
  @$pb.TagNumber(8)
  void clearImage() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get quantity => $_getIZ(8);
  @$pb.TagNumber(9)
  set quantity($core.int v) { $_setSignedInt32(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasQuantity() => $_has(8);
  @$pb.TagNumber(9)
  void clearQuantity() => clearField(9);

  @$pb.TagNumber(10)
  $core.double get rate => $_getN(9);
  @$pb.TagNumber(10)
  set rate($core.double v) { $_setDouble(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasRate() => $_has(9);
  @$pb.TagNumber(10)
  void clearRate() => clearField(10);

  @$pb.TagNumber(11)
  $core.int get inStockQuantity => $_getIZ(10);
  @$pb.TagNumber(11)
  set inStockQuantity($core.int v) { $_setSignedInt32(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasInStockQuantity() => $_has(10);
  @$pb.TagNumber(11)
  void clearInStockQuantity() => clearField(11);

  @$pb.TagNumber(12)
  $core.double get discount => $_getN(11);
  @$pb.TagNumber(12)
  set discount($core.double v) { $_setDouble(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasDiscount() => $_has(11);
  @$pb.TagNumber(12)
  void clearDiscount() => clearField(12);
}

class MenuResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data')
    ..hasRequiredFields = false
  ;

  MenuResponse._() : super();
  factory MenuResponse({
    $core.String? status,
    $core.String? message,
    $core.String? data,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (data != null) {
      _result.data = data;
    }
    return _result;
  }
  factory MenuResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuResponse clone() => MenuResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuResponse copyWith(void Function(MenuResponse) updates) => super.copyWith((message) => updates(message as MenuResponse)) as MenuResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuResponse create() => MenuResponse._();
  MenuResponse createEmptyInstance() => create();
  static $pb.PbList<MenuResponse> createRepeated() => $pb.PbList<MenuResponse>();
  @$core.pragma('dart2js:noInline')
  static MenuResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuResponse>(create);
  static MenuResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get data => $_getSZ(2);
  @$pb.TagNumber(3)
  set data($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasData() => $_has(2);
  @$pb.TagNumber(3)
  void clearData() => clearField(3);
}

class ListMenuResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListMenuResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<MenuItems>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: MenuItems.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  ListMenuResponse._() : super();
  factory ListMenuResponse({
    $core.Iterable<MenuItems>? data,
    $core.String? status,
    $core.String? message,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory ListMenuResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListMenuResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListMenuResponse clone() => ListMenuResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListMenuResponse copyWith(void Function(ListMenuResponse) updates) => super.copyWith((message) => updates(message as ListMenuResponse)) as ListMenuResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListMenuResponse create() => ListMenuResponse._();
  ListMenuResponse createEmptyInstance() => create();
  static $pb.PbList<ListMenuResponse> createRepeated() => $pb.PbList<ListMenuResponse>();
  @$core.pragma('dart2js:noInline')
  static ListMenuResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListMenuResponse>(create);
  static ListMenuResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<MenuItems> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get message => $_getSZ(2);
  @$pb.TagNumber(3)
  set message($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMessage() => $_has(2);
  @$pb.TagNumber(3)
  void clearMessage() => clearField(3);
}

class ViewMenuResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ViewMenuResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOM<MenuItems>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: MenuItems.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  ViewMenuResponse._() : super();
  factory ViewMenuResponse({
    $core.String? status,
    MenuItems? data,
    $core.String? message,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (data != null) {
      _result.data = data;
    }
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory ViewMenuResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ViewMenuResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ViewMenuResponse clone() => ViewMenuResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ViewMenuResponse copyWith(void Function(ViewMenuResponse) updates) => super.copyWith((message) => updates(message as ViewMenuResponse)) as ViewMenuResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ViewMenuResponse create() => ViewMenuResponse._();
  ViewMenuResponse createEmptyInstance() => create();
  static $pb.PbList<ViewMenuResponse> createRepeated() => $pb.PbList<ViewMenuResponse>();
  @$core.pragma('dart2js:noInline')
  static ViewMenuResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ViewMenuResponse>(create);
  static ViewMenuResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  MenuItems get data => $_getN(1);
  @$pb.TagNumber(2)
  set data(MenuItems v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasData() => $_has(1);
  @$pb.TagNumber(2)
  void clearData() => clearField(2);
  @$pb.TagNumber(2)
  MenuItems ensureData() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get message => $_getSZ(2);
  @$pb.TagNumber(3)
  set message($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMessage() => $_has(2);
  @$pb.TagNumber(3)
  void clearMessage() => clearField(3);
}

class Item extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Item', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemCategoryID', protoName: 'itemCategoryID')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'unit')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt')
    ..hasRequiredFields = false
  ;

  Item._() : super();
  factory Item({
    $core.String? id,
    $core.String? name,
    $core.String? description,
    $core.String? itemCategoryID,
    $core.String? image,
    $core.String? unit,
    $core.String? createdAt,
    $core.String? updatedAt,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    if (description != null) {
      _result.description = description;
    }
    if (itemCategoryID != null) {
      _result.itemCategoryID = itemCategoryID;
    }
    if (image != null) {
      _result.image = image;
    }
    if (unit != null) {
      _result.unit = unit;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    return _result;
  }
  factory Item.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Item.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Item clone() => Item()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Item copyWith(void Function(Item) updates) => super.copyWith((message) => updates(message as Item)) as Item; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Item create() => Item._();
  Item createEmptyInstance() => create();
  static $pb.PbList<Item> createRepeated() => $pb.PbList<Item>();
  @$core.pragma('dart2js:noInline')
  static Item getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Item>(create);
  static Item? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get itemCategoryID => $_getSZ(3);
  @$pb.TagNumber(4)
  set itemCategoryID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasItemCategoryID() => $_has(3);
  @$pb.TagNumber(4)
  void clearItemCategoryID() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get image => $_getSZ(4);
  @$pb.TagNumber(5)
  set image($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasImage() => $_has(4);
  @$pb.TagNumber(5)
  void clearImage() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get unit => $_getSZ(5);
  @$pb.TagNumber(6)
  set unit($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasUnit() => $_has(5);
  @$pb.TagNumber(6)
  void clearUnit() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get createdAt => $_getSZ(6);
  @$pb.TagNumber(7)
  set createdAt($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasCreatedAt() => $_has(6);
  @$pb.TagNumber(7)
  void clearCreatedAt() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get updatedAt => $_getSZ(7);
  @$pb.TagNumber(8)
  set updatedAt($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasUpdatedAt() => $_has(7);
  @$pb.TagNumber(8)
  void clearUpdatedAt() => clearField(8);
}

class ListItemsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListItemsResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<Item>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: Item.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ListItemsResponse._() : super();
  factory ListItemsResponse({
    $core.Iterable<Item>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ListItemsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListItemsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListItemsResponse clone() => ListItemsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListItemsResponse copyWith(void Function(ListItemsResponse) updates) => super.copyWith((message) => updates(message as ListItemsResponse)) as ListItemsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListItemsResponse create() => ListItemsResponse._();
  ListItemsResponse createEmptyInstance() => create();
  static $pb.PbList<ListItemsResponse> createRepeated() => $pb.PbList<ListItemsResponse>();
  @$core.pragma('dart2js:noInline')
  static ListItemsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListItemsResponse>(create);
  static ListItemsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Item> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class ItemRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..hasRequiredFields = false
  ;

  ItemRequest._() : super();
  factory ItemRequest({
    $core.String? itemID,
  }) {
    final _result = create();
    if (itemID != null) {
      _result.itemID = itemID;
    }
    return _result;
  }
  factory ItemRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemRequest clone() => ItemRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemRequest copyWith(void Function(ItemRequest) updates) => super.copyWith((message) => updates(message as ItemRequest)) as ItemRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemRequest create() => ItemRequest._();
  ItemRequest createEmptyInstance() => create();
  static $pb.PbList<ItemRequest> createRepeated() => $pb.PbList<ItemRequest>();
  @$core.pragma('dart2js:noInline')
  static ItemRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemRequest>(create);
  static ItemRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get itemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set itemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemID() => clearField(1);
}

class ItemCategory extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemCategory', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..hasRequiredFields = false
  ;

  ItemCategory._() : super();
  factory ItemCategory({
    $core.String? id,
    $core.String? name,
    $core.String? description,
    $core.String? image,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    if (description != null) {
      _result.description = description;
    }
    if (image != null) {
      _result.image = image;
    }
    return _result;
  }
  factory ItemCategory.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemCategory.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemCategory clone() => ItemCategory()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemCategory copyWith(void Function(ItemCategory) updates) => super.copyWith((message) => updates(message as ItemCategory)) as ItemCategory; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemCategory create() => ItemCategory._();
  ItemCategory createEmptyInstance() => create();
  static $pb.PbList<ItemCategory> createRepeated() => $pb.PbList<ItemCategory>();
  @$core.pragma('dart2js:noInline')
  static ItemCategory getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemCategory>(create);
  static ItemCategory? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get image => $_getSZ(3);
  @$pb.TagNumber(4)
  set image($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasImage() => $_has(3);
  @$pb.TagNumber(4)
  void clearImage() => clearField(4);
}

class ListItemCategoryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListItemCategoryResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<ItemCategory>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: ItemCategory.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ListItemCategoryResponse._() : super();
  factory ListItemCategoryResponse({
    $core.Iterable<ItemCategory>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ListItemCategoryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListItemCategoryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListItemCategoryResponse clone() => ListItemCategoryResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListItemCategoryResponse copyWith(void Function(ListItemCategoryResponse) updates) => super.copyWith((message) => updates(message as ListItemCategoryResponse)) as ListItemCategoryResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListItemCategoryResponse create() => ListItemCategoryResponse._();
  ListItemCategoryResponse createEmptyInstance() => create();
  static $pb.PbList<ListItemCategoryResponse> createRepeated() => $pb.PbList<ListItemCategoryResponse>();
  @$core.pragma('dart2js:noInline')
  static ListItemCategoryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListItemCategoryResponse>(create);
  static ListItemCategoryResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<ItemCategory> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class ItemCategoryRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemCategoryRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemCategoryID', protoName: 'itemCategoryID')
    ..hasRequiredFields = false
  ;

  ItemCategoryRequest._() : super();
  factory ItemCategoryRequest({
    $core.String? itemCategoryID,
  }) {
    final _result = create();
    if (itemCategoryID != null) {
      _result.itemCategoryID = itemCategoryID;
    }
    return _result;
  }
  factory ItemCategoryRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemCategoryRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemCategoryRequest clone() => ItemCategoryRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemCategoryRequest copyWith(void Function(ItemCategoryRequest) updates) => super.copyWith((message) => updates(message as ItemCategoryRequest)) as ItemCategoryRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemCategoryRequest create() => ItemCategoryRequest._();
  ItemCategoryRequest createEmptyInstance() => create();
  static $pb.PbList<ItemCategoryRequest> createRepeated() => $pb.PbList<ItemCategoryRequest>();
  @$core.pragma('dart2js:noInline')
  static ItemCategoryRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemCategoryRequest>(create);
  static ItemCategoryRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get itemCategoryID => $_getSZ(0);
  @$pb.TagNumber(1)
  set itemCategoryID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemCategoryID() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemCategoryID() => clearField(1);
}

class PackageItemCreate extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageItemCreate', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'category')
    ..hasRequiredFields = false
  ;

  PackageItemCreate._() : super();
  factory PackageItemCreate({
    $core.String? packageID,
    $core.String? category,
  }) {
    final _result = create();
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (category != null) {
      _result.category = category;
    }
    return _result;
  }
  factory PackageItemCreate.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageItemCreate.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageItemCreate clone() => PackageItemCreate()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageItemCreate copyWith(void Function(PackageItemCreate) updates) => super.copyWith((message) => updates(message as PackageItemCreate)) as PackageItemCreate; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageItemCreate create() => PackageItemCreate._();
  PackageItemCreate createEmptyInstance() => create();
  static $pb.PbList<PackageItemCreate> createRepeated() => $pb.PbList<PackageItemCreate>();
  @$core.pragma('dart2js:noInline')
  static PackageItemCreate getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageItemCreate>(create);
  static PackageItemCreate? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get category => $_getSZ(1);
  @$pb.TagNumber(2)
  set category($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCategory() => $_has(1);
  @$pb.TagNumber(2)
  void clearCategory() => clearField(2);
}

class PackageItemData extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageItemData', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageItemID', protoName: 'packageItemID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'category')
    ..hasRequiredFields = false
  ;

  PackageItemData._() : super();
  factory PackageItemData({
    $core.String? packageItemID,
    $core.String? packageID,
    $core.String? category,
  }) {
    final _result = create();
    if (packageItemID != null) {
      _result.packageItemID = packageItemID;
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (category != null) {
      _result.category = category;
    }
    return _result;
  }
  factory PackageItemData.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageItemData.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageItemData clone() => PackageItemData()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageItemData copyWith(void Function(PackageItemData) updates) => super.copyWith((message) => updates(message as PackageItemData)) as PackageItemData; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageItemData create() => PackageItemData._();
  PackageItemData createEmptyInstance() => create();
  static $pb.PbList<PackageItemData> createRepeated() => $pb.PbList<PackageItemData>();
  @$core.pragma('dart2js:noInline')
  static PackageItemData getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageItemData>(create);
  static PackageItemData? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageItemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageItemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageItemID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get packageID => $_getSZ(1);
  @$pb.TagNumber(2)
  set packageID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackageID() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackageID() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get category => $_getSZ(2);
  @$pb.TagNumber(3)
  set category($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCategory() => $_has(2);
  @$pb.TagNumber(3)
  void clearCategory() => clearField(3);
}

class ListPackageItemResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListPackageItemResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<PackageItemData>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: PackageItemData.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  ListPackageItemResponse._() : super();
  factory ListPackageItemResponse({
    $core.Iterable<PackageItemData>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ListPackageItemResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListPackageItemResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListPackageItemResponse clone() => ListPackageItemResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListPackageItemResponse copyWith(void Function(ListPackageItemResponse) updates) => super.copyWith((message) => updates(message as ListPackageItemResponse)) as ListPackageItemResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListPackageItemResponse create() => ListPackageItemResponse._();
  ListPackageItemResponse createEmptyInstance() => create();
  static $pb.PbList<ListPackageItemResponse> createRepeated() => $pb.PbList<ListPackageItemResponse>();
  @$core.pragma('dart2js:noInline')
  static ListPackageItemResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListPackageItemResponse>(create);
  static ListPackageItemResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<PackageItemData> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class ItemData extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemData', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.OD)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'price', $pb.PbFieldType.OD)
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isRequired', protoName: 'isRequired')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isDefault', protoName: 'isDefault')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'units')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..hasRequiredFields = false
  ;

  ItemData._() : super();
  factory ItemData({
    $core.String? itemID,
    $core.double? quantity,
    $core.double? price,
    $core.bool? isRequired,
    $core.bool? isDefault,
    $core.String? description,
    $core.String? units,
    $core.String? name,
    $core.String? image,
  }) {
    final _result = create();
    if (itemID != null) {
      _result.itemID = itemID;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (price != null) {
      _result.price = price;
    }
    if (isRequired != null) {
      _result.isRequired = isRequired;
    }
    if (isDefault != null) {
      _result.isDefault = isDefault;
    }
    if (description != null) {
      _result.description = description;
    }
    if (units != null) {
      _result.units = units;
    }
    if (name != null) {
      _result.name = name;
    }
    if (image != null) {
      _result.image = image;
    }
    return _result;
  }
  factory ItemData.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemData.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemData clone() => ItemData()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemData copyWith(void Function(ItemData) updates) => super.copyWith((message) => updates(message as ItemData)) as ItemData; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemData create() => ItemData._();
  ItemData createEmptyInstance() => create();
  static $pb.PbList<ItemData> createRepeated() => $pb.PbList<ItemData>();
  @$core.pragma('dart2js:noInline')
  static ItemData getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemData>(create);
  static ItemData? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get itemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set itemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemID() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get quantity => $_getN(1);
  @$pb.TagNumber(2)
  set quantity($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasQuantity() => $_has(1);
  @$pb.TagNumber(2)
  void clearQuantity() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get price => $_getN(2);
  @$pb.TagNumber(3)
  set price($core.double v) { $_setDouble(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPrice() => $_has(2);
  @$pb.TagNumber(3)
  void clearPrice() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get isRequired => $_getBF(3);
  @$pb.TagNumber(4)
  set isRequired($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasIsRequired() => $_has(3);
  @$pb.TagNumber(4)
  void clearIsRequired() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get isDefault => $_getBF(4);
  @$pb.TagNumber(5)
  set isDefault($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasIsDefault() => $_has(4);
  @$pb.TagNumber(5)
  void clearIsDefault() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get description => $_getSZ(5);
  @$pb.TagNumber(6)
  set description($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDescription() => $_has(5);
  @$pb.TagNumber(6)
  void clearDescription() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get units => $_getSZ(6);
  @$pb.TagNumber(7)
  set units($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasUnits() => $_has(6);
  @$pb.TagNumber(7)
  void clearUnits() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get name => $_getSZ(7);
  @$pb.TagNumber(8)
  set name($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasName() => $_has(7);
  @$pb.TagNumber(8)
  void clearName() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get image => $_getSZ(8);
  @$pb.TagNumber(9)
  set image($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasImage() => $_has(8);
  @$pb.TagNumber(9)
  void clearImage() => clearField(9);
}

class AddItemPackage extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddItemPackage', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageItemID', protoName: 'packageItemID')
    ..aOM<ItemData>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item', subBuilder: ItemData.create)
    ..hasRequiredFields = false
  ;

  AddItemPackage._() : super();
  factory AddItemPackage({
    $core.String? packageItemID,
    ItemData? item,
  }) {
    final _result = create();
    if (packageItemID != null) {
      _result.packageItemID = packageItemID;
    }
    if (item != null) {
      _result.item = item;
    }
    return _result;
  }
  factory AddItemPackage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddItemPackage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddItemPackage clone() => AddItemPackage()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddItemPackage copyWith(void Function(AddItemPackage) updates) => super.copyWith((message) => updates(message as AddItemPackage)) as AddItemPackage; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddItemPackage create() => AddItemPackage._();
  AddItemPackage createEmptyInstance() => create();
  static $pb.PbList<AddItemPackage> createRepeated() => $pb.PbList<AddItemPackage>();
  @$core.pragma('dart2js:noInline')
  static AddItemPackage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddItemPackage>(create);
  static AddItemPackage? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageItemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageItemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageItemID() => clearField(1);

  @$pb.TagNumber(2)
  ItemData get item => $_getN(1);
  @$pb.TagNumber(2)
  set item(ItemData v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasItem() => $_has(1);
  @$pb.TagNumber(2)
  void clearItem() => clearField(2);
  @$pb.TagNumber(2)
  ItemData ensureItem() => $_ensure(1);
}

class Images extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Images', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'imageUrl', protoName: 'imageUrl')
    ..hasRequiredFields = false
  ;

  Images._() : super();
  factory Images({
    $core.String? imageUrl,
  }) {
    final _result = create();
    if (imageUrl != null) {
      _result.imageUrl = imageUrl;
    }
    return _result;
  }
  factory Images.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Images.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Images clone() => Images()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Images copyWith(void Function(Images) updates) => super.copyWith((message) => updates(message as Images)) as Images; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Images create() => Images._();
  Images createEmptyInstance() => create();
  static $pb.PbList<Images> createRepeated() => $pb.PbList<Images>();
  @$core.pragma('dart2js:noInline')
  static Images getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Images>(create);
  static Images? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get imageUrl => $_getSZ(0);
  @$pb.TagNumber(1)
  set imageUrl($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasImageUrl() => $_has(0);
  @$pb.TagNumber(1)
  void clearImageUrl() => clearField(1);
}

class PackageItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageItemID', protoName: 'packageItemID')
    ..pc<ItemData>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'substitutes', $pb.PbFieldType.PM, subBuilder: ItemData.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'category')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..hasRequiredFields = false
  ;

  PackageItem._() : super();
  factory PackageItem({
    $core.String? packageItemID,
    $core.Iterable<ItemData>? substitutes,
    $core.String? category,
    $core.String? packageID,
  }) {
    final _result = create();
    if (packageItemID != null) {
      _result.packageItemID = packageItemID;
    }
    if (substitutes != null) {
      _result.substitutes.addAll(substitutes);
    }
    if (category != null) {
      _result.category = category;
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    return _result;
  }
  factory PackageItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageItem clone() => PackageItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageItem copyWith(void Function(PackageItem) updates) => super.copyWith((message) => updates(message as PackageItem)) as PackageItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageItem create() => PackageItem._();
  PackageItem createEmptyInstance() => create();
  static $pb.PbList<PackageItem> createRepeated() => $pb.PbList<PackageItem>();
  @$core.pragma('dart2js:noInline')
  static PackageItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageItem>(create);
  static PackageItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageItemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageItemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageItemID() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<ItemData> get substitutes => $_getList(1);

  @$pb.TagNumber(3)
  $core.String get category => $_getSZ(2);
  @$pb.TagNumber(3)
  set category($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCategory() => $_has(2);
  @$pb.TagNumber(3)
  void clearCategory() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get packageID => $_getSZ(3);
  @$pb.TagNumber(4)
  set packageID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPackageID() => $_has(3);
  @$pb.TagNumber(4)
  void clearPackageID() => clearField(4);
}

class Package extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Package', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt', protoName: 'createdAt')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt', protoName: 'updatedAt')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'estimatedPrice', $pb.PbFieldType.OD, protoName: 'estimatedPrice')
    ..a<$core.double>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxNumberOfItems', $pb.PbFieldType.OD, protoName: 'maxNumberOfItems')
    ..a<$core.double>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'profitMargin', $pb.PbFieldType.OD, protoName: 'profitMargin')
    ..pc<PackageItem>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: PackageItem.create)
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categoryID', protoName: 'categoryID')
    ..aOS(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..aOB(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isFreeDelivery', protoName: 'isFreeDelivery')
    ..aOB(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isPublished', protoName: 'isPublished')
    ..aOB(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isPackageLiked', protoName: 'isPackageLiked')
    ..hasRequiredFields = false
  ;

  Package._() : super();
  factory Package({
    $core.String? name,
    $core.String? description,
    $core.String? createdAt,
    $core.String? updatedAt,
    $core.String? image,
    $core.double? estimatedPrice,
    $core.double? maxNumberOfItems,
    $core.double? profitMargin,
    $core.Iterable<PackageItem>? items,
    $core.String? packageID,
    $core.String? categoryID,
    $core.String? serviceAreaID,
    $core.bool? isFreeDelivery,
    $core.bool? isPublished,
    $core.bool? isPackageLiked,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (description != null) {
      _result.description = description;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    if (image != null) {
      _result.image = image;
    }
    if (estimatedPrice != null) {
      _result.estimatedPrice = estimatedPrice;
    }
    if (maxNumberOfItems != null) {
      _result.maxNumberOfItems = maxNumberOfItems;
    }
    if (profitMargin != null) {
      _result.profitMargin = profitMargin;
    }
    if (items != null) {
      _result.items.addAll(items);
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (categoryID != null) {
      _result.categoryID = categoryID;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    if (isFreeDelivery != null) {
      _result.isFreeDelivery = isFreeDelivery;
    }
    if (isPublished != null) {
      _result.isPublished = isPublished;
    }
    if (isPackageLiked != null) {
      _result.isPackageLiked = isPackageLiked;
    }
    return _result;
  }
  factory Package.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Package.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Package clone() => Package()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Package copyWith(void Function(Package) updates) => super.copyWith((message) => updates(message as Package)) as Package; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Package create() => Package._();
  Package createEmptyInstance() => create();
  static $pb.PbList<Package> createRepeated() => $pb.PbList<Package>();
  @$core.pragma('dart2js:noInline')
  static Package getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Package>(create);
  static Package? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get description => $_getSZ(1);
  @$pb.TagNumber(2)
  set description($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDescription() => $_has(1);
  @$pb.TagNumber(2)
  void clearDescription() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get createdAt => $_getSZ(2);
  @$pb.TagNumber(3)
  set createdAt($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCreatedAt() => $_has(2);
  @$pb.TagNumber(3)
  void clearCreatedAt() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get updatedAt => $_getSZ(3);
  @$pb.TagNumber(4)
  set updatedAt($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasUpdatedAt() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpdatedAt() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get image => $_getSZ(4);
  @$pb.TagNumber(5)
  set image($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasImage() => $_has(4);
  @$pb.TagNumber(5)
  void clearImage() => clearField(5);

  @$pb.TagNumber(6)
  $core.double get estimatedPrice => $_getN(5);
  @$pb.TagNumber(6)
  set estimatedPrice($core.double v) { $_setDouble(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasEstimatedPrice() => $_has(5);
  @$pb.TagNumber(6)
  void clearEstimatedPrice() => clearField(6);

  @$pb.TagNumber(7)
  $core.double get maxNumberOfItems => $_getN(6);
  @$pb.TagNumber(7)
  set maxNumberOfItems($core.double v) { $_setDouble(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasMaxNumberOfItems() => $_has(6);
  @$pb.TagNumber(7)
  void clearMaxNumberOfItems() => clearField(7);

  @$pb.TagNumber(8)
  $core.double get profitMargin => $_getN(7);
  @$pb.TagNumber(8)
  set profitMargin($core.double v) { $_setDouble(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasProfitMargin() => $_has(7);
  @$pb.TagNumber(8)
  void clearProfitMargin() => clearField(8);

  @$pb.TagNumber(9)
  $core.List<PackageItem> get items => $_getList(8);

  @$pb.TagNumber(10)
  $core.String get packageID => $_getSZ(9);
  @$pb.TagNumber(10)
  set packageID($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasPackageID() => $_has(9);
  @$pb.TagNumber(10)
  void clearPackageID() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get categoryID => $_getSZ(10);
  @$pb.TagNumber(11)
  set categoryID($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasCategoryID() => $_has(10);
  @$pb.TagNumber(11)
  void clearCategoryID() => clearField(11);

  @$pb.TagNumber(12)
  $core.String get serviceAreaID => $_getSZ(11);
  @$pb.TagNumber(12)
  set serviceAreaID($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasServiceAreaID() => $_has(11);
  @$pb.TagNumber(12)
  void clearServiceAreaID() => clearField(12);

  @$pb.TagNumber(13)
  $core.bool get isFreeDelivery => $_getBF(12);
  @$pb.TagNumber(13)
  set isFreeDelivery($core.bool v) { $_setBool(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasIsFreeDelivery() => $_has(12);
  @$pb.TagNumber(13)
  void clearIsFreeDelivery() => clearField(13);

  @$pb.TagNumber(14)
  $core.bool get isPublished => $_getBF(13);
  @$pb.TagNumber(14)
  set isPublished($core.bool v) { $_setBool(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasIsPublished() => $_has(13);
  @$pb.TagNumber(14)
  void clearIsPublished() => clearField(14);

  @$pb.TagNumber(15)
  $core.bool get isPackageLiked => $_getBF(14);
  @$pb.TagNumber(15)
  set isPackageLiked($core.bool v) { $_setBool(14, v); }
  @$pb.TagNumber(15)
  $core.bool hasIsPackageLiked() => $_has(14);
  @$pb.TagNumber(15)
  void clearIsPackageLiked() => clearField(15);
}

class PackageRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PackageRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'limit')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'page')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageCatgoryID', protoName: 'packageCatgoryID')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userID', protoName: 'userID')
    ..hasRequiredFields = false
  ;

  PackageRequest._() : super();
  factory PackageRequest({
    $core.String? packageID,
    $core.String? serviceAreaID,
    $fixnum.Int64? limit,
    $fixnum.Int64? page,
    $core.String? packageCatgoryID,
    $core.String? userID,
  }) {
    final _result = create();
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    if (limit != null) {
      _result.limit = limit;
    }
    if (page != null) {
      _result.page = page;
    }
    if (packageCatgoryID != null) {
      _result.packageCatgoryID = packageCatgoryID;
    }
    if (userID != null) {
      _result.userID = userID;
    }
    return _result;
  }
  factory PackageRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PackageRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PackageRequest clone() => PackageRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PackageRequest copyWith(void Function(PackageRequest) updates) => super.copyWith((message) => updates(message as PackageRequest)) as PackageRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PackageRequest create() => PackageRequest._();
  PackageRequest createEmptyInstance() => create();
  static $pb.PbList<PackageRequest> createRepeated() => $pb.PbList<PackageRequest>();
  @$core.pragma('dart2js:noInline')
  static PackageRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PackageRequest>(create);
  static PackageRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get serviceAreaID => $_getSZ(1);
  @$pb.TagNumber(2)
  set serviceAreaID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasServiceAreaID() => $_has(1);
  @$pb.TagNumber(2)
  void clearServiceAreaID() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get limit => $_getI64(2);
  @$pb.TagNumber(3)
  set limit($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasLimit() => $_has(2);
  @$pb.TagNumber(3)
  void clearLimit() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get page => $_getI64(3);
  @$pb.TagNumber(4)
  set page($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPage() => $_has(3);
  @$pb.TagNumber(4)
  void clearPage() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get packageCatgoryID => $_getSZ(4);
  @$pb.TagNumber(5)
  set packageCatgoryID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPackageCatgoryID() => $_has(4);
  @$pb.TagNumber(5)
  void clearPackageCatgoryID() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get userID => $_getSZ(5);
  @$pb.TagNumber(6)
  set userID($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasUserID() => $_has(5);
  @$pb.TagNumber(6)
  void clearUserID() => clearField(6);
}

class Nutrients extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Nutrients', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.OD)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..hasRequiredFields = false
  ;

  Nutrients._() : super();
  factory Nutrients({
    $core.double? quantity,
    $core.String? name,
  }) {
    final _result = create();
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (name != null) {
      _result.name = name;
    }
    return _result;
  }
  factory Nutrients.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Nutrients.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Nutrients clone() => Nutrients()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Nutrients copyWith(void Function(Nutrients) updates) => super.copyWith((message) => updates(message as Nutrients)) as Nutrients; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Nutrients create() => Nutrients._();
  Nutrients createEmptyInstance() => create();
  static $pb.PbList<Nutrients> createRepeated() => $pb.PbList<Nutrients>();
  @$core.pragma('dart2js:noInline')
  static Nutrients getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Nutrients>(create);
  static Nutrients? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get quantity => $_getN(0);
  @$pb.TagNumber(1)
  set quantity($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuantity() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuantity() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);
}

class Location extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Location', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'locationID', protoName: 'locationID')
    ..hasRequiredFields = false
  ;

  Location._() : super();
  factory Location({
    $core.String? locationID,
  }) {
    final _result = create();
    if (locationID != null) {
      _result.locationID = locationID;
    }
    return _result;
  }
  factory Location.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Location.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Location clone() => Location()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Location copyWith(void Function(Location) updates) => super.copyWith((message) => updates(message as Location)) as Location; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Location create() => Location._();
  Location createEmptyInstance() => create();
  static $pb.PbList<Location> createRepeated() => $pb.PbList<Location>();
  @$core.pragma('dart2js:noInline')
  static Location getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Location>(create);
  static Location? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get locationID => $_getSZ(0);
  @$pb.TagNumber(1)
  set locationID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLocationID() => $_has(0);
  @$pb.TagNumber(1)
  void clearLocationID() => clearField(1);
}

class ListPackageResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListPackageResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<Package>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: Package.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total')
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pageCount', protoName: 'pageCount')
    ..hasRequiredFields = false
  ;

  ListPackageResponse._() : super();
  factory ListPackageResponse({
    $core.Iterable<Package>? data,
    $core.String? status,
    $core.String? message,
    $fixnum.Int64? total,
    $fixnum.Int64? pageCount,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (total != null) {
      _result.total = total;
    }
    if (pageCount != null) {
      _result.pageCount = pageCount;
    }
    return _result;
  }
  factory ListPackageResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListPackageResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListPackageResponse clone() => ListPackageResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListPackageResponse copyWith(void Function(ListPackageResponse) updates) => super.copyWith((message) => updates(message as ListPackageResponse)) as ListPackageResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListPackageResponse create() => ListPackageResponse._();
  ListPackageResponse createEmptyInstance() => create();
  static $pb.PbList<ListPackageResponse> createRepeated() => $pb.PbList<ListPackageResponse>();
  @$core.pragma('dart2js:noInline')
  static ListPackageResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListPackageResponse>(create);
  static ListPackageResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Package> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get message => $_getSZ(2);
  @$pb.TagNumber(3)
  set message($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMessage() => $_has(2);
  @$pb.TagNumber(3)
  void clearMessage() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get total => $_getI64(3);
  @$pb.TagNumber(4)
  set total($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTotal() => $_has(3);
  @$pb.TagNumber(4)
  void clearTotal() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get pageCount => $_getI64(4);
  @$pb.TagNumber(5)
  set pageCount($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPageCount() => $_has(4);
  @$pb.TagNumber(5)
  void clearPageCount() => clearField(5);
}

class LikeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LikeRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userID', protoName: 'userID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..hasRequiredFields = false
  ;

  LikeRequest._() : super();
  factory LikeRequest({
    $core.String? userID,
    $core.String? packageID,
  }) {
    final _result = create();
    if (userID != null) {
      _result.userID = userID;
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    return _result;
  }
  factory LikeRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LikeRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LikeRequest clone() => LikeRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LikeRequest copyWith(void Function(LikeRequest) updates) => super.copyWith((message) => updates(message as LikeRequest)) as LikeRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LikeRequest create() => LikeRequest._();
  LikeRequest createEmptyInstance() => create();
  static $pb.PbList<LikeRequest> createRepeated() => $pb.PbList<LikeRequest>();
  @$core.pragma('dart2js:noInline')
  static LikeRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LikeRequest>(create);
  static LikeRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get userID => $_getSZ(0);
  @$pb.TagNumber(1)
  set userID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserID() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get packageID => $_getSZ(1);
  @$pb.TagNumber(2)
  set packageID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackageID() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackageID() => clearField(2);
}

class Nutrient extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Nutrient', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nutrientName', protoName: 'nutrientName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nutrientID', protoName: 'nutrientID')
    ..hasRequiredFields = false
  ;

  Nutrient._() : super();
  factory Nutrient({
    $core.String? nutrientName,
    $core.String? itemID,
    $core.String? nutrientID,
  }) {
    final _result = create();
    if (nutrientName != null) {
      _result.nutrientName = nutrientName;
    }
    if (itemID != null) {
      _result.itemID = itemID;
    }
    if (nutrientID != null) {
      _result.nutrientID = nutrientID;
    }
    return _result;
  }
  factory Nutrient.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Nutrient.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Nutrient clone() => Nutrient()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Nutrient copyWith(void Function(Nutrient) updates) => super.copyWith((message) => updates(message as Nutrient)) as Nutrient; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Nutrient create() => Nutrient._();
  Nutrient createEmptyInstance() => create();
  static $pb.PbList<Nutrient> createRepeated() => $pb.PbList<Nutrient>();
  @$core.pragma('dart2js:noInline')
  static Nutrient getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Nutrient>(create);
  static Nutrient? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get nutrientName => $_getSZ(0);
  @$pb.TagNumber(1)
  set nutrientName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNutrientName() => $_has(0);
  @$pb.TagNumber(1)
  void clearNutrientName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get itemID => $_getSZ(1);
  @$pb.TagNumber(2)
  set itemID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemID() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemID() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get nutrientID => $_getSZ(2);
  @$pb.TagNumber(3)
  set nutrientID($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasNutrientID() => $_has(2);
  @$pb.TagNumber(3)
  void clearNutrientID() => clearField(3);
}

class NutrientItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NutrientItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nutrientName', protoName: 'nutrientName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nutrientID', protoName: 'nutrientID')
    ..hasRequiredFields = false
  ;

  NutrientItem._() : super();
  factory NutrientItem({
    $core.String? nutrientName,
    $core.String? nutrientID,
  }) {
    final _result = create();
    if (nutrientName != null) {
      _result.nutrientName = nutrientName;
    }
    if (nutrientID != null) {
      _result.nutrientID = nutrientID;
    }
    return _result;
  }
  factory NutrientItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NutrientItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NutrientItem clone() => NutrientItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NutrientItem copyWith(void Function(NutrientItem) updates) => super.copyWith((message) => updates(message as NutrientItem)) as NutrientItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NutrientItem create() => NutrientItem._();
  NutrientItem createEmptyInstance() => create();
  static $pb.PbList<NutrientItem> createRepeated() => $pb.PbList<NutrientItem>();
  @$core.pragma('dart2js:noInline')
  static NutrientItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NutrientItem>(create);
  static NutrientItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get nutrientName => $_getSZ(0);
  @$pb.TagNumber(1)
  set nutrientName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNutrientName() => $_has(0);
  @$pb.TagNumber(1)
  void clearNutrientName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get nutrientID => $_getSZ(1);
  @$pb.TagNumber(2)
  set nutrientID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNutrientID() => $_has(1);
  @$pb.TagNumber(2)
  void clearNutrientID() => clearField(2);
}

class NutrientItemRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NutrientItemRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nutrientID', protoName: 'nutrientID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..hasRequiredFields = false
  ;

  NutrientItemRequest._() : super();
  factory NutrientItemRequest({
    $core.String? nutrientID,
    $core.String? itemID,
  }) {
    final _result = create();
    if (nutrientID != null) {
      _result.nutrientID = nutrientID;
    }
    if (itemID != null) {
      _result.itemID = itemID;
    }
    return _result;
  }
  factory NutrientItemRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NutrientItemRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NutrientItemRequest clone() => NutrientItemRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NutrientItemRequest copyWith(void Function(NutrientItemRequest) updates) => super.copyWith((message) => updates(message as NutrientItemRequest)) as NutrientItemRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NutrientItemRequest create() => NutrientItemRequest._();
  NutrientItemRequest createEmptyInstance() => create();
  static $pb.PbList<NutrientItemRequest> createRepeated() => $pb.PbList<NutrientItemRequest>();
  @$core.pragma('dart2js:noInline')
  static NutrientItemRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NutrientItemRequest>(create);
  static NutrientItemRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get nutrientID => $_getSZ(0);
  @$pb.TagNumber(1)
  set nutrientID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNutrientID() => $_has(0);
  @$pb.TagNumber(1)
  void clearNutrientID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get itemID => $_getSZ(1);
  @$pb.TagNumber(2)
  set itemID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemID() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemID() => clearField(2);
}

class NutrientList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NutrientList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<NutrientItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: NutrientItem.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  NutrientList._() : super();
  factory NutrientList({
    $core.Iterable<NutrientItem>? data,
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory NutrientList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NutrientList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NutrientList clone() => NutrientList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NutrientList copyWith(void Function(NutrientList) updates) => super.copyWith((message) => updates(message as NutrientList)) as NutrientList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NutrientList create() => NutrientList._();
  NutrientList createEmptyInstance() => create();
  static $pb.PbList<NutrientList> createRepeated() => $pb.PbList<NutrientList>();
  @$core.pragma('dart2js:noInline')
  static NutrientList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NutrientList>(create);
  static NutrientList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<NutrientItem> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(2);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class FeedItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FeedItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'icon')
    ..pc<Package>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packages', $pb.PbFieldType.PM, subBuilder: Package.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceArea', protoName: 'serviceArea')
    ..hasRequiredFields = false
  ;

  FeedItem._() : super();
  factory FeedItem({
    $core.String? name,
    $core.String? icon,
    $core.Iterable<Package>? packages,
    $core.String? serviceArea,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (icon != null) {
      _result.icon = icon;
    }
    if (packages != null) {
      _result.packages.addAll(packages);
    }
    if (serviceArea != null) {
      _result.serviceArea = serviceArea;
    }
    return _result;
  }
  factory FeedItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FeedItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FeedItem clone() => FeedItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FeedItem copyWith(void Function(FeedItem) updates) => super.copyWith((message) => updates(message as FeedItem)) as FeedItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FeedItem create() => FeedItem._();
  FeedItem createEmptyInstance() => create();
  static $pb.PbList<FeedItem> createRepeated() => $pb.PbList<FeedItem>();
  @$core.pragma('dart2js:noInline')
  static FeedItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FeedItem>(create);
  static FeedItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get icon => $_getSZ(1);
  @$pb.TagNumber(2)
  set icon($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasIcon() => $_has(1);
  @$pb.TagNumber(2)
  void clearIcon() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<Package> get packages => $_getList(2);

  @$pb.TagNumber(4)
  $core.String get serviceArea => $_getSZ(3);
  @$pb.TagNumber(4)
  set serviceArea($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasServiceArea() => $_has(3);
  @$pb.TagNumber(4)
  void clearServiceArea() => clearField(4);
}

class FeedList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FeedList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..pc<FeedItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'feeds', $pb.PbFieldType.PM, subBuilder: FeedItem.create)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'total')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pageCount', protoName: 'pageCount')
    ..hasRequiredFields = false
  ;

  FeedList._() : super();
  factory FeedList({
    $core.Iterable<FeedItem>? feeds,
    $fixnum.Int64? total,
    $fixnum.Int64? pageCount,
  }) {
    final _result = create();
    if (feeds != null) {
      _result.feeds.addAll(feeds);
    }
    if (total != null) {
      _result.total = total;
    }
    if (pageCount != null) {
      _result.pageCount = pageCount;
    }
    return _result;
  }
  factory FeedList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FeedList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FeedList clone() => FeedList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FeedList copyWith(void Function(FeedList) updates) => super.copyWith((message) => updates(message as FeedList)) as FeedList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FeedList create() => FeedList._();
  FeedList createEmptyInstance() => create();
  static $pb.PbList<FeedList> createRepeated() => $pb.PbList<FeedList>();
  @$core.pragma('dart2js:noInline')
  static FeedList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FeedList>(create);
  static FeedList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<FeedItem> get feeds => $_getList(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get total => $_getI64(1);
  @$pb.TagNumber(2)
  set total($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTotal() => $_has(1);
  @$pb.TagNumber(2)
  void clearTotal() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get pageCount => $_getI64(2);
  @$pb.TagNumber(3)
  set pageCount($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPageCount() => $_has(2);
  @$pb.TagNumber(3)
  void clearPageCount() => clearField(3);
}

class MenuSearchItemSuggestionsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchItemSuggestionsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  MenuSearchItemSuggestionsRequest._() : super();
  factory MenuSearchItemSuggestionsRequest({
    $core.String? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory MenuSearchItemSuggestionsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchItemSuggestionsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchItemSuggestionsRequest clone() => MenuSearchItemSuggestionsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchItemSuggestionsRequest copyWith(void Function(MenuSearchItemSuggestionsRequest) updates) => super.copyWith((message) => updates(message as MenuSearchItemSuggestionsRequest)) as MenuSearchItemSuggestionsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchItemSuggestionsRequest create() => MenuSearchItemSuggestionsRequest._();
  MenuSearchItemSuggestionsRequest createEmptyInstance() => create();
  static $pb.PbList<MenuSearchItemSuggestionsRequest> createRepeated() => $pb.PbList<MenuSearchItemSuggestionsRequest>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchItemSuggestionsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchItemSuggestionsRequest>(create);
  static MenuSearchItemSuggestionsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class MenuSearchItemSuggestionsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchItemSuggestionsResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'suggestions')
    ..hasRequiredFields = false
  ;

  MenuSearchItemSuggestionsResponse._() : super();
  factory MenuSearchItemSuggestionsResponse({
    $core.String? status,
    $core.Iterable<$core.String>? suggestions,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (suggestions != null) {
      _result.suggestions.addAll(suggestions);
    }
    return _result;
  }
  factory MenuSearchItemSuggestionsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchItemSuggestionsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchItemSuggestionsResponse clone() => MenuSearchItemSuggestionsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchItemSuggestionsResponse copyWith(void Function(MenuSearchItemSuggestionsResponse) updates) => super.copyWith((message) => updates(message as MenuSearchItemSuggestionsResponse)) as MenuSearchItemSuggestionsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchItemSuggestionsResponse create() => MenuSearchItemSuggestionsResponse._();
  MenuSearchItemSuggestionsResponse createEmptyInstance() => create();
  static $pb.PbList<MenuSearchItemSuggestionsResponse> createRepeated() => $pb.PbList<MenuSearchItemSuggestionsResponse>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchItemSuggestionsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchItemSuggestionsResponse>(create);
  static MenuSearchItemSuggestionsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get suggestions => $_getList(1);
}

class MenuSearchPackageSuggestionsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchPackageSuggestionsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  MenuSearchPackageSuggestionsRequest._() : super();
  factory MenuSearchPackageSuggestionsRequest({
    $core.String? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory MenuSearchPackageSuggestionsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchPackageSuggestionsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchPackageSuggestionsRequest clone() => MenuSearchPackageSuggestionsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchPackageSuggestionsRequest copyWith(void Function(MenuSearchPackageSuggestionsRequest) updates) => super.copyWith((message) => updates(message as MenuSearchPackageSuggestionsRequest)) as MenuSearchPackageSuggestionsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchPackageSuggestionsRequest create() => MenuSearchPackageSuggestionsRequest._();
  MenuSearchPackageSuggestionsRequest createEmptyInstance() => create();
  static $pb.PbList<MenuSearchPackageSuggestionsRequest> createRepeated() => $pb.PbList<MenuSearchPackageSuggestionsRequest>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchPackageSuggestionsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchPackageSuggestionsRequest>(create);
  static MenuSearchPackageSuggestionsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class MenuSearchPackageSuggestionsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchPackageSuggestionsResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'suggestions')
    ..hasRequiredFields = false
  ;

  MenuSearchPackageSuggestionsResponse._() : super();
  factory MenuSearchPackageSuggestionsResponse({
    $core.String? status,
    $core.Iterable<$core.String>? suggestions,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (suggestions != null) {
      _result.suggestions.addAll(suggestions);
    }
    return _result;
  }
  factory MenuSearchPackageSuggestionsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchPackageSuggestionsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchPackageSuggestionsResponse clone() => MenuSearchPackageSuggestionsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchPackageSuggestionsResponse copyWith(void Function(MenuSearchPackageSuggestionsResponse) updates) => super.copyWith((message) => updates(message as MenuSearchPackageSuggestionsResponse)) as MenuSearchPackageSuggestionsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchPackageSuggestionsResponse create() => MenuSearchPackageSuggestionsResponse._();
  MenuSearchPackageSuggestionsResponse createEmptyInstance() => create();
  static $pb.PbList<MenuSearchPackageSuggestionsResponse> createRepeated() => $pb.PbList<MenuSearchPackageSuggestionsResponse>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchPackageSuggestionsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchPackageSuggestionsResponse>(create);
  static MenuSearchPackageSuggestionsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get suggestions => $_getList(1);
}

class MenuSearchItemRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchItemRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  MenuSearchItemRequest._() : super();
  factory MenuSearchItemRequest({
    $core.String? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory MenuSearchItemRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchItemRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchItemRequest clone() => MenuSearchItemRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchItemRequest copyWith(void Function(MenuSearchItemRequest) updates) => super.copyWith((message) => updates(message as MenuSearchItemRequest)) as MenuSearchItemRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchItemRequest create() => MenuSearchItemRequest._();
  MenuSearchItemRequest createEmptyInstance() => create();
  static $pb.PbList<MenuSearchItemRequest> createRepeated() => $pb.PbList<MenuSearchItemRequest>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchItemRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchItemRequest>(create);
  static MenuSearchItemRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class MenuSearchItemResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchItemResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pc<Item>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemResponse', $pb.PbFieldType.PM, subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  MenuSearchItemResponse._() : super();
  factory MenuSearchItemResponse({
    $core.String? status,
    $core.Iterable<Item>? itemResponse,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (itemResponse != null) {
      _result.itemResponse.addAll(itemResponse);
    }
    return _result;
  }
  factory MenuSearchItemResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchItemResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchItemResponse clone() => MenuSearchItemResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchItemResponse copyWith(void Function(MenuSearchItemResponse) updates) => super.copyWith((message) => updates(message as MenuSearchItemResponse)) as MenuSearchItemResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchItemResponse create() => MenuSearchItemResponse._();
  MenuSearchItemResponse createEmptyInstance() => create();
  static $pb.PbList<MenuSearchItemResponse> createRepeated() => $pb.PbList<MenuSearchItemResponse>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchItemResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchItemResponse>(create);
  static MenuSearchItemResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Item> get itemResponse => $_getList(1);
}

class MenuSearchPackageRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchPackageRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  MenuSearchPackageRequest._() : super();
  factory MenuSearchPackageRequest({
    $core.String? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory MenuSearchPackageRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchPackageRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchPackageRequest clone() => MenuSearchPackageRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchPackageRequest copyWith(void Function(MenuSearchPackageRequest) updates) => super.copyWith((message) => updates(message as MenuSearchPackageRequest)) as MenuSearchPackageRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchPackageRequest create() => MenuSearchPackageRequest._();
  MenuSearchPackageRequest createEmptyInstance() => create();
  static $pb.PbList<MenuSearchPackageRequest> createRepeated() => $pb.PbList<MenuSearchPackageRequest>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchPackageRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchPackageRequest>(create);
  static MenuSearchPackageRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class MenuSearchPackageResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchPackageResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pc<Package>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageResponse', $pb.PbFieldType.PM, subBuilder: Package.create)
    ..hasRequiredFields = false
  ;

  MenuSearchPackageResponse._() : super();
  factory MenuSearchPackageResponse({
    $core.String? status,
    $core.Iterable<Package>? packageResponse,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (packageResponse != null) {
      _result.packageResponse.addAll(packageResponse);
    }
    return _result;
  }
  factory MenuSearchPackageResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchPackageResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchPackageResponse clone() => MenuSearchPackageResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchPackageResponse copyWith(void Function(MenuSearchPackageResponse) updates) => super.copyWith((message) => updates(message as MenuSearchPackageResponse)) as MenuSearchPackageResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchPackageResponse create() => MenuSearchPackageResponse._();
  MenuSearchPackageResponse createEmptyInstance() => create();
  static $pb.PbList<MenuSearchPackageResponse> createRepeated() => $pb.PbList<MenuSearchPackageResponse>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchPackageResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchPackageResponse>(create);
  static MenuSearchPackageResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Package> get packageResponse => $_getList(1);
}

class SearchRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SearchRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'index')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  SearchRequest._() : super();
  factory SearchRequest({
    $core.String? index,
    $core.String? value,
  }) {
    final _result = create();
    if (index != null) {
      _result.index = index;
    }
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory SearchRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SearchRequest clone() => SearchRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SearchRequest copyWith(void Function(SearchRequest) updates) => super.copyWith((message) => updates(message as SearchRequest)) as SearchRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchRequest create() => SearchRequest._();
  SearchRequest createEmptyInstance() => create();
  static $pb.PbList<SearchRequest> createRepeated() => $pb.PbList<SearchRequest>();
  @$core.pragma('dart2js:noInline')
  static SearchRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchRequest>(create);
  static SearchRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get index => $_getSZ(0);
  @$pb.TagNumber(1)
  set index($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIndex() => $_has(0);
  @$pb.TagNumber(1)
  void clearIndex() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get value => $_getSZ(1);
  @$pb.TagNumber(2)
  set value($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
}

class SearchResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SearchResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pc<Package>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageResponse', $pb.PbFieldType.PM, subBuilder: Package.create)
    ..pc<Item>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemResponse', $pb.PbFieldType.PM, subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  SearchResponse._() : super();
  factory SearchResponse({
    $core.String? status,
    $core.Iterable<Package>? packageResponse,
    $core.Iterable<Item>? itemResponse,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (packageResponse != null) {
      _result.packageResponse.addAll(packageResponse);
    }
    if (itemResponse != null) {
      _result.itemResponse.addAll(itemResponse);
    }
    return _result;
  }
  factory SearchResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SearchResponse clone() => SearchResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SearchResponse copyWith(void Function(SearchResponse) updates) => super.copyWith((message) => updates(message as SearchResponse)) as SearchResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchResponse create() => SearchResponse._();
  SearchResponse createEmptyInstance() => create();
  static $pb.PbList<SearchResponse> createRepeated() => $pb.PbList<SearchResponse>();
  @$core.pragma('dart2js:noInline')
  static SearchResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchResponse>(create);
  static SearchResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Package> get packageResponse => $_getList(1);

  @$pb.TagNumber(3)
  $core.List<Item> get itemResponse => $_getList(2);
}

class MenuSearchSuggestionsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchSuggestionsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'index')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value')
    ..hasRequiredFields = false
  ;

  MenuSearchSuggestionsRequest._() : super();
  factory MenuSearchSuggestionsRequest({
    $core.String? index,
    $core.String? value,
  }) {
    final _result = create();
    if (index != null) {
      _result.index = index;
    }
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory MenuSearchSuggestionsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchSuggestionsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchSuggestionsRequest clone() => MenuSearchSuggestionsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchSuggestionsRequest copyWith(void Function(MenuSearchSuggestionsRequest) updates) => super.copyWith((message) => updates(message as MenuSearchSuggestionsRequest)) as MenuSearchSuggestionsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchSuggestionsRequest create() => MenuSearchSuggestionsRequest._();
  MenuSearchSuggestionsRequest createEmptyInstance() => create();
  static $pb.PbList<MenuSearchSuggestionsRequest> createRepeated() => $pb.PbList<MenuSearchSuggestionsRequest>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchSuggestionsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchSuggestionsRequest>(create);
  static MenuSearchSuggestionsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get index => $_getSZ(0);
  @$pb.TagNumber(1)
  set index($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIndex() => $_has(0);
  @$pb.TagNumber(1)
  void clearIndex() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get value => $_getSZ(1);
  @$pb.TagNumber(2)
  set value($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
}

class MenuSearchSuggestionsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MenuSearchSuggestionsResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'menu_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'suggestions')
    ..hasRequiredFields = false
  ;

  MenuSearchSuggestionsResponse._() : super();
  factory MenuSearchSuggestionsResponse({
    $core.String? status,
    $core.Iterable<$core.String>? suggestions,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (suggestions != null) {
      _result.suggestions.addAll(suggestions);
    }
    return _result;
  }
  factory MenuSearchSuggestionsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MenuSearchSuggestionsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MenuSearchSuggestionsResponse clone() => MenuSearchSuggestionsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MenuSearchSuggestionsResponse copyWith(void Function(MenuSearchSuggestionsResponse) updates) => super.copyWith((message) => updates(message as MenuSearchSuggestionsResponse)) as MenuSearchSuggestionsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MenuSearchSuggestionsResponse create() => MenuSearchSuggestionsResponse._();
  MenuSearchSuggestionsResponse createEmptyInstance() => create();
  static $pb.PbList<MenuSearchSuggestionsResponse> createRepeated() => $pb.PbList<MenuSearchSuggestionsResponse>();
  @$core.pragma('dart2js:noInline')
  static MenuSearchSuggestionsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MenuSearchSuggestionsResponse>(create);
  static MenuSearchSuggestionsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get suggestions => $_getList(1);
}

