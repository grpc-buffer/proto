///
//  Generated code. Do not modify.
//  source: order.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class NewOrderRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewOrderRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reference')
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'subtotal', $pb.PbFieldType.OD)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryCost', $pb.PbFieldType.OD)
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'totalCost', $pb.PbFieldType.OD)
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryType', protoName: 'delivery_Type')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOM<InNewOrderDeliveryAddress>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryAddress', subBuilder: InNewOrderDeliveryAddress.create)
    ..pc<InNewOrderItems>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderItems', $pb.PbFieldType.PM, subBuilder: InNewOrderItems.create)
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'paymentMethod', protoName: 'paymentMethod')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..a<$core.double>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tip', $pb.PbFieldType.OD)
    ..aOS(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schedule')
    ..hasRequiredFields = false
  ;

  NewOrderRequest._() : super();
  factory NewOrderRequest({
    $core.String? orderId,
    $core.String? reference,
    $core.double? subtotal,
    $core.double? deliveryCost,
    $core.double? totalCost,
    $core.String? deliveryType,
    $core.String? userId,
    InNewOrderDeliveryAddress? deliveryAddress,
    $core.Iterable<InNewOrderItems>? orderItems,
    $core.String? paymentMethod,
    $core.String? currency,
    $core.double? tip,
    $core.String? schedule,
  }) {
    final _result = create();
    if (orderId != null) {
      _result.orderId = orderId;
    }
    if (reference != null) {
      _result.reference = reference;
    }
    if (subtotal != null) {
      _result.subtotal = subtotal;
    }
    if (deliveryCost != null) {
      _result.deliveryCost = deliveryCost;
    }
    if (totalCost != null) {
      _result.totalCost = totalCost;
    }
    if (deliveryType != null) {
      _result.deliveryType = deliveryType;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (deliveryAddress != null) {
      _result.deliveryAddress = deliveryAddress;
    }
    if (orderItems != null) {
      _result.orderItems.addAll(orderItems);
    }
    if (paymentMethod != null) {
      _result.paymentMethod = paymentMethod;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    if (tip != null) {
      _result.tip = tip;
    }
    if (schedule != null) {
      _result.schedule = schedule;
    }
    return _result;
  }
  factory NewOrderRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewOrderRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewOrderRequest clone() => NewOrderRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewOrderRequest copyWith(void Function(NewOrderRequest) updates) => super.copyWith((message) => updates(message as NewOrderRequest)) as NewOrderRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewOrderRequest create() => NewOrderRequest._();
  NewOrderRequest createEmptyInstance() => create();
  static $pb.PbList<NewOrderRequest> createRepeated() => $pb.PbList<NewOrderRequest>();
  @$core.pragma('dart2js:noInline')
  static NewOrderRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewOrderRequest>(create);
  static NewOrderRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get orderId => $_getSZ(0);
  @$pb.TagNumber(1)
  set orderId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get reference => $_getSZ(1);
  @$pb.TagNumber(2)
  set reference($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReference() => $_has(1);
  @$pb.TagNumber(2)
  void clearReference() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get subtotal => $_getN(2);
  @$pb.TagNumber(3)
  set subtotal($core.double v) { $_setDouble(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSubtotal() => $_has(2);
  @$pb.TagNumber(3)
  void clearSubtotal() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get deliveryCost => $_getN(3);
  @$pb.TagNumber(4)
  set deliveryCost($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDeliveryCost() => $_has(3);
  @$pb.TagNumber(4)
  void clearDeliveryCost() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get totalCost => $_getN(4);
  @$pb.TagNumber(5)
  set totalCost($core.double v) { $_setDouble(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTotalCost() => $_has(4);
  @$pb.TagNumber(5)
  void clearTotalCost() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get deliveryType => $_getSZ(5);
  @$pb.TagNumber(6)
  set deliveryType($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDeliveryType() => $_has(5);
  @$pb.TagNumber(6)
  void clearDeliveryType() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get userId => $_getSZ(6);
  @$pb.TagNumber(7)
  set userId($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasUserId() => $_has(6);
  @$pb.TagNumber(7)
  void clearUserId() => clearField(7);

  @$pb.TagNumber(8)
  InNewOrderDeliveryAddress get deliveryAddress => $_getN(7);
  @$pb.TagNumber(8)
  set deliveryAddress(InNewOrderDeliveryAddress v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasDeliveryAddress() => $_has(7);
  @$pb.TagNumber(8)
  void clearDeliveryAddress() => clearField(8);
  @$pb.TagNumber(8)
  InNewOrderDeliveryAddress ensureDeliveryAddress() => $_ensure(7);

  @$pb.TagNumber(9)
  $core.List<InNewOrderItems> get orderItems => $_getList(8);

  @$pb.TagNumber(10)
  $core.String get paymentMethod => $_getSZ(9);
  @$pb.TagNumber(10)
  set paymentMethod($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasPaymentMethod() => $_has(9);
  @$pb.TagNumber(10)
  void clearPaymentMethod() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get currency => $_getSZ(10);
  @$pb.TagNumber(11)
  set currency($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasCurrency() => $_has(10);
  @$pb.TagNumber(11)
  void clearCurrency() => clearField(11);

  @$pb.TagNumber(12)
  $core.double get tip => $_getN(11);
  @$pb.TagNumber(12)
  set tip($core.double v) { $_setDouble(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasTip() => $_has(11);
  @$pb.TagNumber(12)
  void clearTip() => clearField(12);

  @$pb.TagNumber(13)
  $core.String get schedule => $_getSZ(12);
  @$pb.TagNumber(13)
  set schedule($core.String v) { $_setString(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasSchedule() => $_has(12);
  @$pb.TagNumber(13)
  void clearSchedule() => clearField(13);
}

class NewOrderResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewOrderResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  NewOrderResponse._() : super();
  factory NewOrderResponse({
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory NewOrderResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewOrderResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewOrderResponse clone() => NewOrderResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewOrderResponse copyWith(void Function(NewOrderResponse) updates) => super.copyWith((message) => updates(message as NewOrderResponse)) as NewOrderResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewOrderResponse create() => NewOrderResponse._();
  NewOrderResponse createEmptyInstance() => create();
  static $pb.PbList<NewOrderResponse> createRepeated() => $pb.PbList<NewOrderResponse>();
  @$core.pragma('dart2js:noInline')
  static NewOrderResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewOrderResponse>(create);
  static NewOrderResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class InNewOrderDeliveryAddress extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InNewOrderDeliveryAddress', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..aOM<InNewOrderCoordinates>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'coordinates', subBuilder: InNewOrderCoordinates.create)
    ..hasRequiredFields = false
  ;

  InNewOrderDeliveryAddress._() : super();
  factory InNewOrderDeliveryAddress({
    $core.String? address,
    InNewOrderCoordinates? coordinates,
  }) {
    final _result = create();
    if (address != null) {
      _result.address = address;
    }
    if (coordinates != null) {
      _result.coordinates = coordinates;
    }
    return _result;
  }
  factory InNewOrderDeliveryAddress.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InNewOrderDeliveryAddress.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InNewOrderDeliveryAddress clone() => InNewOrderDeliveryAddress()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InNewOrderDeliveryAddress copyWith(void Function(InNewOrderDeliveryAddress) updates) => super.copyWith((message) => updates(message as InNewOrderDeliveryAddress)) as InNewOrderDeliveryAddress; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InNewOrderDeliveryAddress create() => InNewOrderDeliveryAddress._();
  InNewOrderDeliveryAddress createEmptyInstance() => create();
  static $pb.PbList<InNewOrderDeliveryAddress> createRepeated() => $pb.PbList<InNewOrderDeliveryAddress>();
  @$core.pragma('dart2js:noInline')
  static InNewOrderDeliveryAddress getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InNewOrderDeliveryAddress>(create);
  static InNewOrderDeliveryAddress? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get address => $_getSZ(0);
  @$pb.TagNumber(1)
  set address($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAddress() => $_has(0);
  @$pb.TagNumber(1)
  void clearAddress() => clearField(1);

  @$pb.TagNumber(2)
  InNewOrderCoordinates get coordinates => $_getN(1);
  @$pb.TagNumber(2)
  set coordinates(InNewOrderCoordinates v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasCoordinates() => $_has(1);
  @$pb.TagNumber(2)
  void clearCoordinates() => clearField(2);
  @$pb.TagNumber(2)
  InNewOrderCoordinates ensureCoordinates() => $_ensure(1);
}

class InNewOrderCoordinates extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InNewOrderCoordinates', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'latitude', $pb.PbFieldType.OD)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'longitude', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  InNewOrderCoordinates._() : super();
  factory InNewOrderCoordinates({
    $core.double? latitude,
    $core.double? longitude,
  }) {
    final _result = create();
    if (latitude != null) {
      _result.latitude = latitude;
    }
    if (longitude != null) {
      _result.longitude = longitude;
    }
    return _result;
  }
  factory InNewOrderCoordinates.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InNewOrderCoordinates.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InNewOrderCoordinates clone() => InNewOrderCoordinates()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InNewOrderCoordinates copyWith(void Function(InNewOrderCoordinates) updates) => super.copyWith((message) => updates(message as InNewOrderCoordinates)) as InNewOrderCoordinates; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InNewOrderCoordinates create() => InNewOrderCoordinates._();
  InNewOrderCoordinates createEmptyInstance() => create();
  static $pb.PbList<InNewOrderCoordinates> createRepeated() => $pb.PbList<InNewOrderCoordinates>();
  @$core.pragma('dart2js:noInline')
  static InNewOrderCoordinates getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InNewOrderCoordinates>(create);
  static InNewOrderCoordinates? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get latitude => $_getN(0);
  @$pb.TagNumber(1)
  set latitude($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLatitude() => $_has(0);
  @$pb.TagNumber(1)
  void clearLatitude() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get longitude => $_getN(1);
  @$pb.TagNumber(2)
  set longitude($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLongitude() => $_has(1);
  @$pb.TagNumber(2)
  void clearLongitude() => clearField(2);
}

class InNewOrderItems extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InNewOrderItems', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageName', protoName: 'packageName')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'basePrice', $pb.PbFieldType.OD, protoName: 'basePrice')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..a<$core.int>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.O3)
    ..pc<InNewOrderPackageItem>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: InNewOrderPackageItem.create)
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'cartID', protoName: 'cartID')
    ..hasRequiredFields = false
  ;

  InNewOrderItems._() : super();
  factory InNewOrderItems({
    $core.String? packageID,
    $core.String? packageName,
    $core.String? description,
    $core.double? basePrice,
    $core.String? serviceAreaID,
    $core.String? image,
    $core.int? quantity,
    $core.Iterable<InNewOrderPackageItem>? items,
    $core.String? cartID,
  }) {
    final _result = create();
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (packageName != null) {
      _result.packageName = packageName;
    }
    if (description != null) {
      _result.description = description;
    }
    if (basePrice != null) {
      _result.basePrice = basePrice;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    if (image != null) {
      _result.image = image;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (items != null) {
      _result.items.addAll(items);
    }
    if (cartID != null) {
      _result.cartID = cartID;
    }
    return _result;
  }
  factory InNewOrderItems.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InNewOrderItems.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InNewOrderItems clone() => InNewOrderItems()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InNewOrderItems copyWith(void Function(InNewOrderItems) updates) => super.copyWith((message) => updates(message as InNewOrderItems)) as InNewOrderItems; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InNewOrderItems create() => InNewOrderItems._();
  InNewOrderItems createEmptyInstance() => create();
  static $pb.PbList<InNewOrderItems> createRepeated() => $pb.PbList<InNewOrderItems>();
  @$core.pragma('dart2js:noInline')
  static InNewOrderItems getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InNewOrderItems>(create);
  static InNewOrderItems? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get packageName => $_getSZ(1);
  @$pb.TagNumber(2)
  set packageName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackageName() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackageName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get basePrice => $_getN(3);
  @$pb.TagNumber(4)
  set basePrice($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasBasePrice() => $_has(3);
  @$pb.TagNumber(4)
  void clearBasePrice() => clearField(4);

  @$pb.TagNumber(6)
  $core.String get serviceAreaID => $_getSZ(4);
  @$pb.TagNumber(6)
  set serviceAreaID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(6)
  $core.bool hasServiceAreaID() => $_has(4);
  @$pb.TagNumber(6)
  void clearServiceAreaID() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get image => $_getSZ(5);
  @$pb.TagNumber(7)
  set image($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(7)
  $core.bool hasImage() => $_has(5);
  @$pb.TagNumber(7)
  void clearImage() => clearField(7);

  @$pb.TagNumber(8)
  $core.int get quantity => $_getIZ(6);
  @$pb.TagNumber(8)
  set quantity($core.int v) { $_setSignedInt32(6, v); }
  @$pb.TagNumber(8)
  $core.bool hasQuantity() => $_has(6);
  @$pb.TagNumber(8)
  void clearQuantity() => clearField(8);

  @$pb.TagNumber(9)
  $core.List<InNewOrderPackageItem> get items => $_getList(7);

  @$pb.TagNumber(10)
  $core.String get cartID => $_getSZ(8);
  @$pb.TagNumber(10)
  set cartID($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(10)
  $core.bool hasCartID() => $_has(8);
  @$pb.TagNumber(10)
  void clearCartID() => clearField(10);
}

class InNewOrderPackageItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InNewOrderPackageItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemCategoryID', protoName: 'itemCategoryID')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'unit')
    ..hasRequiredFields = false
  ;

  InNewOrderPackageItem._() : super();
  factory InNewOrderPackageItem({
    $core.String? itemID,
    $core.String? name,
    $core.String? description,
    $core.String? itemCategoryID,
    $core.String? image,
    $core.String? unit,
  }) {
    final _result = create();
    if (itemID != null) {
      _result.itemID = itemID;
    }
    if (name != null) {
      _result.name = name;
    }
    if (description != null) {
      _result.description = description;
    }
    if (itemCategoryID != null) {
      _result.itemCategoryID = itemCategoryID;
    }
    if (image != null) {
      _result.image = image;
    }
    if (unit != null) {
      _result.unit = unit;
    }
    return _result;
  }
  factory InNewOrderPackageItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InNewOrderPackageItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InNewOrderPackageItem clone() => InNewOrderPackageItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InNewOrderPackageItem copyWith(void Function(InNewOrderPackageItem) updates) => super.copyWith((message) => updates(message as InNewOrderPackageItem)) as InNewOrderPackageItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InNewOrderPackageItem create() => InNewOrderPackageItem._();
  InNewOrderPackageItem createEmptyInstance() => create();
  static $pb.PbList<InNewOrderPackageItem> createRepeated() => $pb.PbList<InNewOrderPackageItem>();
  @$core.pragma('dart2js:noInline')
  static InNewOrderPackageItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InNewOrderPackageItem>(create);
  static InNewOrderPackageItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get itemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set itemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get itemCategoryID => $_getSZ(3);
  @$pb.TagNumber(4)
  set itemCategoryID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasItemCategoryID() => $_has(3);
  @$pb.TagNumber(4)
  void clearItemCategoryID() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get image => $_getSZ(4);
  @$pb.TagNumber(5)
  set image($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasImage() => $_has(4);
  @$pb.TagNumber(5)
  void clearImage() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get unit => $_getSZ(5);
  @$pb.TagNumber(6)
  set unit($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasUnit() => $_has(5);
  @$pb.TagNumber(6)
  void clearUnit() => clearField(6);
}

class NewGetUserOrderRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewGetUserOrderRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'limit', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offset', $pb.PbFieldType.O3)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..hasRequiredFields = false
  ;

  NewGetUserOrderRequest._() : super();
  factory NewGetUserOrderRequest({
    $core.int? limit,
    $core.int? offset,
    $core.String? userId,
  }) {
    final _result = create();
    if (limit != null) {
      _result.limit = limit;
    }
    if (offset != null) {
      _result.offset = offset;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory NewGetUserOrderRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewGetUserOrderRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewGetUserOrderRequest clone() => NewGetUserOrderRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewGetUserOrderRequest copyWith(void Function(NewGetUserOrderRequest) updates) => super.copyWith((message) => updates(message as NewGetUserOrderRequest)) as NewGetUserOrderRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewGetUserOrderRequest create() => NewGetUserOrderRequest._();
  NewGetUserOrderRequest createEmptyInstance() => create();
  static $pb.PbList<NewGetUserOrderRequest> createRepeated() => $pb.PbList<NewGetUserOrderRequest>();
  @$core.pragma('dart2js:noInline')
  static NewGetUserOrderRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewGetUserOrderRequest>(create);
  static NewGetUserOrderRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get limit => $_getIZ(0);
  @$pb.TagNumber(1)
  set limit($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLimit() => $_has(0);
  @$pb.TagNumber(1)
  void clearLimit() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get offset => $_getIZ(1);
  @$pb.TagNumber(2)
  set offset($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOffset() => $_has(1);
  @$pb.TagNumber(2)
  void clearOffset() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get userId => $_getSZ(2);
  @$pb.TagNumber(3)
  set userId($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasUserId() => $_has(2);
  @$pb.TagNumber(3)
  void clearUserId() => clearField(3);
}

class NewGetUserOrderResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewGetUserOrderResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..pc<OrderProperties>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: OrderProperties.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  NewGetUserOrderResponse._() : super();
  factory NewGetUserOrderResponse({
    $core.Iterable<OrderProperties>? data,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory NewGetUserOrderResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewGetUserOrderResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewGetUserOrderResponse clone() => NewGetUserOrderResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewGetUserOrderResponse copyWith(void Function(NewGetUserOrderResponse) updates) => super.copyWith((message) => updates(message as NewGetUserOrderResponse)) as NewGetUserOrderResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewGetUserOrderResponse create() => NewGetUserOrderResponse._();
  NewGetUserOrderResponse createEmptyInstance() => create();
  static $pb.PbList<NewGetUserOrderResponse> createRepeated() => $pb.PbList<NewGetUserOrderResponse>();
  @$core.pragma('dart2js:noInline')
  static NewGetUserOrderResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewGetUserOrderResponse>(create);
  static NewGetUserOrderResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<OrderProperties> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class NewListenToUserOrderResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewListenToUserOrderResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOM<OrderProperties>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: OrderProperties.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  NewListenToUserOrderResponse._() : super();
  factory NewListenToUserOrderResponse({
    OrderProperties? data,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data = data;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory NewListenToUserOrderResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewListenToUserOrderResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewListenToUserOrderResponse clone() => NewListenToUserOrderResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewListenToUserOrderResponse copyWith(void Function(NewListenToUserOrderResponse) updates) => super.copyWith((message) => updates(message as NewListenToUserOrderResponse)) as NewListenToUserOrderResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewListenToUserOrderResponse create() => NewListenToUserOrderResponse._();
  NewListenToUserOrderResponse createEmptyInstance() => create();
  static $pb.PbList<NewListenToUserOrderResponse> createRepeated() => $pb.PbList<NewListenToUserOrderResponse>();
  @$core.pragma('dart2js:noInline')
  static NewListenToUserOrderResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewListenToUserOrderResponse>(create);
  static NewListenToUserOrderResponse? _defaultInstance;

  @$pb.TagNumber(1)
  OrderProperties get data => $_getN(0);
  @$pb.TagNumber(1)
  set data(OrderProperties v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasData() => $_has(0);
  @$pb.TagNumber(1)
  void clearData() => clearField(1);
  @$pb.TagNumber(1)
  OrderProperties ensureData() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class DeleteUserOrdersRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeleteUserOrdersRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..hasRequiredFields = false
  ;

  DeleteUserOrdersRequest._() : super();
  factory DeleteUserOrdersRequest({
    $core.String? userId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory DeleteUserOrdersRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeleteUserOrdersRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeleteUserOrdersRequest clone() => DeleteUserOrdersRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeleteUserOrdersRequest copyWith(void Function(DeleteUserOrdersRequest) updates) => super.copyWith((message) => updates(message as DeleteUserOrdersRequest)) as DeleteUserOrdersRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeleteUserOrdersRequest create() => DeleteUserOrdersRequest._();
  DeleteUserOrdersRequest createEmptyInstance() => create();
  static $pb.PbList<DeleteUserOrdersRequest> createRepeated() => $pb.PbList<DeleteUserOrdersRequest>();
  @$core.pragma('dart2js:noInline')
  static DeleteUserOrdersRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeleteUserOrdersRequest>(create);
  static DeleteUserOrdersRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get userId => $_getSZ(0);
  @$pb.TagNumber(1)
  set userId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);
}

class DeleteUserOrdersResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeleteUserOrdersResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  DeleteUserOrdersResponse._() : super();
  factory DeleteUserOrdersResponse({
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory DeleteUserOrdersResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeleteUserOrdersResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeleteUserOrdersResponse clone() => DeleteUserOrdersResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeleteUserOrdersResponse copyWith(void Function(DeleteUserOrdersResponse) updates) => super.copyWith((message) => updates(message as DeleteUserOrdersResponse)) as DeleteUserOrdersResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeleteUserOrdersResponse create() => DeleteUserOrdersResponse._();
  DeleteUserOrdersResponse createEmptyInstance() => create();
  static $pb.PbList<DeleteUserOrdersResponse> createRepeated() => $pb.PbList<DeleteUserOrdersResponse>();
  @$core.pragma('dart2js:noInline')
  static DeleteUserOrdersResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeleteUserOrdersResponse>(create);
  static DeleteUserOrdersResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class UpdateOrderStatusRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateOrderStatusRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  UpdateOrderStatusRequest._() : super();
  factory UpdateOrderStatusRequest({
    $core.String? orderId,
    $core.String? status,
  }) {
    final _result = create();
    if (orderId != null) {
      _result.orderId = orderId;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory UpdateOrderStatusRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateOrderStatusRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateOrderStatusRequest clone() => UpdateOrderStatusRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateOrderStatusRequest copyWith(void Function(UpdateOrderStatusRequest) updates) => super.copyWith((message) => updates(message as UpdateOrderStatusRequest)) as UpdateOrderStatusRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateOrderStatusRequest create() => UpdateOrderStatusRequest._();
  UpdateOrderStatusRequest createEmptyInstance() => create();
  static $pb.PbList<UpdateOrderStatusRequest> createRepeated() => $pb.PbList<UpdateOrderStatusRequest>();
  @$core.pragma('dart2js:noInline')
  static UpdateOrderStatusRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateOrderStatusRequest>(create);
  static UpdateOrderStatusRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get orderId => $_getSZ(0);
  @$pb.TagNumber(1)
  set orderId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class UpdateOrderStatusResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateOrderStatusResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  UpdateOrderStatusResponse._() : super();
  factory UpdateOrderStatusResponse({
    $core.String? message,
    $core.String? status,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory UpdateOrderStatusResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateOrderStatusResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateOrderStatusResponse clone() => UpdateOrderStatusResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateOrderStatusResponse copyWith(void Function(UpdateOrderStatusResponse) updates) => super.copyWith((message) => updates(message as UpdateOrderStatusResponse)) as UpdateOrderStatusResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateOrderStatusResponse create() => UpdateOrderStatusResponse._();
  UpdateOrderStatusResponse createEmptyInstance() => create();
  static $pb.PbList<UpdateOrderStatusResponse> createRepeated() => $pb.PbList<UpdateOrderStatusResponse>();
  @$core.pragma('dart2js:noInline')
  static UpdateOrderStatusResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateOrderStatusResponse>(create);
  static UpdateOrderStatusResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class GetOrderIdRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetOrderIdRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderId')
    ..hasRequiredFields = false
  ;

  GetOrderIdRequest._() : super();
  factory GetOrderIdRequest({
    $core.String? orderId,
  }) {
    final _result = create();
    if (orderId != null) {
      _result.orderId = orderId;
    }
    return _result;
  }
  factory GetOrderIdRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetOrderIdRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetOrderIdRequest clone() => GetOrderIdRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetOrderIdRequest copyWith(void Function(GetOrderIdRequest) updates) => super.copyWith((message) => updates(message as GetOrderIdRequest)) as GetOrderIdRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetOrderIdRequest create() => GetOrderIdRequest._();
  GetOrderIdRequest createEmptyInstance() => create();
  static $pb.PbList<GetOrderIdRequest> createRepeated() => $pb.PbList<GetOrderIdRequest>();
  @$core.pragma('dart2js:noInline')
  static GetOrderIdRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetOrderIdRequest>(create);
  static GetOrderIdRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get orderId => $_getSZ(0);
  @$pb.TagNumber(1)
  set orderId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderId() => clearField(1);
}

class GetOrderIdResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetOrderIdResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOM<OrderProperties>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: OrderProperties.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  GetOrderIdResponse._() : super();
  factory GetOrderIdResponse({
    OrderProperties? data,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data = data;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory GetOrderIdResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetOrderIdResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetOrderIdResponse clone() => GetOrderIdResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetOrderIdResponse copyWith(void Function(GetOrderIdResponse) updates) => super.copyWith((message) => updates(message as GetOrderIdResponse)) as GetOrderIdResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetOrderIdResponse create() => GetOrderIdResponse._();
  GetOrderIdResponse createEmptyInstance() => create();
  static $pb.PbList<GetOrderIdResponse> createRepeated() => $pb.PbList<GetOrderIdResponse>();
  @$core.pragma('dart2js:noInline')
  static GetOrderIdResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetOrderIdResponse>(create);
  static GetOrderIdResponse? _defaultInstance;

  @$pb.TagNumber(1)
  OrderProperties get data => $_getN(0);
  @$pb.TagNumber(1)
  set data(OrderProperties v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasData() => $_has(0);
  @$pb.TagNumber(1)
  void clearData() => clearField(1);
  @$pb.TagNumber(1)
  OrderProperties ensureData() => $_ensure(0);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class GetStatusRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetStatusRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'limit', $pb.PbFieldType.O3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offset', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  GetStatusRequest._() : super();
  factory GetStatusRequest({
    $core.String? userId,
    $core.String? status,
    $core.int? limit,
    $core.int? offset,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (status != null) {
      _result.status = status;
    }
    if (limit != null) {
      _result.limit = limit;
    }
    if (offset != null) {
      _result.offset = offset;
    }
    return _result;
  }
  factory GetStatusRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetStatusRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetStatusRequest clone() => GetStatusRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetStatusRequest copyWith(void Function(GetStatusRequest) updates) => super.copyWith((message) => updates(message as GetStatusRequest)) as GetStatusRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetStatusRequest create() => GetStatusRequest._();
  GetStatusRequest createEmptyInstance() => create();
  static $pb.PbList<GetStatusRequest> createRepeated() => $pb.PbList<GetStatusRequest>();
  @$core.pragma('dart2js:noInline')
  static GetStatusRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetStatusRequest>(create);
  static GetStatusRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get userId => $_getSZ(0);
  @$pb.TagNumber(1)
  set userId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get limit => $_getIZ(2);
  @$pb.TagNumber(3)
  set limit($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasLimit() => $_has(2);
  @$pb.TagNumber(3)
  void clearLimit() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get offset => $_getIZ(3);
  @$pb.TagNumber(4)
  set offset($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasOffset() => $_has(3);
  @$pb.TagNumber(4)
  void clearOffset() => clearField(4);
}

class GetStatusResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetStatusResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..pc<OrderProperties>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: OrderProperties.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  GetStatusResponse._() : super();
  factory GetStatusResponse({
    $core.Iterable<OrderProperties>? data,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory GetStatusResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetStatusResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetStatusResponse clone() => GetStatusResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetStatusResponse copyWith(void Function(GetStatusResponse) updates) => super.copyWith((message) => updates(message as GetStatusResponse)) as GetStatusResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetStatusResponse create() => GetStatusResponse._();
  GetStatusResponse createEmptyInstance() => create();
  static $pb.PbList<GetStatusResponse> createRepeated() => $pb.PbList<GetStatusResponse>();
  @$core.pragma('dart2js:noInline')
  static GetStatusResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetStatusResponse>(create);
  static GetStatusResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<OrderProperties> get data => $_getList(0);

  @$pb.TagNumber(3)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(3)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
}

class AllOrdersRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AllOrdersRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'limit', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'offset', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  AllOrdersRequest._() : super();
  factory AllOrdersRequest({
    $core.int? limit,
    $core.int? offset,
  }) {
    final _result = create();
    if (limit != null) {
      _result.limit = limit;
    }
    if (offset != null) {
      _result.offset = offset;
    }
    return _result;
  }
  factory AllOrdersRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AllOrdersRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AllOrdersRequest clone() => AllOrdersRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AllOrdersRequest copyWith(void Function(AllOrdersRequest) updates) => super.copyWith((message) => updates(message as AllOrdersRequest)) as AllOrdersRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AllOrdersRequest create() => AllOrdersRequest._();
  AllOrdersRequest createEmptyInstance() => create();
  static $pb.PbList<AllOrdersRequest> createRepeated() => $pb.PbList<AllOrdersRequest>();
  @$core.pragma('dart2js:noInline')
  static AllOrdersRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AllOrdersRequest>(create);
  static AllOrdersRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get limit => $_getIZ(0);
  @$pb.TagNumber(1)
  set limit($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLimit() => $_has(0);
  @$pb.TagNumber(1)
  void clearLimit() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get offset => $_getIZ(1);
  @$pb.TagNumber(2)
  set offset($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOffset() => $_has(1);
  @$pb.TagNumber(2)
  void clearOffset() => clearField(2);
}

class AllOrdersResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AllOrdersResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..pc<OrderProperties>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: OrderProperties.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  AllOrdersResponse._() : super();
  factory AllOrdersResponse({
    $core.Iterable<OrderProperties>? data,
    $core.String? status,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory AllOrdersResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AllOrdersResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AllOrdersResponse clone() => AllOrdersResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AllOrdersResponse copyWith(void Function(AllOrdersResponse) updates) => super.copyWith((message) => updates(message as AllOrdersResponse)) as AllOrdersResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AllOrdersResponse create() => AllOrdersResponse._();
  AllOrdersResponse createEmptyInstance() => create();
  static $pb.PbList<AllOrdersResponse> createRepeated() => $pb.PbList<AllOrdersResponse>();
  @$core.pragma('dart2js:noInline')
  static AllOrdersResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AllOrdersResponse>(create);
  static AllOrdersResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<OrderProperties> get data => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class GetDeliveryCodeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetDeliveryCodeRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderId')
    ..hasRequiredFields = false
  ;

  GetDeliveryCodeRequest._() : super();
  factory GetDeliveryCodeRequest({
    $core.String? orderId,
  }) {
    final _result = create();
    if (orderId != null) {
      _result.orderId = orderId;
    }
    return _result;
  }
  factory GetDeliveryCodeRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetDeliveryCodeRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetDeliveryCodeRequest clone() => GetDeliveryCodeRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetDeliveryCodeRequest copyWith(void Function(GetDeliveryCodeRequest) updates) => super.copyWith((message) => updates(message as GetDeliveryCodeRequest)) as GetDeliveryCodeRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetDeliveryCodeRequest create() => GetDeliveryCodeRequest._();
  GetDeliveryCodeRequest createEmptyInstance() => create();
  static $pb.PbList<GetDeliveryCodeRequest> createRepeated() => $pb.PbList<GetDeliveryCodeRequest>();
  @$core.pragma('dart2js:noInline')
  static GetDeliveryCodeRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetDeliveryCodeRequest>(create);
  static GetDeliveryCodeRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get orderId => $_getSZ(0);
  @$pb.TagNumber(1)
  set orderId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderId() => clearField(1);
}

class GetDeliveryCodeResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetDeliveryCodeResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryCode', $pb.PbFieldType.O3)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  GetDeliveryCodeResponse._() : super();
  factory GetDeliveryCodeResponse({
    $core.int? deliveryCode,
    $core.String? status,
  }) {
    final _result = create();
    if (deliveryCode != null) {
      _result.deliveryCode = deliveryCode;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory GetDeliveryCodeResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetDeliveryCodeResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetDeliveryCodeResponse clone() => GetDeliveryCodeResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetDeliveryCodeResponse copyWith(void Function(GetDeliveryCodeResponse) updates) => super.copyWith((message) => updates(message as GetDeliveryCodeResponse)) as GetDeliveryCodeResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetDeliveryCodeResponse create() => GetDeliveryCodeResponse._();
  GetDeliveryCodeResponse createEmptyInstance() => create();
  static $pb.PbList<GetDeliveryCodeResponse> createRepeated() => $pb.PbList<GetDeliveryCodeResponse>();
  @$core.pragma('dart2js:noInline')
  static GetDeliveryCodeResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetDeliveryCodeResponse>(create);
  static GetDeliveryCodeResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get deliveryCode => $_getIZ(0);
  @$pb.TagNumber(1)
  set deliveryCode($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDeliveryCode() => $_has(0);
  @$pb.TagNumber(1)
  void clearDeliveryCode() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

class OrderProperties extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderProperties', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'order.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reference')
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'subtotal', $pb.PbFieldType.OD)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryCost', $pb.PbFieldType.OD)
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'totalCost', $pb.PbFieldType.OD)
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryType', protoName: 'delivery_Type')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOM<InNewOrderDeliveryAddress>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryAddress', subBuilder: InNewOrderDeliveryAddress.create)
    ..pc<InNewOrderItems>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderItems', $pb.PbFieldType.PM, subBuilder: InNewOrderItems.create)
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aOS(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt')
    ..aOS(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currency')
    ..aOS(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'paymentMethod', protoName: 'paymentMethod')
    ..a<$core.int>(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryCode', $pb.PbFieldType.O3)
    ..aOS(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'acceptanceTime')
    ..aOS(17, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'shopperAssignedTime')
    ..aOS(18, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'shoppingCompletedTime')
    ..aOS(19, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inProgressTime')
    ..aOS(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryTime')
    ..aOS(21, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schedule')
    ..a<$core.double>(22, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tip', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  OrderProperties._() : super();
  factory OrderProperties({
    $core.String? orderId,
    $core.String? reference,
    $core.double? subtotal,
    $core.double? deliveryCost,
    $core.double? totalCost,
    $core.String? deliveryType,
    $core.String? userId,
    InNewOrderDeliveryAddress? deliveryAddress,
    $core.Iterable<InNewOrderItems>? orderItems,
    $core.String? status,
    $core.String? createdAt,
    $core.String? updatedAt,
    $core.String? currency,
    $core.String? paymentMethod,
    $core.int? deliveryCode,
    $core.String? acceptanceTime,
    $core.String? shopperAssignedTime,
    $core.String? shoppingCompletedTime,
    $core.String? inProgressTime,
    $core.String? deliveryTime,
    $core.String? schedule,
    $core.double? tip,
  }) {
    final _result = create();
    if (orderId != null) {
      _result.orderId = orderId;
    }
    if (reference != null) {
      _result.reference = reference;
    }
    if (subtotal != null) {
      _result.subtotal = subtotal;
    }
    if (deliveryCost != null) {
      _result.deliveryCost = deliveryCost;
    }
    if (totalCost != null) {
      _result.totalCost = totalCost;
    }
    if (deliveryType != null) {
      _result.deliveryType = deliveryType;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (deliveryAddress != null) {
      _result.deliveryAddress = deliveryAddress;
    }
    if (orderItems != null) {
      _result.orderItems.addAll(orderItems);
    }
    if (status != null) {
      _result.status = status;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    if (currency != null) {
      _result.currency = currency;
    }
    if (paymentMethod != null) {
      _result.paymentMethod = paymentMethod;
    }
    if (deliveryCode != null) {
      _result.deliveryCode = deliveryCode;
    }
    if (acceptanceTime != null) {
      _result.acceptanceTime = acceptanceTime;
    }
    if (shopperAssignedTime != null) {
      _result.shopperAssignedTime = shopperAssignedTime;
    }
    if (shoppingCompletedTime != null) {
      _result.shoppingCompletedTime = shoppingCompletedTime;
    }
    if (inProgressTime != null) {
      _result.inProgressTime = inProgressTime;
    }
    if (deliveryTime != null) {
      _result.deliveryTime = deliveryTime;
    }
    if (schedule != null) {
      _result.schedule = schedule;
    }
    if (tip != null) {
      _result.tip = tip;
    }
    return _result;
  }
  factory OrderProperties.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderProperties.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderProperties clone() => OrderProperties()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderProperties copyWith(void Function(OrderProperties) updates) => super.copyWith((message) => updates(message as OrderProperties)) as OrderProperties; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderProperties create() => OrderProperties._();
  OrderProperties createEmptyInstance() => create();
  static $pb.PbList<OrderProperties> createRepeated() => $pb.PbList<OrderProperties>();
  @$core.pragma('dart2js:noInline')
  static OrderProperties getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderProperties>(create);
  static OrderProperties? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get orderId => $_getSZ(0);
  @$pb.TagNumber(1)
  set orderId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOrderId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOrderId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get reference => $_getSZ(1);
  @$pb.TagNumber(2)
  set reference($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReference() => $_has(1);
  @$pb.TagNumber(2)
  void clearReference() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get subtotal => $_getN(2);
  @$pb.TagNumber(3)
  set subtotal($core.double v) { $_setDouble(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSubtotal() => $_has(2);
  @$pb.TagNumber(3)
  void clearSubtotal() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get deliveryCost => $_getN(3);
  @$pb.TagNumber(4)
  set deliveryCost($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDeliveryCost() => $_has(3);
  @$pb.TagNumber(4)
  void clearDeliveryCost() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get totalCost => $_getN(4);
  @$pb.TagNumber(5)
  set totalCost($core.double v) { $_setDouble(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTotalCost() => $_has(4);
  @$pb.TagNumber(5)
  void clearTotalCost() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get deliveryType => $_getSZ(5);
  @$pb.TagNumber(6)
  set deliveryType($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDeliveryType() => $_has(5);
  @$pb.TagNumber(6)
  void clearDeliveryType() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get userId => $_getSZ(6);
  @$pb.TagNumber(7)
  set userId($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasUserId() => $_has(6);
  @$pb.TagNumber(7)
  void clearUserId() => clearField(7);

  @$pb.TagNumber(8)
  InNewOrderDeliveryAddress get deliveryAddress => $_getN(7);
  @$pb.TagNumber(8)
  set deliveryAddress(InNewOrderDeliveryAddress v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasDeliveryAddress() => $_has(7);
  @$pb.TagNumber(8)
  void clearDeliveryAddress() => clearField(8);
  @$pb.TagNumber(8)
  InNewOrderDeliveryAddress ensureDeliveryAddress() => $_ensure(7);

  @$pb.TagNumber(9)
  $core.List<InNewOrderItems> get orderItems => $_getList(8);

  @$pb.TagNumber(10)
  $core.String get status => $_getSZ(9);
  @$pb.TagNumber(10)
  set status($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasStatus() => $_has(9);
  @$pb.TagNumber(10)
  void clearStatus() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get createdAt => $_getSZ(10);
  @$pb.TagNumber(11)
  set createdAt($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasCreatedAt() => $_has(10);
  @$pb.TagNumber(11)
  void clearCreatedAt() => clearField(11);

  @$pb.TagNumber(12)
  $core.String get updatedAt => $_getSZ(11);
  @$pb.TagNumber(12)
  set updatedAt($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasUpdatedAt() => $_has(11);
  @$pb.TagNumber(12)
  void clearUpdatedAt() => clearField(12);

  @$pb.TagNumber(13)
  $core.String get currency => $_getSZ(12);
  @$pb.TagNumber(13)
  set currency($core.String v) { $_setString(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasCurrency() => $_has(12);
  @$pb.TagNumber(13)
  void clearCurrency() => clearField(13);

  @$pb.TagNumber(14)
  $core.String get paymentMethod => $_getSZ(13);
  @$pb.TagNumber(14)
  set paymentMethod($core.String v) { $_setString(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasPaymentMethod() => $_has(13);
  @$pb.TagNumber(14)
  void clearPaymentMethod() => clearField(14);

  @$pb.TagNumber(15)
  $core.int get deliveryCode => $_getIZ(14);
  @$pb.TagNumber(15)
  set deliveryCode($core.int v) { $_setSignedInt32(14, v); }
  @$pb.TagNumber(15)
  $core.bool hasDeliveryCode() => $_has(14);
  @$pb.TagNumber(15)
  void clearDeliveryCode() => clearField(15);

  @$pb.TagNumber(16)
  $core.String get acceptanceTime => $_getSZ(15);
  @$pb.TagNumber(16)
  set acceptanceTime($core.String v) { $_setString(15, v); }
  @$pb.TagNumber(16)
  $core.bool hasAcceptanceTime() => $_has(15);
  @$pb.TagNumber(16)
  void clearAcceptanceTime() => clearField(16);

  @$pb.TagNumber(17)
  $core.String get shopperAssignedTime => $_getSZ(16);
  @$pb.TagNumber(17)
  set shopperAssignedTime($core.String v) { $_setString(16, v); }
  @$pb.TagNumber(17)
  $core.bool hasShopperAssignedTime() => $_has(16);
  @$pb.TagNumber(17)
  void clearShopperAssignedTime() => clearField(17);

  @$pb.TagNumber(18)
  $core.String get shoppingCompletedTime => $_getSZ(17);
  @$pb.TagNumber(18)
  set shoppingCompletedTime($core.String v) { $_setString(17, v); }
  @$pb.TagNumber(18)
  $core.bool hasShoppingCompletedTime() => $_has(17);
  @$pb.TagNumber(18)
  void clearShoppingCompletedTime() => clearField(18);

  @$pb.TagNumber(19)
  $core.String get inProgressTime => $_getSZ(18);
  @$pb.TagNumber(19)
  set inProgressTime($core.String v) { $_setString(18, v); }
  @$pb.TagNumber(19)
  $core.bool hasInProgressTime() => $_has(18);
  @$pb.TagNumber(19)
  void clearInProgressTime() => clearField(19);

  @$pb.TagNumber(20)
  $core.String get deliveryTime => $_getSZ(19);
  @$pb.TagNumber(20)
  set deliveryTime($core.String v) { $_setString(19, v); }
  @$pb.TagNumber(20)
  $core.bool hasDeliveryTime() => $_has(19);
  @$pb.TagNumber(20)
  void clearDeliveryTime() => clearField(20);

  @$pb.TagNumber(21)
  $core.String get schedule => $_getSZ(20);
  @$pb.TagNumber(21)
  set schedule($core.String v) { $_setString(20, v); }
  @$pb.TagNumber(21)
  $core.bool hasSchedule() => $_has(20);
  @$pb.TagNumber(21)
  void clearSchedule() => clearField(21);

  @$pb.TagNumber(22)
  $core.double get tip => $_getN(21);
  @$pb.TagNumber(22)
  set tip($core.double v) { $_setDouble(21, v); }
  @$pb.TagNumber(22)
  $core.bool hasTip() => $_has(21);
  @$pb.TagNumber(22)
  void clearTip() => clearField(22);
}

