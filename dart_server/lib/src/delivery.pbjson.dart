///
//  Generated code. Do not modify.
//  source: delivery.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use inDeliveryRequestDescriptor instead')
const InDeliveryRequest$json = const {
  '1': 'InDeliveryRequest',
  '2': const [
    const {'1': 'schedule', '3': 1, '4': 1, '5': 9, '10': 'schedule'},
    const {'1': 'customer_address', '3': 2, '4': 1, '5': 9, '10': 'customerAddress'},
    const {'1': 'customer_coordinates', '3': 3, '4': 1, '5': 11, '6': '.delivery_service.v1.InDeliveryCoordinatesRequest', '10': 'customerCoordinates'},
    const {'1': 'urgency_details', '3': 4, '4': 1, '5': 11, '6': '.delivery_service.v1.Delivery_Urgency', '10': 'urgencyDetails'},
    const {'1': 'currency', '3': 5, '4': 1, '5': 9, '10': 'currency'},
  ],
};

/// Descriptor for `InDeliveryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inDeliveryRequestDescriptor = $convert.base64Decode('ChFJbkRlbGl2ZXJ5UmVxdWVzdBIaCghzY2hlZHVsZRgBIAEoCVIIc2NoZWR1bGUSKQoQY3VzdG9tZXJfYWRkcmVzcxgCIAEoCVIPY3VzdG9tZXJBZGRyZXNzEmQKFGN1c3RvbWVyX2Nvb3JkaW5hdGVzGAMgASgLMjEuZGVsaXZlcnlfc2VydmljZS52MS5JbkRlbGl2ZXJ5Q29vcmRpbmF0ZXNSZXF1ZXN0UhNjdXN0b21lckNvb3JkaW5hdGVzEk4KD3VyZ2VuY3lfZGV0YWlscxgEIAEoCzIlLmRlbGl2ZXJ5X3NlcnZpY2UudjEuRGVsaXZlcnlfVXJnZW5jeVIOdXJnZW5jeURldGFpbHMSGgoIY3VycmVuY3kYBSABKAlSCGN1cnJlbmN5');
@$core.Deprecated('Use inDeliveryResponseDescriptor instead')
const InDeliveryResponse$json = const {
  '1': 'InDeliveryResponse',
  '2': const [
    const {'1': 'delivery_fee', '3': 1, '4': 1, '5': 1, '10': 'deliveryFee'},
    const {'1': 'currency', '3': 2, '4': 1, '5': 9, '10': 'currency'},
  ],
};

/// Descriptor for `InDeliveryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inDeliveryResponseDescriptor = $convert.base64Decode('ChJJbkRlbGl2ZXJ5UmVzcG9uc2USIQoMZGVsaXZlcnlfZmVlGAEgASgBUgtkZWxpdmVyeUZlZRIaCghjdXJyZW5jeRgCIAEoCVIIY3VycmVuY3k=');
@$core.Deprecated('Use inDeliveryCoordinatesRequestDescriptor instead')
const InDeliveryCoordinatesRequest$json = const {
  '1': 'InDeliveryCoordinatesRequest',
  '2': const [
    const {'1': 'latitude', '3': 1, '4': 1, '5': 1, '10': 'latitude'},
    const {'1': 'longitude', '3': 2, '4': 1, '5': 1, '10': 'longitude'},
  ],
};

/// Descriptor for `InDeliveryCoordinatesRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inDeliveryCoordinatesRequestDescriptor = $convert.base64Decode('ChxJbkRlbGl2ZXJ5Q29vcmRpbmF0ZXNSZXF1ZXN0EhoKCGxhdGl0dWRlGAEgASgBUghsYXRpdHVkZRIcCglsb25naXR1ZGUYAiABKAFSCWxvbmdpdHVkZQ==');
@$core.Deprecated('Use tipRequestDescriptor instead')
const TipRequest$json = const {
  '1': 'TipRequest',
  '2': const [
    const {'1': 'tip', '3': 1, '4': 1, '5': 11, '6': '.delivery_service.v1.Tip', '10': 'tip'},
    const {'1': 'currency', '3': 2, '4': 1, '5': 9, '10': 'currency'},
  ],
};

/// Descriptor for `TipRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tipRequestDescriptor = $convert.base64Decode('CgpUaXBSZXF1ZXN0EioKA3RpcBgBIAEoCzIYLmRlbGl2ZXJ5X3NlcnZpY2UudjEuVGlwUgN0aXASGgoIY3VycmVuY3kYAiABKAlSCGN1cnJlbmN5');
@$core.Deprecated('Use tipResponseDescriptor instead')
const TipResponse$json = const {
  '1': 'TipResponse',
  '2': const [
    const {'1': 'tip', '3': 1, '4': 1, '5': 1, '10': 'tip'},
    const {'1': 'currency', '3': 2, '4': 1, '5': 9, '10': 'currency'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `TipResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tipResponseDescriptor = $convert.base64Decode('CgtUaXBSZXNwb25zZRIQCgN0aXAYASABKAFSA3RpcBIaCghjdXJyZW5jeRgCIAEoCVIIY3VycmVuY3kSFgoGc3RhdHVzGAMgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use getPayLoadResponseDescriptor instead')
const GetPayLoadResponse$json = const {
  '1': 'GetPayLoadResponse',
  '2': const [
    const {'1': 'Data', '3': 1, '4': 1, '5': 11, '6': '.delivery_service.v1.DeliveryPayload', '10': 'Data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `GetPayLoadResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getPayLoadResponseDescriptor = $convert.base64Decode('ChJHZXRQYXlMb2FkUmVzcG9uc2USOAoERGF0YRgBIAEoCzIkLmRlbGl2ZXJ5X3NlcnZpY2UudjEuRGVsaXZlcnlQYXlsb2FkUgREYXRhEhYKBnN0YXR1cxgCIAEoCVIGc3RhdHVz');
@$core.Deprecated('Use deliveryPayloadDescriptor instead')
const DeliveryPayload$json = const {
  '1': 'DeliveryPayload',
  '2': const [
    const {'1': 'service_area', '3': 1, '4': 1, '5': 9, '10': 'serviceArea'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'type', '3': 3, '4': 1, '5': 9, '10': 'type'},
    const {'1': 'country', '3': 4, '4': 1, '5': 9, '10': 'country'},
    const {'1': 'currency', '3': 5, '4': 1, '5': 9, '10': 'currency'},
    const {'1': 'tips', '3': 6, '4': 3, '5': 11, '6': '.delivery_service.v1.Tip', '10': 'tips'},
    const {'1': 'delivery_urgencies', '3': 7, '4': 3, '5': 11, '6': '.delivery_service.v1.Delivery_Urgency', '10': 'deliveryUrgencies'},
  ],
};

/// Descriptor for `DeliveryPayload`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deliveryPayloadDescriptor = $convert.base64Decode('Cg9EZWxpdmVyeVBheWxvYWQSIQoMc2VydmljZV9hcmVhGAEgASgJUgtzZXJ2aWNlQXJlYRIgCgtkZXNjcmlwdGlvbhgCIAEoCVILZGVzY3JpcHRpb24SEgoEdHlwZRgDIAEoCVIEdHlwZRIYCgdjb3VudHJ5GAQgASgJUgdjb3VudHJ5EhoKCGN1cnJlbmN5GAUgASgJUghjdXJyZW5jeRIsCgR0aXBzGAYgAygLMhguZGVsaXZlcnlfc2VydmljZS52MS5UaXBSBHRpcHMSVAoSZGVsaXZlcnlfdXJnZW5jaWVzGAcgAygLMiUuZGVsaXZlcnlfc2VydmljZS52MS5EZWxpdmVyeV9VcmdlbmN5UhFkZWxpdmVyeVVyZ2VuY2llcw==');
@$core.Deprecated('Use deliveryResponseDescriptor instead')
const DeliveryResponse$json = const {
  '1': 'DeliveryResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `DeliveryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deliveryResponseDescriptor = $convert.base64Decode('ChBEZWxpdmVyeVJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USFgoGc3RhdHVzGAIgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use deliveryRequestDescriptor instead')
const DeliveryRequest$json = const {
  '1': 'DeliveryRequest',
  '2': const [
    const {'1': 'service_area', '3': 1, '4': 1, '5': 9, '10': 'serviceArea'},
    const {'1': 'limit', '3': 2, '4': 1, '5': 5, '10': 'limit'},
    const {'1': 'offset', '3': 3, '4': 1, '5': 5, '10': 'offset'},
  ],
};

/// Descriptor for `DeliveryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deliveryRequestDescriptor = $convert.base64Decode('Cg9EZWxpdmVyeVJlcXVlc3QSIQoMc2VydmljZV9hcmVhGAEgASgJUgtzZXJ2aWNlQXJlYRIUCgVsaW1pdBgCIAEoBVIFbGltaXQSFgoGb2Zmc2V0GAMgASgFUgZvZmZzZXQ=');
@$core.Deprecated('Use deliveryDeleteRequestDescriptor instead')
const DeliveryDeleteRequest$json = const {
  '1': 'DeliveryDeleteRequest',
  '2': const [
    const {'1': 'service_area', '3': 1, '4': 1, '5': 9, '10': 'serviceArea'},
  ],
};

/// Descriptor for `DeliveryDeleteRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deliveryDeleteRequestDescriptor = $convert.base64Decode('ChVEZWxpdmVyeURlbGV0ZVJlcXVlc3QSIQoMc2VydmljZV9hcmVhGAEgASgJUgtzZXJ2aWNlQXJlYQ==');
@$core.Deprecated('Use getAllDeliveriesRequestDescriptor instead')
const GetAllDeliveriesRequest$json = const {
  '1': 'GetAllDeliveriesRequest',
  '2': const [
    const {'1': 'limit', '3': 1, '4': 1, '5': 5, '10': 'limit'},
    const {'1': 'offset', '3': 2, '4': 1, '5': 5, '10': 'offset'},
  ],
};

/// Descriptor for `GetAllDeliveriesRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllDeliveriesRequestDescriptor = $convert.base64Decode('ChdHZXRBbGxEZWxpdmVyaWVzUmVxdWVzdBIUCgVsaW1pdBgBIAEoBVIFbGltaXQSFgoGb2Zmc2V0GAIgASgFUgZvZmZzZXQ=');
@$core.Deprecated('Use getAllDeliveryResponseDescriptor instead')
const GetAllDeliveryResponse$json = const {
  '1': 'GetAllDeliveryResponse',
  '2': const [
    const {'1': 'Data', '3': 1, '4': 3, '5': 11, '6': '.delivery_service.v1.DeliveryPayload', '10': 'Data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `GetAllDeliveryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllDeliveryResponseDescriptor = $convert.base64Decode('ChZHZXRBbGxEZWxpdmVyeVJlc3BvbnNlEjgKBERhdGEYASADKAsyJC5kZWxpdmVyeV9zZXJ2aWNlLnYxLkRlbGl2ZXJ5UGF5bG9hZFIERGF0YRIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use tipDescriptor instead')
const Tip$json = const {
  '1': 'Tip',
  '2': const [
    const {'1': 'amount', '3': 1, '4': 1, '5': 1, '10': 'amount'},
  ],
};

/// Descriptor for `Tip`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tipDescriptor = $convert.base64Decode('CgNUaXASFgoGYW1vdW50GAEgASgBUgZhbW91bnQ=');
@$core.Deprecated('Use delivery_UrgencyDescriptor instead')
const Delivery_Urgency$json = const {
  '1': 'Delivery_Urgency',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'amount', '3': 2, '4': 1, '5': 1, '10': 'amount'},
  ],
};

/// Descriptor for `Delivery_Urgency`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List delivery_UrgencyDescriptor = $convert.base64Decode('ChBEZWxpdmVyeV9VcmdlbmN5EhIKBG5hbWUYASABKAlSBG5hbWUSFgoGYW1vdW50GAIgASgBUgZhbW91bnQ=');
