///
//  Generated code. Do not modify.
//  source: cart.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'cart.pb.dart' as $0;
export 'cart.pb.dart';

class CartServiceClient extends $grpc.Client {
  static final _$createCart =
      $grpc.ClientMethod<$0.CartRequest, $0.CartResponse>(
          '/cart_service.v1.CartService/CreateCart',
          ($0.CartRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.CartResponse.fromBuffer(value));
  static final _$updateCart =
      $grpc.ClientMethod<$0.CartRequestItem, $0.CartResponse>(
          '/cart_service.v1.CartService/UpdateCart',
          ($0.CartRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.CartResponse.fromBuffer(value));
  static final _$deleteCart =
      $grpc.ClientMethod<$0.CartRequestItem, $0.CartResponse>(
          '/cart_service.v1.CartService/DeleteCart',
          ($0.CartRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.CartResponse.fromBuffer(value));
  static final _$deleteUserCart =
      $grpc.ClientMethod<$0.CartRequestItem, $0.CartResponse>(
          '/cart_service.v1.CartService/DeleteUserCart',
          ($0.CartRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.CartResponse.fromBuffer(value));
  static final _$listCart = $grpc.ClientMethod<$0.Empty, $0.ListCartResponse>(
      '/cart_service.v1.CartService/ListCart',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ListCartResponse.fromBuffer(value));
  static final _$listByRestaurant =
      $grpc.ClientMethod<$0.CartRequestItem, $0.ListCartResponse>(
          '/cart_service.v1.CartService/ListByRestaurant',
          ($0.CartRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListCartResponse.fromBuffer(value));
  static final _$listCartByUser =
      $grpc.ClientMethod<$0.CartRequestItem, $0.ListCartResponse>(
          '/cart_service.v1.CartService/ListCartByUser',
          ($0.CartRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListCartResponse.fromBuffer(value));
  static final _$listCartByRestaurantStream =
      $grpc.ClientMethod<$0.CartRequestItem, $0.ListCartResponse>(
          '/cart_service.v1.CartService/ListCartByRestaurantStream',
          ($0.CartRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListCartResponse.fromBuffer(value));

  CartServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.CartResponse> createCart($0.CartRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createCart, request, options: options);
  }

  $grpc.ResponseFuture<$0.CartResponse> updateCart($0.CartRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateCart, request, options: options);
  }

  $grpc.ResponseFuture<$0.CartResponse> deleteCart($0.CartRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteCart, request, options: options);
  }

  $grpc.ResponseFuture<$0.CartResponse> deleteUserCart(
      $0.CartRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteUserCart, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListCartResponse> listCart($0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listCart, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListCartResponse> listByRestaurant(
      $0.CartRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listByRestaurant, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListCartResponse> listCartByUser(
      $0.CartRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listCartByUser, request, options: options);
  }

  $grpc.ResponseStream<$0.ListCartResponse> listCartByRestaurantStream(
      $async.Stream<$0.CartRequestItem> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$listCartByRestaurantStream, request,
        options: options);
  }
}

abstract class CartServiceBase extends $grpc.Service {
  $core.String get $name => 'cart_service.v1.CartService';

  CartServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CartRequest, $0.CartResponse>(
        'CreateCart',
        createCart_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CartRequest.fromBuffer(value),
        ($0.CartResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CartRequestItem, $0.CartResponse>(
        'UpdateCart',
        updateCart_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CartRequestItem.fromBuffer(value),
        ($0.CartResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CartRequestItem, $0.CartResponse>(
        'DeleteCart',
        deleteCart_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CartRequestItem.fromBuffer(value),
        ($0.CartResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CartRequestItem, $0.CartResponse>(
        'DeleteUserCart',
        deleteUserCart_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CartRequestItem.fromBuffer(value),
        ($0.CartResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $0.ListCartResponse>(
        'ListCart',
        listCart_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($0.ListCartResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CartRequestItem, $0.ListCartResponse>(
        'ListByRestaurant',
        listByRestaurant_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CartRequestItem.fromBuffer(value),
        ($0.ListCartResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CartRequestItem, $0.ListCartResponse>(
        'ListCartByUser',
        listCartByUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CartRequestItem.fromBuffer(value),
        ($0.ListCartResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CartRequestItem, $0.ListCartResponse>(
        'ListCartByRestaurantStream',
        listCartByRestaurantStream,
        true,
        true,
        ($core.List<$core.int> value) => $0.CartRequestItem.fromBuffer(value),
        ($0.ListCartResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.CartResponse> createCart_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CartRequest> request) async {
    return createCart(call, await request);
  }

  $async.Future<$0.CartResponse> updateCart_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CartRequestItem> request) async {
    return updateCart(call, await request);
  }

  $async.Future<$0.CartResponse> deleteCart_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CartRequestItem> request) async {
    return deleteCart(call, await request);
  }

  $async.Future<$0.CartResponse> deleteUserCart_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CartRequestItem> request) async {
    return deleteUserCart(call, await request);
  }

  $async.Future<$0.ListCartResponse> listCart_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return listCart(call, await request);
  }

  $async.Future<$0.ListCartResponse> listByRestaurant_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CartRequestItem> request) async {
    return listByRestaurant(call, await request);
  }

  $async.Future<$0.ListCartResponse> listCartByUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CartRequestItem> request) async {
    return listCartByUser(call, await request);
  }

  $async.Future<$0.CartResponse> createCart(
      $grpc.ServiceCall call, $0.CartRequest request);
  $async.Future<$0.CartResponse> updateCart(
      $grpc.ServiceCall call, $0.CartRequestItem request);
  $async.Future<$0.CartResponse> deleteCart(
      $grpc.ServiceCall call, $0.CartRequestItem request);
  $async.Future<$0.CartResponse> deleteUserCart(
      $grpc.ServiceCall call, $0.CartRequestItem request);
  $async.Future<$0.ListCartResponse> listCart(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$0.ListCartResponse> listByRestaurant(
      $grpc.ServiceCall call, $0.CartRequestItem request);
  $async.Future<$0.ListCartResponse> listCartByUser(
      $grpc.ServiceCall call, $0.CartRequestItem request);
  $async.Stream<$0.ListCartResponse> listCartByRestaurantStream(
      $grpc.ServiceCall call, $async.Stream<$0.CartRequestItem> request);
}
