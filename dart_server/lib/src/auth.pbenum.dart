///
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class OtpType extends $pb.ProtobufEnum {
  static const OtpType LOGIN = OtpType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'LOGIN');
  static const OtpType REG = OtpType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'REG');
  static const OtpType RESET_PASSWORD = OtpType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'RESET_PASSWORD');

  static const $core.List<OtpType> values = <OtpType> [
    LOGIN,
    REG,
    RESET_PASSWORD,
  ];

  static final $core.Map<$core.int, OtpType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static OtpType? valueOf($core.int value) => _byValue[value];

  const OtpType._($core.int v, $core.String n) : super(v, n);
}

class LoginWithGoogleRequest_Channel extends $pb.ProtobufEnum {
  static const LoginWithGoogleRequest_Channel WEB = LoginWithGoogleRequest_Channel._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'WEB');
  static const LoginWithGoogleRequest_Channel MOBILE = LoginWithGoogleRequest_Channel._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'MOBILE');

  static const $core.List<LoginWithGoogleRequest_Channel> values = <LoginWithGoogleRequest_Channel> [
    WEB,
    MOBILE,
  ];

  static final $core.Map<$core.int, LoginWithGoogleRequest_Channel> _byValue = $pb.ProtobufEnum.initByValue(values);
  static LoginWithGoogleRequest_Channel? valueOf($core.int value) => _byValue[value];

  const LoginWithGoogleRequest_Channel._($core.int v, $core.String n) : super(v, n);
}

class EmailVerificationRequest_OtpType extends $pb.ProtobufEnum {
  static const EmailVerificationRequest_OtpType LOGIN = EmailVerificationRequest_OtpType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'LOGIN');
  static const EmailVerificationRequest_OtpType REG = EmailVerificationRequest_OtpType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'REG');

  static const $core.List<EmailVerificationRequest_OtpType> values = <EmailVerificationRequest_OtpType> [
    LOGIN,
    REG,
  ];

  static final $core.Map<$core.int, EmailVerificationRequest_OtpType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static EmailVerificationRequest_OtpType? valueOf($core.int value) => _byValue[value];

  const EmailVerificationRequest_OtpType._($core.int v, $core.String n) : super(v, n);
}

