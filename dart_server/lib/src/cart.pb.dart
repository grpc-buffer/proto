///
//  Generated code. Do not modify.
//  source: cart.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Empty extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Empty', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'cart_service.v1'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Empty._() : super();
  factory Empty() => create();
  factory Empty.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Empty.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Empty clone() => Empty()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Empty copyWith(void Function(Empty) updates) => super.copyWith((message) => updates(message as Empty)) as Empty; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Empty create() => Empty._();
  Empty createEmptyInstance() => create();
  static $pb.PbList<Empty> createRepeated() => $pb.PbList<Empty>();
  @$core.pragma('dart2js:noInline')
  static Empty getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Empty>(create);
  static Empty? _defaultInstance;
}

class CartRequestItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CartRequestItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'cart_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'requestId', protoName: 'requestId')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.O3)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userID', protoName: 'userID')
    ..hasRequiredFields = false
  ;

  CartRequestItem._() : super();
  factory CartRequestItem({
    $core.String? requestId,
    $core.int? quantity,
    $core.String? item,
    $core.String? packageID,
    $core.String? userID,
  }) {
    final _result = create();
    if (requestId != null) {
      _result.requestId = requestId;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (item != null) {
      _result.item = item;
    }
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (userID != null) {
      _result.userID = userID;
    }
    return _result;
  }
  factory CartRequestItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CartRequestItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CartRequestItem clone() => CartRequestItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CartRequestItem copyWith(void Function(CartRequestItem) updates) => super.copyWith((message) => updates(message as CartRequestItem)) as CartRequestItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CartRequestItem create() => CartRequestItem._();
  CartRequestItem createEmptyInstance() => create();
  static $pb.PbList<CartRequestItem> createRepeated() => $pb.PbList<CartRequestItem>();
  @$core.pragma('dart2js:noInline')
  static CartRequestItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CartRequestItem>(create);
  static CartRequestItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get requestId => $_getSZ(0);
  @$pb.TagNumber(1)
  set requestId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRequestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRequestId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get quantity => $_getIZ(1);
  @$pb.TagNumber(2)
  set quantity($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasQuantity() => $_has(1);
  @$pb.TagNumber(2)
  void clearQuantity() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get item => $_getSZ(2);
  @$pb.TagNumber(3)
  set item($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasItem() => $_has(2);
  @$pb.TagNumber(3)
  void clearItem() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get packageID => $_getSZ(3);
  @$pb.TagNumber(4)
  set packageID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPackageID() => $_has(3);
  @$pb.TagNumber(4)
  void clearPackageID() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get userID => $_getSZ(4);
  @$pb.TagNumber(5)
  set userID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasUserID() => $_has(4);
  @$pb.TagNumber(5)
  void clearUserID() => clearField(5);
}

class CartResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CartResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'cart_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..aOM<CartItems>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', subBuilder: CartItems.create)
    ..hasRequiredFields = false
  ;

  CartResponse._() : super();
  factory CartResponse({
    $core.String? status,
    $core.String? message,
    CartItems? data,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (data != null) {
      _result.data = data;
    }
    return _result;
  }
  factory CartResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CartResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CartResponse clone() => CartResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CartResponse copyWith(void Function(CartResponse) updates) => super.copyWith((message) => updates(message as CartResponse)) as CartResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CartResponse create() => CartResponse._();
  CartResponse createEmptyInstance() => create();
  static $pb.PbList<CartResponse> createRepeated() => $pb.PbList<CartResponse>();
  @$core.pragma('dart2js:noInline')
  static CartResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CartResponse>(create);
  static CartResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  CartItems get data => $_getN(2);
  @$pb.TagNumber(3)
  set data(CartItems v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasData() => $_has(2);
  @$pb.TagNumber(3)
  void clearData() => clearField(3);
  @$pb.TagNumber(3)
  CartItems ensureData() => $_ensure(2);
}

class CartItems extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CartItems', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'cart_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageName', protoName: 'packageName')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'basePrice', $pb.PbFieldType.OD, protoName: 'basePrice')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userID', protoName: 'userID')
    ..a<$core.int>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.O3)
    ..pc<CartPackageItem>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: CartPackageItem.create)
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'cartID', protoName: 'cartID')
    ..hasRequiredFields = false
  ;

  CartItems._() : super();
  factory CartItems({
    $core.String? packageID,
    $core.String? packageName,
    $core.String? description,
    $core.double? basePrice,
    $core.String? serviceAreaID,
    $core.String? image,
    $core.String? userID,
    $core.int? quantity,
    $core.Iterable<CartPackageItem>? items,
    $core.String? cartID,
  }) {
    final _result = create();
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (packageName != null) {
      _result.packageName = packageName;
    }
    if (description != null) {
      _result.description = description;
    }
    if (basePrice != null) {
      _result.basePrice = basePrice;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    if (image != null) {
      _result.image = image;
    }
    if (userID != null) {
      _result.userID = userID;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (items != null) {
      _result.items.addAll(items);
    }
    if (cartID != null) {
      _result.cartID = cartID;
    }
    return _result;
  }
  factory CartItems.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CartItems.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CartItems clone() => CartItems()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CartItems copyWith(void Function(CartItems) updates) => super.copyWith((message) => updates(message as CartItems)) as CartItems; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CartItems create() => CartItems._();
  CartItems createEmptyInstance() => create();
  static $pb.PbList<CartItems> createRepeated() => $pb.PbList<CartItems>();
  @$core.pragma('dart2js:noInline')
  static CartItems getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CartItems>(create);
  static CartItems? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get packageName => $_getSZ(1);
  @$pb.TagNumber(2)
  set packageName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackageName() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackageName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get basePrice => $_getN(3);
  @$pb.TagNumber(4)
  set basePrice($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasBasePrice() => $_has(3);
  @$pb.TagNumber(4)
  void clearBasePrice() => clearField(4);

  @$pb.TagNumber(6)
  $core.String get serviceAreaID => $_getSZ(4);
  @$pb.TagNumber(6)
  set serviceAreaID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(6)
  $core.bool hasServiceAreaID() => $_has(4);
  @$pb.TagNumber(6)
  void clearServiceAreaID() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get image => $_getSZ(5);
  @$pb.TagNumber(7)
  set image($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(7)
  $core.bool hasImage() => $_has(5);
  @$pb.TagNumber(7)
  void clearImage() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get userID => $_getSZ(6);
  @$pb.TagNumber(8)
  set userID($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(8)
  $core.bool hasUserID() => $_has(6);
  @$pb.TagNumber(8)
  void clearUserID() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get quantity => $_getIZ(7);
  @$pb.TagNumber(9)
  set quantity($core.int v) { $_setSignedInt32(7, v); }
  @$pb.TagNumber(9)
  $core.bool hasQuantity() => $_has(7);
  @$pb.TagNumber(9)
  void clearQuantity() => clearField(9);

  @$pb.TagNumber(10)
  $core.List<CartPackageItem> get items => $_getList(8);

  @$pb.TagNumber(11)
  $core.String get cartID => $_getSZ(9);
  @$pb.TagNumber(11)
  set cartID($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(11)
  $core.bool hasCartID() => $_has(9);
  @$pb.TagNumber(11)
  void clearCartID() => clearField(11);
}

class CartPackageItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CartPackageItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'cart_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemID', protoName: 'itemID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemCategoryID', protoName: 'itemCategoryID')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'unit')
    ..hasRequiredFields = false
  ;

  CartPackageItem._() : super();
  factory CartPackageItem({
    $core.String? itemID,
    $core.String? name,
    $core.String? description,
    $core.String? itemCategoryID,
    $core.String? image,
    $core.String? unit,
  }) {
    final _result = create();
    if (itemID != null) {
      _result.itemID = itemID;
    }
    if (name != null) {
      _result.name = name;
    }
    if (description != null) {
      _result.description = description;
    }
    if (itemCategoryID != null) {
      _result.itemCategoryID = itemCategoryID;
    }
    if (image != null) {
      _result.image = image;
    }
    if (unit != null) {
      _result.unit = unit;
    }
    return _result;
  }
  factory CartPackageItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CartPackageItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CartPackageItem clone() => CartPackageItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CartPackageItem copyWith(void Function(CartPackageItem) updates) => super.copyWith((message) => updates(message as CartPackageItem)) as CartPackageItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CartPackageItem create() => CartPackageItem._();
  CartPackageItem createEmptyInstance() => create();
  static $pb.PbList<CartPackageItem> createRepeated() => $pb.PbList<CartPackageItem>();
  @$core.pragma('dart2js:noInline')
  static CartPackageItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CartPackageItem>(create);
  static CartPackageItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get itemID => $_getSZ(0);
  @$pb.TagNumber(1)
  set itemID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemID() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get itemCategoryID => $_getSZ(3);
  @$pb.TagNumber(4)
  set itemCategoryID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasItemCategoryID() => $_has(3);
  @$pb.TagNumber(4)
  void clearItemCategoryID() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get image => $_getSZ(4);
  @$pb.TagNumber(5)
  set image($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasImage() => $_has(4);
  @$pb.TagNumber(5)
  void clearImage() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get unit => $_getSZ(5);
  @$pb.TagNumber(6)
  set unit($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasUnit() => $_has(5);
  @$pb.TagNumber(6)
  void clearUnit() => clearField(6);
}

class CartRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CartRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'cart_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageID', protoName: 'packageID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'packageName', protoName: 'packageName')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'basePrice', $pb.PbFieldType.OD, protoName: 'basePrice')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userID', protoName: 'userID')
    ..a<$core.int>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.O3)
    ..pc<CartPackageItem>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: CartPackageItem.create)
    ..hasRequiredFields = false
  ;

  CartRequest._() : super();
  factory CartRequest({
    $core.String? packageID,
    $core.String? packageName,
    $core.String? description,
    $core.double? basePrice,
    $core.String? serviceAreaID,
    $core.String? image,
    $core.String? userID,
    $core.int? quantity,
    $core.Iterable<CartPackageItem>? items,
  }) {
    final _result = create();
    if (packageID != null) {
      _result.packageID = packageID;
    }
    if (packageName != null) {
      _result.packageName = packageName;
    }
    if (description != null) {
      _result.description = description;
    }
    if (basePrice != null) {
      _result.basePrice = basePrice;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    if (image != null) {
      _result.image = image;
    }
    if (userID != null) {
      _result.userID = userID;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory CartRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CartRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CartRequest clone() => CartRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CartRequest copyWith(void Function(CartRequest) updates) => super.copyWith((message) => updates(message as CartRequest)) as CartRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CartRequest create() => CartRequest._();
  CartRequest createEmptyInstance() => create();
  static $pb.PbList<CartRequest> createRepeated() => $pb.PbList<CartRequest>();
  @$core.pragma('dart2js:noInline')
  static CartRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CartRequest>(create);
  static CartRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get packageID => $_getSZ(0);
  @$pb.TagNumber(1)
  set packageID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackageID() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackageID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get packageName => $_getSZ(1);
  @$pb.TagNumber(2)
  set packageName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackageName() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackageName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get basePrice => $_getN(3);
  @$pb.TagNumber(4)
  set basePrice($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasBasePrice() => $_has(3);
  @$pb.TagNumber(4)
  void clearBasePrice() => clearField(4);

  @$pb.TagNumber(6)
  $core.String get serviceAreaID => $_getSZ(4);
  @$pb.TagNumber(6)
  set serviceAreaID($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(6)
  $core.bool hasServiceAreaID() => $_has(4);
  @$pb.TagNumber(6)
  void clearServiceAreaID() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get image => $_getSZ(5);
  @$pb.TagNumber(7)
  set image($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(7)
  $core.bool hasImage() => $_has(5);
  @$pb.TagNumber(7)
  void clearImage() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get userID => $_getSZ(6);
  @$pb.TagNumber(8)
  set userID($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(8)
  $core.bool hasUserID() => $_has(6);
  @$pb.TagNumber(8)
  void clearUserID() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get quantity => $_getIZ(7);
  @$pb.TagNumber(9)
  set quantity($core.int v) { $_setSignedInt32(7, v); }
  @$pb.TagNumber(9)
  $core.bool hasQuantity() => $_has(7);
  @$pb.TagNumber(9)
  void clearQuantity() => clearField(9);

  @$pb.TagNumber(10)
  $core.List<CartPackageItem> get items => $_getList(8);
}

class ListCartResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ListCartResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'cart_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..pc<CartItems>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: CartItems.create)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'count', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  ListCartResponse._() : super();
  factory ListCartResponse({
    $core.String? status,
    $core.String? message,
    $core.Iterable<CartItems>? data,
    $core.int? count,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (data != null) {
      _result.data.addAll(data);
    }
    if (count != null) {
      _result.count = count;
    }
    return _result;
  }
  factory ListCartResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ListCartResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ListCartResponse clone() => ListCartResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ListCartResponse copyWith(void Function(ListCartResponse) updates) => super.copyWith((message) => updates(message as ListCartResponse)) as ListCartResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ListCartResponse create() => ListCartResponse._();
  ListCartResponse createEmptyInstance() => create();
  static $pb.PbList<ListCartResponse> createRepeated() => $pb.PbList<ListCartResponse>();
  @$core.pragma('dart2js:noInline')
  static ListCartResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ListCartResponse>(create);
  static ListCartResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get status => $_getSZ(0);
  @$pb.TagNumber(1)
  set status($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<CartItems> get data => $_getList(2);

  @$pb.TagNumber(4)
  $core.int get count => $_getIZ(3);
  @$pb.TagNumber(4)
  set count($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCount() => $_has(3);
  @$pb.TagNumber(4)
  void clearCount() => clearField(4);
}

