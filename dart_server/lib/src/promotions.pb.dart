///
//  Generated code. Do not modify.
//  source: promotions.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class BannerInfo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BannerInfo', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'promotion_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bannerID', protoName: 'bannerID')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bannerImage', protoName: 'bannerImage')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bucketPath', protoName: 'bucketPath')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'imageType', protoName: 'imageType')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'link')
    ..hasRequiredFields = false
  ;

  BannerInfo._() : super();
  factory BannerInfo({
    $core.String? title,
    $core.String? description,
    $core.String? serviceAreaID,
    $core.String? bannerID,
    $core.String? bannerImage,
    $core.String? bucketPath,
    $core.String? imageType,
    $core.String? link,
  }) {
    final _result = create();
    if (title != null) {
      _result.title = title;
    }
    if (description != null) {
      _result.description = description;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    if (bannerID != null) {
      _result.bannerID = bannerID;
    }
    if (bannerImage != null) {
      _result.bannerImage = bannerImage;
    }
    if (bucketPath != null) {
      _result.bucketPath = bucketPath;
    }
    if (imageType != null) {
      _result.imageType = imageType;
    }
    if (link != null) {
      _result.link = link;
    }
    return _result;
  }
  factory BannerInfo.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BannerInfo.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BannerInfo clone() => BannerInfo()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BannerInfo copyWith(void Function(BannerInfo) updates) => super.copyWith((message) => updates(message as BannerInfo)) as BannerInfo; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BannerInfo create() => BannerInfo._();
  BannerInfo createEmptyInstance() => create();
  static $pb.PbList<BannerInfo> createRepeated() => $pb.PbList<BannerInfo>();
  @$core.pragma('dart2js:noInline')
  static BannerInfo getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BannerInfo>(create);
  static BannerInfo? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get title => $_getSZ(0);
  @$pb.TagNumber(1)
  set title($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTitle() => $_has(0);
  @$pb.TagNumber(1)
  void clearTitle() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get description => $_getSZ(1);
  @$pb.TagNumber(2)
  set description($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDescription() => $_has(1);
  @$pb.TagNumber(2)
  void clearDescription() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get serviceAreaID => $_getSZ(2);
  @$pb.TagNumber(3)
  set serviceAreaID($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasServiceAreaID() => $_has(2);
  @$pb.TagNumber(3)
  void clearServiceAreaID() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get bannerID => $_getSZ(3);
  @$pb.TagNumber(4)
  set bannerID($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasBannerID() => $_has(3);
  @$pb.TagNumber(4)
  void clearBannerID() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get bannerImage => $_getSZ(4);
  @$pb.TagNumber(5)
  set bannerImage($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasBannerImage() => $_has(4);
  @$pb.TagNumber(5)
  void clearBannerImage() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get bucketPath => $_getSZ(5);
  @$pb.TagNumber(6)
  set bucketPath($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasBucketPath() => $_has(5);
  @$pb.TagNumber(6)
  void clearBucketPath() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get imageType => $_getSZ(6);
  @$pb.TagNumber(7)
  set imageType($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasImageType() => $_has(6);
  @$pb.TagNumber(7)
  void clearImageType() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get link => $_getSZ(7);
  @$pb.TagNumber(8)
  set link($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasLink() => $_has(7);
  @$pb.TagNumber(8)
  void clearLink() => clearField(8);
}

class BannerList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BannerList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'promotion_service.v1'), createEmptyInstance: create)
    ..pc<BannerInfo>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.PM, subBuilder: BannerInfo.create)
    ..hasRequiredFields = false
  ;

  BannerList._() : super();
  factory BannerList({
    $core.Iterable<BannerInfo>? data,
  }) {
    final _result = create();
    if (data != null) {
      _result.data.addAll(data);
    }
    return _result;
  }
  factory BannerList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BannerList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BannerList clone() => BannerList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BannerList copyWith(void Function(BannerList) updates) => super.copyWith((message) => updates(message as BannerList)) as BannerList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BannerList create() => BannerList._();
  BannerList createEmptyInstance() => create();
  static $pb.PbList<BannerList> createRepeated() => $pb.PbList<BannerList>();
  @$core.pragma('dart2js:noInline')
  static BannerList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BannerList>(create);
  static BannerList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<BannerInfo> get data => $_getList(0);
}

class BannerRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BannerRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'promotion_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bannerID', protoName: 'bannerID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serviceAreaID', protoName: 'serviceAreaID')
    ..hasRequiredFields = false
  ;

  BannerRequest._() : super();
  factory BannerRequest({
    $core.String? bannerID,
    $core.String? serviceAreaID,
  }) {
    final _result = create();
    if (bannerID != null) {
      _result.bannerID = bannerID;
    }
    if (serviceAreaID != null) {
      _result.serviceAreaID = serviceAreaID;
    }
    return _result;
  }
  factory BannerRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BannerRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BannerRequest clone() => BannerRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BannerRequest copyWith(void Function(BannerRequest) updates) => super.copyWith((message) => updates(message as BannerRequest)) as BannerRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BannerRequest create() => BannerRequest._();
  BannerRequest createEmptyInstance() => create();
  static $pb.PbList<BannerRequest> createRepeated() => $pb.PbList<BannerRequest>();
  @$core.pragma('dart2js:noInline')
  static BannerRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BannerRequest>(create);
  static BannerRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get bannerID => $_getSZ(0);
  @$pb.TagNumber(1)
  set bannerID($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasBannerID() => $_has(0);
  @$pb.TagNumber(1)
  void clearBannerID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get serviceAreaID => $_getSZ(1);
  @$pb.TagNumber(2)
  set serviceAreaID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasServiceAreaID() => $_has(1);
  @$pb.TagNumber(2)
  void clearServiceAreaID() => clearField(2);
}

class PromotionsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PromotionsResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'promotion_service.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..hasRequiredFields = false
  ;

  PromotionsResponse._() : super();
  factory PromotionsResponse({
    $core.String? message,
  }) {
    final _result = create();
    if (message != null) {
      _result.message = message;
    }
    return _result;
  }
  factory PromotionsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PromotionsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PromotionsResponse clone() => PromotionsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PromotionsResponse copyWith(void Function(PromotionsResponse) updates) => super.copyWith((message) => updates(message as PromotionsResponse)) as PromotionsResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PromotionsResponse create() => PromotionsResponse._();
  PromotionsResponse createEmptyInstance() => create();
  static $pb.PbList<PromotionsResponse> createRepeated() => $pb.PbList<PromotionsResponse>();
  @$core.pragma('dart2js:noInline')
  static PromotionsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PromotionsResponse>(create);
  static PromotionsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

