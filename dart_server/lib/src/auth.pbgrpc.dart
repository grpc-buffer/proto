///
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'auth.pb.dart' as $0;
export 'auth.pb.dart';

class AuthServiceClient extends $grpc.Client {
  static final _$login = $grpc.ClientMethod<$0.LoginRequest, $0.LoginResponse>(
      '/AuthService/Login',
      ($0.LoginRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.LoginResponse.fromBuffer(value));
  static final _$loginNoVerification =
      $grpc.ClientMethod<$0.LoginRequest, $0.VerifyLoginResponse>(
          '/AuthService/LoginNoVerification',
          ($0.LoginRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.VerifyLoginResponse.fromBuffer(value));
  static final _$loginWithGoogle =
      $grpc.ClientMethod<$0.LoginWithGoogleRequest, $0.LoginWithGoogleResponse>(
          '/AuthService/LoginWithGoogle',
          ($0.LoginWithGoogleRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LoginWithGoogleResponse.fromBuffer(value));
  static final _$loginAsAgent =
      $grpc.ClientMethod<$0.AgentLoginRequest, $0.AgentLoginResponse>(
          '/AuthService/LoginAsAgent',
          ($0.AgentLoginRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AgentLoginResponse.fromBuffer(value));
  static final _$register =
      $grpc.ClientMethod<$0.RegisterRequest, $0.RegisterResponse>(
          '/AuthService/Register',
          ($0.RegisterRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RegisterResponse.fromBuffer(value));
  static final _$initEmailVerification = $grpc.ClientMethod<
          $0.InitEmailVerificationRequest, $0.InitEmailVerificationResponse>(
      '/AuthService/InitEmailVerification',
      ($0.InitEmailVerificationRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.InitEmailVerificationResponse.fromBuffer(value));
  static final _$initPhoneVerification = $grpc.ClientMethod<
          $0.InitPhoneVerificationRequest, $0.InitPhoneVerificationResponse>(
      '/AuthService/InitPhoneVerification',
      ($0.InitPhoneVerificationRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.InitPhoneVerificationResponse.fromBuffer(value));
  static final _$verifyPhone = $grpc.ClientMethod<$0.PhoneVerificationRequest,
          $0.PhoneVerificationResponse>(
      '/AuthService/VerifyPhone',
      ($0.PhoneVerificationRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.PhoneVerificationResponse.fromBuffer(value));
  static final _$verifyEmail = $grpc.ClientMethod<$0.EmailVerificationRequest,
          $0.EmailVerificationResponse>(
      '/AuthService/VerifyEmail',
      ($0.EmailVerificationRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.EmailVerificationResponse.fromBuffer(value));
  static final _$verifyLogin =
      $grpc.ClientMethod<$0.VerifyLoginRequest, $0.VerifyLoginResponse>(
          '/AuthService/VerifyLogin',
          ($0.VerifyLoginRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.VerifyLoginResponse.fromBuffer(value));
  static final _$logout =
      $grpc.ClientMethod<$0.LogoutRequest, $0.LogoutResponse>(
          '/AuthService/Logout',
          ($0.LogoutRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.LogoutResponse.fromBuffer(value));
  static final _$viewUser = $grpc.ClientMethod<$0.UserRequest, $0.User>(
      '/AuthService/ViewUser',
      ($0.UserRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));
  static final _$updatePassword =
      $grpc.ClientMethod<$0.UserRequest, $0.UpdatePasswordResponse>(
          '/AuthService/UpdatePassword',
          ($0.UserRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UpdatePasswordResponse.fromBuffer(value));
  static final _$resetPassword =
      $grpc.ClientMethod<$0.ResetPasswordRequest, $0.ResetPasswordResponse>(
          '/AuthService/ResetPassword',
          ($0.ResetPasswordRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ResetPasswordResponse.fromBuffer(value));
  static final _$verifyPasswordReset = $grpc.ClientMethod<
          $0.VerifyPasswordResetRequest, $0.VerifyPasswordResetResponse>(
      '/AuthService/VerifyPasswordReset',
      ($0.VerifyPasswordResetRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.VerifyPasswordResetResponse.fromBuffer(value));
  static final _$setNewPassword =
      $grpc.ClientMethod<$0.SetNewPasswordRequest, $0.SetNewPasswordResponse>(
          '/AuthService/SetNewPassword',
          ($0.SetNewPasswordRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SetNewPasswordResponse.fromBuffer(value));
  static final _$getAllCountries =
      $grpc.ClientMethod<$0.GetAllCountryRequest, $0.GetAllCountryResponse>(
          '/AuthService/GetAllCountries',
          ($0.GetAllCountryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetAllCountryResponse.fromBuffer(value));
  static final _$addCountry =
      $grpc.ClientMethod<$0.Country, $0.GeneralResponse>(
          '/AuthService/AddCountry',
          ($0.Country value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GeneralResponse.fromBuffer(value));
  static final _$removeCountry =
      $grpc.ClientMethod<$0.CountryRequest, $0.GeneralResponse>(
          '/AuthService/RemoveCountry',
          ($0.CountryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GeneralResponse.fromBuffer(value));
  static final _$assignPermissionsToRole = $grpc.ClientMethod<
          $0.AssignPermissionsToRoleRequest,
          $0.AssignPermissionsToRoleResponse>(
      '/AuthService/AssignPermissionsToRole',
      ($0.AssignPermissionsToRoleRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.AssignPermissionsToRoleResponse.fromBuffer(value));
  static final _$removePermissionsFromRole = $grpc.ClientMethod<
          $0.RemovePermissionsFromRoleRequest,
          $0.RemovePermissionsFromRoleResponse>(
      '/AuthService/RemovePermissionsFromRole',
      ($0.RemovePermissionsFromRoleRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.RemovePermissionsFromRoleResponse.fromBuffer(value));
  static final _$createPermission = $grpc.ClientMethod<
          $0.CreatePermissionRequest, $0.CreatePermissionResponse>(
      '/AuthService/CreatePermission',
      ($0.CreatePermissionRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.CreatePermissionResponse.fromBuffer(value));
  static final _$getAllPermissions = $grpc.ClientMethod<
          $0.GetAllPermissionsRequest, $0.GetAllPermissionsResponse>(
      '/AuthService/GetAllPermissions',
      ($0.GetAllPermissionsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetAllPermissionsResponse.fromBuffer(value));
  static final _$getPermissionsByRole = $grpc.ClientMethod<
          $0.GetPermissionsByRoleRequest, $0.GetPermissionsByRoleResponse>(
      '/AuthService/GetPermissionsByRole',
      ($0.GetPermissionsByRoleRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetPermissionsByRoleResponse.fromBuffer(value));
  static final _$getPermissionByTitle = $grpc.ClientMethod<
          $0.GetPermissionByTitleRequest, $0.GetPermissionByTitleResponse>(
      '/AuthService/GetPermissionByTitle',
      ($0.GetPermissionByTitleRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetPermissionByTitleResponse.fromBuffer(value));
  static final _$hasPermission =
      $grpc.ClientMethod<$0.HasPermissionRequest, $0.HasPermissionResponse>(
          '/AuthService/HasPermission',
          ($0.HasPermissionRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.HasPermissionResponse.fromBuffer(value));
  static final _$updateUserProfile = $grpc.ClientMethod<
          $0.UpdateUserInformationRequest, $0.UpdateUserInformationResponse>(
      '/AuthService/UpdateUserProfile',
      ($0.UpdateUserInformationRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.UpdateUserInformationResponse.fromBuffer(value));
  static final _$deleteUser =
      $grpc.ClientMethod<$0.DeleteUserRequest, $0.DeleteUserResponse>(
          '/AuthService/DeleteUser',
          ($0.DeleteUserRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.DeleteUserResponse.fromBuffer(value));
  static final _$listUsers =
      $grpc.ClientMethod<$0.EmptyUserListRequest, $0.UserListResponse>(
          '/AuthService/ListUsers',
          ($0.EmptyUserListRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UserListResponse.fromBuffer(value));
  static final _$listUsersByReferralUsed =
      $grpc.ClientMethod<$0.ReferralListRequest, $0.UserListResponse>(
          '/AuthService/ListUsersByReferralUsed',
          ($0.ReferralListRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UserListResponse.fromBuffer(value));
  static final _$assignUserToRole =
      $grpc.ClientMethod<$0.UserToRoleRequest, $0.UserToRoleResponse>(
          '/AuthService/AssignUserToRole',
          ($0.UserToRoleRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UserToRoleResponse.fromBuffer(value));
  static final _$removeUserFromRole =
      $grpc.ClientMethod<$0.UserToRoleRequest, $0.UserToRoleResponse>(
          '/AuthService/RemoveUserFromRole',
          ($0.UserToRoleRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UserToRoleResponse.fromBuffer(value));
  static final _$createRole =
      $grpc.ClientMethod<$0.CreateRoleRequest, $0.CreateRoleResponse>(
          '/AuthService/CreateRole',
          ($0.CreateRoleRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.CreateRoleResponse.fromBuffer(value));
  static final _$getAllRoles =
      $grpc.ClientMethod<$0.GetAllRolesRequest, $0.GetAllRolesResponse>(
          '/AuthService/GetAllRoles',
          ($0.GetAllRolesRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetAllRolesResponse.fromBuffer(value));
  static final _$deleteRole =
      $grpc.ClientMethod<$0.DeleteRoleRequest, $0.DeleteRoleResponse>(
          '/AuthService/DeleteRole',
          ($0.DeleteRoleRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.DeleteRoleResponse.fromBuffer(value));
  static final _$updateRole =
      $grpc.ClientMethod<$0.UpdateRoleRequest, $0.UpdateRoleResponse>(
          '/AuthService/UpdateRole',
          ($0.UpdateRoleRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UpdateRoleResponse.fromBuffer(value));
  static final _$uploadImage =
      $grpc.ClientMethod<$0.UploadDPRequest, $0.UploadImageResponse>(
          '/AuthService/UploadImage',
          ($0.UploadDPRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UploadImageResponse.fromBuffer(value));

  AuthServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.LoginResponse> login($0.LoginRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$login, request, options: options);
  }

  $grpc.ResponseFuture<$0.VerifyLoginResponse> loginNoVerification(
      $0.LoginRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$loginNoVerification, request, options: options);
  }

  $grpc.ResponseFuture<$0.LoginWithGoogleResponse> loginWithGoogle(
      $0.LoginWithGoogleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$loginWithGoogle, request, options: options);
  }

  $grpc.ResponseFuture<$0.AgentLoginResponse> loginAsAgent(
      $0.AgentLoginRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$loginAsAgent, request, options: options);
  }

  $grpc.ResponseFuture<$0.RegisterResponse> register($0.RegisterRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$register, request, options: options);
  }

  $grpc.ResponseFuture<$0.InitEmailVerificationResponse> initEmailVerification(
      $0.InitEmailVerificationRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$initEmailVerification, request, options: options);
  }

  $grpc.ResponseFuture<$0.InitPhoneVerificationResponse> initPhoneVerification(
      $0.InitPhoneVerificationRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$initPhoneVerification, request, options: options);
  }

  $grpc.ResponseFuture<$0.PhoneVerificationResponse> verifyPhone(
      $0.PhoneVerificationRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$verifyPhone, request, options: options);
  }

  $grpc.ResponseFuture<$0.EmailVerificationResponse> verifyEmail(
      $0.EmailVerificationRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$verifyEmail, request, options: options);
  }

  $grpc.ResponseFuture<$0.VerifyLoginResponse> verifyLogin(
      $0.VerifyLoginRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$verifyLogin, request, options: options);
  }

  $grpc.ResponseFuture<$0.LogoutResponse> logout($0.LogoutRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$logout, request, options: options);
  }

  $grpc.ResponseFuture<$0.User> viewUser($0.UserRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewUser, request, options: options);
  }

  $grpc.ResponseFuture<$0.UpdatePasswordResponse> updatePassword(
      $0.UserRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updatePassword, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResetPasswordResponse> resetPassword(
      $0.ResetPasswordRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$resetPassword, request, options: options);
  }

  $grpc.ResponseFuture<$0.VerifyPasswordResetResponse> verifyPasswordReset(
      $0.VerifyPasswordResetRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$verifyPasswordReset, request, options: options);
  }

  $grpc.ResponseFuture<$0.SetNewPasswordResponse> setNewPassword(
      $0.SetNewPasswordRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$setNewPassword, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetAllCountryResponse> getAllCountries(
      $0.GetAllCountryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllCountries, request, options: options);
  }

  $grpc.ResponseFuture<$0.GeneralResponse> addCountry($0.Country request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addCountry, request, options: options);
  }

  $grpc.ResponseFuture<$0.GeneralResponse> removeCountry(
      $0.CountryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removeCountry, request, options: options);
  }

  $grpc.ResponseFuture<$0.AssignPermissionsToRoleResponse>
      assignPermissionsToRole($0.AssignPermissionsToRoleRequest request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$assignPermissionsToRole, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.RemovePermissionsFromRoleResponse>
      removePermissionsFromRole($0.RemovePermissionsFromRoleRequest request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removePermissionsFromRole, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.CreatePermissionResponse> createPermission(
      $0.CreatePermissionRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createPermission, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetAllPermissionsResponse> getAllPermissions(
      $0.GetAllPermissionsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllPermissions, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetPermissionsByRoleResponse> getPermissionsByRole(
      $0.GetPermissionsByRoleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getPermissionsByRole, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetPermissionByTitleResponse> getPermissionByTitle(
      $0.GetPermissionByTitleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getPermissionByTitle, request, options: options);
  }

  $grpc.ResponseFuture<$0.HasPermissionResponse> hasPermission(
      $0.HasPermissionRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$hasPermission, request, options: options);
  }

  $grpc.ResponseFuture<$0.UpdateUserInformationResponse> updateUserProfile(
      $0.UpdateUserInformationRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateUserProfile, request, options: options);
  }

  $grpc.ResponseFuture<$0.DeleteUserResponse> deleteUser(
      $0.DeleteUserRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteUser, request, options: options);
  }

  $grpc.ResponseFuture<$0.UserListResponse> listUsers(
      $0.EmptyUserListRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listUsers, request, options: options);
  }

  $grpc.ResponseFuture<$0.UserListResponse> listUsersByReferralUsed(
      $0.ReferralListRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listUsersByReferralUsed, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.UserToRoleResponse> assignUserToRole(
      $0.UserToRoleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$assignUserToRole, request, options: options);
  }

  $grpc.ResponseFuture<$0.UserToRoleResponse> removeUserFromRole(
      $0.UserToRoleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removeUserFromRole, request, options: options);
  }

  $grpc.ResponseFuture<$0.CreateRoleResponse> createRole(
      $0.CreateRoleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createRole, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetAllRolesResponse> getAllRoles(
      $0.GetAllRolesRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllRoles, request, options: options);
  }

  $grpc.ResponseFuture<$0.DeleteRoleResponse> deleteRole(
      $0.DeleteRoleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteRole, request, options: options);
  }

  $grpc.ResponseFuture<$0.UpdateRoleResponse> updateRole(
      $0.UpdateRoleRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateRole, request, options: options);
  }

  $grpc.ResponseFuture<$0.UploadImageResponse> uploadImage(
      $async.Stream<$0.UploadDPRequest> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$uploadImage, request, options: options)
        .single;
  }
}

abstract class AuthServiceBase extends $grpc.Service {
  $core.String get $name => 'AuthService';

  AuthServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.LoginRequest, $0.LoginResponse>(
        'Login',
        login_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LoginRequest.fromBuffer(value),
        ($0.LoginResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LoginRequest, $0.VerifyLoginResponse>(
        'LoginNoVerification',
        loginNoVerification_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LoginRequest.fromBuffer(value),
        ($0.VerifyLoginResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LoginWithGoogleRequest,
            $0.LoginWithGoogleResponse>(
        'LoginWithGoogle',
        loginWithGoogle_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.LoginWithGoogleRequest.fromBuffer(value),
        ($0.LoginWithGoogleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AgentLoginRequest, $0.AgentLoginResponse>(
        'LoginAsAgent',
        loginAsAgent_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AgentLoginRequest.fromBuffer(value),
        ($0.AgentLoginResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RegisterRequest, $0.RegisterResponse>(
        'Register',
        register_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RegisterRequest.fromBuffer(value),
        ($0.RegisterResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.InitEmailVerificationRequest,
            $0.InitEmailVerificationResponse>(
        'InitEmailVerification',
        initEmailVerification_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.InitEmailVerificationRequest.fromBuffer(value),
        ($0.InitEmailVerificationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.InitPhoneVerificationRequest,
            $0.InitPhoneVerificationResponse>(
        'InitPhoneVerification',
        initPhoneVerification_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.InitPhoneVerificationRequest.fromBuffer(value),
        ($0.InitPhoneVerificationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PhoneVerificationRequest,
            $0.PhoneVerificationResponse>(
        'VerifyPhone',
        verifyPhone_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PhoneVerificationRequest.fromBuffer(value),
        ($0.PhoneVerificationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmailVerificationRequest,
            $0.EmailVerificationResponse>(
        'VerifyEmail',
        verifyEmail_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.EmailVerificationRequest.fromBuffer(value),
        ($0.EmailVerificationResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.VerifyLoginRequest, $0.VerifyLoginResponse>(
            'VerifyLogin',
            verifyLogin_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.VerifyLoginRequest.fromBuffer(value),
            ($0.VerifyLoginResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LogoutRequest, $0.LogoutResponse>(
        'Logout',
        logout_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LogoutRequest.fromBuffer(value),
        ($0.LogoutResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserRequest, $0.User>(
        'ViewUser',
        viewUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserRequest.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserRequest, $0.UpdatePasswordResponse>(
        'UpdatePassword',
        updatePassword_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserRequest.fromBuffer(value),
        ($0.UpdatePasswordResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.ResetPasswordRequest, $0.ResetPasswordResponse>(
            'ResetPassword',
            resetPassword_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.ResetPasswordRequest.fromBuffer(value),
            ($0.ResetPasswordResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.VerifyPasswordResetRequest,
            $0.VerifyPasswordResetResponse>(
        'VerifyPasswordReset',
        verifyPasswordReset_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.VerifyPasswordResetRequest.fromBuffer(value),
        ($0.VerifyPasswordResetResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SetNewPasswordRequest,
            $0.SetNewPasswordResponse>(
        'SetNewPassword',
        setNewPassword_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.SetNewPasswordRequest.fromBuffer(value),
        ($0.SetNewPasswordResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.GetAllCountryRequest, $0.GetAllCountryResponse>(
            'GetAllCountries',
            getAllCountries_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.GetAllCountryRequest.fromBuffer(value),
            ($0.GetAllCountryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Country, $0.GeneralResponse>(
        'AddCountry',
        addCountry_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Country.fromBuffer(value),
        ($0.GeneralResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CountryRequest, $0.GeneralResponse>(
        'RemoveCountry',
        removeCountry_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CountryRequest.fromBuffer(value),
        ($0.GeneralResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AssignPermissionsToRoleRequest,
            $0.AssignPermissionsToRoleResponse>(
        'AssignPermissionsToRole',
        assignPermissionsToRole_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.AssignPermissionsToRoleRequest.fromBuffer(value),
        ($0.AssignPermissionsToRoleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RemovePermissionsFromRoleRequest,
            $0.RemovePermissionsFromRoleResponse>(
        'RemovePermissionsFromRole',
        removePermissionsFromRole_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.RemovePermissionsFromRoleRequest.fromBuffer(value),
        ($0.RemovePermissionsFromRoleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CreatePermissionRequest,
            $0.CreatePermissionResponse>(
        'CreatePermission',
        createPermission_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.CreatePermissionRequest.fromBuffer(value),
        ($0.CreatePermissionResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetAllPermissionsRequest,
            $0.GetAllPermissionsResponse>(
        'GetAllPermissions',
        getAllPermissions_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetAllPermissionsRequest.fromBuffer(value),
        ($0.GetAllPermissionsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetPermissionsByRoleRequest,
            $0.GetPermissionsByRoleResponse>(
        'GetPermissionsByRole',
        getPermissionsByRole_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetPermissionsByRoleRequest.fromBuffer(value),
        ($0.GetPermissionsByRoleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetPermissionByTitleRequest,
            $0.GetPermissionByTitleResponse>(
        'GetPermissionByTitle',
        getPermissionByTitle_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetPermissionByTitleRequest.fromBuffer(value),
        ($0.GetPermissionByTitleResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.HasPermissionRequest, $0.HasPermissionResponse>(
            'HasPermission',
            hasPermission_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.HasPermissionRequest.fromBuffer(value),
            ($0.HasPermissionResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UpdateUserInformationRequest,
            $0.UpdateUserInformationResponse>(
        'UpdateUserProfile',
        updateUserProfile_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.UpdateUserInformationRequest.fromBuffer(value),
        ($0.UpdateUserInformationResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeleteUserRequest, $0.DeleteUserResponse>(
        'DeleteUser',
        deleteUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DeleteUserRequest.fromBuffer(value),
        ($0.DeleteUserResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.EmptyUserListRequest, $0.UserListResponse>(
            'ListUsers',
            listUsers_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.EmptyUserListRequest.fromBuffer(value),
            ($0.UserListResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ReferralListRequest, $0.UserListResponse>(
        'ListUsersByReferralUsed',
        listUsersByReferralUsed_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ReferralListRequest.fromBuffer(value),
        ($0.UserListResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserToRoleRequest, $0.UserToRoleResponse>(
        'AssignUserToRole',
        assignUserToRole_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserToRoleRequest.fromBuffer(value),
        ($0.UserToRoleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserToRoleRequest, $0.UserToRoleResponse>(
        'RemoveUserFromRole',
        removeUserFromRole_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserToRoleRequest.fromBuffer(value),
        ($0.UserToRoleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CreateRoleRequest, $0.CreateRoleResponse>(
        'CreateRole',
        createRole_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CreateRoleRequest.fromBuffer(value),
        ($0.CreateRoleResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.GetAllRolesRequest, $0.GetAllRolesResponse>(
            'GetAllRoles',
            getAllRoles_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.GetAllRolesRequest.fromBuffer(value),
            ($0.GetAllRolesResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeleteRoleRequest, $0.DeleteRoleResponse>(
        'DeleteRole',
        deleteRole_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DeleteRoleRequest.fromBuffer(value),
        ($0.DeleteRoleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UpdateRoleRequest, $0.UpdateRoleResponse>(
        'UpdateRole',
        updateRole_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UpdateRoleRequest.fromBuffer(value),
        ($0.UpdateRoleResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UploadDPRequest, $0.UploadImageResponse>(
        'UploadImage',
        uploadImage,
        true,
        false,
        ($core.List<$core.int> value) => $0.UploadDPRequest.fromBuffer(value),
        ($0.UploadImageResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.LoginResponse> login_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LoginRequest> request) async {
    return login(call, await request);
  }

  $async.Future<$0.VerifyLoginResponse> loginNoVerification_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LoginRequest> request) async {
    return loginNoVerification(call, await request);
  }

  $async.Future<$0.LoginWithGoogleResponse> loginWithGoogle_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.LoginWithGoogleRequest> request) async {
    return loginWithGoogle(call, await request);
  }

  $async.Future<$0.AgentLoginResponse> loginAsAgent_Pre($grpc.ServiceCall call,
      $async.Future<$0.AgentLoginRequest> request) async {
    return loginAsAgent(call, await request);
  }

  $async.Future<$0.RegisterResponse> register_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RegisterRequest> request) async {
    return register(call, await request);
  }

  $async.Future<$0.InitEmailVerificationResponse> initEmailVerification_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.InitEmailVerificationRequest> request) async {
    return initEmailVerification(call, await request);
  }

  $async.Future<$0.InitPhoneVerificationResponse> initPhoneVerification_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.InitPhoneVerificationRequest> request) async {
    return initPhoneVerification(call, await request);
  }

  $async.Future<$0.PhoneVerificationResponse> verifyPhone_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.PhoneVerificationRequest> request) async {
    return verifyPhone(call, await request);
  }

  $async.Future<$0.EmailVerificationResponse> verifyEmail_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmailVerificationRequest> request) async {
    return verifyEmail(call, await request);
  }

  $async.Future<$0.VerifyLoginResponse> verifyLogin_Pre($grpc.ServiceCall call,
      $async.Future<$0.VerifyLoginRequest> request) async {
    return verifyLogin(call, await request);
  }

  $async.Future<$0.LogoutResponse> logout_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LogoutRequest> request) async {
    return logout(call, await request);
  }

  $async.Future<$0.User> viewUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserRequest> request) async {
    return viewUser(call, await request);
  }

  $async.Future<$0.UpdatePasswordResponse> updatePassword_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserRequest> request) async {
    return updatePassword(call, await request);
  }

  $async.Future<$0.ResetPasswordResponse> resetPassword_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ResetPasswordRequest> request) async {
    return resetPassword(call, await request);
  }

  $async.Future<$0.VerifyPasswordResetResponse> verifyPasswordReset_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.VerifyPasswordResetRequest> request) async {
    return verifyPasswordReset(call, await request);
  }

  $async.Future<$0.SetNewPasswordResponse> setNewPassword_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.SetNewPasswordRequest> request) async {
    return setNewPassword(call, await request);
  }

  $async.Future<$0.GetAllCountryResponse> getAllCountries_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAllCountryRequest> request) async {
    return getAllCountries(call, await request);
  }

  $async.Future<$0.GeneralResponse> addCountry_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Country> request) async {
    return addCountry(call, await request);
  }

  $async.Future<$0.GeneralResponse> removeCountry_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CountryRequest> request) async {
    return removeCountry(call, await request);
  }

  $async.Future<$0.AssignPermissionsToRoleResponse> assignPermissionsToRole_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.AssignPermissionsToRoleRequest> request) async {
    return assignPermissionsToRole(call, await request);
  }

  $async.Future<$0.RemovePermissionsFromRoleResponse>
      removePermissionsFromRole_Pre($grpc.ServiceCall call,
          $async.Future<$0.RemovePermissionsFromRoleRequest> request) async {
    return removePermissionsFromRole(call, await request);
  }

  $async.Future<$0.CreatePermissionResponse> createPermission_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.CreatePermissionRequest> request) async {
    return createPermission(call, await request);
  }

  $async.Future<$0.GetAllPermissionsResponse> getAllPermissions_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetAllPermissionsRequest> request) async {
    return getAllPermissions(call, await request);
  }

  $async.Future<$0.GetPermissionsByRoleResponse> getPermissionsByRole_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetPermissionsByRoleRequest> request) async {
    return getPermissionsByRole(call, await request);
  }

  $async.Future<$0.GetPermissionByTitleResponse> getPermissionByTitle_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetPermissionByTitleRequest> request) async {
    return getPermissionByTitle(call, await request);
  }

  $async.Future<$0.HasPermissionResponse> hasPermission_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.HasPermissionRequest> request) async {
    return hasPermission(call, await request);
  }

  $async.Future<$0.UpdateUserInformationResponse> updateUserProfile_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.UpdateUserInformationRequest> request) async {
    return updateUserProfile(call, await request);
  }

  $async.Future<$0.DeleteUserResponse> deleteUser_Pre($grpc.ServiceCall call,
      $async.Future<$0.DeleteUserRequest> request) async {
    return deleteUser(call, await request);
  }

  $async.Future<$0.UserListResponse> listUsers_Pre($grpc.ServiceCall call,
      $async.Future<$0.EmptyUserListRequest> request) async {
    return listUsers(call, await request);
  }

  $async.Future<$0.UserListResponse> listUsersByReferralUsed_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ReferralListRequest> request) async {
    return listUsersByReferralUsed(call, await request);
  }

  $async.Future<$0.UserToRoleResponse> assignUserToRole_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.UserToRoleRequest> request) async {
    return assignUserToRole(call, await request);
  }

  $async.Future<$0.UserToRoleResponse> removeUserFromRole_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.UserToRoleRequest> request) async {
    return removeUserFromRole(call, await request);
  }

  $async.Future<$0.CreateRoleResponse> createRole_Pre($grpc.ServiceCall call,
      $async.Future<$0.CreateRoleRequest> request) async {
    return createRole(call, await request);
  }

  $async.Future<$0.GetAllRolesResponse> getAllRoles_Pre($grpc.ServiceCall call,
      $async.Future<$0.GetAllRolesRequest> request) async {
    return getAllRoles(call, await request);
  }

  $async.Future<$0.DeleteRoleResponse> deleteRole_Pre($grpc.ServiceCall call,
      $async.Future<$0.DeleteRoleRequest> request) async {
    return deleteRole(call, await request);
  }

  $async.Future<$0.UpdateRoleResponse> updateRole_Pre($grpc.ServiceCall call,
      $async.Future<$0.UpdateRoleRequest> request) async {
    return updateRole(call, await request);
  }

  $async.Future<$0.LoginResponse> login(
      $grpc.ServiceCall call, $0.LoginRequest request);
  $async.Future<$0.VerifyLoginResponse> loginNoVerification(
      $grpc.ServiceCall call, $0.LoginRequest request);
  $async.Future<$0.LoginWithGoogleResponse> loginWithGoogle(
      $grpc.ServiceCall call, $0.LoginWithGoogleRequest request);
  $async.Future<$0.AgentLoginResponse> loginAsAgent(
      $grpc.ServiceCall call, $0.AgentLoginRequest request);
  $async.Future<$0.RegisterResponse> register(
      $grpc.ServiceCall call, $0.RegisterRequest request);
  $async.Future<$0.InitEmailVerificationResponse> initEmailVerification(
      $grpc.ServiceCall call, $0.InitEmailVerificationRequest request);
  $async.Future<$0.InitPhoneVerificationResponse> initPhoneVerification(
      $grpc.ServiceCall call, $0.InitPhoneVerificationRequest request);
  $async.Future<$0.PhoneVerificationResponse> verifyPhone(
      $grpc.ServiceCall call, $0.PhoneVerificationRequest request);
  $async.Future<$0.EmailVerificationResponse> verifyEmail(
      $grpc.ServiceCall call, $0.EmailVerificationRequest request);
  $async.Future<$0.VerifyLoginResponse> verifyLogin(
      $grpc.ServiceCall call, $0.VerifyLoginRequest request);
  $async.Future<$0.LogoutResponse> logout(
      $grpc.ServiceCall call, $0.LogoutRequest request);
  $async.Future<$0.User> viewUser(
      $grpc.ServiceCall call, $0.UserRequest request);
  $async.Future<$0.UpdatePasswordResponse> updatePassword(
      $grpc.ServiceCall call, $0.UserRequest request);
  $async.Future<$0.ResetPasswordResponse> resetPassword(
      $grpc.ServiceCall call, $0.ResetPasswordRequest request);
  $async.Future<$0.VerifyPasswordResetResponse> verifyPasswordReset(
      $grpc.ServiceCall call, $0.VerifyPasswordResetRequest request);
  $async.Future<$0.SetNewPasswordResponse> setNewPassword(
      $grpc.ServiceCall call, $0.SetNewPasswordRequest request);
  $async.Future<$0.GetAllCountryResponse> getAllCountries(
      $grpc.ServiceCall call, $0.GetAllCountryRequest request);
  $async.Future<$0.GeneralResponse> addCountry(
      $grpc.ServiceCall call, $0.Country request);
  $async.Future<$0.GeneralResponse> removeCountry(
      $grpc.ServiceCall call, $0.CountryRequest request);
  $async.Future<$0.AssignPermissionsToRoleResponse> assignPermissionsToRole(
      $grpc.ServiceCall call, $0.AssignPermissionsToRoleRequest request);
  $async.Future<$0.RemovePermissionsFromRoleResponse> removePermissionsFromRole(
      $grpc.ServiceCall call, $0.RemovePermissionsFromRoleRequest request);
  $async.Future<$0.CreatePermissionResponse> createPermission(
      $grpc.ServiceCall call, $0.CreatePermissionRequest request);
  $async.Future<$0.GetAllPermissionsResponse> getAllPermissions(
      $grpc.ServiceCall call, $0.GetAllPermissionsRequest request);
  $async.Future<$0.GetPermissionsByRoleResponse> getPermissionsByRole(
      $grpc.ServiceCall call, $0.GetPermissionsByRoleRequest request);
  $async.Future<$0.GetPermissionByTitleResponse> getPermissionByTitle(
      $grpc.ServiceCall call, $0.GetPermissionByTitleRequest request);
  $async.Future<$0.HasPermissionResponse> hasPermission(
      $grpc.ServiceCall call, $0.HasPermissionRequest request);
  $async.Future<$0.UpdateUserInformationResponse> updateUserProfile(
      $grpc.ServiceCall call, $0.UpdateUserInformationRequest request);
  $async.Future<$0.DeleteUserResponse> deleteUser(
      $grpc.ServiceCall call, $0.DeleteUserRequest request);
  $async.Future<$0.UserListResponse> listUsers(
      $grpc.ServiceCall call, $0.EmptyUserListRequest request);
  $async.Future<$0.UserListResponse> listUsersByReferralUsed(
      $grpc.ServiceCall call, $0.ReferralListRequest request);
  $async.Future<$0.UserToRoleResponse> assignUserToRole(
      $grpc.ServiceCall call, $0.UserToRoleRequest request);
  $async.Future<$0.UserToRoleResponse> removeUserFromRole(
      $grpc.ServiceCall call, $0.UserToRoleRequest request);
  $async.Future<$0.CreateRoleResponse> createRole(
      $grpc.ServiceCall call, $0.CreateRoleRequest request);
  $async.Future<$0.GetAllRolesResponse> getAllRoles(
      $grpc.ServiceCall call, $0.GetAllRolesRequest request);
  $async.Future<$0.DeleteRoleResponse> deleteRole(
      $grpc.ServiceCall call, $0.DeleteRoleRequest request);
  $async.Future<$0.UpdateRoleResponse> updateRole(
      $grpc.ServiceCall call, $0.UpdateRoleRequest request);
  $async.Future<$0.UploadImageResponse> uploadImage(
      $grpc.ServiceCall call, $async.Stream<$0.UploadDPRequest> request);
}
