///
//  Generated code. Do not modify.
//  source: menu.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use customPackageDescriptor instead')
const CustomPackage$json = const {
  '1': 'CustomPackage',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'themeUrl', '3': 2, '4': 1, '5': 9, '10': 'themeUrl'},
    const {'1': 'items', '3': 3, '4': 3, '5': 11, '6': '.menu_service.v1.ItemData', '10': 'items'},
    const {'1': 'userID', '3': 4, '4': 1, '5': 9, '10': 'userID'},
    const {'1': 'updatedAt', '3': 5, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'createdAt', '3': 6, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'ID', '3': 7, '4': 1, '5': 9, '10': 'ID'},
    const {'1': 'estimatedPrice', '3': 8, '4': 1, '5': 1, '10': 'estimatedPrice'},
  ],
};

/// Descriptor for `CustomPackage`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List customPackageDescriptor = $convert.base64Decode('Cg1DdXN0b21QYWNrYWdlEhIKBG5hbWUYASABKAlSBG5hbWUSGgoIdGhlbWVVcmwYAiABKAlSCHRoZW1lVXJsEi8KBWl0ZW1zGAMgAygLMhkubWVudV9zZXJ2aWNlLnYxLkl0ZW1EYXRhUgVpdGVtcxIWCgZ1c2VySUQYBCABKAlSBnVzZXJJRBIcCgl1cGRhdGVkQXQYBSABKAlSCXVwZGF0ZWRBdBIcCgljcmVhdGVkQXQYBiABKAlSCWNyZWF0ZWRBdBIOCgJJRBgHIAEoCVICSUQSJgoOZXN0aW1hdGVkUHJpY2UYCCABKAFSDmVzdGltYXRlZFByaWNl');
@$core.Deprecated('Use listCustomPackageRequestDescriptor instead')
const ListCustomPackageRequest$json = const {
  '1': 'ListCustomPackageRequest',
  '2': const [
    const {'1': 'userID', '3': 1, '4': 1, '5': 9, '10': 'userID'},
    const {'1': 'limit', '3': 2, '4': 1, '5': 3, '10': 'limit'},
    const {'1': 'page', '3': 3, '4': 1, '5': 3, '10': 'page'},
  ],
};

/// Descriptor for `ListCustomPackageRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listCustomPackageRequestDescriptor = $convert.base64Decode('ChhMaXN0Q3VzdG9tUGFja2FnZVJlcXVlc3QSFgoGdXNlcklEGAEgASgJUgZ1c2VySUQSFAoFbGltaXQYAiABKANSBWxpbWl0EhIKBHBhZ2UYAyABKANSBHBhZ2U=');
@$core.Deprecated('Use addCustomPackageRequestDescriptor instead')
const AddCustomPackageRequest$json = const {
  '1': 'AddCustomPackageRequest',
  '2': const [
    const {'1': 'customPackageID', '3': 1, '4': 1, '5': 9, '10': 'customPackageID'},
    const {'1': 'itemName', '3': 2, '4': 1, '5': 9, '10': 'itemName'},
    const {'1': 'itemImage', '3': 3, '4': 1, '5': 9, '10': 'itemImage'},
    const {'1': 'itemPrice', '3': 4, '4': 1, '5': 1, '10': 'itemPrice'},
  ],
};

/// Descriptor for `AddCustomPackageRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addCustomPackageRequestDescriptor = $convert.base64Decode('ChdBZGRDdXN0b21QYWNrYWdlUmVxdWVzdBIoCg9jdXN0b21QYWNrYWdlSUQYASABKAlSD2N1c3RvbVBhY2thZ2VJRBIaCghpdGVtTmFtZRgCIAEoCVIIaXRlbU5hbWUSHAoJaXRlbUltYWdlGAMgASgJUglpdGVtSW1hZ2USHAoJaXRlbVByaWNlGAQgASgBUglpdGVtUHJpY2U=');
@$core.Deprecated('Use listCustomPackageResponseDescriptor instead')
const ListCustomPackageResponse$json = const {
  '1': 'ListCustomPackageResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.CustomPackage', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'total', '3': 4, '4': 1, '5': 3, '10': 'total'},
    const {'1': 'pageCount', '3': 5, '4': 1, '5': 3, '10': 'pageCount'},
  ],
};

/// Descriptor for `ListCustomPackageResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listCustomPackageResponseDescriptor = $convert.base64Decode('ChlMaXN0Q3VzdG9tUGFja2FnZVJlc3BvbnNlEjIKBGRhdGEYASADKAsyHi5tZW51X3NlcnZpY2UudjEuQ3VzdG9tUGFja2FnZVIEZGF0YRIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cxIYCgdtZXNzYWdlGAMgASgJUgdtZXNzYWdlEhQKBXRvdGFsGAQgASgDUgV0b3RhbBIcCglwYWdlQ291bnQYBSABKANSCXBhZ2VDb3VudA==');
@$core.Deprecated('Use imageInfoDescriptor instead')
const ImageInfo$json = const {
  '1': 'ImageInfo',
  '2': const [
    const {'1': 'section', '3': 1, '4': 1, '5': 9, '10': 'section'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'imageType', '3': 3, '4': 1, '5': 9, '10': 'imageType'},
    const {'1': 'serviceAreaID', '3': 4, '4': 1, '5': 9, '10': 'serviceAreaID'},
    const {'1': 'title', '3': 5, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'sectionID', '3': 6, '4': 1, '5': 9, '10': 'sectionID'},
  ],
};

/// Descriptor for `ImageInfo`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List imageInfoDescriptor = $convert.base64Decode('CglJbWFnZUluZm8SGAoHc2VjdGlvbhgBIAEoCVIHc2VjdGlvbhISCgRuYW1lGAIgASgJUgRuYW1lEhwKCWltYWdlVHlwZRgDIAEoCVIJaW1hZ2VUeXBlEiQKDXNlcnZpY2VBcmVhSUQYBCABKAlSDXNlcnZpY2VBcmVhSUQSFAoFdGl0bGUYBSABKAlSBXRpdGxlEhwKCXNlY3Rpb25JRBgGIAEoCVIJc2VjdGlvbklE');
@$core.Deprecated('Use uploadImageRequestDescriptor instead')
const UploadImageRequest$json = const {
  '1': 'UploadImageRequest',
  '2': const [
    const {'1': 'info', '3': 1, '4': 1, '5': 11, '6': '.menu_service.v1.ImageInfo', '9': 0, '10': 'info'},
    const {'1': 'chunk_data', '3': 2, '4': 1, '5': 12, '9': 0, '10': 'chunkData'},
  ],
  '8': const [
    const {'1': 'data'},
  ],
};

/// Descriptor for `UploadImageRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List uploadImageRequestDescriptor = $convert.base64Decode('ChJVcGxvYWRJbWFnZVJlcXVlc3QSMAoEaW5mbxgBIAEoCzIaLm1lbnVfc2VydmljZS52MS5JbWFnZUluZm9IAFIEaW5mbxIfCgpjaHVua19kYXRhGAIgASgMSABSCWNodW5rRGF0YUIGCgRkYXRh');
@$core.Deprecated('Use itemTagCreationDescriptor instead')
const ItemTagCreation$json = const {
  '1': 'ItemTagCreation',
  '2': const [
    const {'1': 'tagName', '3': 1, '4': 1, '5': 9, '10': 'tagName'},
    const {'1': 'itemID', '3': 2, '4': 1, '5': 9, '10': 'itemID'},
  ],
};

/// Descriptor for `ItemTagCreation`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemTagCreationDescriptor = $convert.base64Decode('Cg9JdGVtVGFnQ3JlYXRpb24SGAoHdGFnTmFtZRgBIAEoCVIHdGFnTmFtZRIWCgZpdGVtSUQYAiABKAlSBml0ZW1JRA==');
@$core.Deprecated('Use itemTagRequestDescriptor instead')
const ItemTagRequest$json = const {
  '1': 'ItemTagRequest',
  '2': const [
    const {'1': 'tagID', '3': 1, '4': 1, '5': 9, '10': 'tagID'},
    const {'1': 'itemID', '3': 2, '4': 1, '5': 9, '10': 'itemID'},
  ],
};

/// Descriptor for `ItemTagRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemTagRequestDescriptor = $convert.base64Decode('Cg5JdGVtVGFnUmVxdWVzdBIUCgV0YWdJRBgBIAEoCVIFdGFnSUQSFgoGaXRlbUlEGAIgASgJUgZpdGVtSUQ=');
@$core.Deprecated('Use itemTagDescriptor instead')
const ItemTag$json = const {
  '1': 'ItemTag',
  '2': const [
    const {'1': 'tagName', '3': 1, '4': 1, '5': 9, '10': 'tagName'},
    const {'1': 'itemID', '3': 2, '4': 1, '5': 9, '10': 'itemID'},
    const {'1': 'created_at', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 4, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'tagID', '3': 5, '4': 1, '5': 9, '10': 'tagID'},
  ],
};

/// Descriptor for `ItemTag`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemTagDescriptor = $convert.base64Decode('CgdJdGVtVGFnEhgKB3RhZ05hbWUYASABKAlSB3RhZ05hbWUSFgoGaXRlbUlEGAIgASgJUgZpdGVtSUQSHQoKY3JlYXRlZF9hdBgDIAEoCVIJY3JlYXRlZEF0Eh0KCnVwZGF0ZWRfYXQYBCABKAlSCXVwZGF0ZWRBdBIUCgV0YWdJRBgFIAEoCVIFdGFnSUQ=');
@$core.Deprecated('Use itemTagResponseDescriptor instead')
const ItemTagResponse$json = const {
  '1': 'ItemTagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `ItemTagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemTagResponseDescriptor = $convert.base64Decode('Cg9JdGVtVGFnUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use viewItemTagResponseDescriptor instead')
const ViewItemTagResponse$json = const {
  '1': 'ViewItemTagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 11, '6': '.menu_service.v1.ItemTag', '10': 'data'},
  ],
};

/// Descriptor for `ViewItemTagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewItemTagResponseDescriptor = $convert.base64Decode('ChNWaWV3SXRlbVRhZ1Jlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USLAoEZGF0YRgDIAEoCzIYLm1lbnVfc2VydmljZS52MS5JdGVtVGFnUgRkYXRh');
@$core.Deprecated('Use listItemTagResponseDescriptor instead')
const ListItemTagResponse$json = const {
  '1': 'ListItemTagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.menu_service.v1.ItemTag', '10': 'data'},
  ],
};

/// Descriptor for `ListItemTagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listItemTagResponseDescriptor = $convert.base64Decode('ChNMaXN0SXRlbVRhZ1Jlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USLAoEZGF0YRgDIAMoCzIYLm1lbnVfc2VydmljZS52MS5JdGVtVGFnUgRkYXRh');
@$core.Deprecated('Use packageTagRequestDescriptor instead')
const PackageTagRequest$json = const {
  '1': 'PackageTagRequest',
  '2': const [
    const {'1': 'tagName', '3': 1, '4': 1, '5': 9, '10': 'tagName'},
    const {'1': 'packageID', '3': 2, '4': 1, '5': 9, '10': 'packageID'},
  ],
};

/// Descriptor for `PackageTagRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageTagRequestDescriptor = $convert.base64Decode('ChFQYWNrYWdlVGFnUmVxdWVzdBIYCgd0YWdOYW1lGAEgASgJUgd0YWdOYW1lEhwKCXBhY2thZ2VJRBgCIAEoCVIJcGFja2FnZUlE');
@$core.Deprecated('Use packageTagRequestItemDescriptor instead')
const PackageTagRequestItem$json = const {
  '1': 'PackageTagRequestItem',
  '2': const [
    const {'1': 'tagID', '3': 1, '4': 1, '5': 9, '10': 'tagID'},
    const {'1': 'packageID', '3': 2, '4': 1, '5': 9, '10': 'packageID'},
  ],
};

/// Descriptor for `PackageTagRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageTagRequestItemDescriptor = $convert.base64Decode('ChVQYWNrYWdlVGFnUmVxdWVzdEl0ZW0SFAoFdGFnSUQYASABKAlSBXRhZ0lEEhwKCXBhY2thZ2VJRBgCIAEoCVIJcGFja2FnZUlE');
@$core.Deprecated('Use packageTagItemDescriptor instead')
const PackageTagItem$json = const {
  '1': 'PackageTagItem',
  '2': const [
    const {'1': 'tagName', '3': 1, '4': 1, '5': 9, '10': 'tagName'},
    const {'1': 'packageID', '3': 2, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'created_at', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 4, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'tagID', '3': 5, '4': 1, '5': 9, '10': 'tagID'},
  ],
};

/// Descriptor for `PackageTagItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageTagItemDescriptor = $convert.base64Decode('Cg5QYWNrYWdlVGFnSXRlbRIYCgd0YWdOYW1lGAEgASgJUgd0YWdOYW1lEhwKCXBhY2thZ2VJRBgCIAEoCVIJcGFja2FnZUlEEh0KCmNyZWF0ZWRfYXQYAyABKAlSCWNyZWF0ZWRBdBIdCgp1cGRhdGVkX2F0GAQgASgJUgl1cGRhdGVkQXQSFAoFdGFnSUQYBSABKAlSBXRhZ0lE');
@$core.Deprecated('Use packageTagResponseDescriptor instead')
const PackageTagResponse$json = const {
  '1': 'PackageTagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `PackageTagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageTagResponseDescriptor = $convert.base64Decode('ChJQYWNrYWdlVGFnUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZQ==');
@$core.Deprecated('Use viewPackageTagResponseDescriptor instead')
const ViewPackageTagResponse$json = const {
  '1': 'ViewPackageTagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 11, '6': '.menu_service.v1.PackageTagItem', '10': 'data'},
  ],
};

/// Descriptor for `ViewPackageTagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewPackageTagResponseDescriptor = $convert.base64Decode('ChZWaWV3UGFja2FnZVRhZ1Jlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USMwoEZGF0YRgDIAEoCzIfLm1lbnVfc2VydmljZS52MS5QYWNrYWdlVGFnSXRlbVIEZGF0YQ==');
@$core.Deprecated('Use listPackageTagResponseDescriptor instead')
const ListPackageTagResponse$json = const {
  '1': 'ListPackageTagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.menu_service.v1.PackageTagItem', '10': 'data'},
  ],
};

/// Descriptor for `ListPackageTagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listPackageTagResponseDescriptor = $convert.base64Decode('ChZMaXN0UGFja2FnZVRhZ1Jlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USMwoEZGF0YRgDIAMoCzIfLm1lbnVfc2VydmljZS52MS5QYWNrYWdlVGFnSXRlbVIEZGF0YQ==');
@$core.Deprecated('Use addPackageCategoryRequestDescriptor instead')
const AddPackageCategoryRequest$json = const {
  '1': 'AddPackageCategoryRequest',
  '2': const [
    const {'1': 'categoryName', '3': 1, '4': 1, '5': 9, '10': 'categoryName'},
    const {'1': 'icon', '3': 2, '4': 1, '5': 9, '10': 'icon'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'image', '3': 4, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'serviceAreaID', '3': 5, '4': 1, '5': 9, '10': 'serviceAreaID'},
  ],
};

/// Descriptor for `AddPackageCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addPackageCategoryRequestDescriptor = $convert.base64Decode('ChlBZGRQYWNrYWdlQ2F0ZWdvcnlSZXF1ZXN0EiIKDGNhdGVnb3J5TmFtZRgBIAEoCVIMY2F0ZWdvcnlOYW1lEhIKBGljb24YAiABKAlSBGljb24SIAoLZGVzY3JpcHRpb24YAyABKAlSC2Rlc2NyaXB0aW9uEhQKBWltYWdlGAQgASgJUgVpbWFnZRIkCg1zZXJ2aWNlQXJlYUlEGAUgASgJUg1zZXJ2aWNlQXJlYUlE');
@$core.Deprecated('Use packageCategoryItemDescriptor instead')
const PackageCategoryItem$json = const {
  '1': 'PackageCategoryItem',
  '2': const [
    const {'1': 'categoryName', '3': 1, '4': 1, '5': 9, '10': 'categoryName'},
    const {'1': 'icon', '3': 2, '4': 1, '5': 9, '10': 'icon'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'image', '3': 4, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'categoryID', '3': 5, '4': 1, '5': 9, '10': 'categoryID'},
    const {'1': 'serviceAreaID', '3': 6, '4': 1, '5': 9, '10': 'serviceAreaID'},
  ],
};

/// Descriptor for `PackageCategoryItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageCategoryItemDescriptor = $convert.base64Decode('ChNQYWNrYWdlQ2F0ZWdvcnlJdGVtEiIKDGNhdGVnb3J5TmFtZRgBIAEoCVIMY2F0ZWdvcnlOYW1lEhIKBGljb24YAiABKAlSBGljb24SIAoLZGVzY3JpcHRpb24YAyABKAlSC2Rlc2NyaXB0aW9uEhQKBWltYWdlGAQgASgJUgVpbWFnZRIeCgpjYXRlZ29yeUlEGAUgASgJUgpjYXRlZ29yeUlEEiQKDXNlcnZpY2VBcmVhSUQYBiABKAlSDXNlcnZpY2VBcmVhSUQ=');
@$core.Deprecated('Use packageCategoryRequestDescriptor instead')
const PackageCategoryRequest$json = const {
  '1': 'PackageCategoryRequest',
  '2': const [
    const {'1': 'categoryID', '3': 1, '4': 1, '5': 9, '10': 'categoryID'},
  ],
};

/// Descriptor for `PackageCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageCategoryRequestDescriptor = $convert.base64Decode('ChZQYWNrYWdlQ2F0ZWdvcnlSZXF1ZXN0Eh4KCmNhdGVnb3J5SUQYASABKAlSCmNhdGVnb3J5SUQ=');
@$core.Deprecated('Use listPackageCategoryResponseDescriptor instead')
const ListPackageCategoryResponse$json = const {
  '1': 'ListPackageCategoryResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.PackageCategoryItem', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ListPackageCategoryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listPackageCategoryResponseDescriptor = $convert.base64Decode('ChtMaXN0UGFja2FnZUNhdGVnb3J5UmVzcG9uc2USOAoEZGF0YRgBIAMoCzIkLm1lbnVfc2VydmljZS52MS5QYWNrYWdlQ2F0ZWdvcnlJdGVtUgRkYXRhEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USFgoGc3RhdHVzGAMgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use viewPackageCategoryResponseDescriptor instead')
const ViewPackageCategoryResponse$json = const {
  '1': 'ViewPackageCategoryResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.menu_service.v1.PackageCategoryItem', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ViewPackageCategoryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewPackageCategoryResponseDescriptor = $convert.base64Decode('ChtWaWV3UGFja2FnZUNhdGVnb3J5UmVzcG9uc2USOAoEZGF0YRgBIAEoCzIkLm1lbnVfc2VydmljZS52MS5QYWNrYWdlQ2F0ZWdvcnlJdGVtUgRkYXRhEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USFgoGc3RhdHVzGAMgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use emptyMenuRequestDescriptor instead')
const EmptyMenuRequest$json = const {
  '1': 'EmptyMenuRequest',
};

/// Descriptor for `EmptyMenuRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyMenuRequestDescriptor = $convert.base64Decode('ChBFbXB0eU1lbnVSZXF1ZXN0');
@$core.Deprecated('Use decreaseMenuItemDescriptor instead')
const DecreaseMenuItem$json = const {
  '1': 'DecreaseMenuItem',
  '2': const [
    const {'1': 'menuID', '3': 1, '4': 1, '5': 9, '10': 'menuID'},
    const {'1': 'quantity', '3': 2, '4': 1, '5': 5, '10': 'quantity'},
  ],
};

/// Descriptor for `DecreaseMenuItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List decreaseMenuItemDescriptor = $convert.base64Decode('ChBEZWNyZWFzZU1lbnVJdGVtEhYKBm1lbnVJRBgBIAEoCVIGbWVudUlEEhoKCHF1YW50aXR5GAIgASgFUghxdWFudGl0eQ==');
@$core.Deprecated('Use decreaseMenuRequestDescriptor instead')
const DecreaseMenuRequest$json = const {
  '1': 'DecreaseMenuRequest',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.DecreaseMenuItem', '10': 'items'},
  ],
};

/// Descriptor for `DecreaseMenuRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List decreaseMenuRequestDescriptor = $convert.base64Decode('ChNEZWNyZWFzZU1lbnVSZXF1ZXN0EjcKBWl0ZW1zGAEgAygLMiEubWVudV9zZXJ2aWNlLnYxLkRlY3JlYXNlTWVudUl0ZW1SBWl0ZW1z');
@$core.Deprecated('Use menuRequestItemDescriptor instead')
const MenuRequestItem$json = const {
  '1': 'MenuRequestItem',
  '2': const [
    const {'1': 'requestId', '3': 1, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'type', '3': 2, '4': 1, '5': 9, '10': 'type'},
  ],
};

/// Descriptor for `MenuRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuRequestItemDescriptor = $convert.base64Decode('Cg9NZW51UmVxdWVzdEl0ZW0SHAoJcmVxdWVzdElkGAEgASgJUglyZXF1ZXN0SWQSEgoEdHlwZRgCIAEoCVIEdHlwZQ==');
@$core.Deprecated('Use menuItemsDescriptor instead')
const MenuItems$json = const {
  '1': 'MenuItems',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'category', '3': 2, '4': 1, '5': 9, '10': 'category'},
    const {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'location', '3': 4, '4': 1, '5': 9, '10': 'location'},
    const {'1': 'restaurant', '3': 5, '4': 1, '5': 9, '10': 'restaurant'},
    const {'1': 'details', '3': 6, '4': 1, '5': 9, '10': 'details'},
    const {'1': 'price', '3': 7, '4': 1, '5': 1, '10': 'price'},
    const {'1': 'image', '3': 8, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'quantity', '3': 9, '4': 1, '5': 5, '10': 'quantity'},
    const {'1': 'rate', '3': 10, '4': 1, '5': 1, '10': 'rate'},
    const {'1': 'in_stock_quantity', '3': 11, '4': 1, '5': 5, '10': 'inStockQuantity'},
    const {'1': 'discount', '3': 12, '4': 1, '5': 1, '10': 'discount'},
  ],
};

/// Descriptor for `MenuItems`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuItemsDescriptor = $convert.base64Decode('CglNZW51SXRlbXMSDgoCaWQYASABKAlSAmlkEhoKCGNhdGVnb3J5GAIgASgJUghjYXRlZ29yeRISCgRuYW1lGAMgASgJUgRuYW1lEhoKCGxvY2F0aW9uGAQgASgJUghsb2NhdGlvbhIeCgpyZXN0YXVyYW50GAUgASgJUgpyZXN0YXVyYW50EhgKB2RldGFpbHMYBiABKAlSB2RldGFpbHMSFAoFcHJpY2UYByABKAFSBXByaWNlEhQKBWltYWdlGAggASgJUgVpbWFnZRIaCghxdWFudGl0eRgJIAEoBVIIcXVhbnRpdHkSEgoEcmF0ZRgKIAEoAVIEcmF0ZRIqChFpbl9zdG9ja19xdWFudGl0eRgLIAEoBVIPaW5TdG9ja1F1YW50aXR5EhoKCGRpc2NvdW50GAwgASgBUghkaXNjb3VudA==');
@$core.Deprecated('Use menuResponseDescriptor instead')
const MenuResponse$json = const {
  '1': 'MenuResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 9, '10': 'data'},
  ],
};

/// Descriptor for `MenuResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuResponseDescriptor = $convert.base64Decode('CgxNZW51UmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRISCgRkYXRhGAMgASgJUgRkYXRh');
@$core.Deprecated('Use listMenuResponseDescriptor instead')
const ListMenuResponse$json = const {
  '1': 'ListMenuResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.MenuItems', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `ListMenuResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listMenuResponseDescriptor = $convert.base64Decode('ChBMaXN0TWVudVJlc3BvbnNlEi4KBGRhdGEYASADKAsyGi5tZW51X3NlcnZpY2UudjEuTWVudUl0ZW1zUgRkYXRhEhYKBnN0YXR1cxgCIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAyABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use viewMenuResponseDescriptor instead')
const ViewMenuResponse$json = const {
  '1': 'ViewMenuResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'data', '3': 2, '4': 1, '5': 11, '6': '.menu_service.v1.MenuItems', '10': 'data'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `ViewMenuResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewMenuResponseDescriptor = $convert.base64Decode('ChBWaWV3TWVudVJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEi4KBGRhdGEYAiABKAsyGi5tZW51X3NlcnZpY2UudjEuTWVudUl0ZW1zUgRkYXRhEhgKB21lc3NhZ2UYAyABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use itemDescriptor instead')
const Item$json = const {
  '1': 'Item',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'itemCategoryID', '3': 4, '4': 1, '5': 9, '10': 'itemCategoryID'},
    const {'1': 'image', '3': 5, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'unit', '3': 6, '4': 1, '5': 9, '10': 'unit'},
    const {'1': 'created_at', '3': 7, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 8, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `Item`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDescriptor = $convert.base64Decode('CgRJdGVtEg4KAmlkGAEgASgJUgJpZBISCgRuYW1lGAIgASgJUgRuYW1lEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbhImCg5pdGVtQ2F0ZWdvcnlJRBgEIAEoCVIOaXRlbUNhdGVnb3J5SUQSFAoFaW1hZ2UYBSABKAlSBWltYWdlEhIKBHVuaXQYBiABKAlSBHVuaXQSHQoKY3JlYXRlZF9hdBgHIAEoCVIJY3JlYXRlZEF0Eh0KCnVwZGF0ZWRfYXQYCCABKAlSCXVwZGF0ZWRBdA==');
@$core.Deprecated('Use listItemsResponseDescriptor instead')
const ListItemsResponse$json = const {
  '1': 'ListItemsResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.Item', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ListItemsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listItemsResponseDescriptor = $convert.base64Decode('ChFMaXN0SXRlbXNSZXNwb25zZRIpCgRkYXRhGAEgAygLMhUubWVudV9zZXJ2aWNlLnYxLkl0ZW1SBGRhdGESGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIWCgZzdGF0dXMYAyABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use itemRequestDescriptor instead')
const ItemRequest$json = const {
  '1': 'ItemRequest',
  '2': const [
    const {'1': 'itemID', '3': 1, '4': 1, '5': 9, '10': 'itemID'},
  ],
};

/// Descriptor for `ItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemRequestDescriptor = $convert.base64Decode('CgtJdGVtUmVxdWVzdBIWCgZpdGVtSUQYASABKAlSBml0ZW1JRA==');
@$core.Deprecated('Use itemCategoryDescriptor instead')
const ItemCategory$json = const {
  '1': 'ItemCategory',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'image', '3': 4, '4': 1, '5': 9, '10': 'image'},
  ],
};

/// Descriptor for `ItemCategory`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemCategoryDescriptor = $convert.base64Decode('CgxJdGVtQ2F0ZWdvcnkSDgoCaWQYASABKAlSAmlkEhIKBG5hbWUYAiABKAlSBG5hbWUSIAoLZGVzY3JpcHRpb24YAyABKAlSC2Rlc2NyaXB0aW9uEhQKBWltYWdlGAQgASgJUgVpbWFnZQ==');
@$core.Deprecated('Use listItemCategoryResponseDescriptor instead')
const ListItemCategoryResponse$json = const {
  '1': 'ListItemCategoryResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.ItemCategory', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ListItemCategoryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listItemCategoryResponseDescriptor = $convert.base64Decode('ChhMaXN0SXRlbUNhdGVnb3J5UmVzcG9uc2USMQoEZGF0YRgBIAMoCzIdLm1lbnVfc2VydmljZS52MS5JdGVtQ2F0ZWdvcnlSBGRhdGESGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIWCgZzdGF0dXMYAyABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use itemCategoryRequestDescriptor instead')
const ItemCategoryRequest$json = const {
  '1': 'ItemCategoryRequest',
  '2': const [
    const {'1': 'itemCategoryID', '3': 1, '4': 1, '5': 9, '10': 'itemCategoryID'},
  ],
};

/// Descriptor for `ItemCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemCategoryRequestDescriptor = $convert.base64Decode('ChNJdGVtQ2F0ZWdvcnlSZXF1ZXN0EiYKDml0ZW1DYXRlZ29yeUlEGAEgASgJUg5pdGVtQ2F0ZWdvcnlJRA==');
@$core.Deprecated('Use packageItemCreateDescriptor instead')
const PackageItemCreate$json = const {
  '1': 'PackageItemCreate',
  '2': const [
    const {'1': 'packageID', '3': 1, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'category', '3': 2, '4': 1, '5': 9, '10': 'category'},
  ],
};

/// Descriptor for `PackageItemCreate`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageItemCreateDescriptor = $convert.base64Decode('ChFQYWNrYWdlSXRlbUNyZWF0ZRIcCglwYWNrYWdlSUQYASABKAlSCXBhY2thZ2VJRBIaCghjYXRlZ29yeRgCIAEoCVIIY2F0ZWdvcnk=');
@$core.Deprecated('Use packageItemDataDescriptor instead')
const PackageItemData$json = const {
  '1': 'PackageItemData',
  '2': const [
    const {'1': 'packageItemID', '3': 1, '4': 1, '5': 9, '10': 'packageItemID'},
    const {'1': 'packageID', '3': 2, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'category', '3': 3, '4': 1, '5': 9, '10': 'category'},
  ],
};

/// Descriptor for `PackageItemData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageItemDataDescriptor = $convert.base64Decode('Cg9QYWNrYWdlSXRlbURhdGESJAoNcGFja2FnZUl0ZW1JRBgBIAEoCVINcGFja2FnZUl0ZW1JRBIcCglwYWNrYWdlSUQYAiABKAlSCXBhY2thZ2VJRBIaCghjYXRlZ29yeRgDIAEoCVIIY2F0ZWdvcnk=');
@$core.Deprecated('Use listPackageItemResponseDescriptor instead')
const ListPackageItemResponse$json = const {
  '1': 'ListPackageItemResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.PackageItemData', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `ListPackageItemResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listPackageItemResponseDescriptor = $convert.base64Decode('ChdMaXN0UGFja2FnZUl0ZW1SZXNwb25zZRI0CgRkYXRhGAEgAygLMiAubWVudV9zZXJ2aWNlLnYxLlBhY2thZ2VJdGVtRGF0YVIEZGF0YRIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEhYKBnN0YXR1cxgDIAEoCVIGc3RhdHVz');
@$core.Deprecated('Use itemDataDescriptor instead')
const ItemData$json = const {
  '1': 'ItemData',
  '2': const [
    const {'1': 'itemID', '3': 1, '4': 1, '5': 9, '10': 'itemID'},
    const {'1': 'quantity', '3': 2, '4': 1, '5': 1, '10': 'quantity'},
    const {'1': 'price', '3': 3, '4': 1, '5': 1, '10': 'price'},
    const {'1': 'isRequired', '3': 4, '4': 1, '5': 8, '10': 'isRequired'},
    const {'1': 'isDefault', '3': 5, '4': 1, '5': 8, '10': 'isDefault'},
    const {'1': 'description', '3': 6, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'units', '3': 7, '4': 1, '5': 9, '10': 'units'},
    const {'1': 'name', '3': 8, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'image', '3': 9, '4': 1, '5': 9, '10': 'image'},
  ],
};

/// Descriptor for `ItemData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDataDescriptor = $convert.base64Decode('CghJdGVtRGF0YRIWCgZpdGVtSUQYASABKAlSBml0ZW1JRBIaCghxdWFudGl0eRgCIAEoAVIIcXVhbnRpdHkSFAoFcHJpY2UYAyABKAFSBXByaWNlEh4KCmlzUmVxdWlyZWQYBCABKAhSCmlzUmVxdWlyZWQSHAoJaXNEZWZhdWx0GAUgASgIUglpc0RlZmF1bHQSIAoLZGVzY3JpcHRpb24YBiABKAlSC2Rlc2NyaXB0aW9uEhQKBXVuaXRzGAcgASgJUgV1bml0cxISCgRuYW1lGAggASgJUgRuYW1lEhQKBWltYWdlGAkgASgJUgVpbWFnZQ==');
@$core.Deprecated('Use addItemPackageDescriptor instead')
const AddItemPackage$json = const {
  '1': 'AddItemPackage',
  '2': const [
    const {'1': 'packageItemID', '3': 1, '4': 1, '5': 9, '10': 'packageItemID'},
    const {'1': 'item', '3': 2, '4': 1, '5': 11, '6': '.menu_service.v1.ItemData', '10': 'item'},
  ],
};

/// Descriptor for `AddItemPackage`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addItemPackageDescriptor = $convert.base64Decode('Cg5BZGRJdGVtUGFja2FnZRIkCg1wYWNrYWdlSXRlbUlEGAEgASgJUg1wYWNrYWdlSXRlbUlEEi0KBGl0ZW0YAiABKAsyGS5tZW51X3NlcnZpY2UudjEuSXRlbURhdGFSBGl0ZW0=');
@$core.Deprecated('Use imagesDescriptor instead')
const Images$json = const {
  '1': 'Images',
  '2': const [
    const {'1': 'imageUrl', '3': 1, '4': 1, '5': 9, '10': 'imageUrl'},
  ],
};

/// Descriptor for `Images`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List imagesDescriptor = $convert.base64Decode('CgZJbWFnZXMSGgoIaW1hZ2VVcmwYASABKAlSCGltYWdlVXJs');
@$core.Deprecated('Use packageItemDescriptor instead')
const PackageItem$json = const {
  '1': 'PackageItem',
  '2': const [
    const {'1': 'packageItemID', '3': 1, '4': 1, '5': 9, '10': 'packageItemID'},
    const {'1': 'substitutes', '3': 2, '4': 3, '5': 11, '6': '.menu_service.v1.ItemData', '10': 'substitutes'},
    const {'1': 'category', '3': 3, '4': 1, '5': 9, '10': 'category'},
    const {'1': 'packageID', '3': 4, '4': 1, '5': 9, '10': 'packageID'},
  ],
};

/// Descriptor for `PackageItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageItemDescriptor = $convert.base64Decode('CgtQYWNrYWdlSXRlbRIkCg1wYWNrYWdlSXRlbUlEGAEgASgJUg1wYWNrYWdlSXRlbUlEEjsKC3N1YnN0aXR1dGVzGAIgAygLMhkubWVudV9zZXJ2aWNlLnYxLkl0ZW1EYXRhUgtzdWJzdGl0dXRlcxIaCghjYXRlZ29yeRgDIAEoCVIIY2F0ZWdvcnkSHAoJcGFja2FnZUlEGAQgASgJUglwYWNrYWdlSUQ=');
@$core.Deprecated('Use packageDescriptor instead')
const Package$json = const {
  '1': 'Package',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'createdAt', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updatedAt', '3': 4, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'image', '3': 5, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'estimatedPrice', '3': 6, '4': 1, '5': 1, '10': 'estimatedPrice'},
    const {'1': 'maxNumberOfItems', '3': 7, '4': 1, '5': 1, '10': 'maxNumberOfItems'},
    const {'1': 'profitMargin', '3': 8, '4': 1, '5': 1, '10': 'profitMargin'},
    const {'1': 'items', '3': 9, '4': 3, '5': 11, '6': '.menu_service.v1.PackageItem', '10': 'items'},
    const {'1': 'packageID', '3': 10, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'categoryID', '3': 11, '4': 1, '5': 9, '10': 'categoryID'},
    const {'1': 'serviceAreaID', '3': 12, '4': 1, '5': 9, '10': 'serviceAreaID'},
    const {'1': 'isFreeDelivery', '3': 13, '4': 1, '5': 8, '10': 'isFreeDelivery'},
    const {'1': 'isPublished', '3': 14, '4': 1, '5': 8, '10': 'isPublished'},
    const {'1': 'isPackageLiked', '3': 15, '4': 1, '5': 8, '10': 'isPackageLiked'},
  ],
};

/// Descriptor for `Package`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageDescriptor = $convert.base64Decode('CgdQYWNrYWdlEhIKBG5hbWUYASABKAlSBG5hbWUSIAoLZGVzY3JpcHRpb24YAiABKAlSC2Rlc2NyaXB0aW9uEhwKCWNyZWF0ZWRBdBgDIAEoCVIJY3JlYXRlZEF0EhwKCXVwZGF0ZWRBdBgEIAEoCVIJdXBkYXRlZEF0EhQKBWltYWdlGAUgASgJUgVpbWFnZRImCg5lc3RpbWF0ZWRQcmljZRgGIAEoAVIOZXN0aW1hdGVkUHJpY2USKgoQbWF4TnVtYmVyT2ZJdGVtcxgHIAEoAVIQbWF4TnVtYmVyT2ZJdGVtcxIiCgxwcm9maXRNYXJnaW4YCCABKAFSDHByb2ZpdE1hcmdpbhIyCgVpdGVtcxgJIAMoCzIcLm1lbnVfc2VydmljZS52MS5QYWNrYWdlSXRlbVIFaXRlbXMSHAoJcGFja2FnZUlEGAogASgJUglwYWNrYWdlSUQSHgoKY2F0ZWdvcnlJRBgLIAEoCVIKY2F0ZWdvcnlJRBIkCg1zZXJ2aWNlQXJlYUlEGAwgASgJUg1zZXJ2aWNlQXJlYUlEEiYKDmlzRnJlZURlbGl2ZXJ5GA0gASgIUg5pc0ZyZWVEZWxpdmVyeRIgCgtpc1B1Ymxpc2hlZBgOIAEoCFILaXNQdWJsaXNoZWQSJgoOaXNQYWNrYWdlTGlrZWQYDyABKAhSDmlzUGFja2FnZUxpa2Vk');
@$core.Deprecated('Use packageRequestDescriptor instead')
const PackageRequest$json = const {
  '1': 'PackageRequest',
  '2': const [
    const {'1': 'packageID', '3': 1, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'serviceAreaID', '3': 2, '4': 1, '5': 9, '10': 'serviceAreaID'},
    const {'1': 'limit', '3': 3, '4': 1, '5': 3, '10': 'limit'},
    const {'1': 'page', '3': 4, '4': 1, '5': 3, '10': 'page'},
    const {'1': 'packageCatgoryID', '3': 5, '4': 1, '5': 9, '10': 'packageCatgoryID'},
    const {'1': 'userID', '3': 6, '4': 1, '5': 9, '10': 'userID'},
  ],
};

/// Descriptor for `PackageRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packageRequestDescriptor = $convert.base64Decode('Cg5QYWNrYWdlUmVxdWVzdBIcCglwYWNrYWdlSUQYASABKAlSCXBhY2thZ2VJRBIkCg1zZXJ2aWNlQXJlYUlEGAIgASgJUg1zZXJ2aWNlQXJlYUlEEhQKBWxpbWl0GAMgASgDUgVsaW1pdBISCgRwYWdlGAQgASgDUgRwYWdlEioKEHBhY2thZ2VDYXRnb3J5SUQYBSABKAlSEHBhY2thZ2VDYXRnb3J5SUQSFgoGdXNlcklEGAYgASgJUgZ1c2VySUQ=');
@$core.Deprecated('Use nutrientsDescriptor instead')
const Nutrients$json = const {
  '1': 'Nutrients',
  '2': const [
    const {'1': 'quantity', '3': 1, '4': 1, '5': 1, '10': 'quantity'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `Nutrients`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nutrientsDescriptor = $convert.base64Decode('CglOdXRyaWVudHMSGgoIcXVhbnRpdHkYASABKAFSCHF1YW50aXR5EhIKBG5hbWUYAiABKAlSBG5hbWU=');
@$core.Deprecated('Use locationDescriptor instead')
const Location$json = const {
  '1': 'Location',
  '2': const [
    const {'1': 'locationID', '3': 1, '4': 1, '5': 9, '10': 'locationID'},
  ],
};

/// Descriptor for `Location`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationDescriptor = $convert.base64Decode('CghMb2NhdGlvbhIeCgpsb2NhdGlvbklEGAEgASgJUgpsb2NhdGlvbklE');
@$core.Deprecated('Use listPackageResponseDescriptor instead')
const ListPackageResponse$json = const {
  '1': 'ListPackageResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.Package', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'total', '3': 4, '4': 1, '5': 3, '10': 'total'},
    const {'1': 'pageCount', '3': 5, '4': 1, '5': 3, '10': 'pageCount'},
  ],
};

/// Descriptor for `ListPackageResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listPackageResponseDescriptor = $convert.base64Decode('ChNMaXN0UGFja2FnZVJlc3BvbnNlEiwKBGRhdGEYASADKAsyGC5tZW51X3NlcnZpY2UudjEuUGFja2FnZVIEZGF0YRIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cxIYCgdtZXNzYWdlGAMgASgJUgdtZXNzYWdlEhQKBXRvdGFsGAQgASgDUgV0b3RhbBIcCglwYWdlQ291bnQYBSABKANSCXBhZ2VDb3VudA==');
@$core.Deprecated('Use likeRequestDescriptor instead')
const LikeRequest$json = const {
  '1': 'LikeRequest',
  '2': const [
    const {'1': 'userID', '3': 1, '4': 1, '5': 9, '10': 'userID'},
    const {'1': 'packageID', '3': 2, '4': 1, '5': 9, '10': 'packageID'},
  ],
};

/// Descriptor for `LikeRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List likeRequestDescriptor = $convert.base64Decode('CgtMaWtlUmVxdWVzdBIWCgZ1c2VySUQYASABKAlSBnVzZXJJRBIcCglwYWNrYWdlSUQYAiABKAlSCXBhY2thZ2VJRA==');
@$core.Deprecated('Use nutrientDescriptor instead')
const Nutrient$json = const {
  '1': 'Nutrient',
  '2': const [
    const {'1': 'nutrientName', '3': 1, '4': 1, '5': 9, '10': 'nutrientName'},
    const {'1': 'itemID', '3': 2, '4': 1, '5': 9, '10': 'itemID'},
    const {'1': 'nutrientID', '3': 3, '4': 1, '5': 9, '10': 'nutrientID'},
  ],
};

/// Descriptor for `Nutrient`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nutrientDescriptor = $convert.base64Decode('CghOdXRyaWVudBIiCgxudXRyaWVudE5hbWUYASABKAlSDG51dHJpZW50TmFtZRIWCgZpdGVtSUQYAiABKAlSBml0ZW1JRBIeCgpudXRyaWVudElEGAMgASgJUgpudXRyaWVudElE');
@$core.Deprecated('Use nutrientItemDescriptor instead')
const NutrientItem$json = const {
  '1': 'NutrientItem',
  '2': const [
    const {'1': 'nutrientName', '3': 1, '4': 1, '5': 9, '10': 'nutrientName'},
    const {'1': 'nutrientID', '3': 2, '4': 1, '5': 9, '10': 'nutrientID'},
  ],
};

/// Descriptor for `NutrientItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nutrientItemDescriptor = $convert.base64Decode('CgxOdXRyaWVudEl0ZW0SIgoMbnV0cmllbnROYW1lGAEgASgJUgxudXRyaWVudE5hbWUSHgoKbnV0cmllbnRJRBgCIAEoCVIKbnV0cmllbnRJRA==');
@$core.Deprecated('Use nutrientItemRequestDescriptor instead')
const NutrientItemRequest$json = const {
  '1': 'NutrientItemRequest',
  '2': const [
    const {'1': 'nutrientID', '3': 1, '4': 1, '5': 9, '10': 'nutrientID'},
    const {'1': 'itemID', '3': 2, '4': 1, '5': 9, '10': 'itemID'},
  ],
};

/// Descriptor for `NutrientItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nutrientItemRequestDescriptor = $convert.base64Decode('ChNOdXRyaWVudEl0ZW1SZXF1ZXN0Eh4KCm51dHJpZW50SUQYASABKAlSCm51dHJpZW50SUQSFgoGaXRlbUlEGAIgASgJUgZpdGVtSUQ=');
@$core.Deprecated('Use nutrientListDescriptor instead')
const NutrientList$json = const {
  '1': 'NutrientList',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.NutrientItem', '10': 'data'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `NutrientList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nutrientListDescriptor = $convert.base64Decode('CgxOdXRyaWVudExpc3QSMQoEZGF0YRgBIAMoCzIdLm1lbnVfc2VydmljZS52MS5OdXRyaWVudEl0ZW1SBGRhdGESGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIWCgZzdGF0dXMYAyABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use feedItemDescriptor instead')
const FeedItem$json = const {
  '1': 'FeedItem',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'icon', '3': 2, '4': 1, '5': 9, '10': 'icon'},
    const {'1': 'packages', '3': 3, '4': 3, '5': 11, '6': '.menu_service.v1.Package', '10': 'packages'},
    const {'1': 'serviceArea', '3': 4, '4': 1, '5': 9, '10': 'serviceArea'},
  ],
};

/// Descriptor for `FeedItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List feedItemDescriptor = $convert.base64Decode('CghGZWVkSXRlbRISCgRuYW1lGAEgASgJUgRuYW1lEhIKBGljb24YAiABKAlSBGljb24SNAoIcGFja2FnZXMYAyADKAsyGC5tZW51X3NlcnZpY2UudjEuUGFja2FnZVIIcGFja2FnZXMSIAoLc2VydmljZUFyZWEYBCABKAlSC3NlcnZpY2VBcmVh');
@$core.Deprecated('Use feedListDescriptor instead')
const FeedList$json = const {
  '1': 'FeedList',
  '2': const [
    const {'1': 'feeds', '3': 1, '4': 3, '5': 11, '6': '.menu_service.v1.FeedItem', '10': 'feeds'},
    const {'1': 'total', '3': 2, '4': 1, '5': 3, '10': 'total'},
    const {'1': 'pageCount', '3': 3, '4': 1, '5': 3, '10': 'pageCount'},
  ],
};

/// Descriptor for `FeedList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List feedListDescriptor = $convert.base64Decode('CghGZWVkTGlzdBIvCgVmZWVkcxgBIAMoCzIZLm1lbnVfc2VydmljZS52MS5GZWVkSXRlbVIFZmVlZHMSFAoFdG90YWwYAiABKANSBXRvdGFsEhwKCXBhZ2VDb3VudBgDIAEoA1IJcGFnZUNvdW50');
@$core.Deprecated('Use menuSearchItemSuggestionsRequestDescriptor instead')
const MenuSearchItemSuggestionsRequest$json = const {
  '1': 'MenuSearchItemSuggestionsRequest',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `MenuSearchItemSuggestionsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchItemSuggestionsRequestDescriptor = $convert.base64Decode('CiBNZW51U2VhcmNoSXRlbVN1Z2dlc3Rpb25zUmVxdWVzdBIUCgV2YWx1ZRgBIAEoCVIFdmFsdWU=');
@$core.Deprecated('Use menuSearchItemSuggestionsResponseDescriptor instead')
const MenuSearchItemSuggestionsResponse$json = const {
  '1': 'MenuSearchItemSuggestionsResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'suggestions', '3': 2, '4': 3, '5': 9, '10': 'suggestions'},
  ],
};

/// Descriptor for `MenuSearchItemSuggestionsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchItemSuggestionsResponseDescriptor = $convert.base64Decode('CiFNZW51U2VhcmNoSXRlbVN1Z2dlc3Rpb25zUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSIAoLc3VnZ2VzdGlvbnMYAiADKAlSC3N1Z2dlc3Rpb25z');
@$core.Deprecated('Use menuSearchPackageSuggestionsRequestDescriptor instead')
const MenuSearchPackageSuggestionsRequest$json = const {
  '1': 'MenuSearchPackageSuggestionsRequest',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `MenuSearchPackageSuggestionsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchPackageSuggestionsRequestDescriptor = $convert.base64Decode('CiNNZW51U2VhcmNoUGFja2FnZVN1Z2dlc3Rpb25zUmVxdWVzdBIUCgV2YWx1ZRgBIAEoCVIFdmFsdWU=');
@$core.Deprecated('Use menuSearchPackageSuggestionsResponseDescriptor instead')
const MenuSearchPackageSuggestionsResponse$json = const {
  '1': 'MenuSearchPackageSuggestionsResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'suggestions', '3': 2, '4': 3, '5': 9, '10': 'suggestions'},
  ],
};

/// Descriptor for `MenuSearchPackageSuggestionsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchPackageSuggestionsResponseDescriptor = $convert.base64Decode('CiRNZW51U2VhcmNoUGFja2FnZVN1Z2dlc3Rpb25zUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSIAoLc3VnZ2VzdGlvbnMYAiADKAlSC3N1Z2dlc3Rpb25z');
@$core.Deprecated('Use menuSearchItemRequestDescriptor instead')
const MenuSearchItemRequest$json = const {
  '1': 'MenuSearchItemRequest',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `MenuSearchItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchItemRequestDescriptor = $convert.base64Decode('ChVNZW51U2VhcmNoSXRlbVJlcXVlc3QSFAoFdmFsdWUYASABKAlSBXZhbHVl');
@$core.Deprecated('Use menuSearchItemResponseDescriptor instead')
const MenuSearchItemResponse$json = const {
  '1': 'MenuSearchItemResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'item_response', '3': 2, '4': 3, '5': 11, '6': '.menu_service.v1.Item', '10': 'itemResponse'},
  ],
};

/// Descriptor for `MenuSearchItemResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchItemResponseDescriptor = $convert.base64Decode('ChZNZW51U2VhcmNoSXRlbVJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEjoKDWl0ZW1fcmVzcG9uc2UYAiADKAsyFS5tZW51X3NlcnZpY2UudjEuSXRlbVIMaXRlbVJlc3BvbnNl');
@$core.Deprecated('Use menuSearchPackageRequestDescriptor instead')
const MenuSearchPackageRequest$json = const {
  '1': 'MenuSearchPackageRequest',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `MenuSearchPackageRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchPackageRequestDescriptor = $convert.base64Decode('ChhNZW51U2VhcmNoUGFja2FnZVJlcXVlc3QSFAoFdmFsdWUYASABKAlSBXZhbHVl');
@$core.Deprecated('Use menuSearchPackageResponseDescriptor instead')
const MenuSearchPackageResponse$json = const {
  '1': 'MenuSearchPackageResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'package_response', '3': 2, '4': 3, '5': 11, '6': '.menu_service.v1.Package', '10': 'packageResponse'},
  ],
};

/// Descriptor for `MenuSearchPackageResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchPackageResponseDescriptor = $convert.base64Decode('ChlNZW51U2VhcmNoUGFja2FnZVJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEkMKEHBhY2thZ2VfcmVzcG9uc2UYAiADKAsyGC5tZW51X3NlcnZpY2UudjEuUGFja2FnZVIPcGFja2FnZVJlc3BvbnNl');
@$core.Deprecated('Use searchRequestDescriptor instead')
const SearchRequest$json = const {
  '1': 'SearchRequest',
  '2': const [
    const {'1': 'index', '3': 1, '4': 1, '5': 9, '10': 'index'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `SearchRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchRequestDescriptor = $convert.base64Decode('Cg1TZWFyY2hSZXF1ZXN0EhQKBWluZGV4GAEgASgJUgVpbmRleBIUCgV2YWx1ZRgCIAEoCVIFdmFsdWU=');
@$core.Deprecated('Use searchResponseDescriptor instead')
const SearchResponse$json = const {
  '1': 'SearchResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'package_response', '3': 2, '4': 3, '5': 11, '6': '.menu_service.v1.Package', '10': 'packageResponse'},
    const {'1': 'item_response', '3': 3, '4': 3, '5': 11, '6': '.menu_service.v1.Item', '10': 'itemResponse'},
  ],
};

/// Descriptor for `SearchResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchResponseDescriptor = $convert.base64Decode('Cg5TZWFyY2hSZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxJDChBwYWNrYWdlX3Jlc3BvbnNlGAIgAygLMhgubWVudV9zZXJ2aWNlLnYxLlBhY2thZ2VSD3BhY2thZ2VSZXNwb25zZRI6Cg1pdGVtX3Jlc3BvbnNlGAMgAygLMhUubWVudV9zZXJ2aWNlLnYxLkl0ZW1SDGl0ZW1SZXNwb25zZQ==');
@$core.Deprecated('Use menuSearchSuggestionsRequestDescriptor instead')
const MenuSearchSuggestionsRequest$json = const {
  '1': 'MenuSearchSuggestionsRequest',
  '2': const [
    const {'1': 'index', '3': 1, '4': 1, '5': 9, '10': 'index'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `MenuSearchSuggestionsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchSuggestionsRequestDescriptor = $convert.base64Decode('ChxNZW51U2VhcmNoU3VnZ2VzdGlvbnNSZXF1ZXN0EhQKBWluZGV4GAEgASgJUgVpbmRleBIUCgV2YWx1ZRgCIAEoCVIFdmFsdWU=');
@$core.Deprecated('Use menuSearchSuggestionsResponseDescriptor instead')
const MenuSearchSuggestionsResponse$json = const {
  '1': 'MenuSearchSuggestionsResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'suggestions', '3': 2, '4': 3, '5': 9, '10': 'suggestions'},
  ],
};

/// Descriptor for `MenuSearchSuggestionsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List menuSearchSuggestionsResponseDescriptor = $convert.base64Decode('Ch1NZW51U2VhcmNoU3VnZ2VzdGlvbnNSZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxIgCgtzdWdnZXN0aW9ucxgCIAMoCVILc3VnZ2VzdGlvbnM=');
