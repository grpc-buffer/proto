///
//  Generated code. Do not modify.
//  source: search.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'search.pb.dart' as $0;
export 'search.pb.dart';

class searchServiceClient extends $grpc.Client {
  static final _$searchService =
      $grpc.ClientMethod<$0.InSearchRequest, $0.InSearchResponse>(
          '/search_service.v1.searchService/SearchService',
          ($0.InSearchRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.InSearchResponse.fromBuffer(value));
  static final _$searchSuggestions = $grpc.ClientMethod<
          $0.InSearchSuggestionsRequest, $0.InSearchSuggestionsResponse>(
      '/search_service.v1.searchService/SearchSuggestions',
      ($0.InSearchSuggestionsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.InSearchSuggestionsResponse.fromBuffer(value));
  static final _$searchItemSuggestions = $grpc.ClientMethod<
          $0.InSearchItemSuggestionsRequest,
          $0.InSearchItemSuggestionsResponse>(
      '/search_service.v1.searchService/SearchItemSuggestions',
      ($0.InSearchItemSuggestionsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.InSearchItemSuggestionsResponse.fromBuffer(value));
  static final _$searchPackageSuggestions = $grpc.ClientMethod<
          $0.InSearchPackageSuggestionsRequest,
          $0.InSearchPackageSuggestionsResponse>(
      '/search_service.v1.searchService/SearchPackageSuggestions',
      ($0.InSearchPackageSuggestionsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.InSearchPackageSuggestionsResponse.fromBuffer(value));
  static final _$searchItems =
      $grpc.ClientMethod<$0.InSearchItemRequest, $0.InSearchItemResponse>(
          '/search_service.v1.searchService/SearchItems',
          ($0.InSearchItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.InSearchItemResponse.fromBuffer(value));
  static final _$searchPackages =
      $grpc.ClientMethod<$0.InSearchPackageRequest, $0.InSearchPackageResponse>(
          '/search_service.v1.searchService/SearchPackages',
          ($0.InSearchPackageRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.InSearchPackageResponse.fromBuffer(value));

  searchServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.InSearchResponse> searchService(
      $0.InSearchRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$searchService, request, options: options);
  }

  $grpc.ResponseFuture<$0.InSearchSuggestionsResponse> searchSuggestions(
      $0.InSearchSuggestionsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$searchSuggestions, request, options: options);
  }

  $grpc.ResponseFuture<$0.InSearchItemSuggestionsResponse>
      searchItemSuggestions($0.InSearchItemSuggestionsRequest request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$searchItemSuggestions, request, options: options);
  }

  $grpc.ResponseFuture<$0.InSearchPackageSuggestionsResponse>
      searchPackageSuggestions($0.InSearchPackageSuggestionsRequest request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$searchPackageSuggestions, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.InSearchItemResponse> searchItems(
      $0.InSearchItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$searchItems, request, options: options);
  }

  $grpc.ResponseFuture<$0.InSearchPackageResponse> searchPackages(
      $0.InSearchPackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$searchPackages, request, options: options);
  }
}

abstract class searchServiceBase extends $grpc.Service {
  $core.String get $name => 'search_service.v1.searchService';

  searchServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.InSearchRequest, $0.InSearchResponse>(
        'SearchService',
        searchService_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.InSearchRequest.fromBuffer(value),
        ($0.InSearchResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.InSearchSuggestionsRequest,
            $0.InSearchSuggestionsResponse>(
        'SearchSuggestions',
        searchSuggestions_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.InSearchSuggestionsRequest.fromBuffer(value),
        ($0.InSearchSuggestionsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.InSearchItemSuggestionsRequest,
            $0.InSearchItemSuggestionsResponse>(
        'SearchItemSuggestions',
        searchItemSuggestions_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.InSearchItemSuggestionsRequest.fromBuffer(value),
        ($0.InSearchItemSuggestionsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.InSearchPackageSuggestionsRequest,
            $0.InSearchPackageSuggestionsResponse>(
        'SearchPackageSuggestions',
        searchPackageSuggestions_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.InSearchPackageSuggestionsRequest.fromBuffer(value),
        ($0.InSearchPackageSuggestionsResponse value) =>
            value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.InSearchItemRequest, $0.InSearchItemResponse>(
            'SearchItems',
            searchItems_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.InSearchItemRequest.fromBuffer(value),
            ($0.InSearchItemResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.InSearchPackageRequest,
            $0.InSearchPackageResponse>(
        'SearchPackages',
        searchPackages_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.InSearchPackageRequest.fromBuffer(value),
        ($0.InSearchPackageResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.InSearchResponse> searchService_Pre(
      $grpc.ServiceCall call, $async.Future<$0.InSearchRequest> request) async {
    return searchService(call, await request);
  }

  $async.Future<$0.InSearchSuggestionsResponse> searchSuggestions_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.InSearchSuggestionsRequest> request) async {
    return searchSuggestions(call, await request);
  }

  $async.Future<$0.InSearchItemSuggestionsResponse> searchItemSuggestions_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.InSearchItemSuggestionsRequest> request) async {
    return searchItemSuggestions(call, await request);
  }

  $async.Future<$0.InSearchPackageSuggestionsResponse>
      searchPackageSuggestions_Pre($grpc.ServiceCall call,
          $async.Future<$0.InSearchPackageSuggestionsRequest> request) async {
    return searchPackageSuggestions(call, await request);
  }

  $async.Future<$0.InSearchItemResponse> searchItems_Pre($grpc.ServiceCall call,
      $async.Future<$0.InSearchItemRequest> request) async {
    return searchItems(call, await request);
  }

  $async.Future<$0.InSearchPackageResponse> searchPackages_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.InSearchPackageRequest> request) async {
    return searchPackages(call, await request);
  }

  $async.Future<$0.InSearchResponse> searchService(
      $grpc.ServiceCall call, $0.InSearchRequest request);
  $async.Future<$0.InSearchSuggestionsResponse> searchSuggestions(
      $grpc.ServiceCall call, $0.InSearchSuggestionsRequest request);
  $async.Future<$0.InSearchItemSuggestionsResponse> searchItemSuggestions(
      $grpc.ServiceCall call, $0.InSearchItemSuggestionsRequest request);
  $async.Future<$0.InSearchPackageSuggestionsResponse> searchPackageSuggestions(
      $grpc.ServiceCall call, $0.InSearchPackageSuggestionsRequest request);
  $async.Future<$0.InSearchItemResponse> searchItems(
      $grpc.ServiceCall call, $0.InSearchItemRequest request);
  $async.Future<$0.InSearchPackageResponse> searchPackages(
      $grpc.ServiceCall call, $0.InSearchPackageRequest request);
}
