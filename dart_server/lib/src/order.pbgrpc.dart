///
//  Generated code. Do not modify.
//  source: order.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'order.pb.dart' as $0;
export 'order.pb.dart';

class OrderClient extends $grpc.Client {
  static final _$createNewOrder =
      $grpc.ClientMethod<$0.NewOrderRequest, $0.NewOrderResponse>(
          '/order.v1.Order/CreateNewOrder',
          ($0.NewOrderRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.NewOrderResponse.fromBuffer(value));
  static final _$fetchOrdersByUser =
      $grpc.ClientMethod<$0.NewGetUserOrderRequest, $0.NewGetUserOrderResponse>(
          '/order.v1.Order/FetchOrdersByUser',
          ($0.NewGetUserOrderRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.NewGetUserOrderResponse.fromBuffer(value));
  static final _$listenToOrdersByUser = $grpc.ClientMethod<
          $0.NewGetUserOrderRequest, $0.NewListenToUserOrderResponse>(
      '/order.v1.Order/ListenToOrdersByUser',
      ($0.NewGetUserOrderRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.NewListenToUserOrderResponse.fromBuffer(value));
  static final _$deleteUserOrders = $grpc.ClientMethod<
          $0.DeleteUserOrdersRequest, $0.DeleteUserOrdersResponse>(
      '/order.v1.Order/DeleteUserOrders',
      ($0.DeleteUserOrdersRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.DeleteUserOrdersResponse.fromBuffer(value));
  static final _$updateOrderStatus = $grpc.ClientMethod<
          $0.UpdateOrderStatusRequest, $0.UpdateOrderStatusResponse>(
      '/order.v1.Order/UpdateOrderStatus',
      ($0.UpdateOrderStatusRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.UpdateOrderStatusResponse.fromBuffer(value));
  static final _$fetchOderByStatus =
      $grpc.ClientMethod<$0.GetStatusRequest, $0.GetStatusResponse>(
          '/order.v1.Order/FetchOderByStatus',
          ($0.GetStatusRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetStatusResponse.fromBuffer(value));
  static final _$fetchAllOrders =
      $grpc.ClientMethod<$0.AllOrdersRequest, $0.AllOrdersResponse>(
          '/order.v1.Order/FetchAllOrders',
          ($0.AllOrdersRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AllOrdersResponse.fromBuffer(value));
  static final _$getDeliveryCode =
      $grpc.ClientMethod<$0.GetDeliveryCodeRequest, $0.GetDeliveryCodeResponse>(
          '/order.v1.Order/GetDeliveryCode',
          ($0.GetDeliveryCodeRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetDeliveryCodeResponse.fromBuffer(value));
  static final _$fetchOrderById =
      $grpc.ClientMethod<$0.GetOrderIdRequest, $0.GetOrderIdResponse>(
          '/order.v1.Order/FetchOrderById',
          ($0.GetOrderIdRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetOrderIdResponse.fromBuffer(value));
  static final _$getOrderById =
      $grpc.ClientMethod<$0.GetOrderIdRequest, $0.GetOrderIdResponse>(
          '/order.v1.Order/GetOrderById',
          ($0.GetOrderIdRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetOrderIdResponse.fromBuffer(value));

  OrderClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.NewOrderResponse> createNewOrder(
      $0.NewOrderRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createNewOrder, request, options: options);
  }

  $grpc.ResponseFuture<$0.NewGetUserOrderResponse> fetchOrdersByUser(
      $0.NewGetUserOrderRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$fetchOrdersByUser, request, options: options);
  }

  $grpc.ResponseStream<$0.NewListenToUserOrderResponse> listenToOrdersByUser(
      $0.NewGetUserOrderRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$listenToOrdersByUser, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$0.DeleteUserOrdersResponse> deleteUserOrders(
      $0.DeleteUserOrdersRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteUserOrders, request, options: options);
  }

  $grpc.ResponseStream<$0.UpdateOrderStatusResponse> updateOrderStatus(
      $0.UpdateOrderStatusRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$updateOrderStatus, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$0.GetStatusResponse> fetchOderByStatus(
      $0.GetStatusRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$fetchOderByStatus, request, options: options);
  }

  $grpc.ResponseFuture<$0.AllOrdersResponse> fetchAllOrders(
      $0.AllOrdersRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$fetchAllOrders, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetDeliveryCodeResponse> getDeliveryCode(
      $0.GetDeliveryCodeRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getDeliveryCode, request, options: options);
  }

  $grpc.ResponseStream<$0.GetOrderIdResponse> fetchOrderById(
      $0.GetOrderIdRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$fetchOrderById, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$0.GetOrderIdResponse> getOrderById(
      $0.GetOrderIdRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getOrderById, request, options: options);
  }
}

abstract class OrderServiceBase extends $grpc.Service {
  $core.String get $name => 'order.v1.Order';

  OrderServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.NewOrderRequest, $0.NewOrderResponse>(
        'CreateNewOrder',
        createNewOrder_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.NewOrderRequest.fromBuffer(value),
        ($0.NewOrderResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.NewGetUserOrderRequest,
            $0.NewGetUserOrderResponse>(
        'FetchOrdersByUser',
        fetchOrdersByUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.NewGetUserOrderRequest.fromBuffer(value),
        ($0.NewGetUserOrderResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.NewGetUserOrderRequest,
            $0.NewListenToUserOrderResponse>(
        'ListenToOrdersByUser',
        listenToOrdersByUser_Pre,
        false,
        true,
        ($core.List<$core.int> value) =>
            $0.NewGetUserOrderRequest.fromBuffer(value),
        ($0.NewListenToUserOrderResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeleteUserOrdersRequest,
            $0.DeleteUserOrdersResponse>(
        'DeleteUserOrders',
        deleteUserOrders_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.DeleteUserOrdersRequest.fromBuffer(value),
        ($0.DeleteUserOrdersResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UpdateOrderStatusRequest,
            $0.UpdateOrderStatusResponse>(
        'UpdateOrderStatus',
        updateOrderStatus_Pre,
        false,
        true,
        ($core.List<$core.int> value) =>
            $0.UpdateOrderStatusRequest.fromBuffer(value),
        ($0.UpdateOrderStatusResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetStatusRequest, $0.GetStatusResponse>(
        'FetchOderByStatus',
        fetchOderByStatus_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.GetStatusRequest.fromBuffer(value),
        ($0.GetStatusResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AllOrdersRequest, $0.AllOrdersResponse>(
        'FetchAllOrders',
        fetchAllOrders_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AllOrdersRequest.fromBuffer(value),
        ($0.AllOrdersResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetDeliveryCodeRequest,
            $0.GetDeliveryCodeResponse>(
        'GetDeliveryCode',
        getDeliveryCode_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetDeliveryCodeRequest.fromBuffer(value),
        ($0.GetDeliveryCodeResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetOrderIdRequest, $0.GetOrderIdResponse>(
        'FetchOrderById',
        fetchOrderById_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.GetOrderIdRequest.fromBuffer(value),
        ($0.GetOrderIdResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetOrderIdRequest, $0.GetOrderIdResponse>(
        'GetOrderById',
        getOrderById_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.GetOrderIdRequest.fromBuffer(value),
        ($0.GetOrderIdResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.NewOrderResponse> createNewOrder_Pre(
      $grpc.ServiceCall call, $async.Future<$0.NewOrderRequest> request) async {
    return createNewOrder(call, await request);
  }

  $async.Future<$0.NewGetUserOrderResponse> fetchOrdersByUser_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.NewGetUserOrderRequest> request) async {
    return fetchOrdersByUser(call, await request);
  }

  $async.Stream<$0.NewListenToUserOrderResponse> listenToOrdersByUser_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.NewGetUserOrderRequest> request) async* {
    yield* listenToOrdersByUser(call, await request);
  }

  $async.Future<$0.DeleteUserOrdersResponse> deleteUserOrders_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.DeleteUserOrdersRequest> request) async {
    return deleteUserOrders(call, await request);
  }

  $async.Stream<$0.UpdateOrderStatusResponse> updateOrderStatus_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.UpdateOrderStatusRequest> request) async* {
    yield* updateOrderStatus(call, await request);
  }

  $async.Future<$0.GetStatusResponse> fetchOderByStatus_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetStatusRequest> request) async {
    return fetchOderByStatus(call, await request);
  }

  $async.Future<$0.AllOrdersResponse> fetchAllOrders_Pre($grpc.ServiceCall call,
      $async.Future<$0.AllOrdersRequest> request) async {
    return fetchAllOrders(call, await request);
  }

  $async.Future<$0.GetDeliveryCodeResponse> getDeliveryCode_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetDeliveryCodeRequest> request) async {
    return getDeliveryCode(call, await request);
  }

  $async.Stream<$0.GetOrderIdResponse> fetchOrderById_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetOrderIdRequest> request) async* {
    yield* fetchOrderById(call, await request);
  }

  $async.Future<$0.GetOrderIdResponse> getOrderById_Pre($grpc.ServiceCall call,
      $async.Future<$0.GetOrderIdRequest> request) async {
    return getOrderById(call, await request);
  }

  $async.Future<$0.NewOrderResponse> createNewOrder(
      $grpc.ServiceCall call, $0.NewOrderRequest request);
  $async.Future<$0.NewGetUserOrderResponse> fetchOrdersByUser(
      $grpc.ServiceCall call, $0.NewGetUserOrderRequest request);
  $async.Stream<$0.NewListenToUserOrderResponse> listenToOrdersByUser(
      $grpc.ServiceCall call, $0.NewGetUserOrderRequest request);
  $async.Future<$0.DeleteUserOrdersResponse> deleteUserOrders(
      $grpc.ServiceCall call, $0.DeleteUserOrdersRequest request);
  $async.Stream<$0.UpdateOrderStatusResponse> updateOrderStatus(
      $grpc.ServiceCall call, $0.UpdateOrderStatusRequest request);
  $async.Future<$0.GetStatusResponse> fetchOderByStatus(
      $grpc.ServiceCall call, $0.GetStatusRequest request);
  $async.Future<$0.AllOrdersResponse> fetchAllOrders(
      $grpc.ServiceCall call, $0.AllOrdersRequest request);
  $async.Future<$0.GetDeliveryCodeResponse> getDeliveryCode(
      $grpc.ServiceCall call, $0.GetDeliveryCodeRequest request);
  $async.Stream<$0.GetOrderIdResponse> fetchOrderById(
      $grpc.ServiceCall call, $0.GetOrderIdRequest request);
  $async.Future<$0.GetOrderIdResponse> getOrderById(
      $grpc.ServiceCall call, $0.GetOrderIdRequest request);
}
