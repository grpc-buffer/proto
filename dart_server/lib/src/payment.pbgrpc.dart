///
//  Generated code. Do not modify.
//  source: payment.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'payment.pb.dart' as $0;
export 'payment.pb.dart';

class PaymentServiceClient extends $grpc.Client {
  static final _$initializePayment = $grpc.ClientMethod<
          $0.InitializePaymentRequest, $0.InitializePaymentResponse>(
      '/PaymentService/InitializePayment',
      ($0.InitializePaymentRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.InitializePaymentResponse.fromBuffer(value));
  static final _$verifyPayment =
      $grpc.ClientMethod<$0.VerifyPaymentRequest, $0.VerifyPaymentResponse>(
          '/PaymentService/VerifyPayment',
          ($0.VerifyPaymentRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.VerifyPaymentResponse.fromBuffer(value));

  PaymentServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.InitializePaymentResponse> initializePayment(
      $0.InitializePaymentRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$initializePayment, request, options: options);
  }

  $grpc.ResponseFuture<$0.VerifyPaymentResponse> verifyPayment(
      $0.VerifyPaymentRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$verifyPayment, request, options: options);
  }
}

abstract class PaymentServiceBase extends $grpc.Service {
  $core.String get $name => 'PaymentService';

  PaymentServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.InitializePaymentRequest,
            $0.InitializePaymentResponse>(
        'InitializePayment',
        initializePayment_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.InitializePaymentRequest.fromBuffer(value),
        ($0.InitializePaymentResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.VerifyPaymentRequest, $0.VerifyPaymentResponse>(
            'VerifyPayment',
            verifyPayment_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.VerifyPaymentRequest.fromBuffer(value),
            ($0.VerifyPaymentResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.InitializePaymentResponse> initializePayment_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.InitializePaymentRequest> request) async {
    return initializePayment(call, await request);
  }

  $async.Future<$0.VerifyPaymentResponse> verifyPayment_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.VerifyPaymentRequest> request) async {
    return verifyPayment(call, await request);
  }

  $async.Future<$0.InitializePaymentResponse> initializePayment(
      $grpc.ServiceCall call, $0.InitializePaymentRequest request);
  $async.Future<$0.VerifyPaymentResponse> verifyPayment(
      $grpc.ServiceCall call, $0.VerifyPaymentRequest request);
}
