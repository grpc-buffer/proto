///
//  Generated code. Do not modify.
//  source: price.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'price.pb.dart' as $0;
export 'price.pb.dart';

class PricingServiceClient extends $grpc.Client {
  static final _$addPricing = $grpc.ClientMethod<$0.PriceRequest, $0.PriceItem>(
      '/pricing_service.v1.PricingService/AddPricing',
      ($0.PriceRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.PriceItem.fromBuffer(value));
  static final _$updateByItem = $grpc.ClientMethod<$0.PriceItem, $0.PriceItem>(
      '/pricing_service.v1.PricingService/UpdateByItem',
      ($0.PriceItem value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.PriceItem.fromBuffer(value));
  static final _$updateById = $grpc.ClientMethod<$0.PriceItem, $0.PriceItem>(
      '/pricing_service.v1.PricingService/UpdateById',
      ($0.PriceItem value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.PriceItem.fromBuffer(value));
  static final _$viewByItem =
      $grpc.ClientMethod<$0.PriceRequestItem, $0.ViewPriceResponse>(
          '/pricing_service.v1.PricingService/ViewByItem',
          ($0.PriceRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ViewPriceResponse.fromBuffer(value));
  static final _$viewById =
      $grpc.ClientMethod<$0.PriceRequestItem, $0.ViewPriceResponse>(
          '/pricing_service.v1.PricingService/ViewById',
          ($0.PriceRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ViewPriceResponse.fromBuffer(value));
  static final _$deletePricing =
      $grpc.ClientMethod<$0.PriceRequestItem, $0.PriceResponse>(
          '/pricing_service.v1.PricingService/DeletePricing',
          ($0.PriceRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.PriceResponse.fromBuffer(value));
  static final _$listPrices =
      $grpc.ClientMethod<$0.EmptyPriceRequest, $0.ListPriceResponse>(
          '/pricing_service.v1.PricingService/ListPrices',
          ($0.EmptyPriceRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPriceResponse.fromBuffer(value));
  static final _$listByLocation =
      $grpc.ClientMethod<$0.PriceRequestItem, $0.ListPriceResponse>(
          '/pricing_service.v1.PricingService/ListByLocation',
          ($0.PriceRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPriceResponse.fromBuffer(value));
  static final _$listByDate =
      $grpc.ClientMethod<$0.PriceRequestItem, $0.ListPriceResponse>(
          '/pricing_service.v1.PricingService/ListByDate',
          ($0.PriceRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPriceResponse.fromBuffer(value));

  PricingServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.PriceItem> addPricing($0.PriceRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addPricing, request, options: options);
  }

  $grpc.ResponseFuture<$0.PriceItem> updateByItem($0.PriceItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateByItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.PriceItem> updateById($0.PriceItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateById, request, options: options);
  }

  $grpc.ResponseFuture<$0.ViewPriceResponse> viewByItem(
      $0.PriceRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewByItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.ViewPriceResponse> viewById(
      $0.PriceRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewById, request, options: options);
  }

  $grpc.ResponseFuture<$0.PriceResponse> deletePricing(
      $0.PriceRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deletePricing, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPriceResponse> listPrices(
      $0.EmptyPriceRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listPrices, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPriceResponse> listByLocation(
      $0.PriceRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listByLocation, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPriceResponse> listByDate(
      $0.PriceRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listByDate, request, options: options);
  }
}

abstract class PricingServiceBase extends $grpc.Service {
  $core.String get $name => 'pricing_service.v1.PricingService';

  PricingServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.PriceRequest, $0.PriceItem>(
        'AddPricing',
        addPricing_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PriceRequest.fromBuffer(value),
        ($0.PriceItem value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PriceItem, $0.PriceItem>(
        'UpdateByItem',
        updateByItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PriceItem.fromBuffer(value),
        ($0.PriceItem value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PriceItem, $0.PriceItem>(
        'UpdateById',
        updateById_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PriceItem.fromBuffer(value),
        ($0.PriceItem value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PriceRequestItem, $0.ViewPriceResponse>(
        'ViewByItem',
        viewByItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PriceRequestItem.fromBuffer(value),
        ($0.ViewPriceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PriceRequestItem, $0.ViewPriceResponse>(
        'ViewById',
        viewById_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PriceRequestItem.fromBuffer(value),
        ($0.ViewPriceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PriceRequestItem, $0.PriceResponse>(
        'DeletePricing',
        deletePricing_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PriceRequestItem.fromBuffer(value),
        ($0.PriceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyPriceRequest, $0.ListPriceResponse>(
        'ListPrices',
        listPrices_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EmptyPriceRequest.fromBuffer(value),
        ($0.ListPriceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PriceRequestItem, $0.ListPriceResponse>(
        'ListByLocation',
        listByLocation_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PriceRequestItem.fromBuffer(value),
        ($0.ListPriceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PriceRequestItem, $0.ListPriceResponse>(
        'ListByDate',
        listByDate_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PriceRequestItem.fromBuffer(value),
        ($0.ListPriceResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.PriceItem> addPricing_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PriceRequest> request) async {
    return addPricing(call, await request);
  }

  $async.Future<$0.PriceItem> updateByItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PriceItem> request) async {
    return updateByItem(call, await request);
  }

  $async.Future<$0.PriceItem> updateById_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PriceItem> request) async {
    return updateById(call, await request);
  }

  $async.Future<$0.ViewPriceResponse> viewByItem_Pre($grpc.ServiceCall call,
      $async.Future<$0.PriceRequestItem> request) async {
    return viewByItem(call, await request);
  }

  $async.Future<$0.ViewPriceResponse> viewById_Pre($grpc.ServiceCall call,
      $async.Future<$0.PriceRequestItem> request) async {
    return viewById(call, await request);
  }

  $async.Future<$0.PriceResponse> deletePricing_Pre($grpc.ServiceCall call,
      $async.Future<$0.PriceRequestItem> request) async {
    return deletePricing(call, await request);
  }

  $async.Future<$0.ListPriceResponse> listPrices_Pre($grpc.ServiceCall call,
      $async.Future<$0.EmptyPriceRequest> request) async {
    return listPrices(call, await request);
  }

  $async.Future<$0.ListPriceResponse> listByLocation_Pre($grpc.ServiceCall call,
      $async.Future<$0.PriceRequestItem> request) async {
    return listByLocation(call, await request);
  }

  $async.Future<$0.ListPriceResponse> listByDate_Pre($grpc.ServiceCall call,
      $async.Future<$0.PriceRequestItem> request) async {
    return listByDate(call, await request);
  }

  $async.Future<$0.PriceItem> addPricing(
      $grpc.ServiceCall call, $0.PriceRequest request);
  $async.Future<$0.PriceItem> updateByItem(
      $grpc.ServiceCall call, $0.PriceItem request);
  $async.Future<$0.PriceItem> updateById(
      $grpc.ServiceCall call, $0.PriceItem request);
  $async.Future<$0.ViewPriceResponse> viewByItem(
      $grpc.ServiceCall call, $0.PriceRequestItem request);
  $async.Future<$0.ViewPriceResponse> viewById(
      $grpc.ServiceCall call, $0.PriceRequestItem request);
  $async.Future<$0.PriceResponse> deletePricing(
      $grpc.ServiceCall call, $0.PriceRequestItem request);
  $async.Future<$0.ListPriceResponse> listPrices(
      $grpc.ServiceCall call, $0.EmptyPriceRequest request);
  $async.Future<$0.ListPriceResponse> listByLocation(
      $grpc.ServiceCall call, $0.PriceRequestItem request);
  $async.Future<$0.ListPriceResponse> listByDate(
      $grpc.ServiceCall call, $0.PriceRequestItem request);
}
