///
//  Generated code. Do not modify.
//  source: order.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use newOrderRequestDescriptor instead')
const NewOrderRequest$json = const {
  '1': 'NewOrderRequest',
  '2': const [
    const {'1': 'order_id', '3': 1, '4': 1, '5': 9, '10': 'orderId'},
    const {'1': 'reference', '3': 2, '4': 1, '5': 9, '10': 'reference'},
    const {'1': 'subtotal', '3': 3, '4': 1, '5': 1, '10': 'subtotal'},
    const {'1': 'delivery_cost', '3': 4, '4': 1, '5': 1, '10': 'deliveryCost'},
    const {'1': 'total_cost', '3': 5, '4': 1, '5': 1, '10': 'totalCost'},
    const {'1': 'delivery_Type', '3': 6, '4': 1, '5': 9, '10': 'deliveryType'},
    const {'1': 'user_id', '3': 7, '4': 1, '5': 9, '10': 'userId'},
    const {'1': 'delivery_address', '3': 8, '4': 1, '5': 11, '6': '.order.v1.InNewOrderDeliveryAddress', '10': 'deliveryAddress'},
    const {'1': 'order_items', '3': 9, '4': 3, '5': 11, '6': '.order.v1.InNewOrderItems', '10': 'orderItems'},
    const {'1': 'paymentMethod', '3': 10, '4': 1, '5': 9, '10': 'paymentMethod'},
    const {'1': 'currency', '3': 11, '4': 1, '5': 9, '10': 'currency'},
    const {'1': 'tip', '3': 12, '4': 1, '5': 1, '10': 'tip'},
    const {'1': 'schedule', '3': 13, '4': 1, '5': 9, '10': 'schedule'},
  ],
};

/// Descriptor for `NewOrderRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newOrderRequestDescriptor = $convert.base64Decode('Cg9OZXdPcmRlclJlcXVlc3QSGQoIb3JkZXJfaWQYASABKAlSB29yZGVySWQSHAoJcmVmZXJlbmNlGAIgASgJUglyZWZlcmVuY2USGgoIc3VidG90YWwYAyABKAFSCHN1YnRvdGFsEiMKDWRlbGl2ZXJ5X2Nvc3QYBCABKAFSDGRlbGl2ZXJ5Q29zdBIdCgp0b3RhbF9jb3N0GAUgASgBUgl0b3RhbENvc3QSIwoNZGVsaXZlcnlfVHlwZRgGIAEoCVIMZGVsaXZlcnlUeXBlEhcKB3VzZXJfaWQYByABKAlSBnVzZXJJZBJOChBkZWxpdmVyeV9hZGRyZXNzGAggASgLMiMub3JkZXIudjEuSW5OZXdPcmRlckRlbGl2ZXJ5QWRkcmVzc1IPZGVsaXZlcnlBZGRyZXNzEjoKC29yZGVyX2l0ZW1zGAkgAygLMhkub3JkZXIudjEuSW5OZXdPcmRlckl0ZW1zUgpvcmRlckl0ZW1zEiQKDXBheW1lbnRNZXRob2QYCiABKAlSDXBheW1lbnRNZXRob2QSGgoIY3VycmVuY3kYCyABKAlSCGN1cnJlbmN5EhAKA3RpcBgMIAEoAVIDdGlwEhoKCHNjaGVkdWxlGA0gASgJUghzY2hlZHVsZQ==');
@$core.Deprecated('Use newOrderResponseDescriptor instead')
const NewOrderResponse$json = const {
  '1': 'NewOrderResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `NewOrderResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newOrderResponseDescriptor = $convert.base64Decode('ChBOZXdPcmRlclJlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USFgoGc3RhdHVzGAIgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use inNewOrderDeliveryAddressDescriptor instead')
const InNewOrderDeliveryAddress$json = const {
  '1': 'InNewOrderDeliveryAddress',
  '2': const [
    const {'1': 'address', '3': 1, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'coordinates', '3': 2, '4': 1, '5': 11, '6': '.order.v1.InNewOrderCoordinates', '10': 'coordinates'},
  ],
};

/// Descriptor for `InNewOrderDeliveryAddress`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inNewOrderDeliveryAddressDescriptor = $convert.base64Decode('ChlJbk5ld09yZGVyRGVsaXZlcnlBZGRyZXNzEhgKB2FkZHJlc3MYASABKAlSB2FkZHJlc3MSQQoLY29vcmRpbmF0ZXMYAiABKAsyHy5vcmRlci52MS5Jbk5ld09yZGVyQ29vcmRpbmF0ZXNSC2Nvb3JkaW5hdGVz');
@$core.Deprecated('Use inNewOrderCoordinatesDescriptor instead')
const InNewOrderCoordinates$json = const {
  '1': 'InNewOrderCoordinates',
  '2': const [
    const {'1': 'latitude', '3': 1, '4': 1, '5': 1, '10': 'latitude'},
    const {'1': 'longitude', '3': 2, '4': 1, '5': 1, '10': 'longitude'},
  ],
};

/// Descriptor for `InNewOrderCoordinates`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inNewOrderCoordinatesDescriptor = $convert.base64Decode('ChVJbk5ld09yZGVyQ29vcmRpbmF0ZXMSGgoIbGF0aXR1ZGUYASABKAFSCGxhdGl0dWRlEhwKCWxvbmdpdHVkZRgCIAEoAVIJbG9uZ2l0dWRl');
@$core.Deprecated('Use inNewOrderItemsDescriptor instead')
const InNewOrderItems$json = const {
  '1': 'InNewOrderItems',
  '2': const [
    const {'1': 'packageID', '3': 1, '4': 1, '5': 9, '10': 'packageID'},
    const {'1': 'packageName', '3': 2, '4': 1, '5': 9, '10': 'packageName'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'basePrice', '3': 4, '4': 1, '5': 1, '10': 'basePrice'},
    const {'1': 'serviceAreaID', '3': 6, '4': 1, '5': 9, '10': 'serviceAreaID'},
    const {'1': 'image', '3': 7, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'quantity', '3': 8, '4': 1, '5': 5, '10': 'quantity'},
    const {'1': 'items', '3': 9, '4': 3, '5': 11, '6': '.order.v1.InNewOrderPackageItem', '10': 'items'},
    const {'1': 'cartID', '3': 10, '4': 1, '5': 9, '10': 'cartID'},
  ],
};

/// Descriptor for `InNewOrderItems`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inNewOrderItemsDescriptor = $convert.base64Decode('Cg9Jbk5ld09yZGVySXRlbXMSHAoJcGFja2FnZUlEGAEgASgJUglwYWNrYWdlSUQSIAoLcGFja2FnZU5hbWUYAiABKAlSC3BhY2thZ2VOYW1lEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbhIcCgliYXNlUHJpY2UYBCABKAFSCWJhc2VQcmljZRIkCg1zZXJ2aWNlQXJlYUlEGAYgASgJUg1zZXJ2aWNlQXJlYUlEEhQKBWltYWdlGAcgASgJUgVpbWFnZRIaCghxdWFudGl0eRgIIAEoBVIIcXVhbnRpdHkSNQoFaXRlbXMYCSADKAsyHy5vcmRlci52MS5Jbk5ld09yZGVyUGFja2FnZUl0ZW1SBWl0ZW1zEhYKBmNhcnRJRBgKIAEoCVIGY2FydElE');
@$core.Deprecated('Use inNewOrderPackageItemDescriptor instead')
const InNewOrderPackageItem$json = const {
  '1': 'InNewOrderPackageItem',
  '2': const [
    const {'1': 'itemID', '3': 1, '4': 1, '5': 9, '10': 'itemID'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'itemCategoryID', '3': 4, '4': 1, '5': 9, '10': 'itemCategoryID'},
    const {'1': 'image', '3': 5, '4': 1, '5': 9, '10': 'image'},
    const {'1': 'unit', '3': 6, '4': 1, '5': 9, '10': 'unit'},
  ],
};

/// Descriptor for `InNewOrderPackageItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inNewOrderPackageItemDescriptor = $convert.base64Decode('ChVJbk5ld09yZGVyUGFja2FnZUl0ZW0SFgoGaXRlbUlEGAEgASgJUgZpdGVtSUQSEgoEbmFtZRgCIAEoCVIEbmFtZRIgCgtkZXNjcmlwdGlvbhgDIAEoCVILZGVzY3JpcHRpb24SJgoOaXRlbUNhdGVnb3J5SUQYBCABKAlSDml0ZW1DYXRlZ29yeUlEEhQKBWltYWdlGAUgASgJUgVpbWFnZRISCgR1bml0GAYgASgJUgR1bml0');
@$core.Deprecated('Use newGetUserOrderRequestDescriptor instead')
const NewGetUserOrderRequest$json = const {
  '1': 'NewGetUserOrderRequest',
  '2': const [
    const {'1': 'limit', '3': 1, '4': 1, '5': 5, '10': 'limit'},
    const {'1': 'offset', '3': 2, '4': 1, '5': 5, '10': 'offset'},
    const {'1': 'user_id', '3': 3, '4': 1, '5': 9, '10': 'userId'},
  ],
};

/// Descriptor for `NewGetUserOrderRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newGetUserOrderRequestDescriptor = $convert.base64Decode('ChZOZXdHZXRVc2VyT3JkZXJSZXF1ZXN0EhQKBWxpbWl0GAEgASgFUgVsaW1pdBIWCgZvZmZzZXQYAiABKAVSBm9mZnNldBIXCgd1c2VyX2lkGAMgASgJUgZ1c2VySWQ=');
@$core.Deprecated('Use newGetUserOrderResponseDescriptor instead')
const NewGetUserOrderResponse$json = const {
  '1': 'NewGetUserOrderResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.order.v1.OrderProperties', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `NewGetUserOrderResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newGetUserOrderResponseDescriptor = $convert.base64Decode('ChdOZXdHZXRVc2VyT3JkZXJSZXNwb25zZRItCgRkYXRhGAEgAygLMhkub3JkZXIudjEuT3JkZXJQcm9wZXJ0aWVzUgRkYXRhEhYKBnN0YXR1cxgCIAEoCVIGc3RhdHVz');
@$core.Deprecated('Use newListenToUserOrderResponseDescriptor instead')
const NewListenToUserOrderResponse$json = const {
  '1': 'NewListenToUserOrderResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.order.v1.OrderProperties', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `NewListenToUserOrderResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newListenToUserOrderResponseDescriptor = $convert.base64Decode('ChxOZXdMaXN0ZW5Ub1VzZXJPcmRlclJlc3BvbnNlEi0KBGRhdGEYASABKAsyGS5vcmRlci52MS5PcmRlclByb3BlcnRpZXNSBGRhdGESFgoGc3RhdHVzGAIgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use deleteUserOrdersRequestDescriptor instead')
const DeleteUserOrdersRequest$json = const {
  '1': 'DeleteUserOrdersRequest',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 9, '10': 'userId'},
  ],
};

/// Descriptor for `DeleteUserOrdersRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteUserOrdersRequestDescriptor = $convert.base64Decode('ChdEZWxldGVVc2VyT3JkZXJzUmVxdWVzdBIXCgd1c2VyX2lkGAEgASgJUgZ1c2VySWQ=');
@$core.Deprecated('Use deleteUserOrdersResponseDescriptor instead')
const DeleteUserOrdersResponse$json = const {
  '1': 'DeleteUserOrdersResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `DeleteUserOrdersResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteUserOrdersResponseDescriptor = $convert.base64Decode('ChhEZWxldGVVc2VyT3JkZXJzUmVzcG9uc2USGAoHbWVzc2FnZRgBIAEoCVIHbWVzc2FnZRIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use updateOrderStatusRequestDescriptor instead')
const UpdateOrderStatusRequest$json = const {
  '1': 'UpdateOrderStatusRequest',
  '2': const [
    const {'1': 'order_id', '3': 1, '4': 1, '5': 9, '10': 'orderId'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `UpdateOrderStatusRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateOrderStatusRequestDescriptor = $convert.base64Decode('ChhVcGRhdGVPcmRlclN0YXR1c1JlcXVlc3QSGQoIb3JkZXJfaWQYASABKAlSB29yZGVySWQSFgoGc3RhdHVzGAIgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use updateOrderStatusResponseDescriptor instead')
const UpdateOrderStatusResponse$json = const {
  '1': 'UpdateOrderStatusResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `UpdateOrderStatusResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateOrderStatusResponseDescriptor = $convert.base64Decode('ChlVcGRhdGVPcmRlclN0YXR1c1Jlc3BvbnNlEhgKB21lc3NhZ2UYASABKAlSB21lc3NhZ2USFgoGc3RhdHVzGAIgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use getOrderIdRequestDescriptor instead')
const GetOrderIdRequest$json = const {
  '1': 'GetOrderIdRequest',
  '2': const [
    const {'1': 'order_id', '3': 1, '4': 1, '5': 9, '10': 'orderId'},
  ],
};

/// Descriptor for `GetOrderIdRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getOrderIdRequestDescriptor = $convert.base64Decode('ChFHZXRPcmRlcklkUmVxdWVzdBIZCghvcmRlcl9pZBgBIAEoCVIHb3JkZXJJZA==');
@$core.Deprecated('Use getOrderIdResponseDescriptor instead')
const GetOrderIdResponse$json = const {
  '1': 'GetOrderIdResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 1, '5': 11, '6': '.order.v1.OrderProperties', '10': 'data'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `GetOrderIdResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getOrderIdResponseDescriptor = $convert.base64Decode('ChJHZXRPcmRlcklkUmVzcG9uc2USLQoEZGF0YRgBIAEoCzIZLm9yZGVyLnYxLk9yZGVyUHJvcGVydGllc1IEZGF0YRIWCgZzdGF0dXMYAyABKAlSBnN0YXR1cw==');
@$core.Deprecated('Use getStatusRequestDescriptor instead')
const GetStatusRequest$json = const {
  '1': 'GetStatusRequest',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 9, '10': 'userId'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'limit', '3': 3, '4': 1, '5': 5, '10': 'limit'},
    const {'1': 'offset', '3': 4, '4': 1, '5': 5, '10': 'offset'},
  ],
};

/// Descriptor for `GetStatusRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getStatusRequestDescriptor = $convert.base64Decode('ChBHZXRTdGF0dXNSZXF1ZXN0EhcKB3VzZXJfaWQYASABKAlSBnVzZXJJZBIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cxIUCgVsaW1pdBgDIAEoBVIFbGltaXQSFgoGb2Zmc2V0GAQgASgFUgZvZmZzZXQ=');
@$core.Deprecated('Use getStatusResponseDescriptor instead')
const GetStatusResponse$json = const {
  '1': 'GetStatusResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.order.v1.OrderProperties', '10': 'data'},
    const {'1': 'status', '3': 3, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `GetStatusResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getStatusResponseDescriptor = $convert.base64Decode('ChFHZXRTdGF0dXNSZXNwb25zZRItCgRkYXRhGAEgAygLMhkub3JkZXIudjEuT3JkZXJQcm9wZXJ0aWVzUgRkYXRhEhYKBnN0YXR1cxgDIAEoCVIGc3RhdHVz');
@$core.Deprecated('Use allOrdersRequestDescriptor instead')
const AllOrdersRequest$json = const {
  '1': 'AllOrdersRequest',
  '2': const [
    const {'1': 'limit', '3': 1, '4': 1, '5': 5, '10': 'limit'},
    const {'1': 'offset', '3': 2, '4': 1, '5': 5, '10': 'offset'},
  ],
};

/// Descriptor for `AllOrdersRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List allOrdersRequestDescriptor = $convert.base64Decode('ChBBbGxPcmRlcnNSZXF1ZXN0EhQKBWxpbWl0GAEgASgFUgVsaW1pdBIWCgZvZmZzZXQYAiABKAVSBm9mZnNldA==');
@$core.Deprecated('Use allOrdersResponseDescriptor instead')
const AllOrdersResponse$json = const {
  '1': 'AllOrdersResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.order.v1.OrderProperties', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `AllOrdersResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List allOrdersResponseDescriptor = $convert.base64Decode('ChFBbGxPcmRlcnNSZXNwb25zZRItCgRkYXRhGAEgAygLMhkub3JkZXIudjEuT3JkZXJQcm9wZXJ0aWVzUgRkYXRhEhYKBnN0YXR1cxgCIAEoCVIGc3RhdHVz');
@$core.Deprecated('Use getDeliveryCodeRequestDescriptor instead')
const GetDeliveryCodeRequest$json = const {
  '1': 'GetDeliveryCodeRequest',
  '2': const [
    const {'1': 'order_id', '3': 1, '4': 1, '5': 9, '10': 'orderId'},
  ],
};

/// Descriptor for `GetDeliveryCodeRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getDeliveryCodeRequestDescriptor = $convert.base64Decode('ChZHZXREZWxpdmVyeUNvZGVSZXF1ZXN0EhkKCG9yZGVyX2lkGAEgASgJUgdvcmRlcklk');
@$core.Deprecated('Use getDeliveryCodeResponseDescriptor instead')
const GetDeliveryCodeResponse$json = const {
  '1': 'GetDeliveryCodeResponse',
  '2': const [
    const {'1': 'delivery_code', '3': 1, '4': 1, '5': 5, '10': 'deliveryCode'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `GetDeliveryCodeResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getDeliveryCodeResponseDescriptor = $convert.base64Decode('ChdHZXREZWxpdmVyeUNvZGVSZXNwb25zZRIjCg1kZWxpdmVyeV9jb2RlGAEgASgFUgxkZWxpdmVyeUNvZGUSFgoGc3RhdHVzGAIgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use orderPropertiesDescriptor instead')
const OrderProperties$json = const {
  '1': 'OrderProperties',
  '2': const [
    const {'1': 'order_id', '3': 1, '4': 1, '5': 9, '10': 'orderId'},
    const {'1': 'reference', '3': 2, '4': 1, '5': 9, '10': 'reference'},
    const {'1': 'subtotal', '3': 3, '4': 1, '5': 1, '10': 'subtotal'},
    const {'1': 'delivery_cost', '3': 4, '4': 1, '5': 1, '10': 'deliveryCost'},
    const {'1': 'total_cost', '3': 5, '4': 1, '5': 1, '10': 'totalCost'},
    const {'1': 'delivery_Type', '3': 6, '4': 1, '5': 9, '10': 'deliveryType'},
    const {'1': 'user_id', '3': 7, '4': 1, '5': 9, '10': 'userId'},
    const {'1': 'delivery_address', '3': 8, '4': 1, '5': 11, '6': '.order.v1.InNewOrderDeliveryAddress', '10': 'deliveryAddress'},
    const {'1': 'order_items', '3': 9, '4': 3, '5': 11, '6': '.order.v1.InNewOrderItems', '10': 'orderItems'},
    const {'1': 'status', '3': 10, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'created_at', '3': 11, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 12, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'currency', '3': 13, '4': 1, '5': 9, '10': 'currency'},
    const {'1': 'paymentMethod', '3': 14, '4': 1, '5': 9, '10': 'paymentMethod'},
    const {'1': 'delivery_code', '3': 15, '4': 1, '5': 5, '10': 'deliveryCode'},
    const {'1': 'acceptance_time', '3': 16, '4': 1, '5': 9, '10': 'acceptanceTime'},
    const {'1': 'shopper_assigned_time', '3': 17, '4': 1, '5': 9, '10': 'shopperAssignedTime'},
    const {'1': 'shopping_completed_time', '3': 18, '4': 1, '5': 9, '10': 'shoppingCompletedTime'},
    const {'1': 'in_progress_time', '3': 19, '4': 1, '5': 9, '10': 'inProgressTime'},
    const {'1': 'delivery_time', '3': 20, '4': 1, '5': 9, '10': 'deliveryTime'},
    const {'1': 'schedule', '3': 21, '4': 1, '5': 9, '10': 'schedule'},
    const {'1': 'tip', '3': 22, '4': 1, '5': 1, '10': 'tip'},
  ],
};

/// Descriptor for `OrderProperties`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderPropertiesDescriptor = $convert.base64Decode('Cg9PcmRlclByb3BlcnRpZXMSGQoIb3JkZXJfaWQYASABKAlSB29yZGVySWQSHAoJcmVmZXJlbmNlGAIgASgJUglyZWZlcmVuY2USGgoIc3VidG90YWwYAyABKAFSCHN1YnRvdGFsEiMKDWRlbGl2ZXJ5X2Nvc3QYBCABKAFSDGRlbGl2ZXJ5Q29zdBIdCgp0b3RhbF9jb3N0GAUgASgBUgl0b3RhbENvc3QSIwoNZGVsaXZlcnlfVHlwZRgGIAEoCVIMZGVsaXZlcnlUeXBlEhcKB3VzZXJfaWQYByABKAlSBnVzZXJJZBJOChBkZWxpdmVyeV9hZGRyZXNzGAggASgLMiMub3JkZXIudjEuSW5OZXdPcmRlckRlbGl2ZXJ5QWRkcmVzc1IPZGVsaXZlcnlBZGRyZXNzEjoKC29yZGVyX2l0ZW1zGAkgAygLMhkub3JkZXIudjEuSW5OZXdPcmRlckl0ZW1zUgpvcmRlckl0ZW1zEhYKBnN0YXR1cxgKIAEoCVIGc3RhdHVzEh0KCmNyZWF0ZWRfYXQYCyABKAlSCWNyZWF0ZWRBdBIdCgp1cGRhdGVkX2F0GAwgASgJUgl1cGRhdGVkQXQSGgoIY3VycmVuY3kYDSABKAlSCGN1cnJlbmN5EiQKDXBheW1lbnRNZXRob2QYDiABKAlSDXBheW1lbnRNZXRob2QSIwoNZGVsaXZlcnlfY29kZRgPIAEoBVIMZGVsaXZlcnlDb2RlEicKD2FjY2VwdGFuY2VfdGltZRgQIAEoCVIOYWNjZXB0YW5jZVRpbWUSMgoVc2hvcHBlcl9hc3NpZ25lZF90aW1lGBEgASgJUhNzaG9wcGVyQXNzaWduZWRUaW1lEjYKF3Nob3BwaW5nX2NvbXBsZXRlZF90aW1lGBIgASgJUhVzaG9wcGluZ0NvbXBsZXRlZFRpbWUSKAoQaW5fcHJvZ3Jlc3NfdGltZRgTIAEoCVIOaW5Qcm9ncmVzc1RpbWUSIwoNZGVsaXZlcnlfdGltZRgUIAEoCVIMZGVsaXZlcnlUaW1lEhoKCHNjaGVkdWxlGBUgASgJUghzY2hlZHVsZRIQCgN0aXAYFiABKAFSA3RpcA==');
