///
//  Generated code. Do not modify.
//  source: agent.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class AvailabilityStatus extends $pb.ProtobufEnum {
  static const AvailabilityStatus AVAILABLE = AvailabilityStatus._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AVAILABLE');
  static const AvailabilityStatus ON_ERRAND = AvailabilityStatus._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ON_ERRAND');
  static const AvailabilityStatus UNAVAILABLE = AvailabilityStatus._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'UNAVAILABLE');

  static const $core.List<AvailabilityStatus> values = <AvailabilityStatus> [
    AVAILABLE,
    ON_ERRAND,
    UNAVAILABLE,
  ];

  static final $core.Map<$core.int, AvailabilityStatus> _byValue = $pb.ProtobufEnum.initByValue(values);
  static AvailabilityStatus? valueOf($core.int value) => _byValue[value];

  const AvailabilityStatus._($core.int v, $core.String n) : super(v, n);
}

class OrderRequestStatus extends $pb.ProtobufEnum {
  static const OrderRequestStatus UNASSIGNED = OrderRequestStatus._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'UNASSIGNED');
  static const OrderRequestStatus ASSIGNED = OrderRequestStatus._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ASSIGNED');
  static const OrderRequestStatus ARRIVED_AT_STORE = OrderRequestStatus._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ARRIVED_AT_STORE');
  static const OrderRequestStatus PURCHASED_ITEM = OrderRequestStatus._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PURCHASED_ITEM');
  static const OrderRequestStatus AWAITING_CONFIRMATION = OrderRequestStatus._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AWAITING_CONFIRMATION');
  static const OrderRequestStatus DROPPED_OFF = OrderRequestStatus._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'DROPPED_OFF');
  static const OrderRequestStatus ON_HOLD = OrderRequestStatus._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ON_HOLD');
  static const OrderRequestStatus COMPLETED = OrderRequestStatus._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'COMPLETED');

  static const $core.List<OrderRequestStatus> values = <OrderRequestStatus> [
    UNASSIGNED,
    ASSIGNED,
    ARRIVED_AT_STORE,
    PURCHASED_ITEM,
    AWAITING_CONFIRMATION,
    DROPPED_OFF,
    ON_HOLD,
    COMPLETED,
  ];

  static final $core.Map<$core.int, OrderRequestStatus> _byValue = $pb.ProtobufEnum.initByValue(values);
  static OrderRequestStatus? valueOf($core.int value) => _byValue[value];

  const OrderRequestStatus._($core.int v, $core.String n) : super(v, n);
}

class AcceptanceStatus extends $pb.ProtobufEnum {
  static const AcceptanceStatus accept = AcceptanceStatus._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'accept');
  static const AcceptanceStatus decline = AcceptanceStatus._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'decline');

  static const $core.List<AcceptanceStatus> values = <AcceptanceStatus> [
    accept,
    decline,
  ];

  static final $core.Map<$core.int, AcceptanceStatus> _byValue = $pb.ProtobufEnum.initByValue(values);
  static AcceptanceStatus? valueOf($core.int value) => _byValue[value];

  const AcceptanceStatus._($core.int v, $core.String n) : super(v, n);
}

