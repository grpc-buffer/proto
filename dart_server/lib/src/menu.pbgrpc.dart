///
//  Generated code. Do not modify.
//  source: menu.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'menu.pb.dart' as $0;
export 'menu.pb.dart';

class menuServiceClient extends $grpc.Client {
  static final _$createMenu = $grpc.ClientMethod<$0.MenuItems, $0.MenuResponse>(
      '/menu_service.v1.menuService/CreateMenu',
      ($0.MenuItems value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$updatemenu = $grpc.ClientMethod<$0.MenuItems, $0.MenuResponse>(
      '/menu_service.v1.menuService/Updatemenu',
      ($0.MenuItems value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$viewMenu =
      $grpc.ClientMethod<$0.MenuRequestItem, $0.ViewMenuResponse>(
          '/menu_service.v1.menuService/ViewMenu',
          ($0.MenuRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ViewMenuResponse.fromBuffer(value));
  static final _$decreaseMenu =
      $grpc.ClientMethod<$0.DecreaseMenuRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/DecreaseMenu',
          ($0.DecreaseMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$deleteMenu =
      $grpc.ClientMethod<$0.MenuRequestItem, $0.MenuResponse>(
          '/menu_service.v1.menuService/DeleteMenu',
          ($0.MenuRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$listMenus =
      $grpc.ClientMethod<$0.EmptyMenuRequest, $0.ListMenuResponse>(
          '/menu_service.v1.menuService/ListMenus',
          ($0.EmptyMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListMenuResponse.fromBuffer(value));
  static final _$listMenusByField =
      $grpc.ClientMethod<$0.MenuRequestItem, $0.ListMenuResponse>(
          '/menu_service.v1.menuService/ListMenusByField',
          ($0.MenuRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListMenuResponse.fromBuffer(value));
  static final _$addPackageCategory =
      $grpc.ClientMethod<$0.AddPackageCategoryRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/AddPackageCategory',
          ($0.AddPackageCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$viewPackageCategory = $grpc.ClientMethod<
          $0.PackageCategoryRequest, $0.ViewPackageCategoryResponse>(
      '/menu_service.v1.menuService/ViewPackageCategory',
      ($0.PackageCategoryRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ViewPackageCategoryResponse.fromBuffer(value));
  static final _$listAllPackageCategory =
      $grpc.ClientMethod<$0.EmptyMenuRequest, $0.ListPackageCategoryResponse>(
          '/menu_service.v1.menuService/ListAllPackageCategory',
          ($0.EmptyMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPackageCategoryResponse.fromBuffer(value));
  static final _$editPackageCategory =
      $grpc.ClientMethod<$0.PackageCategoryItem, $0.MenuResponse>(
          '/menu_service.v1.menuService/EditPackageCategory',
          ($0.PackageCategoryItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$removePackageCategory =
      $grpc.ClientMethod<$0.PackageCategoryRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/RemovePackageCategory',
          ($0.PackageCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$createItem = $grpc.ClientMethod<$0.Item, $0.MenuResponse>(
      '/menu_service.v1.menuService/CreateItem',
      ($0.Item value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$viewItem = $grpc.ClientMethod<$0.ItemRequest, $0.Item>(
      '/menu_service.v1.menuService/ViewItem',
      ($0.ItemRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Item.fromBuffer(value));
  static final _$listItems =
      $grpc.ClientMethod<$0.EmptyMenuRequest, $0.ListItemsResponse>(
          '/menu_service.v1.menuService/ListItems',
          ($0.EmptyMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListItemsResponse.fromBuffer(value));
  static final _$editItem = $grpc.ClientMethod<$0.Item, $0.MenuResponse>(
      '/menu_service.v1.menuService/EditItem',
      ($0.Item value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$deleteItem =
      $grpc.ClientMethod<$0.ItemRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/DeleteItem',
          ($0.ItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$addItemCategory =
      $grpc.ClientMethod<$0.ItemCategory, $0.MenuResponse>(
          '/menu_service.v1.menuService/AddItemCategory',
          ($0.ItemCategory value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$viewItemCategory =
      $grpc.ClientMethod<$0.ItemCategoryRequest, $0.ItemCategory>(
          '/menu_service.v1.menuService/ViewItemCategory',
          ($0.ItemCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ItemCategory.fromBuffer(value));
  static final _$listAllItemCategory =
      $grpc.ClientMethod<$0.EmptyMenuRequest, $0.ListItemCategoryResponse>(
          '/menu_service.v1.menuService/ListAllItemCategory',
          ($0.EmptyMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListItemCategoryResponse.fromBuffer(value));
  static final _$editItemCategory =
      $grpc.ClientMethod<$0.ItemCategory, $0.MenuResponse>(
          '/menu_service.v1.menuService/EditItemCategory',
          ($0.ItemCategory value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$removeItemCategory =
      $grpc.ClientMethod<$0.ItemCategoryRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/RemoveItemCategory',
          ($0.ItemCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$createPackage =
      $grpc.ClientMethod<$0.Package, $0.MenuResponse>(
          '/menu_service.v1.menuService/CreatePackage',
          ($0.Package value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$likePackage =
      $grpc.ClientMethod<$0.LikeRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/LikePackage',
          ($0.LikeRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$unLikePackage =
      $grpc.ClientMethod<$0.LikeRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/UnLikePackage',
          ($0.LikeRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$listUserLikedPackage =
      $grpc.ClientMethod<$0.LikeRequest, $0.ListPackageResponse>(
          '/menu_service.v1.menuService/ListUserLikedPackage',
          ($0.LikeRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPackageResponse.fromBuffer(value));
  static final _$viewPackage =
      $grpc.ClientMethod<$0.PackageRequest, $0.Package>(
          '/menu_service.v1.menuService/ViewPackage',
          ($0.PackageRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Package.fromBuffer(value));
  static final _$listAllPackages =
      $grpc.ClientMethod<$0.PackageRequest, $0.ListPackageResponse>(
          '/menu_service.v1.menuService/ListAllPackages',
          ($0.PackageRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPackageResponse.fromBuffer(value));
  static final _$listPackagesByServiceArea =
      $grpc.ClientMethod<$0.PackageRequest, $0.ListPackageResponse>(
          '/menu_service.v1.menuService/ListPackagesByServiceArea',
          ($0.PackageRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPackageResponse.fromBuffer(value));
  static final _$listPackagesByCategory =
      $grpc.ClientMethod<$0.PackageRequest, $0.ListPackageResponse>(
          '/menu_service.v1.menuService/ListPackagesByCategory',
          ($0.PackageRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPackageResponse.fromBuffer(value));
  static final _$editPackage = $grpc.ClientMethod<$0.Package, $0.MenuResponse>(
      '/menu_service.v1.menuService/EditPackage',
      ($0.Package value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$removePackage =
      $grpc.ClientMethod<$0.PackageRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/RemovePackage',
          ($0.PackageRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$listFeed = $grpc.ClientMethod<$0.PackageRequest, $0.FeedList>(
      '/menu_service.v1.menuService/ListFeed',
      ($0.PackageRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.FeedList.fromBuffer(value));
  static final _$createPackageItem =
      $grpc.ClientMethod<$0.PackageItemCreate, $0.MenuResponse>(
          '/menu_service.v1.menuService/CreatePackageItem',
          ($0.PackageItemCreate value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$addItemToPackageItem =
      $grpc.ClientMethod<$0.AddItemPackage, $0.MenuResponse>(
          '/menu_service.v1.menuService/AddItemToPackageItem',
          ($0.AddItemPackage value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$listPackageItems =
      $grpc.ClientMethod<$0.EmptyMenuRequest, $0.ListPackageItemResponse>(
          '/menu_service.v1.menuService/ListPackageItems',
          ($0.EmptyMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPackageItemResponse.fromBuffer(value));
  static final _$updateTag =
      $grpc.ClientMethod<$0.PackageTagItem, $0.MenuResponse>(
          '/menu_service.v1.menuService/UpdateTag',
          ($0.PackageTagItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$addTag =
      $grpc.ClientMethod<$0.PackageTagRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/AddTag',
          ($0.PackageTagRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$viewTag =
      $grpc.ClientMethod<$0.PackageTagRequestItem, $0.PackageTagItem>(
          '/menu_service.v1.menuService/ViewTag',
          ($0.PackageTagRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.PackageTagItem.fromBuffer(value));
  static final _$deleteTag =
      $grpc.ClientMethod<$0.PackageTagRequestItem, $0.MenuResponse>(
          '/menu_service.v1.menuService/DeleteTag',
          ($0.PackageTagRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$listTag =
      $grpc.ClientMethod<$0.EmptyMenuRequest, $0.ListPackageTagResponse>(
          '/menu_service.v1.menuService/ListTag',
          ($0.EmptyMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPackageTagResponse.fromBuffer(value));
  static final _$listTagByPackage =
      $grpc.ClientMethod<$0.PackageTagRequestItem, $0.ListPackageTagResponse>(
          '/menu_service.v1.menuService/ListTagByPackage',
          ($0.PackageTagRequestItem value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListPackageTagResponse.fromBuffer(value));
  static final _$viewItemTags =
      $grpc.ClientMethod<$0.ItemTagRequest, $0.ListItemTagResponse>(
          '/menu_service.v1.menuService/ViewItemTags',
          ($0.ItemTagRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListItemTagResponse.fromBuffer(value));
  static final _$updateItemTag =
      $grpc.ClientMethod<$0.ItemTag, $0.ItemTagResponse>(
          '/menu_service.v1.menuService/UpdateItemTag',
          ($0.ItemTag value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ItemTagResponse.fromBuffer(value));
  static final _$addItemTag =
      $grpc.ClientMethod<$0.ItemTagCreation, $0.ItemTagResponse>(
          '/menu_service.v1.menuService/AddItemTag',
          ($0.ItemTagCreation value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ItemTagResponse.fromBuffer(value));
  static final _$deleteItemTag =
      $grpc.ClientMethod<$0.ItemTagRequest, $0.ItemTagResponse>(
          '/menu_service.v1.menuService/DeleteItemTag',
          ($0.ItemTagRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ItemTagResponse.fromBuffer(value));
  static final _$listAllItemTags =
      $grpc.ClientMethod<$0.EmptyMenuRequest, $0.ListItemTagResponse>(
          '/menu_service.v1.menuService/ListAllItemTags',
          ($0.EmptyMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListItemTagResponse.fromBuffer(value));
  static final _$listItemTagsByItem =
      $grpc.ClientMethod<$0.ItemTagRequest, $0.ListItemTagResponse>(
          '/menu_service.v1.menuService/ListItemTagsByItem',
          ($0.ItemTagRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ListItemTagResponse.fromBuffer(value));
  static final _$createNutrient =
      $grpc.ClientMethod<$0.Nutrient, $0.MenuResponse>(
          '/menu_service.v1.menuService/CreateNutrient',
          ($0.Nutrient value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$addItemNutrient =
      $grpc.ClientMethod<$0.NutrientItemRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/AddItemNutrient',
          ($0.NutrientItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$removeItemNutrient =
      $grpc.ClientMethod<$0.NutrientItemRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/RemoveItemNutrient',
          ($0.NutrientItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$viewItemNutrients =
      $grpc.ClientMethod<$0.NutrientItemRequest, $0.NutrientList>(
          '/menu_service.v1.menuService/ViewItemNutrients',
          ($0.NutrientItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.NutrientList.fromBuffer(value));
  static final _$viewNutrientItems =
      $grpc.ClientMethod<$0.NutrientItemRequest, $0.NutrientList>(
          '/menu_service.v1.menuService/ViewNutrientItems',
          ($0.NutrientItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.NutrientList.fromBuffer(value));
  static final _$listNutrients =
      $grpc.ClientMethod<$0.EmptyMenuRequest, $0.NutrientList>(
          '/menu_service.v1.menuService/ListNutrients',
          ($0.EmptyMenuRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.NutrientList.fromBuffer(value));
  static final _$search =
      $grpc.ClientMethod<$0.SearchRequest, $0.SearchResponse>(
          '/menu_service.v1.menuService/Search',
          ($0.SearchRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.SearchResponse.fromBuffer(value));
  static final _$menuSearchSuggestions = $grpc.ClientMethod<
          $0.MenuSearchSuggestionsRequest, $0.MenuSearchSuggestionsResponse>(
      '/menu_service.v1.menuService/MenuSearchSuggestions',
      ($0.MenuSearchSuggestionsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.MenuSearchSuggestionsResponse.fromBuffer(value));
  static final _$menuSearchItemSuggestions = $grpc.ClientMethod<
          $0.MenuSearchItemSuggestionsRequest,
          $0.MenuSearchItemSuggestionsResponse>(
      '/menu_service.v1.menuService/MenuSearchItemSuggestions',
      ($0.MenuSearchItemSuggestionsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.MenuSearchItemSuggestionsResponse.fromBuffer(value));
  static final _$menuSearchPackageSuggestions = $grpc.ClientMethod<
          $0.MenuSearchPackageSuggestionsRequest,
          $0.MenuSearchPackageSuggestionsResponse>(
      '/menu_service.v1.menuService/MenuSearchPackageSuggestions',
      ($0.MenuSearchPackageSuggestionsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.MenuSearchPackageSuggestionsResponse.fromBuffer(value));
  static final _$menuSearchItems =
      $grpc.ClientMethod<$0.MenuSearchItemRequest, $0.MenuSearchItemResponse>(
          '/menu_service.v1.menuService/MenuSearchItems',
          ($0.MenuSearchItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.MenuSearchItemResponse.fromBuffer(value));
  static final _$menuSearchPackages = $grpc.ClientMethod<
          $0.MenuSearchPackageRequest, $0.MenuSearchPackageResponse>(
      '/menu_service.v1.menuService/MenuSearchPackages',
      ($0.MenuSearchPackageRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.MenuSearchPackageResponse.fromBuffer(value));
  static final _$uploadImage =
      $grpc.ClientMethod<$0.UploadImageRequest, $0.MenuResponse>(
          '/menu_service.v1.menuService/UploadImage',
          ($0.UploadImageRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$createCustomPackage =
      $grpc.ClientMethod<$0.CustomPackage, $0.MenuResponse>(
          '/menu_service.v1.menuService/CreateCustomPackage',
          ($0.CustomPackage value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$listMyCustomPackage = $grpc.ClientMethod<
          $0.ListCustomPackageRequest, $0.ListCustomPackageResponse>(
      '/menu_service.v1.menuService/ListMyCustomPackage',
      ($0.ListCustomPackageRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ListCustomPackageResponse.fromBuffer(value));
  static final _$listTemplateCustomPackage = $grpc.ClientMethod<
          $0.ListCustomPackageRequest, $0.ListCustomPackageResponse>(
      '/menu_service.v1.menuService/ListTemplateCustomPackage',
      ($0.ListCustomPackageRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ListCustomPackageResponse.fromBuffer(value));
  static final _$viewCustomPackage =
      $grpc.ClientMethod<$0.CustomPackage, $0.CustomPackage>(
          '/menu_service.v1.menuService/ViewCustomPackage',
          ($0.CustomPackage value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.CustomPackage.fromBuffer(value));
  static final _$deleteCustomPackage =
      $grpc.ClientMethod<$0.CustomPackage, $0.MenuResponse>(
          '/menu_service.v1.menuService/DeleteCustomPackage',
          ($0.CustomPackage value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.MenuResponse.fromBuffer(value));
  static final _$addCustomPackageItems =
      $grpc.ClientMethod<$0.CustomPackage, $0.CustomPackage>(
          '/menu_service.v1.menuService/AddCustomPackageItems',
          ($0.CustomPackage value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.CustomPackage.fromBuffer(value));

  menuServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.MenuResponse> createMenu($0.MenuItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createMenu, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> updatemenu($0.MenuItems request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updatemenu, request, options: options);
  }

  $grpc.ResponseFuture<$0.ViewMenuResponse> viewMenu($0.MenuRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewMenu, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> decreaseMenu(
      $0.DecreaseMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$decreaseMenu, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> deleteMenu($0.MenuRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteMenu, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListMenuResponse> listMenus(
      $0.EmptyMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listMenus, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListMenuResponse> listMenusByField(
      $0.MenuRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listMenusByField, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> addPackageCategory(
      $0.AddPackageCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addPackageCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.ViewPackageCategoryResponse> viewPackageCategory(
      $0.PackageCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewPackageCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPackageCategoryResponse> listAllPackageCategory(
      $0.EmptyMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listAllPackageCategory, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> editPackageCategory(
      $0.PackageCategoryItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$editPackageCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> removePackageCategory(
      $0.PackageCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removePackageCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> createItem($0.Item request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.Item> viewItem($0.ItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListItemsResponse> listItems(
      $0.EmptyMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listItems, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> editItem($0.Item request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$editItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> deleteItem($0.ItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> addItemCategory($0.ItemCategory request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addItemCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.ItemCategory> viewItemCategory(
      $0.ItemCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewItemCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListItemCategoryResponse> listAllItemCategory(
      $0.EmptyMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listAllItemCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> editItemCategory(
      $0.ItemCategory request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$editItemCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> removeItemCategory(
      $0.ItemCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removeItemCategory, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> createPackage($0.Package request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> likePackage($0.LikeRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$likePackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> unLikePackage($0.LikeRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$unLikePackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPackageResponse> listUserLikedPackage(
      $0.LikeRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listUserLikedPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.Package> viewPackage($0.PackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPackageResponse> listAllPackages(
      $0.PackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listAllPackages, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPackageResponse> listPackagesByServiceArea(
      $0.PackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listPackagesByServiceArea, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.ListPackageResponse> listPackagesByCategory(
      $0.PackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listPackagesByCategory, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> editPackage($0.Package request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$editPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> removePackage($0.PackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removePackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.FeedList> listFeed($0.PackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listFeed, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> createPackageItem(
      $0.PackageItemCreate request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createPackageItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> addItemToPackageItem(
      $0.AddItemPackage request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addItemToPackageItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPackageItemResponse> listPackageItems(
      $0.EmptyMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listPackageItems, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> updateTag($0.PackageTagItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> addTag($0.PackageTagRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.PackageTagItem> viewTag(
      $0.PackageTagRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> deleteTag(
      $0.PackageTagRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPackageTagResponse> listTag(
      $0.EmptyMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListPackageTagResponse> listTagByPackage(
      $0.PackageTagRequestItem request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listTagByPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListItemTagResponse> viewItemTags(
      $0.ItemTagRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewItemTags, request, options: options);
  }

  $grpc.ResponseFuture<$0.ItemTagResponse> updateItemTag($0.ItemTag request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateItemTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.ItemTagResponse> addItemTag(
      $0.ItemTagCreation request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addItemTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.ItemTagResponse> deleteItemTag(
      $0.ItemTagRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteItemTag, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListItemTagResponse> listAllItemTags(
      $0.EmptyMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listAllItemTags, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListItemTagResponse> listItemTagsByItem(
      $0.ItemTagRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listItemTagsByItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> createNutrient($0.Nutrient request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createNutrient, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> addItemNutrient(
      $0.NutrientItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addItemNutrient, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> removeItemNutrient(
      $0.NutrientItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$removeItemNutrient, request, options: options);
  }

  $grpc.ResponseFuture<$0.NutrientList> viewItemNutrients(
      $0.NutrientItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewItemNutrients, request, options: options);
  }

  $grpc.ResponseFuture<$0.NutrientList> viewNutrientItems(
      $0.NutrientItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewNutrientItems, request, options: options);
  }

  $grpc.ResponseFuture<$0.NutrientList> listNutrients(
      $0.EmptyMenuRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listNutrients, request, options: options);
  }

  $grpc.ResponseFuture<$0.SearchResponse> search($0.SearchRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$search, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuSearchSuggestionsResponse> menuSearchSuggestions(
      $0.MenuSearchSuggestionsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$menuSearchSuggestions, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuSearchItemSuggestionsResponse>
      menuSearchItemSuggestions($0.MenuSearchItemSuggestionsRequest request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$menuSearchItemSuggestions, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.MenuSearchPackageSuggestionsResponse>
      menuSearchPackageSuggestions(
          $0.MenuSearchPackageSuggestionsRequest request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$menuSearchPackageSuggestions, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.MenuSearchItemResponse> menuSearchItems(
      $0.MenuSearchItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$menuSearchItems, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuSearchPackageResponse> menuSearchPackages(
      $0.MenuSearchPackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$menuSearchPackages, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> uploadImage(
      $async.Stream<$0.UploadImageRequest> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$uploadImage, request, options: options)
        .single;
  }

  $grpc.ResponseFuture<$0.MenuResponse> createCustomPackage(
      $0.CustomPackage request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createCustomPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListCustomPackageResponse> listMyCustomPackage(
      $0.ListCustomPackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listMyCustomPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.ListCustomPackageResponse> listTemplateCustomPackage(
      $0.ListCustomPackageRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listTemplateCustomPackage, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.CustomPackage> viewCustomPackage(
      $0.CustomPackage request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$viewCustomPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.MenuResponse> deleteCustomPackage(
      $0.CustomPackage request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteCustomPackage, request, options: options);
  }

  $grpc.ResponseFuture<$0.CustomPackage> addCustomPackageItems(
      $0.CustomPackage request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addCustomPackageItems, request, options: options);
  }
}

abstract class menuServiceBase extends $grpc.Service {
  $core.String get $name => 'menu_service.v1.menuService';

  menuServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.MenuItems, $0.MenuResponse>(
        'CreateMenu',
        createMenu_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MenuItems.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuItems, $0.MenuResponse>(
        'Updatemenu',
        updatemenu_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MenuItems.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuRequestItem, $0.ViewMenuResponse>(
        'ViewMenu',
        viewMenu_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MenuRequestItem.fromBuffer(value),
        ($0.ViewMenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DecreaseMenuRequest, $0.MenuResponse>(
        'DecreaseMenu',
        decreaseMenu_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.DecreaseMenuRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuRequestItem, $0.MenuResponse>(
        'DeleteMenu',
        deleteMenu_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MenuRequestItem.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyMenuRequest, $0.ListMenuResponse>(
        'ListMenus',
        listMenus_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EmptyMenuRequest.fromBuffer(value),
        ($0.ListMenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuRequestItem, $0.ListMenuResponse>(
        'ListMenusByField',
        listMenusByField_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MenuRequestItem.fromBuffer(value),
        ($0.ListMenuResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.AddPackageCategoryRequest, $0.MenuResponse>(
            'AddPackageCategory',
            addPackageCategory_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.AddPackageCategoryRequest.fromBuffer(value),
            ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageCategoryRequest,
            $0.ViewPackageCategoryResponse>(
        'ViewPackageCategory',
        viewPackageCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PackageCategoryRequest.fromBuffer(value),
        ($0.ViewPackageCategoryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyMenuRequest,
            $0.ListPackageCategoryResponse>(
        'ListAllPackageCategory',
        listAllPackageCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EmptyMenuRequest.fromBuffer(value),
        ($0.ListPackageCategoryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageCategoryItem, $0.MenuResponse>(
        'EditPackageCategory',
        editPackageCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PackageCategoryItem.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageCategoryRequest, $0.MenuResponse>(
        'RemovePackageCategory',
        removePackageCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PackageCategoryRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Item, $0.MenuResponse>(
        'CreateItem',
        createItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Item.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemRequest, $0.Item>(
        'ViewItem',
        viewItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemRequest.fromBuffer(value),
        ($0.Item value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyMenuRequest, $0.ListItemsResponse>(
        'ListItems',
        listItems_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EmptyMenuRequest.fromBuffer(value),
        ($0.ListItemsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Item, $0.MenuResponse>(
        'EditItem',
        editItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Item.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemRequest, $0.MenuResponse>(
        'DeleteItem',
        deleteItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemCategory, $0.MenuResponse>(
        'AddItemCategory',
        addItemCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemCategory.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemCategoryRequest, $0.ItemCategory>(
        'ViewItemCategory',
        viewItemCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ItemCategoryRequest.fromBuffer(value),
        ($0.ItemCategory value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.EmptyMenuRequest, $0.ListItemCategoryResponse>(
            'ListAllItemCategory',
            listAllItemCategory_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.EmptyMenuRequest.fromBuffer(value),
            ($0.ListItemCategoryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemCategory, $0.MenuResponse>(
        'EditItemCategory',
        editItemCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemCategory.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemCategoryRequest, $0.MenuResponse>(
        'RemoveItemCategory',
        removeItemCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ItemCategoryRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Package, $0.MenuResponse>(
        'CreatePackage',
        createPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Package.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LikeRequest, $0.MenuResponse>(
        'LikePackage',
        likePackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LikeRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LikeRequest, $0.MenuResponse>(
        'UnLikePackage',
        unLikePackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LikeRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LikeRequest, $0.ListPackageResponse>(
        'ListUserLikedPackage',
        listUserLikedPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LikeRequest.fromBuffer(value),
        ($0.ListPackageResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageRequest, $0.Package>(
        'ViewPackage',
        viewPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageRequest.fromBuffer(value),
        ($0.Package value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageRequest, $0.ListPackageResponse>(
        'ListAllPackages',
        listAllPackages_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageRequest.fromBuffer(value),
        ($0.ListPackageResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageRequest, $0.ListPackageResponse>(
        'ListPackagesByServiceArea',
        listPackagesByServiceArea_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageRequest.fromBuffer(value),
        ($0.ListPackageResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageRequest, $0.ListPackageResponse>(
        'ListPackagesByCategory',
        listPackagesByCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageRequest.fromBuffer(value),
        ($0.ListPackageResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Package, $0.MenuResponse>(
        'EditPackage',
        editPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Package.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageRequest, $0.MenuResponse>(
        'RemovePackage',
        removePackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageRequest, $0.FeedList>(
        'ListFeed',
        listFeed_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageRequest.fromBuffer(value),
        ($0.FeedList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageItemCreate, $0.MenuResponse>(
        'CreatePackageItem',
        createPackageItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageItemCreate.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AddItemPackage, $0.MenuResponse>(
        'AddItemToPackageItem',
        addItemToPackageItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AddItemPackage.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.EmptyMenuRequest, $0.ListPackageItemResponse>(
            'ListPackageItems',
            listPackageItems_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.EmptyMenuRequest.fromBuffer(value),
            ($0.ListPackageItemResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageTagItem, $0.MenuResponse>(
        'UpdateTag',
        updateTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageTagItem.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageTagRequest, $0.MenuResponse>(
        'AddTag',
        addTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackageTagRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageTagRequestItem, $0.PackageTagItem>(
        'ViewTag',
        viewTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PackageTagRequestItem.fromBuffer(value),
        ($0.PackageTagItem value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageTagRequestItem, $0.MenuResponse>(
        'DeleteTag',
        deleteTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PackageTagRequestItem.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.EmptyMenuRequest, $0.ListPackageTagResponse>(
            'ListTag',
            listTag_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.EmptyMenuRequest.fromBuffer(value),
            ($0.ListPackageTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PackageTagRequestItem,
            $0.ListPackageTagResponse>(
        'ListTagByPackage',
        listTagByPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PackageTagRequestItem.fromBuffer(value),
        ($0.ListPackageTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemTagRequest, $0.ListItemTagResponse>(
        'ViewItemTags',
        viewItemTags_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemTagRequest.fromBuffer(value),
        ($0.ListItemTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemTag, $0.ItemTagResponse>(
        'UpdateItemTag',
        updateItemTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemTag.fromBuffer(value),
        ($0.ItemTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemTagCreation, $0.ItemTagResponse>(
        'AddItemTag',
        addItemTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemTagCreation.fromBuffer(value),
        ($0.ItemTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemTagRequest, $0.ItemTagResponse>(
        'DeleteItemTag',
        deleteItemTag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemTagRequest.fromBuffer(value),
        ($0.ItemTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyMenuRequest, $0.ListItemTagResponse>(
        'ListAllItemTags',
        listAllItemTags_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EmptyMenuRequest.fromBuffer(value),
        ($0.ListItemTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemTagRequest, $0.ListItemTagResponse>(
        'ListItemTagsByItem',
        listItemTagsByItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemTagRequest.fromBuffer(value),
        ($0.ListItemTagResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Nutrient, $0.MenuResponse>(
        'CreateNutrient',
        createNutrient_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Nutrient.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.NutrientItemRequest, $0.MenuResponse>(
        'AddItemNutrient',
        addItemNutrient_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.NutrientItemRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.NutrientItemRequest, $0.MenuResponse>(
        'RemoveItemNutrient',
        removeItemNutrient_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.NutrientItemRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.NutrientItemRequest, $0.NutrientList>(
        'ViewItemNutrients',
        viewItemNutrients_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.NutrientItemRequest.fromBuffer(value),
        ($0.NutrientList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.NutrientItemRequest, $0.NutrientList>(
        'ViewNutrientItems',
        viewNutrientItems_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.NutrientItemRequest.fromBuffer(value),
        ($0.NutrientList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EmptyMenuRequest, $0.NutrientList>(
        'ListNutrients',
        listNutrients_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EmptyMenuRequest.fromBuffer(value),
        ($0.NutrientList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SearchRequest, $0.SearchResponse>(
        'Search',
        search_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SearchRequest.fromBuffer(value),
        ($0.SearchResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuSearchSuggestionsRequest,
            $0.MenuSearchSuggestionsResponse>(
        'MenuSearchSuggestions',
        menuSearchSuggestions_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.MenuSearchSuggestionsRequest.fromBuffer(value),
        ($0.MenuSearchSuggestionsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuSearchItemSuggestionsRequest,
            $0.MenuSearchItemSuggestionsResponse>(
        'MenuSearchItemSuggestions',
        menuSearchItemSuggestions_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.MenuSearchItemSuggestionsRequest.fromBuffer(value),
        ($0.MenuSearchItemSuggestionsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuSearchPackageSuggestionsRequest,
            $0.MenuSearchPackageSuggestionsResponse>(
        'MenuSearchPackageSuggestions',
        menuSearchPackageSuggestions_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.MenuSearchPackageSuggestionsRequest.fromBuffer(value),
        ($0.MenuSearchPackageSuggestionsResponse value) =>
            value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuSearchItemRequest,
            $0.MenuSearchItemResponse>(
        'MenuSearchItems',
        menuSearchItems_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.MenuSearchItemRequest.fromBuffer(value),
        ($0.MenuSearchItemResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MenuSearchPackageRequest,
            $0.MenuSearchPackageResponse>(
        'MenuSearchPackages',
        menuSearchPackages_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.MenuSearchPackageRequest.fromBuffer(value),
        ($0.MenuSearchPackageResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UploadImageRequest, $0.MenuResponse>(
        'UploadImage',
        uploadImage,
        true,
        false,
        ($core.List<$core.int> value) =>
            $0.UploadImageRequest.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CustomPackage, $0.MenuResponse>(
        'CreateCustomPackage',
        createCustomPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CustomPackage.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ListCustomPackageRequest,
            $0.ListCustomPackageResponse>(
        'ListMyCustomPackage',
        listMyCustomPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ListCustomPackageRequest.fromBuffer(value),
        ($0.ListCustomPackageResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ListCustomPackageRequest,
            $0.ListCustomPackageResponse>(
        'ListTemplateCustomPackage',
        listTemplateCustomPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ListCustomPackageRequest.fromBuffer(value),
        ($0.ListCustomPackageResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CustomPackage, $0.CustomPackage>(
        'ViewCustomPackage',
        viewCustomPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CustomPackage.fromBuffer(value),
        ($0.CustomPackage value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CustomPackage, $0.MenuResponse>(
        'DeleteCustomPackage',
        deleteCustomPackage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CustomPackage.fromBuffer(value),
        ($0.MenuResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CustomPackage, $0.CustomPackage>(
        'AddCustomPackageItems',
        addCustomPackageItems_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CustomPackage.fromBuffer(value),
        ($0.CustomPackage value) => value.writeToBuffer()));
  }

  $async.Future<$0.MenuResponse> createMenu_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MenuItems> request) async {
    return createMenu(call, await request);
  }

  $async.Future<$0.MenuResponse> updatemenu_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MenuItems> request) async {
    return updatemenu(call, await request);
  }

  $async.Future<$0.ViewMenuResponse> viewMenu_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MenuRequestItem> request) async {
    return viewMenu(call, await request);
  }

  $async.Future<$0.MenuResponse> decreaseMenu_Pre($grpc.ServiceCall call,
      $async.Future<$0.DecreaseMenuRequest> request) async {
    return decreaseMenu(call, await request);
  }

  $async.Future<$0.MenuResponse> deleteMenu_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MenuRequestItem> request) async {
    return deleteMenu(call, await request);
  }

  $async.Future<$0.ListMenuResponse> listMenus_Pre($grpc.ServiceCall call,
      $async.Future<$0.EmptyMenuRequest> request) async {
    return listMenus(call, await request);
  }

  $async.Future<$0.ListMenuResponse> listMenusByField_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MenuRequestItem> request) async {
    return listMenusByField(call, await request);
  }

  $async.Future<$0.MenuResponse> addPackageCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.AddPackageCategoryRequest> request) async {
    return addPackageCategory(call, await request);
  }

  $async.Future<$0.ViewPackageCategoryResponse> viewPackageCategory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.PackageCategoryRequest> request) async {
    return viewPackageCategory(call, await request);
  }

  $async.Future<$0.ListPackageCategoryResponse> listAllPackageCategory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptyMenuRequest> request) async {
    return listAllPackageCategory(call, await request);
  }

  $async.Future<$0.MenuResponse> editPackageCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.PackageCategoryItem> request) async {
    return editPackageCategory(call, await request);
  }

  $async.Future<$0.MenuResponse> removePackageCategory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.PackageCategoryRequest> request) async {
    return removePackageCategory(call, await request);
  }

  $async.Future<$0.MenuResponse> createItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Item> request) async {
    return createItem(call, await request);
  }

  $async.Future<$0.Item> viewItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemRequest> request) async {
    return viewItem(call, await request);
  }

  $async.Future<$0.ListItemsResponse> listItems_Pre($grpc.ServiceCall call,
      $async.Future<$0.EmptyMenuRequest> request) async {
    return listItems(call, await request);
  }

  $async.Future<$0.MenuResponse> editItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Item> request) async {
    return editItem(call, await request);
  }

  $async.Future<$0.MenuResponse> deleteItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemRequest> request) async {
    return deleteItem(call, await request);
  }

  $async.Future<$0.MenuResponse> addItemCategory_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemCategory> request) async {
    return addItemCategory(call, await request);
  }

  $async.Future<$0.ItemCategory> viewItemCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.ItemCategoryRequest> request) async {
    return viewItemCategory(call, await request);
  }

  $async.Future<$0.ListItemCategoryResponse> listAllItemCategory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptyMenuRequest> request) async {
    return listAllItemCategory(call, await request);
  }

  $async.Future<$0.MenuResponse> editItemCategory_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemCategory> request) async {
    return editItemCategory(call, await request);
  }

  $async.Future<$0.MenuResponse> removeItemCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.ItemCategoryRequest> request) async {
    return removeItemCategory(call, await request);
  }

  $async.Future<$0.MenuResponse> createPackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Package> request) async {
    return createPackage(call, await request);
  }

  $async.Future<$0.MenuResponse> likePackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LikeRequest> request) async {
    return likePackage(call, await request);
  }

  $async.Future<$0.MenuResponse> unLikePackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LikeRequest> request) async {
    return unLikePackage(call, await request);
  }

  $async.Future<$0.ListPackageResponse> listUserLikedPackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LikeRequest> request) async {
    return listUserLikedPackage(call, await request);
  }

  $async.Future<$0.Package> viewPackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PackageRequest> request) async {
    return viewPackage(call, await request);
  }

  $async.Future<$0.ListPackageResponse> listAllPackages_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PackageRequest> request) async {
    return listAllPackages(call, await request);
  }

  $async.Future<$0.ListPackageResponse> listPackagesByServiceArea_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PackageRequest> request) async {
    return listPackagesByServiceArea(call, await request);
  }

  $async.Future<$0.ListPackageResponse> listPackagesByCategory_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PackageRequest> request) async {
    return listPackagesByCategory(call, await request);
  }

  $async.Future<$0.MenuResponse> editPackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Package> request) async {
    return editPackage(call, await request);
  }

  $async.Future<$0.MenuResponse> removePackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PackageRequest> request) async {
    return removePackage(call, await request);
  }

  $async.Future<$0.FeedList> listFeed_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PackageRequest> request) async {
    return listFeed(call, await request);
  }

  $async.Future<$0.MenuResponse> createPackageItem_Pre($grpc.ServiceCall call,
      $async.Future<$0.PackageItemCreate> request) async {
    return createPackageItem(call, await request);
  }

  $async.Future<$0.MenuResponse> addItemToPackageItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.AddItemPackage> request) async {
    return addItemToPackageItem(call, await request);
  }

  $async.Future<$0.ListPackageItemResponse> listPackageItems_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptyMenuRequest> request) async {
    return listPackageItems(call, await request);
  }

  $async.Future<$0.MenuResponse> updateTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PackageTagItem> request) async {
    return updateTag(call, await request);
  }

  $async.Future<$0.MenuResponse> addTag_Pre($grpc.ServiceCall call,
      $async.Future<$0.PackageTagRequest> request) async {
    return addTag(call, await request);
  }

  $async.Future<$0.PackageTagItem> viewTag_Pre($grpc.ServiceCall call,
      $async.Future<$0.PackageTagRequestItem> request) async {
    return viewTag(call, await request);
  }

  $async.Future<$0.MenuResponse> deleteTag_Pre($grpc.ServiceCall call,
      $async.Future<$0.PackageTagRequestItem> request) async {
    return deleteTag(call, await request);
  }

  $async.Future<$0.ListPackageTagResponse> listTag_Pre($grpc.ServiceCall call,
      $async.Future<$0.EmptyMenuRequest> request) async {
    return listTag(call, await request);
  }

  $async.Future<$0.ListPackageTagResponse> listTagByPackage_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.PackageTagRequestItem> request) async {
    return listTagByPackage(call, await request);
  }

  $async.Future<$0.ListItemTagResponse> viewItemTags_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemTagRequest> request) async {
    return viewItemTags(call, await request);
  }

  $async.Future<$0.ItemTagResponse> updateItemTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemTag> request) async {
    return updateItemTag(call, await request);
  }

  $async.Future<$0.ItemTagResponse> addItemTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemTagCreation> request) async {
    return addItemTag(call, await request);
  }

  $async.Future<$0.ItemTagResponse> deleteItemTag_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemTagRequest> request) async {
    return deleteItemTag(call, await request);
  }

  $async.Future<$0.ListItemTagResponse> listAllItemTags_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.EmptyMenuRequest> request) async {
    return listAllItemTags(call, await request);
  }

  $async.Future<$0.ListItemTagResponse> listItemTagsByItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemTagRequest> request) async {
    return listItemTagsByItem(call, await request);
  }

  $async.Future<$0.MenuResponse> createNutrient_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Nutrient> request) async {
    return createNutrient(call, await request);
  }

  $async.Future<$0.MenuResponse> addItemNutrient_Pre($grpc.ServiceCall call,
      $async.Future<$0.NutrientItemRequest> request) async {
    return addItemNutrient(call, await request);
  }

  $async.Future<$0.MenuResponse> removeItemNutrient_Pre($grpc.ServiceCall call,
      $async.Future<$0.NutrientItemRequest> request) async {
    return removeItemNutrient(call, await request);
  }

  $async.Future<$0.NutrientList> viewItemNutrients_Pre($grpc.ServiceCall call,
      $async.Future<$0.NutrientItemRequest> request) async {
    return viewItemNutrients(call, await request);
  }

  $async.Future<$0.NutrientList> viewNutrientItems_Pre($grpc.ServiceCall call,
      $async.Future<$0.NutrientItemRequest> request) async {
    return viewNutrientItems(call, await request);
  }

  $async.Future<$0.NutrientList> listNutrients_Pre($grpc.ServiceCall call,
      $async.Future<$0.EmptyMenuRequest> request) async {
    return listNutrients(call, await request);
  }

  $async.Future<$0.SearchResponse> search_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SearchRequest> request) async {
    return search(call, await request);
  }

  $async.Future<$0.MenuSearchSuggestionsResponse> menuSearchSuggestions_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.MenuSearchSuggestionsRequest> request) async {
    return menuSearchSuggestions(call, await request);
  }

  $async.Future<$0.MenuSearchItemSuggestionsResponse>
      menuSearchItemSuggestions_Pre($grpc.ServiceCall call,
          $async.Future<$0.MenuSearchItemSuggestionsRequest> request) async {
    return menuSearchItemSuggestions(call, await request);
  }

  $async.Future<$0.MenuSearchPackageSuggestionsResponse>
      menuSearchPackageSuggestions_Pre($grpc.ServiceCall call,
          $async.Future<$0.MenuSearchPackageSuggestionsRequest> request) async {
    return menuSearchPackageSuggestions(call, await request);
  }

  $async.Future<$0.MenuSearchItemResponse> menuSearchItems_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.MenuSearchItemRequest> request) async {
    return menuSearchItems(call, await request);
  }

  $async.Future<$0.MenuSearchPackageResponse> menuSearchPackages_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.MenuSearchPackageRequest> request) async {
    return menuSearchPackages(call, await request);
  }

  $async.Future<$0.MenuResponse> createCustomPackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CustomPackage> request) async {
    return createCustomPackage(call, await request);
  }

  $async.Future<$0.ListCustomPackageResponse> listMyCustomPackage_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ListCustomPackageRequest> request) async {
    return listMyCustomPackage(call, await request);
  }

  $async.Future<$0.ListCustomPackageResponse> listTemplateCustomPackage_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ListCustomPackageRequest> request) async {
    return listTemplateCustomPackage(call, await request);
  }

  $async.Future<$0.CustomPackage> viewCustomPackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CustomPackage> request) async {
    return viewCustomPackage(call, await request);
  }

  $async.Future<$0.MenuResponse> deleteCustomPackage_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CustomPackage> request) async {
    return deleteCustomPackage(call, await request);
  }

  $async.Future<$0.CustomPackage> addCustomPackageItems_Pre(
      $grpc.ServiceCall call, $async.Future<$0.CustomPackage> request) async {
    return addCustomPackageItems(call, await request);
  }

  $async.Future<$0.MenuResponse> createMenu(
      $grpc.ServiceCall call, $0.MenuItems request);
  $async.Future<$0.MenuResponse> updatemenu(
      $grpc.ServiceCall call, $0.MenuItems request);
  $async.Future<$0.ViewMenuResponse> viewMenu(
      $grpc.ServiceCall call, $0.MenuRequestItem request);
  $async.Future<$0.MenuResponse> decreaseMenu(
      $grpc.ServiceCall call, $0.DecreaseMenuRequest request);
  $async.Future<$0.MenuResponse> deleteMenu(
      $grpc.ServiceCall call, $0.MenuRequestItem request);
  $async.Future<$0.ListMenuResponse> listMenus(
      $grpc.ServiceCall call, $0.EmptyMenuRequest request);
  $async.Future<$0.ListMenuResponse> listMenusByField(
      $grpc.ServiceCall call, $0.MenuRequestItem request);
  $async.Future<$0.MenuResponse> addPackageCategory(
      $grpc.ServiceCall call, $0.AddPackageCategoryRequest request);
  $async.Future<$0.ViewPackageCategoryResponse> viewPackageCategory(
      $grpc.ServiceCall call, $0.PackageCategoryRequest request);
  $async.Future<$0.ListPackageCategoryResponse> listAllPackageCategory(
      $grpc.ServiceCall call, $0.EmptyMenuRequest request);
  $async.Future<$0.MenuResponse> editPackageCategory(
      $grpc.ServiceCall call, $0.PackageCategoryItem request);
  $async.Future<$0.MenuResponse> removePackageCategory(
      $grpc.ServiceCall call, $0.PackageCategoryRequest request);
  $async.Future<$0.MenuResponse> createItem(
      $grpc.ServiceCall call, $0.Item request);
  $async.Future<$0.Item> viewItem(
      $grpc.ServiceCall call, $0.ItemRequest request);
  $async.Future<$0.ListItemsResponse> listItems(
      $grpc.ServiceCall call, $0.EmptyMenuRequest request);
  $async.Future<$0.MenuResponse> editItem(
      $grpc.ServiceCall call, $0.Item request);
  $async.Future<$0.MenuResponse> deleteItem(
      $grpc.ServiceCall call, $0.ItemRequest request);
  $async.Future<$0.MenuResponse> addItemCategory(
      $grpc.ServiceCall call, $0.ItemCategory request);
  $async.Future<$0.ItemCategory> viewItemCategory(
      $grpc.ServiceCall call, $0.ItemCategoryRequest request);
  $async.Future<$0.ListItemCategoryResponse> listAllItemCategory(
      $grpc.ServiceCall call, $0.EmptyMenuRequest request);
  $async.Future<$0.MenuResponse> editItemCategory(
      $grpc.ServiceCall call, $0.ItemCategory request);
  $async.Future<$0.MenuResponse> removeItemCategory(
      $grpc.ServiceCall call, $0.ItemCategoryRequest request);
  $async.Future<$0.MenuResponse> createPackage(
      $grpc.ServiceCall call, $0.Package request);
  $async.Future<$0.MenuResponse> likePackage(
      $grpc.ServiceCall call, $0.LikeRequest request);
  $async.Future<$0.MenuResponse> unLikePackage(
      $grpc.ServiceCall call, $0.LikeRequest request);
  $async.Future<$0.ListPackageResponse> listUserLikedPackage(
      $grpc.ServiceCall call, $0.LikeRequest request);
  $async.Future<$0.Package> viewPackage(
      $grpc.ServiceCall call, $0.PackageRequest request);
  $async.Future<$0.ListPackageResponse> listAllPackages(
      $grpc.ServiceCall call, $0.PackageRequest request);
  $async.Future<$0.ListPackageResponse> listPackagesByServiceArea(
      $grpc.ServiceCall call, $0.PackageRequest request);
  $async.Future<$0.ListPackageResponse> listPackagesByCategory(
      $grpc.ServiceCall call, $0.PackageRequest request);
  $async.Future<$0.MenuResponse> editPackage(
      $grpc.ServiceCall call, $0.Package request);
  $async.Future<$0.MenuResponse> removePackage(
      $grpc.ServiceCall call, $0.PackageRequest request);
  $async.Future<$0.FeedList> listFeed(
      $grpc.ServiceCall call, $0.PackageRequest request);
  $async.Future<$0.MenuResponse> createPackageItem(
      $grpc.ServiceCall call, $0.PackageItemCreate request);
  $async.Future<$0.MenuResponse> addItemToPackageItem(
      $grpc.ServiceCall call, $0.AddItemPackage request);
  $async.Future<$0.ListPackageItemResponse> listPackageItems(
      $grpc.ServiceCall call, $0.EmptyMenuRequest request);
  $async.Future<$0.MenuResponse> updateTag(
      $grpc.ServiceCall call, $0.PackageTagItem request);
  $async.Future<$0.MenuResponse> addTag(
      $grpc.ServiceCall call, $0.PackageTagRequest request);
  $async.Future<$0.PackageTagItem> viewTag(
      $grpc.ServiceCall call, $0.PackageTagRequestItem request);
  $async.Future<$0.MenuResponse> deleteTag(
      $grpc.ServiceCall call, $0.PackageTagRequestItem request);
  $async.Future<$0.ListPackageTagResponse> listTag(
      $grpc.ServiceCall call, $0.EmptyMenuRequest request);
  $async.Future<$0.ListPackageTagResponse> listTagByPackage(
      $grpc.ServiceCall call, $0.PackageTagRequestItem request);
  $async.Future<$0.ListItemTagResponse> viewItemTags(
      $grpc.ServiceCall call, $0.ItemTagRequest request);
  $async.Future<$0.ItemTagResponse> updateItemTag(
      $grpc.ServiceCall call, $0.ItemTag request);
  $async.Future<$0.ItemTagResponse> addItemTag(
      $grpc.ServiceCall call, $0.ItemTagCreation request);
  $async.Future<$0.ItemTagResponse> deleteItemTag(
      $grpc.ServiceCall call, $0.ItemTagRequest request);
  $async.Future<$0.ListItemTagResponse> listAllItemTags(
      $grpc.ServiceCall call, $0.EmptyMenuRequest request);
  $async.Future<$0.ListItemTagResponse> listItemTagsByItem(
      $grpc.ServiceCall call, $0.ItemTagRequest request);
  $async.Future<$0.MenuResponse> createNutrient(
      $grpc.ServiceCall call, $0.Nutrient request);
  $async.Future<$0.MenuResponse> addItemNutrient(
      $grpc.ServiceCall call, $0.NutrientItemRequest request);
  $async.Future<$0.MenuResponse> removeItemNutrient(
      $grpc.ServiceCall call, $0.NutrientItemRequest request);
  $async.Future<$0.NutrientList> viewItemNutrients(
      $grpc.ServiceCall call, $0.NutrientItemRequest request);
  $async.Future<$0.NutrientList> viewNutrientItems(
      $grpc.ServiceCall call, $0.NutrientItemRequest request);
  $async.Future<$0.NutrientList> listNutrients(
      $grpc.ServiceCall call, $0.EmptyMenuRequest request);
  $async.Future<$0.SearchResponse> search(
      $grpc.ServiceCall call, $0.SearchRequest request);
  $async.Future<$0.MenuSearchSuggestionsResponse> menuSearchSuggestions(
      $grpc.ServiceCall call, $0.MenuSearchSuggestionsRequest request);
  $async.Future<$0.MenuSearchItemSuggestionsResponse> menuSearchItemSuggestions(
      $grpc.ServiceCall call, $0.MenuSearchItemSuggestionsRequest request);
  $async.Future<$0.MenuSearchPackageSuggestionsResponse>
      menuSearchPackageSuggestions($grpc.ServiceCall call,
          $0.MenuSearchPackageSuggestionsRequest request);
  $async.Future<$0.MenuSearchItemResponse> menuSearchItems(
      $grpc.ServiceCall call, $0.MenuSearchItemRequest request);
  $async.Future<$0.MenuSearchPackageResponse> menuSearchPackages(
      $grpc.ServiceCall call, $0.MenuSearchPackageRequest request);
  $async.Future<$0.MenuResponse> uploadImage(
      $grpc.ServiceCall call, $async.Stream<$0.UploadImageRequest> request);
  $async.Future<$0.MenuResponse> createCustomPackage(
      $grpc.ServiceCall call, $0.CustomPackage request);
  $async.Future<$0.ListCustomPackageResponse> listMyCustomPackage(
      $grpc.ServiceCall call, $0.ListCustomPackageRequest request);
  $async.Future<$0.ListCustomPackageResponse> listTemplateCustomPackage(
      $grpc.ServiceCall call, $0.ListCustomPackageRequest request);
  $async.Future<$0.CustomPackage> viewCustomPackage(
      $grpc.ServiceCall call, $0.CustomPackage request);
  $async.Future<$0.MenuResponse> deleteCustomPackage(
      $grpc.ServiceCall call, $0.CustomPackage request);
  $async.Future<$0.CustomPackage> addCustomPackageItems(
      $grpc.ServiceCall call, $0.CustomPackage request);
}
