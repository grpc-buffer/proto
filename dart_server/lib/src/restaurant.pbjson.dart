///
//  Generated code. Do not modify.
//  source: restaurant.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use listRequestDescriptor instead')
const ListRequest$json = const {
  '1': 'ListRequest',
};

/// Descriptor for `ListRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listRequestDescriptor = $convert.base64Decode('CgtMaXN0UmVxdWVzdA==');
@$core.Deprecated('Use viewCategoryRequestDescriptor instead')
const ViewCategoryRequest$json = const {
  '1': 'ViewCategoryRequest',
  '2': const [
    const {'1': 'category_id', '3': 1, '4': 1, '5': 9, '10': 'categoryId'},
    const {'1': 'restaurant_id', '3': 2, '4': 1, '5': 9, '10': 'restaurantId'},
  ],
};

/// Descriptor for `ViewCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewCategoryRequestDescriptor = $convert.base64Decode('ChNWaWV3Q2F0ZWdvcnlSZXF1ZXN0Eh8KC2NhdGVnb3J5X2lkGAEgASgJUgpjYXRlZ29yeUlkEiMKDXJlc3RhdXJhbnRfaWQYAiABKAlSDHJlc3RhdXJhbnRJZA==');
@$core.Deprecated('Use restaurantCategoryRequestDescriptor instead')
const RestaurantCategoryRequest$json = const {
  '1': 'RestaurantCategoryRequest',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'restaurant_id', '3': 2, '4': 1, '5': 9, '10': 'restaurantId'},
  ],
};

/// Descriptor for `RestaurantCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List restaurantCategoryRequestDescriptor = $convert.base64Decode('ChlSZXN0YXVyYW50Q2F0ZWdvcnlSZXF1ZXN0EhIKBG5hbWUYASABKAlSBG5hbWUSIwoNcmVzdGF1cmFudF9pZBgCIAEoCVIMcmVzdGF1cmFudElk');
@$core.Deprecated('Use restaurantCategoryResponseDescriptor instead')
const RestaurantCategoryResponse$json = const {
  '1': 'RestaurantCategoryResponse',
  '2': const [
    const {'1': 'category_id', '3': 1, '4': 1, '5': 9, '10': 'categoryId'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'restaurant_id', '3': 3, '4': 1, '5': 9, '10': 'restaurantId'},
    const {'1': 'created_at', '3': 4, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 5, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `RestaurantCategoryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List restaurantCategoryResponseDescriptor = $convert.base64Decode('ChpSZXN0YXVyYW50Q2F0ZWdvcnlSZXNwb25zZRIfCgtjYXRlZ29yeV9pZBgBIAEoCVIKY2F0ZWdvcnlJZBISCgRuYW1lGAIgASgJUgRuYW1lEiMKDXJlc3RhdXJhbnRfaWQYAyABKAlSDHJlc3RhdXJhbnRJZBIdCgpjcmVhdGVkX2F0GAQgASgJUgljcmVhdGVkQXQSHQoKdXBkYXRlZF9hdBgFIAEoCVIJdXBkYXRlZEF0');
@$core.Deprecated('Use listCategoriesResponseDescriptor instead')
const ListCategoriesResponse$json = const {
  '1': 'ListCategoriesResponse',
  '2': const [
    const {'1': 'categories', '3': 1, '4': 3, '5': 11, '6': '.restaurant_service.v1.RestaurantCategoryResponse', '10': 'categories'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'Message', '3': 3, '4': 1, '5': 9, '10': 'Message'},
  ],
};

/// Descriptor for `ListCategoriesResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listCategoriesResponseDescriptor = $convert.base64Decode('ChZMaXN0Q2F0ZWdvcmllc1Jlc3BvbnNlElEKCmNhdGVnb3JpZXMYASADKAsyMS5yZXN0YXVyYW50X3NlcnZpY2UudjEuUmVzdGF1cmFudENhdGVnb3J5UmVzcG9uc2VSCmNhdGVnb3JpZXMSFgoGc3RhdHVzGAIgASgJUgZzdGF0dXMSGAoHTWVzc2FnZRgDIAEoCVIHTWVzc2FnZQ==');
@$core.Deprecated('Use restaurantImageDescriptor instead')
const RestaurantImage$json = const {
  '1': 'RestaurantImage',
  '2': const [
    const {'1': 'imageUrl', '3': 1, '4': 1, '5': 9, '10': 'imageUrl'},
    const {'1': 'restaurant_id', '3': 2, '4': 1, '5': 9, '10': 'restaurantId'},
    const {'1': 'created_at', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 4, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `RestaurantImage`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List restaurantImageDescriptor = $convert.base64Decode('Cg9SZXN0YXVyYW50SW1hZ2USGgoIaW1hZ2VVcmwYASABKAlSCGltYWdlVXJsEiMKDXJlc3RhdXJhbnRfaWQYAiABKAlSDHJlc3RhdXJhbnRJZBIdCgpjcmVhdGVkX2F0GAMgASgJUgljcmVhdGVkQXQSHQoKdXBkYXRlZF9hdBgEIAEoCVIJdXBkYXRlZEF0');
@$core.Deprecated('Use restaurantRequestItemDescriptor instead')
const RestaurantRequestItem$json = const {
  '1': 'RestaurantRequestItem',
  '2': const [
    const {'1': 'requestId', '3': 1, '4': 1, '5': 9, '10': 'requestId'},
  ],
};

/// Descriptor for `RestaurantRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List restaurantRequestItemDescriptor = $convert.base64Decode('ChVSZXN0YXVyYW50UmVxdWVzdEl0ZW0SHAoJcmVxdWVzdElkGAEgASgJUglyZXF1ZXN0SWQ=');
@$core.Deprecated('Use restaurantItemsDescriptor instead')
const RestaurantItems$json = const {
  '1': 'RestaurantItems',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'owner', '3': 2, '4': 1, '5': 9, '10': 'owner'},
    const {'1': 'logo', '3': 3, '4': 1, '5': 9, '10': 'logo'},
    const {'1': 'name', '3': 4, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'images', '3': 5, '4': 3, '5': 11, '6': '.restaurant_service.v1.RestaurantImage', '10': 'images'},
    const {'1': 'number_of_workers', '3': 6, '4': 1, '5': 9, '10': 'numberOfWorkers'},
    const {'1': 'country', '3': 7, '4': 1, '5': 9, '10': 'country'},
    const {'1': 'state', '3': 8, '4': 1, '5': 9, '10': 'state'},
    const {'1': 'address', '3': 9, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'pickup', '3': 10, '4': 1, '5': 9, '10': 'pickup'},
    const {'1': 'delivery', '3': 11, '4': 1, '5': 9, '10': 'delivery'},
    const {'1': 'schedule', '3': 12, '4': 3, '5': 11, '6': '.restaurant_service.v1.ScheduleItem', '10': 'schedule'},
    const {'1': 'phone', '3': 13, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'tags', '3': 14, '4': 3, '5': 11, '6': '.restaurant_service.v1.TagItem', '10': 'tags'},
    const {'1': 'distance', '3': 15, '4': 1, '5': 1, '10': 'distance'},
    const {'1': 'delivery_fee', '3': 16, '4': 1, '5': 1, '10': 'deliveryFee'},
    const {'1': 'delivery_time', '3': 17, '4': 1, '5': 9, '10': 'deliveryTime'},
    const {'1': 'rating', '3': 18, '4': 1, '5': 1, '10': 'rating'},
    const {'1': 'no_of_ratings', '3': 19, '4': 1, '5': 3, '10': 'noOfRatings'},
    const {'1': 'created_at', '3': 20, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 21, '4': 1, '5': 9, '10': 'updatedAt'},
  ],
};

/// Descriptor for `RestaurantItems`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List restaurantItemsDescriptor = $convert.base64Decode('Cg9SZXN0YXVyYW50SXRlbXMSDgoCaWQYASABKAlSAmlkEhQKBW93bmVyGAIgASgJUgVvd25lchISCgRsb2dvGAMgASgJUgRsb2dvEhIKBG5hbWUYBCABKAlSBG5hbWUSPgoGaW1hZ2VzGAUgAygLMiYucmVzdGF1cmFudF9zZXJ2aWNlLnYxLlJlc3RhdXJhbnRJbWFnZVIGaW1hZ2VzEioKEW51bWJlcl9vZl93b3JrZXJzGAYgASgJUg9udW1iZXJPZldvcmtlcnMSGAoHY291bnRyeRgHIAEoCVIHY291bnRyeRIUCgVzdGF0ZRgIIAEoCVIFc3RhdGUSGAoHYWRkcmVzcxgJIAEoCVIHYWRkcmVzcxIWCgZwaWNrdXAYCiABKAlSBnBpY2t1cBIaCghkZWxpdmVyeRgLIAEoCVIIZGVsaXZlcnkSPwoIc2NoZWR1bGUYDCADKAsyIy5yZXN0YXVyYW50X3NlcnZpY2UudjEuU2NoZWR1bGVJdGVtUghzY2hlZHVsZRIUCgVwaG9uZRgNIAEoCVIFcGhvbmUSMgoEdGFncxgOIAMoCzIeLnJlc3RhdXJhbnRfc2VydmljZS52MS5UYWdJdGVtUgR0YWdzEhoKCGRpc3RhbmNlGA8gASgBUghkaXN0YW5jZRIhCgxkZWxpdmVyeV9mZWUYECABKAFSC2RlbGl2ZXJ5RmVlEiMKDWRlbGl2ZXJ5X3RpbWUYESABKAlSDGRlbGl2ZXJ5VGltZRIWCgZyYXRpbmcYEiABKAFSBnJhdGluZxIiCg1ub19vZl9yYXRpbmdzGBMgASgDUgtub09mUmF0aW5ncxIdCgpjcmVhdGVkX2F0GBQgASgJUgljcmVhdGVkQXQSHQoKdXBkYXRlZF9hdBgVIAEoCVIJdXBkYXRlZEF0');
@$core.Deprecated('Use restaurantResponseDescriptor instead')
const RestaurantResponse$json = const {
  '1': 'RestaurantResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 9, '10': 'data'},
  ],
};

/// Descriptor for `RestaurantResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List restaurantResponseDescriptor = $convert.base64Decode('ChJSZXN0YXVyYW50UmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRISCgRkYXRhGAMgASgJUgRkYXRh');
@$core.Deprecated('Use listRestaurantResponseDescriptor instead')
const ListRestaurantResponse$json = const {
  '1': 'ListRestaurantResponse',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.restaurant_service.v1.RestaurantItems', '10': 'data'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'Message', '3': 3, '4': 1, '5': 9, '10': 'Message'},
  ],
};

/// Descriptor for `ListRestaurantResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listRestaurantResponseDescriptor = $convert.base64Decode('ChZMaXN0UmVzdGF1cmFudFJlc3BvbnNlEjoKBGRhdGEYASADKAsyJi5yZXN0YXVyYW50X3NlcnZpY2UudjEuUmVzdGF1cmFudEl0ZW1zUgRkYXRhEhYKBnN0YXR1cxgCIAEoCVIGc3RhdHVzEhgKB01lc3NhZ2UYAyABKAlSB01lc3NhZ2U=');
@$core.Deprecated('Use viewRestaurantResponseDescriptor instead')
const ViewRestaurantResponse$json = const {
  '1': 'ViewRestaurantResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'data', '3': 2, '4': 1, '5': 11, '6': '.restaurant_service.v1.RestaurantItems', '10': 'data'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `ViewRestaurantResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewRestaurantResponseDescriptor = $convert.base64Decode('ChZWaWV3UmVzdGF1cmFudFJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEjoKBGRhdGEYAiABKAsyJi5yZXN0YXVyYW50X3NlcnZpY2UudjEuUmVzdGF1cmFudEl0ZW1zUgRkYXRhEhgKB21lc3NhZ2UYAyABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use emptyScheduleDescriptor instead')
const EmptySchedule$json = const {
  '1': 'EmptySchedule',
};

/// Descriptor for `EmptySchedule`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyScheduleDescriptor = $convert.base64Decode('Cg1FbXB0eVNjaGVkdWxl');
@$core.Deprecated('Use scheduleRequestDescriptor instead')
const ScheduleRequest$json = const {
  '1': 'ScheduleRequest',
  '2': const [
    const {'1': 'day', '3': 1, '4': 1, '5': 9, '10': 'day'},
    const {'1': 'restaurantID', '3': 2, '4': 1, '5': 9, '10': 'restaurantID'},
    const {'1': 'opening_time', '3': 3, '4': 1, '5': 9, '10': 'openingTime'},
    const {'1': 'closing_time', '3': 4, '4': 1, '5': 9, '10': 'closingTime'},
  ],
};

/// Descriptor for `ScheduleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List scheduleRequestDescriptor = $convert.base64Decode('Cg9TY2hlZHVsZVJlcXVlc3QSEAoDZGF5GAEgASgJUgNkYXkSIgoMcmVzdGF1cmFudElEGAIgASgJUgxyZXN0YXVyYW50SUQSIQoMb3BlbmluZ190aW1lGAMgASgJUgtvcGVuaW5nVGltZRIhCgxjbG9zaW5nX3RpbWUYBCABKAlSC2Nsb3NpbmdUaW1l');
@$core.Deprecated('Use scheduleRequestItemDescriptor instead')
const ScheduleRequestItem$json = const {
  '1': 'ScheduleRequestItem',
  '2': const [
    const {'1': 'requestId', '3': 1, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'restaurantID', '3': 2, '4': 1, '5': 9, '10': 'restaurantID'},
  ],
};

/// Descriptor for `ScheduleRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List scheduleRequestItemDescriptor = $convert.base64Decode('ChNTY2hlZHVsZVJlcXVlc3RJdGVtEhwKCXJlcXVlc3RJZBgBIAEoCVIJcmVxdWVzdElkEiIKDHJlc3RhdXJhbnRJRBgCIAEoCVIMcmVzdGF1cmFudElE');
@$core.Deprecated('Use scheduleItemDescriptor instead')
const ScheduleItem$json = const {
  '1': 'ScheduleItem',
  '2': const [
    const {'1': 'day', '3': 1, '4': 1, '5': 9, '10': 'day'},
    const {'1': 'restaurantID', '3': 2, '4': 1, '5': 9, '10': 'restaurantID'},
    const {'1': 'created_at', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 4, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'ID', '3': 5, '4': 1, '5': 9, '10': 'ID'},
    const {'1': 'opening_time', '3': 6, '4': 1, '5': 9, '10': 'openingTime'},
    const {'1': 'closing_time', '3': 7, '4': 1, '5': 9, '10': 'closingTime'},
  ],
};

/// Descriptor for `ScheduleItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List scheduleItemDescriptor = $convert.base64Decode('CgxTY2hlZHVsZUl0ZW0SEAoDZGF5GAEgASgJUgNkYXkSIgoMcmVzdGF1cmFudElEGAIgASgJUgxyZXN0YXVyYW50SUQSHQoKY3JlYXRlZF9hdBgDIAEoCVIJY3JlYXRlZEF0Eh0KCnVwZGF0ZWRfYXQYBCABKAlSCXVwZGF0ZWRBdBIOCgJJRBgFIAEoCVICSUQSIQoMb3BlbmluZ190aW1lGAYgASgJUgtvcGVuaW5nVGltZRIhCgxjbG9zaW5nX3RpbWUYByABKAlSC2Nsb3NpbmdUaW1l');
@$core.Deprecated('Use scheduleResponseDescriptor instead')
const ScheduleResponse$json = const {
  '1': 'ScheduleResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `ScheduleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List scheduleResponseDescriptor = $convert.base64Decode('ChBTY2hlZHVsZVJlc3BvbnNlEhYKBnN0YXR1cxgBIAEoCVIGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use viewScheduleResponseDescriptor instead')
const ViewScheduleResponse$json = const {
  '1': 'ViewScheduleResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 11, '6': '.restaurant_service.v1.ScheduleItem', '10': 'data'},
  ],
};

/// Descriptor for `ViewScheduleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewScheduleResponseDescriptor = $convert.base64Decode('ChRWaWV3U2NoZWR1bGVSZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEjcKBGRhdGEYAyABKAsyIy5yZXN0YXVyYW50X3NlcnZpY2UudjEuU2NoZWR1bGVJdGVtUgRkYXRh');
@$core.Deprecated('Use listScheduleResponseDescriptor instead')
const ListScheduleResponse$json = const {
  '1': 'ListScheduleResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.restaurant_service.v1.ScheduleItem', '10': 'data'},
  ],
};

/// Descriptor for `ListScheduleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listScheduleResponseDescriptor = $convert.base64Decode('ChRMaXN0U2NoZWR1bGVSZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEjcKBGRhdGEYAyADKAsyIy5yZXN0YXVyYW50X3NlcnZpY2UudjEuU2NoZWR1bGVJdGVtUgRkYXRh');
@$core.Deprecated('Use emptyTagDescriptor instead')
const EmptyTag$json = const {
  '1': 'EmptyTag',
};

/// Descriptor for `EmptyTag`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyTagDescriptor = $convert.base64Decode('CghFbXB0eVRhZw==');
@$core.Deprecated('Use tagRequestDescriptor instead')
const TagRequest$json = const {
  '1': 'TagRequest',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'restaurantID', '3': 2, '4': 1, '5': 9, '10': 'restaurantID'},
  ],
};

/// Descriptor for `TagRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tagRequestDescriptor = $convert.base64Decode('CgpUYWdSZXF1ZXN0EhIKBG5hbWUYASABKAlSBG5hbWUSIgoMcmVzdGF1cmFudElEGAIgASgJUgxyZXN0YXVyYW50SUQ=');
@$core.Deprecated('Use tagRequestItemDescriptor instead')
const TagRequestItem$json = const {
  '1': 'TagRequestItem',
  '2': const [
    const {'1': 'requestId', '3': 1, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'restaurantID', '3': 2, '4': 1, '5': 9, '10': 'restaurantID'},
  ],
};

/// Descriptor for `TagRequestItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tagRequestItemDescriptor = $convert.base64Decode('Cg5UYWdSZXF1ZXN0SXRlbRIcCglyZXF1ZXN0SWQYASABKAlSCXJlcXVlc3RJZBIiCgxyZXN0YXVyYW50SUQYAiABKAlSDHJlc3RhdXJhbnRJRA==');
@$core.Deprecated('Use tagItemDescriptor instead')
const TagItem$json = const {
  '1': 'TagItem',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'restaurantID', '3': 2, '4': 1, '5': 9, '10': 'restaurantID'},
    const {'1': 'created_at', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updated_at', '3': 4, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'ID', '3': 5, '4': 1, '5': 9, '10': 'ID'},
  ],
};

/// Descriptor for `TagItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tagItemDescriptor = $convert.base64Decode('CgdUYWdJdGVtEhIKBG5hbWUYASABKAlSBG5hbWUSIgoMcmVzdGF1cmFudElEGAIgASgJUgxyZXN0YXVyYW50SUQSHQoKY3JlYXRlZF9hdBgDIAEoCVIJY3JlYXRlZEF0Eh0KCnVwZGF0ZWRfYXQYBCABKAlSCXVwZGF0ZWRBdBIOCgJJRBgFIAEoCVICSUQ=');
@$core.Deprecated('Use tagResponseDescriptor instead')
const TagResponse$json = const {
  '1': 'TagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `TagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tagResponseDescriptor = $convert.base64Decode('CgtUYWdSZXNwb25zZRIWCgZzdGF0dXMYASABKAlSBnN0YXR1cxIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdl');
@$core.Deprecated('Use viewTagResponseDescriptor instead')
const ViewTagResponse$json = const {
  '1': 'ViewTagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 11, '6': '.restaurant_service.v1.TagItem', '10': 'data'},
  ],
};

/// Descriptor for `ViewTagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List viewTagResponseDescriptor = $convert.base64Decode('Cg9WaWV3VGFnUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIyCgRkYXRhGAMgASgLMh4ucmVzdGF1cmFudF9zZXJ2aWNlLnYxLlRhZ0l0ZW1SBGRhdGE=');
@$core.Deprecated('Use listTagResponseDescriptor instead')
const ListTagResponse$json = const {
  '1': 'ListTagResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 3, '5': 11, '6': '.restaurant_service.v1.TagItem', '10': 'data'},
  ],
};

/// Descriptor for `ListTagResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List listTagResponseDescriptor = $convert.base64Decode('Cg9MaXN0VGFnUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXMSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIyCgRkYXRhGAMgAygLMh4ucmVzdGF1cmFudF9zZXJ2aWNlLnYxLlRhZ0l0ZW1SBGRhdGE=');
