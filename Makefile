gen-go:
	protoc --go_out=go/pkg/proto --go_opt=paths=source_relative \
    --go-grpc_out=go/pkg/proto --go-grpc_opt=paths=source_relative \
    --proto_path=proto proto/*.proto

dart:
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/auth.proto  
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/agent.proto  
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/restaurant.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/payment.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/menu.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/cart.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/promotions.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/payment.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/price.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/support.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/search.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/order.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/delivery.proto
	protoc --dart_out=grpc:dart_server/lib/src -Iproto proto/location.proto google/protobuf/timestamp.proto

