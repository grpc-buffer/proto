// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.5
// source: delivery.proto

package proto

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// DeliveryServiceClient is the client API for DeliveryService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DeliveryServiceClient interface {
	// Delivery
	CreateDeliveryService(ctx context.Context, in *DeliveryPayload, opts ...grpc.CallOption) (*DeliveryResponse, error)
	FetchDeliveryByServiceArea(ctx context.Context, in *DeliveryRequest, opts ...grpc.CallOption) (*GetPayLoadResponse, error)
	FetchAllDelivery(ctx context.Context, in *GetAllDeliveriesRequest, opts ...grpc.CallOption) (*GetAllDeliveryResponse, error)
	DeleteDeliveryByServiceArea(ctx context.Context, in *DeliveryDeleteRequest, opts ...grpc.CallOption) (*DeliveryResponse, error)
	DeliveryService(ctx context.Context, in *InDeliveryRequest, opts ...grpc.CallOption) (*InDeliveryResponse, error)
	GetTips(ctx context.Context, in *TipRequest, opts ...grpc.CallOption) (*TipResponse, error)
}

type deliveryServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDeliveryServiceClient(cc grpc.ClientConnInterface) DeliveryServiceClient {
	return &deliveryServiceClient{cc}
}

func (c *deliveryServiceClient) CreateDeliveryService(ctx context.Context, in *DeliveryPayload, opts ...grpc.CallOption) (*DeliveryResponse, error) {
	out := new(DeliveryResponse)
	err := c.cc.Invoke(ctx, "/delivery_service.v1.deliveryService/CreateDeliveryService", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *deliveryServiceClient) FetchDeliveryByServiceArea(ctx context.Context, in *DeliveryRequest, opts ...grpc.CallOption) (*GetPayLoadResponse, error) {
	out := new(GetPayLoadResponse)
	err := c.cc.Invoke(ctx, "/delivery_service.v1.deliveryService/FetchDeliveryByServiceArea", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *deliveryServiceClient) FetchAllDelivery(ctx context.Context, in *GetAllDeliveriesRequest, opts ...grpc.CallOption) (*GetAllDeliveryResponse, error) {
	out := new(GetAllDeliveryResponse)
	err := c.cc.Invoke(ctx, "/delivery_service.v1.deliveryService/FetchAllDelivery", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *deliveryServiceClient) DeleteDeliveryByServiceArea(ctx context.Context, in *DeliveryDeleteRequest, opts ...grpc.CallOption) (*DeliveryResponse, error) {
	out := new(DeliveryResponse)
	err := c.cc.Invoke(ctx, "/delivery_service.v1.deliveryService/DeleteDeliveryByServiceArea", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *deliveryServiceClient) DeliveryService(ctx context.Context, in *InDeliveryRequest, opts ...grpc.CallOption) (*InDeliveryResponse, error) {
	out := new(InDeliveryResponse)
	err := c.cc.Invoke(ctx, "/delivery_service.v1.deliveryService/DeliveryService", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *deliveryServiceClient) GetTips(ctx context.Context, in *TipRequest, opts ...grpc.CallOption) (*TipResponse, error) {
	out := new(TipResponse)
	err := c.cc.Invoke(ctx, "/delivery_service.v1.deliveryService/GetTips", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DeliveryServiceServer is the server API for DeliveryService service.
// All implementations must embed UnimplementedDeliveryServiceServer
// for forward compatibility
type DeliveryServiceServer interface {
	// Delivery
	CreateDeliveryService(context.Context, *DeliveryPayload) (*DeliveryResponse, error)
	FetchDeliveryByServiceArea(context.Context, *DeliveryRequest) (*GetPayLoadResponse, error)
	FetchAllDelivery(context.Context, *GetAllDeliveriesRequest) (*GetAllDeliveryResponse, error)
	DeleteDeliveryByServiceArea(context.Context, *DeliveryDeleteRequest) (*DeliveryResponse, error)
	DeliveryService(context.Context, *InDeliveryRequest) (*InDeliveryResponse, error)
	GetTips(context.Context, *TipRequest) (*TipResponse, error)
	mustEmbedUnimplementedDeliveryServiceServer()
}

// UnimplementedDeliveryServiceServer must be embedded to have forward compatible implementations.
type UnimplementedDeliveryServiceServer struct {
}

func (UnimplementedDeliveryServiceServer) CreateDeliveryService(context.Context, *DeliveryPayload) (*DeliveryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateDeliveryService not implemented")
}
func (UnimplementedDeliveryServiceServer) FetchDeliveryByServiceArea(context.Context, *DeliveryRequest) (*GetPayLoadResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FetchDeliveryByServiceArea not implemented")
}
func (UnimplementedDeliveryServiceServer) FetchAllDelivery(context.Context, *GetAllDeliveriesRequest) (*GetAllDeliveryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FetchAllDelivery not implemented")
}
func (UnimplementedDeliveryServiceServer) DeleteDeliveryByServiceArea(context.Context, *DeliveryDeleteRequest) (*DeliveryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteDeliveryByServiceArea not implemented")
}
func (UnimplementedDeliveryServiceServer) DeliveryService(context.Context, *InDeliveryRequest) (*InDeliveryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeliveryService not implemented")
}
func (UnimplementedDeliveryServiceServer) GetTips(context.Context, *TipRequest) (*TipResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetTips not implemented")
}
func (UnimplementedDeliveryServiceServer) mustEmbedUnimplementedDeliveryServiceServer() {}

// UnsafeDeliveryServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DeliveryServiceServer will
// result in compilation errors.
type UnsafeDeliveryServiceServer interface {
	mustEmbedUnimplementedDeliveryServiceServer()
}

func RegisterDeliveryServiceServer(s grpc.ServiceRegistrar, srv DeliveryServiceServer) {
	s.RegisterService(&DeliveryService_ServiceDesc, srv)
}

func _DeliveryService_CreateDeliveryService_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeliveryPayload)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DeliveryServiceServer).CreateDeliveryService(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/delivery_service.v1.deliveryService/CreateDeliveryService",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DeliveryServiceServer).CreateDeliveryService(ctx, req.(*DeliveryPayload))
	}
	return interceptor(ctx, in, info, handler)
}

func _DeliveryService_FetchDeliveryByServiceArea_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeliveryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DeliveryServiceServer).FetchDeliveryByServiceArea(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/delivery_service.v1.deliveryService/FetchDeliveryByServiceArea",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DeliveryServiceServer).FetchDeliveryByServiceArea(ctx, req.(*DeliveryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DeliveryService_FetchAllDelivery_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAllDeliveriesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DeliveryServiceServer).FetchAllDelivery(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/delivery_service.v1.deliveryService/FetchAllDelivery",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DeliveryServiceServer).FetchAllDelivery(ctx, req.(*GetAllDeliveriesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DeliveryService_DeleteDeliveryByServiceArea_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeliveryDeleteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DeliveryServiceServer).DeleteDeliveryByServiceArea(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/delivery_service.v1.deliveryService/DeleteDeliveryByServiceArea",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DeliveryServiceServer).DeleteDeliveryByServiceArea(ctx, req.(*DeliveryDeleteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DeliveryService_DeliveryService_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(InDeliveryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DeliveryServiceServer).DeliveryService(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/delivery_service.v1.deliveryService/DeliveryService",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DeliveryServiceServer).DeliveryService(ctx, req.(*InDeliveryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DeliveryService_GetTips_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TipRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DeliveryServiceServer).GetTips(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/delivery_service.v1.deliveryService/GetTips",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DeliveryServiceServer).GetTips(ctx, req.(*TipRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// DeliveryService_ServiceDesc is the grpc.ServiceDesc for DeliveryService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var DeliveryService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "delivery_service.v1.deliveryService",
	HandlerType: (*DeliveryServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateDeliveryService",
			Handler:    _DeliveryService_CreateDeliveryService_Handler,
		},
		{
			MethodName: "FetchDeliveryByServiceArea",
			Handler:    _DeliveryService_FetchDeliveryByServiceArea_Handler,
		},
		{
			MethodName: "FetchAllDelivery",
			Handler:    _DeliveryService_FetchAllDelivery_Handler,
		},
		{
			MethodName: "DeleteDeliveryByServiceArea",
			Handler:    _DeliveryService_DeleteDeliveryByServiceArea_Handler,
		},
		{
			MethodName: "DeliveryService",
			Handler:    _DeliveryService_DeliveryService_Handler,
		},
		{
			MethodName: "GetTips",
			Handler:    _DeliveryService_GetTips_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "delivery.proto",
}
