syntax = "proto3";
option go_package = "go/pkg/proto";
// import "google/protobuf/Timestamp.proto";


// AGENTS
enum AvailabilityStatus {
    AVAILABLE = 0;
    ON_ERRAND = 1;
    UNAVAILABLE = 2;
}

message Agent {
    string id = 1;
    string email = 2;
    string phone_number = 3;
    string first_name = 4;
    string last_name = 5;
    string address = 6;
    string state = 7;
    Wallet wallet = 8;
    AvailabilityStatus status = 9;
    // google.protobuf.Timestamp created_at = 10;
}

message CreateAgentRequest {
    string id = 1;
    string email = 2;
    string phone_number = 3;
    string first_name = 4;
    string last_name = 5;
    string address = 6;
    string state = 7;
    string country = 8;
}

message CreateAgentResponse {
    string message = 1;
}

message AgentFilter {
    AvailabilityStatus status = 1;
    string state = 2;
    string address = 3;
}

message GetAllAgentsRequest {
    uint32 page = 1;
    uint32 size = 2;
    AgentFilter filterBy = 3;
}

message GetAllAgentsResponse {
    string message = 1;
    repeated Agent agents = 2;
    uint32 page = 3;
    uint32 size = 4;
    int64 total = 5;
}


message GetAgentByIdRequest {
    string id = 1;
}

message GetAgentByIdResponse {
    string message = 1;
    Agent agent = 2;
}

message GetAgentInfoRequest {}

message GetAgentInfoResponse{
    string message = 1;
    Agent agent = 2;
}

message ChangeAvailabilityStatusRequest {
    AvailabilityStatus status = 1;
}

message ChangeAvailabilityStatusResponse {
    string message = 1;
}



// AGENT'S WALLET
message Wallet {
    string id = 1;
    string agent_id = 2;
    float balance = 3;
    string currency = 4;
}

message WalletFilter {
    string currency = 1;
}

message GetAllAgentWalletsRequest {
    uint32 page = 1;
    uint32 size = 2;
    WalletFilter filterBy = 3;
}

message GetAllAgentWalletsResponse {
    string message = 1;
    repeated Wallet wallets = 2;
    uint32 page = 3;
    uint32 size = 4;
    int64 total = 5;
}

message GetWalletByAgentIdRequest {
    string agent_id = 1;
}

message GetWalletByAgentIdResponse {
    string message = 1;
    Wallet wallet = 2;
}


// ORDER REQUESTS
message Value {
    oneof kind {
        string string_value = 1;
        double double_value = 2;
        uint32 uint_value = 3;
    }
}

message OrderItems {
   string name = 1;
   uint64 quantity = 2;
   float price = 3;
}

enum OrderRequestStatus {
    UNASSIGNED = 0;
	ASSIGNED = 1;
	ARRIVED_AT_STORE = 2;
	PURCHASED_ITEM = 3;
	AWAITING_CONFIRMATION = 4;
	DROPPED_OFF = 5;
	ON_HOLD = 6;
	COMPLETED = 7;
}

message OrderRequest {
    string id = 1;
    string order_id = 2;
    repeated OrderItems order_items = 3;
    OrderRequestStatus status = 4;
    uint32 duration = 5;
    float earnable_amount = 6;
    string delivery_location = 7;
    string agent_id = 8;
    string customer_id = 9;
    // google.protobuf.Timestamp completed_at = 10;
    // google.protobuf.Timestamp created_at = 11;
}


message CreateOrderRequest_Request {
    string order_id = 1;
    repeated OrderItems order_items = 2;
    uint32 duration = 3;
    float earnable_amount = 4;
    string delivery_location = 5;
    string customer_id = 6;
}

message CreateOrderRequest_Response {
    string message = 1;
}

message OrderRequestFilter {
    OrderRequestStatus status = 1;
    string agent_id = 2;
    string customer_id = 3;
}

message GetAllOrderRequests_Request {
    uint32 page = 1;
    uint32 size = 2;
    OrderRequestFilter filterBy = 3;
}

message GetAllOrderRequests_Response {
    string message = 1;
    repeated OrderRequest order_requests = 2;
    uint32 page = 3;
    uint32 size = 4;
    int64 total = 5;
}

message SendOrderRequestToAgent_Request {
    string order_request_id = 1;
    string agent_id = 2;
    uint64 acceptance_duration = 3;
}

message SendOrderRequestToAgent_Response {
    string message = 1;
    string request_id = 2;
}

message GetAssignedOrderRequest_Request {
}

message GetAssignedOrderRequest_Response {
    OrderRequest order_request = 1;
}

enum AcceptanceStatus {
    accept = 0;
    decline = 1;
}

message AcceptOrDeclineOrderRequest_Request {
     AcceptanceStatus accepted = 1;
     string order_request_id = 2;
     string reject_reason = 3; 
     string request_id = 4;
}

message AcceptOrDeclineOrderRequest_Response {
    string message = 1;
    OrderRequest order_request = 2;
}

message UpdateOrderRequestStatus_Request {
    string order_request_id = 1;
    OrderRequestStatus status = 2;
}

message UpdateOrderRequestStatus_Response {
    string message = 1;
}

message GetOrderRequestHistory_Request {
    string agent_id = 1;
    uint32 page = 2;
    uint32 size = 3;
}

message GetOrderRequestHistory_Response {
    string message = 1;
    repeated OrderRequest order_requests = 2;
    uint32 page = 3;
    uint32 size = 4;
    int64 total = 5;
}


service AgentService {
    // ORDER REQUESTS
    rpc CreateOrderRequest(CreateOrderRequest_Request) returns (CreateOrderRequest_Response) {}
    rpc GetAllOrderRequests(GetAllOrderRequests_Request) returns (GetAllOrderRequests_Response) {}
    rpc SendOrderRequestToAgent(SendOrderRequestToAgent_Request) returns (SendOrderRequestToAgent_Response) {}
    rpc AcceptOrDeclineOrderRequest(AcceptOrDeclineOrderRequest_Request) returns (AcceptOrDeclineOrderRequest_Response) {}
    rpc UpdateOrderRequestStatus(UpdateOrderRequestStatus_Request) returns (UpdateOrderRequestStatus_Response) {}
    rpc GetOrderRequestHistory(GetOrderRequestHistory_Request) returns (GetOrderRequestHistory_Response) {}

    // TODO: to be deleted. This rpc is only for experimental purposes only.
    rpc GetOrderRequest(GetAssignedOrderRequest_Request) returns (GetAssignedOrderRequest_Response) {}

    
    // AGENTS
    rpc CreateAgent(CreateAgentRequest) returns (CreateAgentResponse) {}
    rpc GetAllAgents(GetAllAgentsRequest) returns (GetAllAgentsResponse) {}
    rpc GetAgentById(GetAgentByIdRequest) returns (GetAgentByIdResponse) {}
    rpc GetAgentInfo(GetAgentInfoRequest) returns (GetAgentInfoResponse) {}
    rpc ChangeAvailabilityStatus(ChangeAvailabilityStatusRequest) returns (ChangeAvailabilityStatusResponse) {}


    // WALLETS
    rpc GetAllAgentWallets(GetAllAgentsRequest) returns (GetAllAgentWalletsResponse) {}
    rpc GetWalletByAgentId(GetWalletByAgentIdRequest) returns (GetWalletByAgentIdResponse) {}
}



