syntax = "proto3";
option go_package = "go/pkg/proto";

enum OtpType {
  LOGIN = 0;
  REG = 1;
  RESET_PASSWORD = 2;
}


message GeneralResponse {
  string message = 1;
}

message LoginRequest {
   string login = 1;
   string password = 2;
   string phoneCode = 3;
}

message LoginResponse {
  string message = 1;
  string requestId = 2;
  string isEmailVerified = 3;
  string isPhoneVerified = 4;
  User user = 5;
}

message LoginWithGoogleRequest {
  string google_access_token = 1;
  enum Channel {
    WEB = 0;
    MOBILE = 1;
  }
  Channel channel = 2;
}

message LoginWithGoogleResponse {
  string isNewUser = 1;
  string isEmailVerified = 2;
  string isPhoneVerified = 3;
  string oauthId = 4;
  User user = 5;
  string access_token = 6;
}

message RegisterRequest {
  string firstName = 1;
  string lastName = 2;
  string email = 3;
  string phoneNumber = 4;
  string phoneCode = 5;
  string password = 6;
  string address = 7;
  string state = 8;
  string country = 9;
  optional string oauthId = 10;
  string referralUsed = 11;
}

message RegisterResponse {
  string message = 1;
  string isEmailVerified = 2;
  string isPhoneVerified = 3;
}

message InitPhoneVerificationRequest {
  string phoneNumber = 1;
  string phoneCode = 2;
  OtpType type = 3;
}

message InitEmailVerificationRequest {
  string email = 1;
  OtpType type = 2;
}

message InitEmailVerificationResponse {
  string message = 1;
  string requestId = 2;
  OtpType type = 3;
}

message InitPhoneVerificationResponse {
  string message = 1;
  string requestId = 2;
}

message EmailVerificationRequest {
  string requestID = 1;
  string otp = 2;
  string email = 3; 
  enum OtpType {
    LOGIN = 0;
    REG = 1;
  }
  OtpType type = 4;
}

message PhoneVerificationRequest {
  string requestID = 1;
  string otp = 2;
  string phoneNumber = 3;
  string phoneCode = 4;
  OtpType type = 5;
}

message PhoneVerificationResponse {
  string message = 1;
}

message VerifyLoginRequest {
  string login = 1;
  string requestId = 2;
  string otp = 3;
  OtpType type = 4;
  optional string phoneCode = 5;
}

message VerifyLoginResponse {
  string access_token = 1;
  User userInfo = 2;
}

message EmailVerificationResponse {
  string message = 1;
}

message LogoutRequest {
  string user_id = 1;
}

message LogoutResponse {
  string message = 1;
}

message Country {
  string id = 1;
  string name = 2;
  string iso = 3;
  string phone_code = 4;
  string imageUrl = 5;
}

message GetAllCountryRequest{}
message CountryRequest{
  string countryID =1;
}

message GetAllCountryResponse {
  repeated Country countries = 1;
}

message UserRequest {
  string userId = 1;
  string oldPassword = 2;
  string newPassword = 3;
}

message UpdatePasswordResponse {
  string message = 1;
}

message ResetPasswordRequest {
  string email = 1;
}

message ResetPasswordResponse {
  string message = 1;
  string requestId = 2;
}

message VerifyPasswordResetRequest {
  string email = 1;
  string requestId = 2;
  string otp = 3;
  OtpType type = 4;
}

message VerifyPasswordResetResponse {
  string message = 1;

}

message SetNewPasswordRequest {
  string email = 1;
  string requestId = 2;
  string newPassword = 3;
}

message SetNewPasswordResponse {
  string message = 1;
}

message User {
  string first_name = 1;
  string last_name = 2;
  string email = 3;
  string phone = 4;
  string phoneCode = 5;
  string password = 6;
  string address = 7;
  string state = 8;
  string country = 9;
  string userID = 10;
  string referralUsed = 11;
  string referralCode = 12;
  int64 referralCount = 13;
  string profilePicture = 14;
  string fcmToken     = 15;

}

message UpdateUserInformationRequest {
  string id = 1;
  string first_name = 2;
  string last_name = 3;
  string address = 4;
  string state = 5;
}

message UpdateUserInformationResponse {
  string message = 1;
  User user = 2;
}

message DeleteUserRequest {
  string id = 1;
}

message DeleteUserResponse {
  string message = 1;
}

message Role {
  string name = 1;
  string description = 2;
  string createdBy = 3;
  string id = 4;
  string updatedAt = 5;
  string createdAt = 6;

 }

message CreateRoleRequest {
  string name = 1;
  string description = 2;
  int32 identifier = 3;
  string createdBy = 4;
  
}

message CreateRoleResponse {
  string role_id= 1;
  string message = 2;
}

message GetAllRolesRequest{}

message GetAllRolesResponse {
  repeated Role roles = 1;
}

message DeleteRoleRequest {
  string role_id = 1;
}

message DeleteRoleResponse {
  string message = 1;
}

message UpdateRoleRequest {
  string role_id = 1;
  string description = 2;
  int32 identifier = 3;
  string userID = 4;
  string name = 5;
}

message UpdateRoleResponse {
  string message = 1;
}

message AssignPermissionsToRoleRequest {
  string role_id = 1;
  repeated string permission_ids = 2;
}

message AssignPermissionsToRoleResponse {
  string message = 1;
}

message RemovePermissionsFromRoleRequest {
  string role_id = 1;
  repeated string permission_ids = 2;
}

message RemovePermissionsFromRoleResponse {
  string message = 1;
}

message Permission {
  string name = 1;
  string description = 2;
}

message CreatePermissionRequest {
  string permission = 1;
  string  description = 2;
}

message CreatePermissionResponse {
  string permission_id = 1;
  string message = 2;
}

message GetAllPermissionsRequest{}

message GetAllPermissionsResponse{
  repeated Permission permissions = 1;
}

message GetPermissionByTitleRequest {
  string title = 1;
}

message GetPermissionByTitleResponse {
  string id = 1;
  string title = 2;
  string description = 3;
}

message GetPermissionsByRoleRequest{
  string role_id = 1;
}

message GetPermissionsByRoleResponse {
  repeated Permission permissions = 1;
}

message HasPermissionRequest {
  string token = 1;
  string permission = 2;
}

message HasPermissionResponse {
  bool granted = 1;
  User user = 2;
}

message AgentLoginRequest {
  string email = 1;
  string password = 2;
}

message AgentLoginResponse {
  string access_token = 1;
  User userInfo = 2;
}

//role
message UserToRoleRequest {
  string userID = 1;
  string roleID = 2;
}

message UserToRoleResponse {
  string message = 1;
  string status = 2;
}

message EmptyUserListRequest {
}
message ReferralListRequest {
  string referralCode = 1;
}

message AdminUser {
  string first_name = 1;
  string last_name = 2;
  string email = 3;
  string phone = 4;
  string phoneCode = 5;
  string password = 6;
  string userID = 7;
}

message UserListResponse {
  repeated User users = 1;
  string status = 2;
}


message ProfilePicsInfo {
  string user_id = 1;
  string image_type = 2;
}

message UploadDPRequest{
  oneof data {
    ProfilePicsInfo info = 1;
    bytes chunk_data = 2;
  };
}

message UploadImageResponse{
  string message = 1;
  string imageUrl = 2;
}
message FirebaseToken{
  string token = 1;
}

service AuthService {
  rpc Login(LoginRequest) returns (LoginResponse) {}
  rpc LoginNoVerification(LoginRequest) returns (VerifyLoginResponse) {}
  rpc LoginWithGoogle(LoginWithGoogleRequest) returns (LoginWithGoogleResponse) {}
  rpc LoginAsAgent(AgentLoginRequest) returns (AgentLoginResponse) {}
  rpc Register(RegisterRequest) returns (RegisterResponse) {}
  rpc InitEmailVerification(InitEmailVerificationRequest) returns (InitEmailVerificationResponse) {}
  rpc InitPhoneVerification(InitPhoneVerificationRequest) returns (InitPhoneVerificationResponse) {}
  rpc VerifyPhone(PhoneVerificationRequest) returns (PhoneVerificationResponse) {}
  rpc VerifyEmail(EmailVerificationRequest) returns (EmailVerificationResponse) {}
  rpc VerifyLogin(VerifyLoginRequest) returns (VerifyLoginResponse) {}
  rpc Logout (LogoutRequest) returns (LogoutResponse) {}

  rpc ViewUser(UserRequest) returns (User) {}
  rpc UpdatePassword(UserRequest) returns (UpdatePasswordResponse) {}
  rpc ResetPassword(ResetPasswordRequest) returns (ResetPasswordResponse) {}
  rpc VerifyPasswordReset(VerifyPasswordResetRequest) returns (VerifyPasswordResetResponse) {}
  rpc SetNewPassword(SetNewPasswordRequest) returns (SetNewPasswordResponse) {}
  
  //country
  rpc GetAllCountries(GetAllCountryRequest) returns (GetAllCountryResponse) {}
  rpc AddCountry(Country) returns (GeneralResponse) {}
  rpc RemoveCountry(CountryRequest) returns (GeneralResponse) {}
  
  rpc AssignPermissionsToRole(AssignPermissionsToRoleRequest) returns (AssignPermissionsToRoleResponse) {}
  rpc RemovePermissionsFromRole(RemovePermissionsFromRoleRequest) returns (RemovePermissionsFromRoleResponse) {}
  rpc CreatePermission(CreatePermissionRequest) returns (CreatePermissionResponse) {}
  rpc GetAllPermissions(GetAllPermissionsRequest) returns (GetAllPermissionsResponse) {}
  rpc GetPermissionsByRole(GetPermissionsByRoleRequest) returns (GetPermissionsByRoleResponse) {}  
  rpc GetPermissionByTitle(GetPermissionByTitleRequest) returns (GetPermissionByTitleResponse) {}
  rpc HasPermission(HasPermissionRequest) returns (HasPermissionResponse) {}
  rpc UpdateUserProfile(UpdateUserInformationRequest) returns (UpdateUserInformationResponse) {}
  rpc DeleteUser(DeleteUserRequest) returns (DeleteUserResponse) {}
  
  //listing userDetails
  rpc ListUsers(EmptyUserListRequest) returns (UserListResponse) {}
  rpc ListUsersByReferralUsed(ReferralListRequest) returns (UserListResponse) {}

  //role
  rpc AssignUserToRole(UserToRoleRequest) returns (UserToRoleResponse) {}
  rpc RemoveUserFromRole(UserToRoleRequest) returns (UserToRoleResponse) {}
  rpc CreateRole(CreateRoleRequest) returns (CreateRoleResponse) {}
  rpc GetAllRoles(GetAllRolesRequest) returns (GetAllRolesResponse) {}
  rpc DeleteRole(DeleteRoleRequest) returns (DeleteRoleResponse) {}
  rpc UpdateRole(UpdateRoleRequest) returns (UpdateRoleResponse) {}


  rpc UploadImage( stream UploadDPRequest) returns (UploadImageResponse) {}
  rpc SetFirebaseToken(FirebaseToken) returns (User) {}
  
  
}