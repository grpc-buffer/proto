# Proto

## Getting started

1. Clone the repository
2. Create a folder for your language specific package
3. Add your language specific command for generating protobuf files under a specific tag (e.g dart,react) to the Makefile.
4. In your terminal call make <tag-name>.
5. Push your changes.
6. Only the backend team should modify the "protos" folder
7. Always pull before you push
